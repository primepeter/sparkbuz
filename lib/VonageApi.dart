import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

class VonageApi {
  final String brandName, apiKey, apiSecret;

  const VonageApi(
      {@required this.brandName,
      @required this.apiKey,
      @required this.apiSecret});

  // curl
  // -X "POST" "https://rest.nexmo.com/sms/json" \
  // -d "from=$VONAGE_BRAND_NAME" \
  // -d "text=A text message sent using the Vonage SMS API" \
  // -d "to=$TO_NUMBER" \
  // -d "api_key=$VONAGE_API_KEY" \
  // -d "api_secret=$VONAGE_API_SECRET"

  sendSMS(
      {@required String number, @required String text, @required onResponse}) {
    http.post(Uri.parse("https://rest.nexmo.com/sms/json"), body: {
      'from': brandName,
      'text': text,
      'to': number,
      'api_key': apiKey,
      'api_secret': apiSecret,
    }).then((value) {
      // {
      //   I/flutter (25343):     "message-count": "1",
      // I/flutter (25343):     "messages": [{
      // I/flutter (25343):         "to": "2348143733836",
      // I/flutter (25343):         "message-id": "1300000162E70DED",
      // I/flutter (25343):         "status": "0",
      // I/flutter (25343):         "remaining-balance": "11.91870000",
      // I/flutter (25343):         "message-price": "0.08130000",
      // I/flutter (25343):         "network": "62130"
      // I/flutter (25343):     }]
      // I/flutter (25343): }
      print(value.body);
      if (value.statusCode == HttpStatus.ok) {
        onResponse("Hurray message sent bro");
      } else {
        onResponse("An error Occurred!.");
      }
    }).catchError((e) {
      onResponse(e.toString());
    });
  }
}
