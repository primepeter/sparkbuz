import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:sparkbuz/assets.dart';

import 'AppEngine.dart';

class BaseModel {
  Map<String, Object> items = new Map();
  Map<String, Object> itemsToUpdate = new Map();

  BaseModel({Map items, DocumentSnapshot doc}) {
    if (items != null) {
      Map<String, Object> theItems = Map.from(items);
      this.items = theItems;
    }
    if (doc != null && doc.exists) {
      this.items = doc.data();
      this.items[OBJECT_ID] = doc.id;
    }
  }

  void put(String key, Object value) {
    items[key] = value;
    itemsToUpdate[key] = value;
  }

  // void putInList(String key, Object value, [bool add = true]) {
  //   List itemsInList = items[key] == null ? [] : List.from(items[key]);
  //   if (add) {
  //     if (value is Map && value[OBJECT_ID] != null) {
  //       int index =
  //           itemsInList.indexWhere((m) => m[OBJECT_ID] == value[OBJECT_ID]);
  //       if (index == -1) itemsInList.add(value);
  //     } else {
  //       if (!itemsInList.contains(value)) itemsInList.add(value);
  //     }
  //   } else {
  //     if (value is Map && value[OBJECT_ID] != null) {
  //       int index =
  //           itemsInList.indexWhere((m) => m[OBJECT_ID] == value[OBJECT_ID]);
  //       if (index != -1) itemsInList.removeAt(index);
  //     } else {
  //       itemsInList.removeWhere((E) => E == value);
  //     }
  //   }
  //   items[key] = itemsInList;
  //
  //   Map update = Map();
  //   update[ADD] = add;
  //   update[VALUE] = value;
  //
  //   itemUpdateList[key] = update;
  // }

  void putInList(String key, Object value, [bool add = true]) {
    items[key] = value;
    itemsToUpdate[key] =
        add ? FieldValue.arrayUnion([value]) : FieldValue.arrayRemove([value]);
  }

  void remove(String key) {
    items.remove(key);
    itemsToUpdate[key] = null;
  }

  String getObjectId() {
    Object value = items[OBJECT_ID];
    return value == null || !(value is String) ? "" : value.toString();
  }

  List getList(String key) {
    Object value = items[key];
    return value == null || !(value is List) ? new List() : List.from(value);
  }

  Map getMap(String key) {
    Object value = items[key];
    return value == null || !(value is Map)
        ? new Map<String, String>()
        : Map.from(value);
  }

  Object get(String key) {
    return items[key];
  }

  String getUserId() {
    Object value = items[USER_ID];
    String id = value == null || !(value is String) ? "" : value.toString();
    return id.isEmpty ? getObjectId() : id;
  }

  String getName() {
    Object value = items[NAME];

    return value == null || !(value is String) ? "" : value.toString();
  }

  String getUserName() {
    Object value = items[USERNAME];
    String name = value == null || !(value is String) ? "" : value.toString();
    if (name.length > 2) {
      name = name.substring(0, 1).toUpperCase() + name.substring(1);
    }
    return name;
  }

  String getString(String key) {
    Object value = items[key];

    return value == null || !(value is String) ? "" : value.toString();
  }

  String getEmail() {
    Object value = items[EMAIL];
    return value == null || !(value is String) ? "" : value.toString();
  }

  String getPassword() {
    Object value = items[PASSWORD];
    return value == null || !(value is String) ? "" : value.toString();
  }

  /*int getCreatedAt() {
    Object value = items[CREATED_AT];
    return value == null || !(value is DateTime)
        ? 0
        : (value as DateTime).millisecond;
  }

  DateTime getCreatedAtDate() {
    Object value = items[CREATED_AT];
    return value == null || !(value is DateTime) ? new DateTime.now() : (value);
  }

  DateTime getUpdatedAtDate() {
    Object value = items[UPDATED_AT];
    return value == null || !(value is DateTime) ? new DateTime.now() : (value);
  }

  int getUpdatedAt() {
    Object value = items[UPDATED_AT];
    return value == null || !(value is Timestamp)
        ? 0
        : (value as Timestamp).millisecondsSinceEpoch;
  }*/

  /*bool isRead({String userId}) {
    List<String> readBy = List.from(getList(READ_BY));
    return readBy.contains(userId != null ? userId : userModel.getObjectId());
  }*/

  bool isMuted(String chatId) {
    List<String> readBy = getList(MUTED);
    return readBy.contains(chatId);
  }

  bool isRated(String chatId) {
    List<String> readBy = getList(HAS_RATED);
    return readBy.contains(chatId);
  }

  bool isSilenced() {
    List<String> silence = getList(SILENCED);
    return silence.contains(userModel.getObjectId());
  }

  bool isKicked() {
    List<String> readBy = getList(KICKED_OUT);
    return readBy.contains(userModel.getObjectId());
  }

  bool isMale() {
    return getInt(GENDER) == 0;
  }

  /*void setRead() {
    List<String> readBy = getList(READ_BY);
    if (!readBy.contains(userModel.getObjectId())) {
      readBy.add(userModel.getObjectId());
      put(READ_BY, readBy);
    }
    updateItem();
  }*/

  /*void setUnRead() {
    List<String> readBy = getList(READ_BY);
    readBy.remove(userModel.getObjectId());
    updateItem();
  }*/

  bool hasItem(String key) {
    return items[key] != null;
  }

  bool myItem() {
    return getUserId() == (userModel.getUserId());
  }

  bool mySentChat() {
    return getBoolean(MY_SENT_CHAT);
  }

  bool isHidden() {
    List<String> readBy = getList(HIDDEN);
    return readBy.contains(userModel.getObjectId());
  }

  // int getInt(String key) {
  //   Object value = items[key];
  //   return value == null || !(value is int) ? 0 : (value);
  // }


  int getInt(String key) {
    Object value = items[key];
    return value == null || !(value is num)
        ? 0
        : num.parse(value.toString()).toInt();
  }

  int getType() {
    Object value = items[TYPE];
    return value == null || !(value is int) ? 0 : value;
  }

  double getDouble(String key) {
    Object value = items[key];
    return value == null || !(value is num)
        ? 0
        : num.parse(value.toString()).toDouble();
  }

  int getTime() {
    Object value = items[TIME];
    return value == null || !(value is int) ? 0 : value;
  }

  /*int getLong(String key) {
    Object value = items[key];
    return value == null || !(value is int) ? 0 : value;
  }*/

  bool getBoolean(String key) {
    Object value = items[key];
    return value == null || !(value is bool) ? false : value;
  }

  bool isAdminItem() {
    return getBoolean(IS_ADMIN);
  }

  bool isLeader() {
    return getBoolean(IS_LEADER);
  }

  bool isJohn() {
    return getEmail() == ("johnebere58@gmail.com");
  }

  bool isMaugost() {
    return getEmail() == ("ammaugost@gmail.com");
  }

  bool isDeveloper() {
    return isJohn() || isMaugost();
  }

  // void updateItems(
  //     {bool updateTime = false,
  //     int delaySeconds = 0,
  //     String db,
  //     String objectId}) async {
  //   Future.delayed(Duration(seconds: delaySeconds), () async {
  //     String dName = db ?? items[DATABASE_NAME];
  //     String id = objectId ?? items[OBJECT_ID];
  //     items.remove(VIDEO_CONTROLLER);
  //     itemUpdate.remove(VIDEO_CONTROLLER);
  //
  //     DocumentSnapshot doc = await FirebaseFirestore.instance
  //         .collection(dName)
  //         .doc(id)
  //         .get(GetOptions(source: Source.server))
  //         .catchError((error) {
  //       delaySeconds = delaySeconds + 10;
  //       delaySeconds = delaySeconds >= 60 ? 0 : delaySeconds;
  //       print("$error... retrying in $delaySeconds seconds");
  //       updateItems(updateTime: updateTime, delaySeconds: delaySeconds);
  //       return null;
  //     });
  //     if (doc == null) return;
  //     if (!doc.exists) return;
  //
  //     Map data = doc.data();
  //     for (String k in itemUpdate.keys) {
  //       data[k] = itemUpdate[k];
  //     }
  //     for (String k in itemUpdateList.keys) {
  //       Map update = itemUpdateList[k];
  //       bool add = update[ADD];
  //       var value = update[VALUE];
  //
  //       List dataList = data[k] == null ? [] : List.from(data[k]);
  //       if (add) {
  //         if (value is Map && value[OBJECT_ID] != null) {
  //           int index =
  //               dataList.indexWhere((m) => m[OBJECT_ID] == value[OBJECT_ID]);
  //           if (index == -1) dataList.add(value);
  //         } else {
  //           if (!dataList.contains(value)) dataList.add(value);
  //         }
  //       } else {
  //         dataList.removeWhere((E) => E == value);
  //       }
  //       data[k] = dataList;
  //     }
  //     for (String k in itemUpdateMap.keys) {
  //       Map update = itemUpdateMap[k];
  //       bool add = update[ADD];
  //       var itemKey = update[KEY];
  //       var itemValue = update[VALUE];
  //
  //       Map dataList = data[k] == null ? Map() : Map.from(data[k]);
  //       if (add) {
  //         dataList[itemKey] = itemValue;
  //       } else {
  //         dataList.remove(itemKey);
  //       }
  //       data[k] = dataList;
  //     }
  //
  //     if (updateTime) {
  //       data[UPDATED_AT] = FieldValue.serverTimestamp();
  //       data[TIME_UPDATED] = DateTime.now().millisecondsSinceEpoch;
  //     }
  //
  //     doc.reference.update(data);
  //   });
  // }

  void updateItems(
      {context,
      bool updateTime = true,
      int delaySeconds = 0,
      String db,
      String objectId,
      int retryMax = 6,
      bool silently = true,
      progressMessage,
      onComplete(error)}) async {
    if (!silently) showProgress(true, context, msg: progressMessage);

    if ((!(await isConnected()))) {
      if (!silently) await Future.delayed(Duration(milliseconds: 600));
      if (!silently) showProgress(false, context);
      if (!silently) await Future.delayed(Duration(milliseconds: 600));
      if (onComplete != null) onComplete("No internet connectivity");
      return;
    }

    Future.delayed(Duration(seconds: delaySeconds), () async {
      String dName = db ?? items[DATABASE_NAME];
      String id = objectId ?? items[OBJECT_ID];

      try {
        var _firestore = FirebaseFirestore.instance;
        await _firestore.runTransaction((transaction) async {
          DocumentReference itemRef = _firestore.collection(dName).doc(id);

          DocumentSnapshot snapshot = await transaction.get(itemRef);
          if (!snapshot.exists) return;

          if (updateTime) {
            itemsToUpdate[UPDATED_AT] = FieldValue.serverTimestamp();
            itemsToUpdate[TIME_UPDATED] = DateTime.now().millisecondsSinceEpoch;
          }
          transaction.update(itemRef, itemsToUpdate);
        }, timeout: Duration(seconds: 10));
        print("Finished Transaction");
        if (!silently) await Future.delayed(Duration(milliseconds: 600));
        if (!silently) showProgress(false, context);
        if (!silently) await Future.delayed(Duration(milliseconds: 600));
        if (onComplete != null) onComplete(null);
      } catch (e) {
        print("Finished with error: $e");
        delaySeconds += 5;
        delaySeconds = delaySeconds >= 60 ? 0 : delaySeconds;
        print("$e... retrying in $delaySeconds seconds");
        retryMax--;
        if (retryMax > 0 && !silently) {
          updateItems(
              context: context,
              updateTime: updateTime,
              delaySeconds: delaySeconds,
              db: db,
              onComplete: onComplete,
              silently: silently,
              retryMax: retryMax,
              progressMessage: progressMessage,
              objectId: objectId);
        } else {
          if (!silently) await Future.delayed(Duration(milliseconds: 600));
          if (!silently) showProgress(false, context);
          if (!silently) await Future.delayed(Duration(milliseconds: 600));
          if (onComplete != null)
            onComplete(/*e.toString()*/ "Error occurred, try again");
        }
      }
    });
  }

  void deleteItem() {
    String dName = items[DATABASE_NAME];
    String id = items[OBJECT_ID];
    FirebaseFirestore db = FirebaseFirestore.instance;
    db.collection(dName).doc(id).delete();
  }

  processSave(String name, bool addMyInfo) {
    items[VISIBILITY] = PUBLIC;
    items[DATABASE_NAME] = name;
    items[UPDATED_AT] = FieldValue.serverTimestamp();
    items[CREATED_AT] = FieldValue.serverTimestamp();
    items[TIME] = DateTime.now().millisecondsSinceEpoch;
    items[TIME_UPDATED] = DateTime.now().millisecondsSinceEpoch;
    if (name != (USER_BASE) && name != (APP_SETTINGS_BASE)
        /* && name != (NOTIFY_BASE)*/
        ) {
      if (addMyInfo) addMyDetails(addMyInfo);
    }

    if (name == VOICE_BASE || name == POSTS_BASE) {
      items[FOLLOWERS] = userModel.getList(FOLLOWERS);
    }
  }

  void addMyDetails(bool addMyInfo) {
    items[USER_ID] = userModel.getUserId();
    items[USER_IMAGE] = userModel.userImage;
    items[USERNAME] = userModel.getUserName();
    items[NAME] = userModel.getString(NAME);
    items[BY_ADMIN] = userModel.isAdminItem();
    items[GENDER] = userModel.getInt(GENDER);
    items[EMAIL] = userModel.getString(EMAIL);
    items[PHONE_NUMBER] = userModel.getString(PHONE_NUMBER);
    items[DEVICE_ID] = userModel.getString(DEVICE_ID);
    items[TOKEN] = userModel.getString(TOKEN);
    items[VIDEO_URL] = userModel.getString(TOKEN);
    items[THUMBNAIL_URL] = userModel.getString(THUMBNAIL_URL);
    items[BIRTH_DATE] = userModel.getString(BIRTH_DATE);
  }

  void saveItem(String name, bool addMyInfo,
      {document, onComplete, bool merged = false}) async {
    processSave(name, addMyInfo);
    await Future.delayed(Duration(seconds: 2));

    if (document == null) {
      FirebaseFirestore.instance.collection(name).add(items).whenComplete(() {
        if (onComplete != null) onComplete();
      });
    } else {
      items[OBJECT_ID] = document;
      FirebaseFirestore.instance
          .collection(name)
          .doc(document)
          .set(items, SetOptions(merge: merged))
          .whenComplete(() {
        if (onComplete != null) onComplete();
      });
    }
  }

  BaseModel getModel(String key) {
    return BaseModel(items: getMap(key));
  }

  List<BaseModel> getListModel(String key) {
    return getList(key).map((e) => BaseModel(items: e)).toList();
  }

  List<BaseModel> get profilePhotos => getListModel(PROFILE_PHOTOS);
  List<BaseModel> get hookUpPhotos => getListModel(HOOKUP_PHOTOS);
  List get followers => getList(FOLLOWERS);
  List get followings => getList(FOLLOWING);
  List get likesCount => getList(LIKES_COUNT);
  List get videosCount => getList(VIDEOS_COUNT);

  List<BaseModel> get images => getListModel(IMAGES);

  bool get isHookUps => selectedQuickHookUp == 0;

  bool get emailNotification => getBoolean(EMAIL_NOTIFICATION);
  bool get pushNotification => getBoolean(PUSH_NOTIFICATION);
  bool get isVideo => getBoolean(IS_VIDEO);
  bool get isLocal => !(getString(IMAGE_URL).startsWith("https://") ||
      getString(IMAGE_URL).startsWith("http://"));
  bool get signUpCompleted => getBoolean(SIGNUP_COMPLETED);
  bool get isPremium =>
      getInt(ACCOUNT_TYPE) == 1 ||
      (getInt(ACCOUNT_TYPE) == 0 &&
          get(SUBSCRIPTION_EXPIRY) != null &&
          !subscriptionExpired) ||
      isAdmin;
  bool get subscriptionExpired {
//    print(DateTime.fromMillisecondsSinceEpoch(getInt(SUBSCRIPTION_EXPIRY)));
//    print("today ${DateTime(2020, 5, 28).millisecondsSinceEpoch}");
//    print("future ${getInt(SUBSCRIPTION_EXPIRY) < 1590620400000}");
//    1622267174401

    int currentMS = DateTime.now().millisecondsSinceEpoch;
    int expiryMS = getInt(SUBSCRIPTION_EXPIRY);
    bool expired = currentMS > expiryMS;

    print(" $currentMS $expiryMS $expired");

    return expired;
  }

  String get package => isPremium ? FEATURES_PREMIUM : FEATURES_REGULAR;
  String get aboutMe => getString(ABOUT_ME);
  String get birthDate => getString(BIRTH_DATE);
  String get imageUrl => getString(IMAGE_URL);
  String get thumbnailUrl => getString(THUMBNAIL_URL);
  String get imagesPath => getString(IMAGE_PATH);
  String get userImage => getString(USER_IMAGE);
  String get firstName => getString(NAME).split(" ")[0];

  String get gender => genderType[selectedGender];
  String get ethnicity => ethnicityType[selectedEthnicity];
  String get preference => preferenceType[selectedPreference];
  String get relationship => relationshipType[selectedRelationship];
  String get quickHookUp => quickHookUps[selectedQuickHookUp];

  int get selectedGender => get(GENDER) == null ? -1 : getInt(GENDER);
  int get selectedEthnicity => get(ETHNICITY) == null ? -1 : getInt(ETHNICITY);
  int get selectedPreference =>
      get(PREFERENCE) == null ? -1 : getInt(PREFERENCE);
  int get selectedRelationship =>
      get(RELATIONSHIP) == null ? -1 : getInt(RELATIONSHIP);
  int get selectedQuickHookUp =>
      get(QUICK_HOOKUP) == null ? -1 : getInt(QUICK_HOOKUP);

  List get myFollowers => getList(FOLLOWERS);
  List get amFollowing => getList(FOLLOWING);
}
