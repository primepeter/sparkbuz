import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:line_icons/line_icons.dart';
import 'package:sparkbuz/AppConfig.dart';
import 'package:sparkbuz/AppEngine.dart';
import 'package:sparkbuz/dialogs/inputDialog.dart';

// import 'package:wechat_assets_picker/wechat_assets_picker.dart';

import 'app/app.dart';
import 'assets.dart';

class EditProfile extends StatefulWidget {
  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  var ranImage = "https://bit.ly/2ZE09z5";
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.of(context).pop("");
        return false;
      },
      child: Scaffold(
        backgroundColor: white,
        body: page(),
      ),
    );
  }

  page() {
    return Column(
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        addSpace(40),
        Padding(
          padding: const EdgeInsets.only(left: 0),
          child: Row(
            children: <Widget>[
              InkWell(
                  onTap: () {
                    Navigator.of(context).pop("");
                  },
                  child: Container(
                    width: 50,
                    height: 50,
                    child: Center(
                        child: Icon(
                      Icons.keyboard_backspace,
                      color: black,
                      size: 25,
                    )),
                  )),
              Flexible(
                  child: Text(
                "Edit Profile",
                style: textStyle(true, 25, black),
              )),
            ],
          ),
        ),
        AnimatedContainer(
          duration: Duration(milliseconds: 500),
          width: double.infinity,
          height: errorText.isEmpty ? 0 : 40,
          color: AppConfig.appYellow,
          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: Center(
              child: Text(
            errorText,
            style: textStyle(true, 16, white),
          )),
        ),
        addLine(.6, black.withOpacity(.1), 0, 0, 0, 0),
        addSpace(30),
        editImage(),
        addSpace(30),
        addLine(.5, black.withOpacity(.1), 0, 0, 0, 0),
        editContent()
      ],
    );
  }

  String errorText = "";
  showError(String text, {bool wasLoading = false}) {
    if (wasLoading) showProgress(false, context);
    errorText = text;
    setState(() {});
    Future.delayed(Duration(seconds: 3), () {
      errorText = "";
      setState(() {});
    });
  }

  editProfileItem(
      {@required String title, @required String subtitle, @required onTapped}) {
    return GestureDetector(
      onTap: () {
        onTapped();
      },
      child: new Container(
        width: double.infinity,
        margin: EdgeInsets.all(10),
        padding: EdgeInsets.all(18),
        color: transparent,
        //height: 50,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Text(
              title,
              style: textStyle(false, 16, black),
              maxLines: 1,
            ),
            addSpaceWidth(10),
            Flexible(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Flexible(
                    child: Text(
                      subtitle,
                      style: textStyle(false, 16, black.withOpacity(.5)),
                      maxLines: 1,
                    ),
                  ),
                  Icon(
                    Icons.navigate_next,
                    color: black.withOpacity(.5),
                    size: 19,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  editImage() {
    return Row(
//      mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              GestureDetector(
                onTap: () {
                  print("ok");

                  pickImages(context,
                      requestType: RequestType.image,
                      singleMode: true, onPicked: (_) {
                    if (null == _) return;
                    Future.delayed(Duration(milliseconds: 100), () async {
                      String path = _[0][PATH];
                      File croppedFile = await ImageCropper.cropImage(
                        sourcePath: path,
                      );
                      if (croppedFile != null) savePhoto(croppedFile);
                      showError("Uploading Photo...");
                      setState(() {});
                    });
                    setState(() {});
                  });

                  // openGallery(context,
                  //     type: PickType.onlyImage,
                  //     singleMode: true, onPicked: (_) {
                  //   if (_.isEmpty) return;
                  //   Future.delayed(Duration(milliseconds: 100), () async {
                  //     String path = _[0].file.path;
                  //     File croppedFile = await ImageCropper.cropImage(
                  //       sourcePath: path,
                  //     );
                  //     if (croppedFile != null) savePhoto(croppedFile);
                  //     showError("Uploading Photo...");
                  //     setState(() {});
                  //   });
                  // });
                },
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(100),
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(100),
                        border:
                            Border.all(color: AppConfig.appYellow, width: 2)),
                    padding: EdgeInsets.all(4),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(100),
                      child: CachedNetworkImage(
                        imageUrl: userModel.userImage,
                        height: 100,
                        width: 100,
                        fit: BoxFit.cover,
                        placeholder: (c, s) {
                          return Container(
                            height: 100,
                            width: 100,
                            child: Icon(LineIcons.user),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(100),
                              color: black.withOpacity(.1),
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                ),
              ),
              addSpace(8),
              Text(
                "Change Photo",
                style: textStyle(false, 17, black.withOpacity(.7)),
              )
            ],
          ),
          // addSpaceWidth(50),
          // Column(
          //   mainAxisSize: MainAxisSize.min,
          //   children: [
          //     GestureDetector(
          //       onTap: () {
          //         openGallery(context,
          //             type: PickType.onlyVideo,
          //             singleMode: true, onPicked: (_) {
          //           if (_.isEmpty) return;
          //           Future.delayed(Duration(milliseconds: 100), () async {
          //             String path = _[0].file.path;
          //             final thumbnail = await VideoCompress.getFileThumbnail(path);
          //             userModel..put(VIDEO_PATH, path)..put(THUMBNAIL_PATH, thumbnail.path)..updateItems();
          //             if (thumbnail != null) saveVideo(File(path),thumbnail);
          //             showError("Uploading Video...");
          //             setState(() {
          //
          //             });
          //           });
          //         });
          //       },
          //       child: ClipRRect(
          //         borderRadius: BorderRadius.circular(100),
          //         child: Container(
          //           decoration: BoxDecoration(
          //               borderRadius: BorderRadius.circular(100),
          //               border:
          //                   Border.all(color: AppConfig.appYellow, width: 2)),
          //           padding: EdgeInsets.all(4),
          //           child: CachedNetworkImage(
          //             imageUrl: userModel.thumbnailUrl,
          //             height: 100,
          //             width: 100,
          //             placeholder: (c, s) {
          //               return Container(
          //                 height: 100,
          //                 width: 100,
          //                 child: Icon(LineIcons.video_camera),
          //                 decoration: BoxDecoration(
          //                   borderRadius: BorderRadius.circular(100),
          //                   color: black.withOpacity(.1),
          //                 ),
          //               );
          //             },
          //           ),
          //         ),
          //       ),
          //     ),
          //     addSpace(8),
          //     Text(
          //       "Change Video",
          //       style: textStyle(false, 17, black.withOpacity(.7)),
          //     )
          //   ],
          // ),
        ]);
  }

  editContent() {
    return Flexible(
      child: Column(
//        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          editProfileItem(
              title: "Name",
              subtitle: userModel.getName(),
              onTapped: () {
                pushAndResult(
                    context,
                    inputDialog(
                      "Name",
                      hint: "Enter your name",
                      message: userModel.getName(),
                    ),
                    depend: false, result: (_) {
                  if (null == _) return;
                  final search =
                      getSearchString('$_ ${userModel.getUserName()}');
                  userModel
                    ..put(NAME, _)
                    ..put(SEARCH, search)
                    ..updateItems();
                  setState(() {});
                });
              }),
          editProfileItem(
              title: "Username",
              subtitle: userModel.getUserName().isEmpty
                  ? "add a username"
                  : userModel.getUserName(),
              onTapped: () {}),
          editProfileItem(
              title: "Bio",
              subtitle: userModel.aboutMe.isEmpty
                  ? "Write about yourself"
                  : userModel.aboutMe,
              onTapped: () {
                pushAndResult(
                    context,
                    inputDialog(
                      "About You",
                      hint: "Write about yourself",
                      message: userModel.aboutMe,
                    ),
                    depend: false, result: (_) {
                  if (null == _) return;

                  userModel
                    ..put(ABOUT_ME, _)
                    ..updateItems();
                  setState(() {});
                });
              })
        ],
      ),
    );
  }
}
