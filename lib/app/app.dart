library app;

import 'dart:io';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sparkbuz/AppConfig.dart';
import 'package:sparkbuz/AppEngine.dart';
import 'package:sparkbuz/app/currencies.dart';
import 'package:sparkbuz/assets.dart';
import 'package:sparkbuz/basemodel.dart';
import 'package:video_compress/video_compress.dart';

// import 'package:wechat_assets_picker/wechat_assets_picker.dart';

import 'countries.dart';

part 'appExtensions.dart';
part 'countryChooser.dart';
part 'currencyChooser.dart';
part 'gridCollage.dart';
// part 'infoDialog.dart';
// part 'inputDialog.dart';
// part 'listDialog.dart';
// part 'messageDialog.dart';
// part 'navigation.dart';
// part 'notificationService.dart';
// part 'placeChooser.dart';
// part 'preview_image.dart';
// part 'progressDialog.dart';
// part 'rating.dart';
// part 'unicons.dart';

class Countries {
  final String countryName;
  final String countryFlag;
  final String countryCode;
  final String countryISO;

  Countries(
      {this.countryName, this.countryFlag, this.countryCode, this.countryISO});
}

class Currencies {
  final String symbol;
  final String name;
  final String symbolNative;
  final int decimalDigits;
  final rounding;
  final String code;
  final String namePlural;

  Currencies(
      {this.symbol,
      this.name,
      this.symbolNative,
      this.decimalDigits,
      this.rounding,
      this.code,
      this.namePlural});
}

List<Currencies> getCurrencies() {
  return currenciesMap.values
      .map((e) => Currencies(
          code: e["code"],
          decimalDigits: e["decimal_digits"],
          name: e["name"],
          namePlural: e["name_plural"],
          rounding: e["rounding"],
          symbol: e["symbol"],
          symbolNative: e["symbol_native"]))
      .toList();
}

List<Countries> getCountries() {
  return countryMap
      .map((c) => Countries(
          countryISO: c["ISO"],
          countryName: c["Name"],
          countryCode: '+${c["Code"]}',
          countryFlag: 'flags/${c["ISO"]}.png'))
      .toList();
}

Countries country =
    getCountries().singleWhere((e) => e.countryName == 'Nigeria');

//List<CameraDescription> cameras = [];

// openGallery(BuildContext context,
//     {bool singleMode = false,
//     PickType type = PickType.all,
//     RequestType requestType = RequestType.all,
//     int maxSelection,
//     int maxVideoDurationInSeconds = 300,
//     @required onPicked(List<PhotoGallery> _)}) {
//
//   PhotoPicker.pickAsset(
//           thumbSize: 250,
//           context: context,
//           provider: I18nProvider.english,
//           pickType: type,
//           maxSelected: singleMode ? 1 : maxSelection,
//           rowCount: 3)
//       .then((value) async {
//     List<PhotoGallery> paths = [];
//     for (var v in value) {
//       File file = await v.originFile;
//       print(v.id);
//       print(file.path);
//       final gallery = PhotoGallery(galleryId: v.id, file: file);
//       paths.add(gallery);
//     }
//     if (paths.length == value.length) onPicked(paths);
//   });
// }

String getSecondsMessage(int seconds) {
  if (seconds < 60) return "$seconds seconds";
  int mins = seconds ~/ 60;
  return "$mins minute${mins > 1 ? "s" : ""}";
}

class PhotoGallery {
  final String galleryId;
  final File file;
  final File thumbFile;
  final bool isVideo;

  PhotoGallery({
    @required this.galleryId,
    @required this.file,
    this.isVideo = false,
    this.thumbFile,
  });
}

enum RequestType { all, image, video }

pickImages(BuildContext context,
    {int maxSec = 300,
    bool singleMode = false,
    RequestType requestType = RequestType.all,
    int maxSelection = 10,
    int maxVideoDurationInSeconds = 300,
    @required onPicked(List<Map> _)}) async {
  final source = ImageSource.gallery;
  bool video = requestType == RequestType.video;
  (requestType == RequestType.image
          ? ImagePicker().getImage(source: source)
          : ImagePicker().getVideo(source: source))
      .then((_) async {
    if (null == _) return;
    Map map = {PATH: _.path, IS_VIDEO: video};
    String path = map[PATH];
    if (video) {
      File thumbnail = await VideoCompress.getFileThumbnail(path);
      // final controller = CachedVideoPlayerController.file(File(path));
      // await controller.initialize();
      // int duration = controller.value.duration.inMilliseconds;
      // if (duration > (maxSec)) {
      //   showMessage(context, Icons.error, red0, "Too Long",
      //       "Video cannot be longer than ${getSecondsMessage(maxSec)}");
      //   return;
      // }
      map[THUMBNAIL_PATH] = thumbnail.path;
      // map[VIDEO_LENGTH] = duration;
    }
    onPicked([map]);
  });

  // if (Platform.isIOS) {
  //   final source = ImageSource.gallery;
  //   bool video = requestType == RequestType.video;
  //   (requestType == RequestType.image
  //           ? ImagePicker().getImage(source: source)
  //           : ImagePicker().getVideo(source: source))
  //       .then((_) async {
  //     if (null == _) return;
  //     Map map = {PATH: _.path, IS_VIDEO: video};
  //     String path = map[PATH];
  //     if (video) {
  //       File thumbnail = await VideoCompress.getFileThumbnail(path);
  //       // final controller = CachedVideoPlayerController.file(File(path));
  //       // await controller.initialize();
  //       // int duration = controller.value.duration.inMilliseconds;
  //       // if (duration > (maxSec)) {
  //       //   showMessage(context, Icons.error, red0, "Too Long",
  //       //       "Video cannot be longer than ${getSecondsMessage(maxSec)}");
  //       //   return;
  //       // }
  //       map[THUMBNAIL_PATH] = thumbnail.path;
  //       // map[VIDEO_LENGTH] = duration;
  //     }
  //     onPicked([map]);
  //   });
  //   return;
  // }

  // AssetPicker.pickAssets(context,
  //         maxAssets: singleMode ? 1 : maxSelection,
  //         textDelegate: EnglishTextDelegate(),
  //         requestType: requestType)
  //     .then((_) async {
  //   if (null == _) return;
  //   if (_ is List && _.isEmpty) return;
  //   List<Map> maps = [];
  //
  //   for (var v in _) {
  //     File file = await v.file;
  //     Map map = {PATH: file.path, IS_VIDEO: v.type == AssetType.video};
  //     String path = map[PATH];
  //     int p = maps.indexWhere((e) => e[PATH] == path);
  //     bool video = map[IS_VIDEO];
  //     if (video) {
  //       File thumbnail = await VideoCompress.getFileThumbnail(path);
  //       int duration = v.duration;
  //       if (duration > (maxSec)) {
  //         showMessage(context, Icons.error, red0, "Too Long",
  //             "Video cannot be longer than ${getSecondsMessage(maxSec)}");
  //         continue;
  //       }
  //       map[THUMBNAIL_PATH] = thumbnail.path;
  //       map[VIDEO_LENGTH] = duration;
  //     }
  //     print("maugost $_ $maps");
  //
  //     if (p == -1)
  //       maps.add(map);
  //     else
  //       maps[p] = map;
  //   }
  //   onPicked(maps);
  // });
}
