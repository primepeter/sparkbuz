import 'dart:async';
import 'dart:core';
import 'dart:io';

import 'package:cached_video_player/cached_video_player.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:screen/screen.dart';
import 'package:sparkbuz/AppConfig.dart';
import 'package:sparkbuz/assets.dart';

import 'AppEngine.dart';
import 'ShowPeople.dart';
import 'basemodel.dart';
import 'main_pages/ShowProfile.dart';

class ShowStatus extends StatefulWidget {
  List<BaseModel> stories;
  int defPosition;
  ShowStatus(this.stories, this.defPosition);

  @override
  _ShowStatusState createState() => _ShowStatusState();
}

class _ShowStatusState extends State<ShowStatus> with TickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  List<BaseModel> stories;
  PageController pc = PageController();
  double currentPage = 0;
  int currentPageInt = 0;
  Timer timer;
  bool tappingDown = false;
  int defPosition;
  List<double> progressValue = List();
  bool showHeart = false;
  BaseModel personModel;
//  double heartSize = 40;

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    controller?.pause();
    Screen.keepOn(false);
    if (timer != null) timer.cancel();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Screen.keepOn(true);
    stories = widget.stories;
    defPosition = widget.defPosition;
    for (int i = 0; i < stories.length; i++) progressValue.add(0);
    pc.addListener(() {
      if (mounted)
        setState(() {
          currentPage = pc.page;
        });
    });

    Future.delayed(Duration(milliseconds: 500), () {
      pc.jumpToPage(defPosition);
      createTimer(defPosition);
      handleSeen(0);
    });
    loadPerson();
  }

  loadPerson() async {
    String id = stories[0].getUserId();
    DocumentSnapshot doc =
        await FirebaseFirestore.instance.collection(USER_BASE).doc(id).get();
    personModel = BaseModel(doc: doc);
    if (mounted) setState(() {});
  }

  handleChange(int index) {
    if (index < currentPageInt) {
      progressValue[index] = 0;
      for (int i = currentPageInt; i > index; i--) {
        progressValue[i] = 0;
      }
    } else if (index > currentPageInt) {
      progressValue[index] = 0;
      for (int i = currentPageInt; i < index; i++) {
        progressValue[i] = 100;
      }
    }
  }

  handleSeen(int index) {
    BaseModel model = stories[index];
    if (stories[index].myItem()) return;

    List shown = model.getList(SHOWN);
    if (shown.contains(userModel.getObjectId())) return;

    model.putInList(SHOWN, userModel.getObjectId(), true);
    model.updateItems(updateTime: false);
    if (mounted) setState(() {});
  }

  createTimer(int position) {
    int seconds = 3;
    BaseModel model = stories[position];
    if (model.isVideo && !hasSetUp) {
      loadCached(position, model);
      return;
    }
    if (timer != null) {
      timer.cancel();
    }
    int milli = seconds * 1000;
    double freq = milli / 100;
    timer = Timer.periodic(Duration(milliseconds: freq.toInt()), (timer) async {
      double val = progressValue[position];
      val = val + 1;
      progressValue[position] = val;
      if (val >= 100) {
        timer.cancel();
        position++;
        if (position < stories.length) {
          await pc.animateToPage(position,
              duration: Duration(milliseconds: 500), curve: Curves.ease);
        } else {
          //if (!isAdmin) Navigator.pop(context, "");
        }
      }
      if (mounted) setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        controller?.pause();
        Navigator.pop(context, "");
        return;
      },
      child: Scaffold(
          key: _scaffoldKey,
          backgroundColor: AppConfig.appColor,
          body: Container(
            color: stories[currentPageInt].getString(COLOR_KEY).isEmpty
                ? black
                : getColorForKey(stories[currentPageInt].getString(COLOR_KEY)),
            child: new Stack(
              fit: StackFit.expand,
              children: <Widget>[
                PageView.builder(
                    itemBuilder: (c, position) {
                      if (position == currentPage.floor()) {
                        return Transform(
                          transform: Matrix4.identity()
                            ..rotateX(currentPage - position),
                          child: page(position),
                        );
                      } else if (position == currentPage.floor() + 1) {
                        return Transform(
                          transform: Matrix4.identity()
                            ..rotateX(currentPage - position),
                          child: page(position),
                        );
                      } else {
                        return page(position);
                      }
                      /*  */
                    },
                    itemCount: stories.length,
                    controller: pc,
                    onPageChanged: (index) {
                      if (timer != null) {
                        timer.cancel();
                      }
                      handleChange(index);
                      currentPageInt = index;
                      createTimer(currentPageInt);
                      handleSeen(index);
                      hasSetUp = false;
                      controller?.pause();
                    }),
                /* Column(
                  children: <Widget>[
                    Expanded(flex: 1, child: Container()),
                    gradientLine(alpha: .7, height: 100)
                  ],
                ),*/
                Container(
                  padding: EdgeInsets.only(top: 35, right: 10, left: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      IgnorePointer(
                        ignoring: true,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(15),
                          child: Container(
                            width: double.infinity,
                            height: 5,
                            child: LinearProgressIndicator(
                              value: (progressValue[currentPageInt] / 100),
                              backgroundColor: stories[currentPageInt]
                                      .getString(STORY_IMAGE)
                                      .isEmpty
                                  ? white
                                  : white.withOpacity(.3),
                              valueColor: AlwaysStoppedAnimation<Color>(
                                white,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          BackButton(
                            color: white,
                            onPressed: () {
                              Navigator.pop(context, "");
                            },
                          ),
                          Flexible(
                              child: personModel == null
                                  ? Container()
                                  : GestureDetector(
                                      onTap: () {
                                        //pushAndResult(context,MyProfile1(personModel));
                                        pushAndResult(
                                            context,
                                            ShowProfile(
                                              showCloseBtn: true,
                                              model: personModel,
                                              profileId:
                                                  personModel.getUserId(),
                                            ),
                                            result: (_) {});
                                      },
                                      child: Container(
                                        color: transparent,
                                        child: Row(
                                          children: <Widget>[
//                                            imageHolder(
//                                                35,
//                                                stories[currentPageInt]
//                                                    .getString(USER_IMAGE),
//                                                strokeColor: blue3,
//                                                stroke: 0),
                                            userImageItem(context,
                                                stories[currentPageInt],
                                                size: 35,
                                                backColor:
                                                    blue3.withOpacity(1)),
                                            addSpaceWidth(10),
                                            Flexible(
                                              flex: 1,
                                              fit: FlexFit.tight,
                                              child: Text(
                                                "${stories[currentPageInt].getString(NAME)}",
                                                style:
                                                    textStyle(true, 16, white),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    )),
                          if (stories[currentPageInt].getString(USER_ID) ==
                                  userModel.getObjectId() ||
                              isAdmin)
                            GestureDetector(
                              onTap: () async {
                                yesNoDialog(context, "Delete Story?",
                                    "Are you sure you want to delete this story?",
                                    () {
                                  BaseModel v = stories[currentPageInt];
                                  v.deleteItem();
                                  Navigator.pop(context, v.getObjectId());
                                  /*stories.removeAt(currentPageInt);
                                  if (stories.isEmpty) {
                                    Navigator.pop(context, "remove");
                                  } else {
                                    progressValue.removeAt(currentPageInt);
                                    positions.removeAt(currentPageInt);
                                    controllers.removeAt(currentPageInt);
                                    if (timer != null) timer.cancel();
                                    pc.jumpToPage(currentPageInt == 0
                                        ? 0
                                        : currentPageInt - 1);
                                    setState(() {});
                                  }*/
                                });
                              },
                              child: Container(
                                  //margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
                                  width: 40,
                                  height: 40,
                                  decoration: BoxDecoration(
                                      color: red, shape: BoxShape.circle),
                                  child: Icon(
                                    Icons.delete,
                                    color: white,
                                    size: 20,
                                  )),
                            ),
                        ],
                      ),
                      addSpace(4),
                      new Expanded(
                        flex: 1,
                        child: Container(
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              GestureDetector(
                                onTap: () {
                                  changePage(false);
                                },
                                onHorizontalDragStart: (_) {
                                  changePage(false);
                                },
                                child: Container(
                                  height: double.infinity,
                                  width: 50,
                                  color: transparent,
                                ),
                              ),
                              Flexible(
                                flex: 1,
                                child: Container(),
                                fit: FlexFit.tight,
                              ),
                              GestureDetector(
                                onTap: () {
                                  changePage(true);
                                },
                                onHorizontalDragStart: (_) {
                                  changePage(true);
                                },
                                child: Container(
                                  height: double.infinity,
                                  width: 50,
                                  color: transparent,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      if (stories[currentPageInt]
                              .getString(STORY_IMAGE)
                              .isNotEmpty &&
                          stories[currentPageInt]
                              .getString(STORY_TEXT)
                              .isNotEmpty)
                        Container(
                          padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                          margin: EdgeInsets.only(bottom: 125),
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color: black.withOpacity(.5),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(0))),
                          child: ReadMoreText(
                            stories[currentPageInt].getString(STORY_TEXT),
                            fontSize: 16,
                            textColor: white,
                            minLength: 150,
                            center: true,
                          ),
                        ),
                    ],
                  ),
                ),
//                Align(
//                  alignment: Alignment.bottomCenter,
//                  child: IgnorePointer(
//                    ignoring: true,
//                    child: Container(
//                      width: double.infinity,
//                      height: 20,
//                      child: LinearProgressIndicator(
//                        value: (progressValue[currentPageInt] / 100),
//                        backgroundColor: stories[currentPageInt]
//                                .getString(STORY_IMAGE)
//                                .isEmpty
//                            ? white.withOpacity(.5)
//                            : white,
//                        valueColor: AlwaysStoppedAnimation<Color>(
//                          black.withOpacity(.2),
//                        ),
//                      ),
//                    ),
//                  ),
//                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: tabIndicator(stories.length, currentPageInt,
                      margin: EdgeInsets.all(10)),
                ),
                if (myStory())
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: GestureDetector(
                      onTap: () async {
                        timer.cancel();
                        pushAndResult(
                            context,
                            ShowPeople(
                              stories[currentPageInt].getList(SHOWN),
                              "Seen By",
                              "Nothing to display",
                            ),
                            opaque: false, result: (_) {
                          createTimer(currentPageInt);
                        });
                      },
                      child: new Container(
                        margin: EdgeInsets.fromLTRB(0, 0, 0, 50),
                        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                        width: 70,
                        height: 30,
                        decoration: BoxDecoration(
                            color: black.withOpacity(.8),
                            borderRadius:
                                BorderRadius.all(Radius.circular(25))),
                        child: Center(
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Text(
                                formatPrice(
                                    "${stories[currentPageInt].getList(SHOWN).length}"),
                                style: textStyle(true, 14, white),
                              ),
                              addSpaceWidth(5),
//                                Image.asset(heart,color: red0,width: 12,height: 12,)
                              Icon(
                                Icons.visibility,
                                //size: 16,
                                color: white,
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    /* child: myStory()
                      ? (GestureDetector(
                          onTap: () async {
                            timer.cancel();
                            pushAndResult(
                                context,
                                ShowPeople(
                                  stories[currentPageInt].getList(LIKED),
                                  "Seen By",
                                  "Nothing to display",
                                ),
                                opaque: false, result: (_) {
                              createTimer(currentPageInt);
                            });
                          },
                          child: new Container(
                            margin: EdgeInsets.fromLTRB(0, 0, 0, 50),
                            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                            width: 70,
                            height: 30,
                            decoration: BoxDecoration(
                                color: black.withOpacity(.8),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(25))),
                            child: Center(
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Text(
                                    formatPrice(
                                        "${stories[currentPageInt].getList(LIKED).length}"),
                                    style: textStyle(true, 14, white),
                                  ),
                                  addSpaceWidth(5),
//                                Image.asset(heart,color: red0,width: 12,height: 12,)
                                  Icon(
                                    Icons.visibility,
                                    //size: 16,
                                    color: white,
                                  )
                                ],
                              ),
                            ),
                          ),
                        ))
                      : Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            if (stories[currentPageInt]
                                .getList(LIKED)
                                .isNotEmpty)
                              Text(
                                "${formatPrice(stories[currentPageInt].getList(LIKED).length.toString())} Like"
                                "${stories[currentPageInt].getList(LIKED).length > 1 ? "s" : ""}",
                                style: textStyle(true, 14, white),
                              ),
                            Container(
                              width: 50,
                              height: 50,
                              margin: EdgeInsets.only(bottom: 50, top: 5),
                              child: FloatingActionButton(
                                onPressed: () {
                                  bool liked = isLiked();
                                  handleLiked(!liked);
                                  if (liked) return;
                                  showHeart = true;
                                  Future.delayed(Duration(milliseconds: 500),
                                      () {
                                    showHeart = false;
                                    setState(() {});
                                  });
                                },
                                heroTag: "h2",
                                shape: CircleBorder(
                                    //   side: BorderSide(color: white,width: 3)
                                    ),
                                child: Icon(
                                  Icons.visibility,
                                  //size: 16,
                                  color: white,
                                ),
                                */ /*Image.asset(
                                  !isLiked() ? heart_border : heart,
                                  width: 25,
                                  height: 25,
                                  color: white,
                                ),*/ /*
                                backgroundColor: AppConfig.appColor,
                                elevation: 5,
                              ),
                            ),
                          ],
                        ),*/
                  ),
                !showHeart
                    ? Container()
                    : Container(
                        color: black.withOpacity(.5),
                        child: Center(
                          child: Image.asset(
                            heart,
                            width: 60,
                            height: 60,
                            color: white,
                          ),
                        ),
                      ),
              ],
            ),
          )),
    );
  }

  bool myStory() {
    return (stories[currentPageInt].getString(USER_ID) ==
        userModel.getObjectId());
  }

  page(int index) {
    BaseModel model = stories[index];
    bool isVideo = model.getBoolean(IS_VIDEO);
    String image = model.getString(isVideo ? THUMBNAIL_URL : STORY_IMAGE);

    String message = model.getString(STORY_TEXT);
    return GestureDetector(
      onTapDown: (_) {
        tappingDown = true;
        if (timer != null) timer.cancel();
      },
      onTapUp: (_) {
        tappingDown = false;
        createTimer(index);
      },
      child: Builder(
        builder: (c) {
          if (!isVideo) {
            return Container(
                color: transparent,
                child: CachedNetworkImage(
                  imageUrl: image,
                  fit: BoxFit.cover,
                  placeholder: (c, s) {
                    return Container(
                        width: 20,
                        height: 20,
                        child: Center(
                          child: CircularProgressIndicator(
                            //value: 20,
                            valueColor: AlwaysStoppedAnimation<Color>(white),
                            strokeWidth: 2,
                          ),
                        ));
                  },
                ));
          }

          if (!hasSetUp)
            return Stack(
              children: [
                CachedNetworkImage(
                  imageUrl: model.getString(
                      THUMBNAIL_URL), //"https://tinyurl.com/yav7jczf",
                  height: getScreenHeight(context),
                  //alignment: Alignment.center,
                  fit: BoxFit.cover,
                ),
                Container(
                  //height: 250,
                  width: double.infinity,
                  //margin: 0.padAt(t: 5, b: 5),
                  color: black.withOpacity(.1),
                  child: Center(
                    child: SizedBox(
                      height: 20,
                      width: 20,
                      child: CircularProgressIndicator(
                        strokeWidth: 2,
                        valueColor: AlwaysStoppedAnimation(white),
                      ),
                    ),
                  ),
                ),
              ],
            );

          return Container(
            width: double.infinity,
            child: AspectRatio(
              aspectRatio: controller.value.aspectRatio,
              child: Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  Container(
                    color: black,
                  ),
                  CachedVideoPlayer(controller),
//               if (showPlayIcon)
//                 Center(
//                   child: Container(
//                     height: 50,
//                     width: 50,
//                     child: Icon(
//                       isPlaying ? Icons.pause : Icons.play_arrow,
//                       color: Colors.white,
//                     ),
//                     decoration: BoxDecoration(
//                         color: Colors.black.withOpacity(0.8),
//                         border: Border.all(color: Colors.white, width: 1.5),
//                         shape: BoxShape.circle),
//                   ),
//                 ),

                  //Container()
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  CachedVideoPlayerController controller;
  bool hasSetUp = false;
  bool showPlayIcon = true;

  loadCached(int p, BaseModel model) async {
    String videoUrl = model.getString(VIDEO_URL);
    String videoPath = model.getString(VIDEO_PATH);
//
//    print(videoPath);
//
//    if (videoUrl.isEmpty) return;
//    String fileName = "${model.getObjectId()}${videoUrl.hashCode}.mp4";
//    File file = (await getLocalFile(fileName));
//    if (model.myItem()) file = File(model.getString(VIDEO_PATH));
//    bool exist = await file.exists();
//
//    if (!exist) {
//      if (model.myItem()) file = (await getLocalFile(fileName));
//      downloadFile(p, model, file, videoUrl);
//      print(exist);
//      return;
//    }
//
//    print("Ready ${file.path}");
    controller = CachedVideoPlayerController.network(videoUrl)
      ..initialize().then((value) {
        controller.play();
        setState(() {
          hasSetUp = true;
        });
        createTimer(p);
      });
    controller.addListener(() {
      bool isPlaying = controller.value.isPlaying;
      if (mounted)
        setState(() {
          showPlayIcon = !isPlaying;
        });
    });
  }

  void downloadFile(int p, BaseModel model, File file, String url) async {
    QuerySnapshot shots = await FirebaseFirestore.instance
        .collection(REFERENCE_BASE)
        .where(FILE_URL, isEqualTo: url)
        .limit(1)
        .get();

    print("shots ${shots.docs[0].data}");
    if (shots.docs.isEmpty) {
      //toastInAndroid("Link not found");
    } else {
      for (DocumentSnapshot doc in shots.docs) {
        if (!doc.exists || doc.data().isEmpty) continue;
        BaseModel model = BaseModel(doc: doc);
        String ref = model.getString(REFERENCE);
        Reference storageReference = FirebaseStorage.instance.ref().child(ref);
        await file.create();
        storageReference.writeToFile(file).then((_) {
          print("path ${file.path}");
          stories[p]
            ..put(VIDEO_PATH, file.path)
            ..updateItems();
          setState(() {});
          loadCached(p, model);
        }, onError: (error) {
          //toastInAndroid(error);
          print("Write error $error");
        }).catchError((error) {
          print("Write error $error");
          //toastInAndroid(error);
        });

        break;
      }
    }
  }

  handleLiked(bool love) {
    BaseModel model = stories[currentPageInt];

    model.putInList(LIKED, userModel.getObjectId(), love);
    model.updateItems(updateTime: false);
    if (mounted) setState(() {});
  }

  bool isLiked() {
    BaseModel model = stories[currentPageInt];

    List liked = model.getList(LIKED);
    return (liked.contains(userModel.getObjectId()));
  }

  changePage(bool next) {
    int position = currentPageInt;
    if (next) position++;
    if (!next) position--;
    if (position < stories.length && next) {
      pc.animateToPage(position,
          duration: Duration(milliseconds: 500), curve: Curves.ease);
    } else if (!next && position >= 0) {
      pc.animateToPage(position,
          duration: Duration(milliseconds: 500), curve: Curves.ease);
    } else {
      Navigator.pop(context, "");
    }
  }

  bool allRead() {
    for (BaseModel bm in stories) {
      if (!bm.getList(SHOWN).contains(userModel.getObjectId())) return false;
    }
    return true;
  }
}
