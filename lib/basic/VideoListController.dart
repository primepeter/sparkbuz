import 'dart:io';

import 'package:cached_video_player/cached_video_player.dart';
import 'package:flutter/material.dart';
import 'package:sparkbuz/MainAdmin.dart';
import 'package:sparkbuz/assets.dart';
import 'package:sparkbuz/basemodel.dart';

typedef void PlayerListener(List<Player> player);

enum VideoListType { network, file, asset }

class VideoListController {
  CachedVideoPlayerController playerOfIndex(int index) =>
      playList[index].controller;
  int get videoCount => playList.length;
  List<Player> playList = [];
  CachedVideoPlayerController get currentPlayer =>
      playList[index.value].controller;
  bool get isPlaying => currentPlayer.value.isPlaying;
  ValueNotifier<int> index = ValueNotifier<int>(0);

  init(PageController pageController, List<BaseModel> initialList, onReady,
      {VideoListType type = VideoListType.network}) {
    addVideoInfo(initialList, onReady, type);
    setPageController(pageController);
  }

  insertVideo(BaseModel model, {int p = 0}) {
    CachedVideoPlayerController controller;
    String videoUrl = model.getString(VIDEO_URL);
    controller = CachedVideoPlayerController.network(videoUrl)
      ..initialize().then((value) {
        controller.play();
        controller.setLooping(true);
        final player = Player(
          model,
          controller,
        );
        playerController.add(player);
        playList.insert(p, player);
      });
  }

  insertVideoMod(Player player, {int p = 0}) {
    playerController.add(player);
    playList.insert(p, player);
  }

  removeVideo(BaseModel model, {int p = 0}) {
    int p = playList
        .indexWhere((e) => e.model.getObjectId() == model.getObjectId());
    if (p == -1) return;
    final player = playList[p];
    player.controller?.pause();
    player.controller?.dispose();
    playerController.add(Player(player.model, player.controller));
    playList.removeAt(p);
  }

  void setPageController(PageController pageController) {
    pageController.addListener(() {
      var p = pageController.page;
      if (p % 1 == 0) {
        int target = p ~/ 1;
        if (index.value == target) return;
        // 播放当前的，暂停其他的
        var oldIndex = index.value;
        var newIndex = target;
        playerOfIndex(oldIndex).seekTo(Duration(seconds: 0));
        playerOfIndex(oldIndex).pause();
        playerOfIndex(newIndex).play();
        // 完成
        index.value = target;
      }
    });
  }

  void addVideoInfo(List<BaseModel> models, onReady, VideoListType type) {
    for (int i = 0; i < models.length; i++) {
      BaseModel model = models[i];
      String videoUrl = model.getString(VIDEO_URL);
      String videoPath = model.getString(VIDEO_PATH);
      CachedVideoPlayerController controller;

      if (type == VideoListType.network) {
        controller = CachedVideoPlayerController.network(videoUrl)
          ..initialize().then((value) {
            if (i == 0) controller.play();
            controller.setLooping(true);
            final player = Player(model, controller);
            playerController.add(player);
            playList.add(player);
          });
      }

      if (type == VideoListType.file) {
        final File file = File(videoPath);
        controller = CachedVideoPlayerController.file(file)
          ..initialize().then((value) {
            if (i == 0) controller.play();
            controller.setLooping(true);
            final player = Player(model, controller);
            playerController.add(player);
            playList.add(player);
          });
      }

      if (type == VideoListType.asset) {
        controller = CachedVideoPlayerController.asset(videoUrl)
          ..initialize().then((value) {
            if (i == 0) controller.play();
            controller.setLooping(true);
            final player = Player(model, controller);
            playerController.add(player);
            playList.add(player);
          });
      }
    }
    onReady();
  }

  void playVideoAt(int p) {
    index.value = p;
  }

  void dispose() {
    for (var player in playList) player.controller?.dispose();
    playList.clear();
  }
}

class Player {
  final BaseModel model;
  final CachedVideoPlayerController controller;

  Player(
    this.model,
    this.controller,
  );
}
