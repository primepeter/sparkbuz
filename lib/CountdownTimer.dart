import 'package:flutter/material.dart';

class CountDownText extends StatefulWidget {
  final int milliseconds;
  final textStyle;
  final onCompleted;

  const CountDownText(
      {Key key, this.milliseconds, this.textStyle, this.onCompleted})
      : super(key: key);

  @override
  _CountDownTextState createState() => _CountDownTextState();
}

class _CountDownTextState extends State<CountDownText> {
  int milliseconds;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    final now = DateTime.now();
    final thenDate = DateTime.fromMillisecondsSinceEpoch(widget.milliseconds);
    final difference = thenDate.difference(now).inMilliseconds / 1000;
    milliseconds = difference.toInt();
    //milliseconds = widget.milliseconds;
    startCountDown();
  }

  startCountDown() async {
    if (milliseconds < 0) {
      widget.onCompleted();
      return;
    }
    await Future.delayed(Duration(seconds: 1));
    milliseconds--;
    if (mounted) setState(() {});
    startCountDown();
  }

  @override
  Widget build(BuildContext context) {
    return Text(
      getTimerText(milliseconds, length: 4, showLetter: true),
      style: widget.textStyle,
    );
  }

  String getTimerText(int seconds, {int length = 2, bool showLetter = false}) {
    int sec = seconds % 60;
    int min = (seconds ~/ 60) % 60;
    int hour = (seconds ~/ Duration.secondsPerHour) % 24;
    int days = seconds ~/ Duration.secondsPerDay;

    String d = days.toString();
    String h = hour.toString();
    String m = min.toString();
    String s = sec.toString();

    String ds = d.length == 1 ? "0$d" : d;
    String hs = h.length == 1 ? "0$h" : h;
    String ms = m.length == 1 ? "0$m" : m;
    String ss = s.length == 1 ? "0$s" : s;

    if (showLetter) {
      ds = "${ds}D";
      hs = "${hs}H";
      ms = "${ms}M";
      ss = "${ss}S";
    }

    return length == 2
        ? "$ms:$ss"
        : length == 3 ? "$hs:$ms:$ss" : "$ds:$hs:$ms:$ss";
  }
}
