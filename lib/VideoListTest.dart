import 'package:cached_video_player/cached_video_player.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sparkbuz/AppEngine.dart';

import 'MainAdmin.dart';
import 'assets.dart';
import 'basemodel.dart';
import 'basic/Tapped.dart';

class VideoListTest extends StatefulWidget {
  @override
  _VideoListTestState createState() => _VideoListTestState();
}

class _VideoListTestState extends State<VideoListTest> {
  CachedVideoPlayerController _videoPlayerController;
  bool loading = false;
  BaseModel selectedVideo;

  int currentPage = 0;
  final vp = PageController();

  final scrollController = ScrollController();
  final refreshController = RefreshController();

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      vp.addListener(() {
        print('scrolling');
        //if (vp.position.outOfRange) return;
        _videoPlayerController?.pause();
        loading = true;
        setState(() {});
      });
      vp.position.isScrollingNotifier.addListener(() {
        if (!vp.position.isScrollingNotifier.value) {
          print('scroll is stopped');
          playNetworkVideos(allPostList[currentPage], false);
        } else {
          print('scroll is started');
        }
      });
    });

    if (allPostList.length > 0) {
      playNetworkVideos(allPostList[0], true);
    }
  }

  playNetworkVideos(BaseModel model, bool initialLoad) async {
    if (!initialLoad) {
      await _videoPlayerController.pause();
      await _videoPlayerController.seekTo(Duration(seconds: 0));
    }
    String videoUrl = model.getString(VIDEO_URL);
    CachedVideoPlayerController videoPlayer =
        CachedVideoPlayerController.network(videoUrl);
    await videoPlayer.initialize();
    loading = false;
    _videoPlayerController = videoPlayer;
    selectedVideo = model;

    setState(() {});
    await Future.delayed(Duration(milliseconds: 100), () {
      _videoPlayerController.setLooping(true);
      _videoPlayerController.play();
      setState(() {});
    });
  }

  @override
  void dispose() {
    _videoPlayerController?.dispose();
    vp?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Video List"),
      ),
      backgroundColor: black,
      body: Stack(
        children: [
          PageView.builder(
            controller: vp,
            itemCount: allPostList.length,
            scrollDirection: Axis.vertical,
            onPageChanged: (p) {
              currentPage = p;
              setState(() {});
            },
            itemBuilder: (c, p) {
              if (null == _videoPlayerController)
                return Center(child: CupertinoActivityIndicator());

              return Stack(
                children: [
                  Center(
                    child: AspectRatio(
                      aspectRatio: _videoPlayerController.value.aspectRatio,
                      child: CachedVideoPlayer(_videoPlayerController),
                    ),
                  ),
                  Tapped(
                    showIcon: !_videoPlayerController.value.isPlaying,
                    // showIcon: videoListController.currentPlayer.value.isPlaying,
                    child: Container(
                      color: transparent,
                      alignment: Alignment.center,
                      child: AnimatedContainer(
                          duration: Duration(milliseconds: 400),
                          child: Icon(
                            _videoPlayerController.value.isPlaying
                                ? Icons.pause
                                : Icons.play_arrow_sharp,
                            color: white.withOpacity(.3),
                            size: 100,
                          )),
                    ),
                    onTapDown: () {
                      //loadVideo();
                    },
                    onTap: () {
                      if (_videoPlayerController.value.isPlaying)
                        _videoPlayerController.pause();
                      else
                        _videoPlayerController.play();
                      setState(() {});
                    },
                  ),
                ],
              );
            },
          ),
          if (loading) Center(child: CupertinoActivityIndicator()),
          Align(
            alignment: Alignment.bottomRight,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                FlatButton(
                  onPressed: () {
                    int p = currentPage + 1;
                    p = p.clamp(0, allPostList.length - 1);
                    vp.jumpToPage(p);
                  },
                  child: Icon(Icons.arrow_upward),
                  color: white,
                  padding: EdgeInsets.all(12),
                  shape: CircleBorder(),
                ),
                addSpace(10),
                FlatButton(
                  onPressed: () {
                    int p = currentPage - 1;
                    p = p.clamp(0, allPostList.length - 1);
                    vp.jumpToPage(p);
                  },
                  child: Icon(Icons.arrow_downward),
                  color: white,
                  padding: EdgeInsets.all(12),
                  shape: CircleBorder(),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
