import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AppConfig {
  static const String appName = "SparkBuz";
  static const String appIcon = "assets/images/ic_launcher.png";
  static const String appFont = "";
  static const int appVersion = 0;
  static const Color appColor = Color(0xfff81618);
  static const Color appYellow = Color(0xffffeb08);
  static const Color bgColor = Color(0XFFF7F7FA);
  static const Color textColor = Color(0xFF5c4eb2);
  static const bool isProduction = false;

  static TextStyle textStyle({double size, FontWeight weight, Color color}) =>
      GoogleFonts.pacifico(
        fontWeight: weight,
        fontSize: size,
        color: color,
      );
}
