import 'dart:async';
import 'dart:io';
import 'dart:io' as io;
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
// import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:location/location.dart';
//import OneSignal
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:package_info/package_info.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sparkbuz/assets.dart';
import 'package:sparkbuz/basemodel.dart';
import 'package:sparkbuz/main_pages/ShowProfile.dart';
import 'package:sparkbuz/main_pages/SparkInvitation.dart';
import 'package:sparkbuz/preview_post_images.dart';
import 'package:synchronized/synchronized.dart';

import 'AppConfig.dart';
import 'AppEngine.dart';
import 'CameraArAndroid.dart';
import 'NewSparkbuzzers.dart';
import 'animations/BeatAnimation.dart';
import 'basic/VideoListController.dart';
import 'main_pages/CameraPage.dart';
import 'main_pages/Discovery.dart';
import 'main_pages/Home.dart';
import 'main_pages/HomeItem.dart';
import 'main_pages/MeetSB.dart';
import 'main_pages/MyProfile.dart';
import 'main_pages/SparkShare.dart';

Map<String, List> unreadCounter = Map();
Map otherPeronInfo = Map();
List<BaseModel> allStories = new List();
bool storiesReady = false;
// final firebaseMessaging = FirebaseMessaging();
final chatMessageController = StreamController<bool>.broadcast();
final homeRefreshController = StreamController<bool>.broadcast();
final uploadingController = StreamController<String>.broadcast();
final pageSubController = StreamController<int>.broadcast();
final subscriptionController = StreamController<bool>.broadcast();
final storiesController = StreamController<BuildContext>.broadcast();
final cameraController = StreamController<BaseModel>.broadcast();
final contextController = StreamController<List<BuildContext>>.broadcast();
final playerInitializeController =
    StreamController<List<BaseModel>>.broadcast();
final pauseController = StreamController<bool>.broadcast();
final loadController = StreamController<bool>.broadcast();
final playerController = StreamController<Player>.broadcast();
List<Player> playList = [];
final profileStateController = StreamController<BaseModel>.broadcast();
final insertVideoController = StreamController<BaseModel>.broadcast();
final deleteVideoController = StreamController<BaseModel>.broadcast();
final loadMoreController = StreamController<bool>.broadcast();
final pageVideoController = StreamController<bool>.broadcast();

final storyController = StreamController<List<BaseModel>>.broadcast();
final stopStoriesController = StreamController<bool>.broadcast();
final storyPageChangeController = StreamController<int>.broadcast();
final closeStoryPageController = StreamController<bool>.broadcast();
final deleteStoryAtController = StreamController<int>.broadcast();

final refreshAnimationController = StreamController<int>.broadcast();

bool sparkInviteVisible = false;
bool globalPause = false;

List connectCount = [];
List<String> stopListening = List();
List<BaseModel> lastMessages = List();
bool chatSetup = false;
List showNewMessageDot = [];
List showNewNotifyDot = [];
List newStoryIds = [];
String visibleChatId;
bool itemsLoaded = false;
bool notifySetup = false;
bool adsSetup = false;
List<BaseModel> nList = List();
List<BaseModel> adsList = List();

// List<BaseModel> myPostList = List();
List<BaseModel> allPostList = [];
List<BaseModel> winnersList = [];
// bool myPostSetup = false;
bool allPostSetup = false;

VideoListController videoListController = VideoListController();

final flareController = StreamController<BaseModel>.broadcast();

Location location = new Location();
GeoFirePoint myLocation;
bool serviceEnabled = false;
PermissionStatus permissionGranted;
LocationData locationData;

var vp = PageController();
int currentPage = 0;
//
// Future<dynamic> myBackgroundMessageHandler(RemoteMessage message) async {
//   await Firebase.initializeApp();
//   print('on background ${message.data}');
// }

class MainAdmin extends StatefulWidget {
  @override
  _MainAdminState createState() => _MainAdminState();
}

class _MainAdminState extends State<MainAdmin>
    with WidgetsBindingObserver, AutomaticKeepAliveClientMixin {
  PageController peoplePageController = PageController();
  List<StreamSubscription> subs = List();
  int timeOnline = 0;
  String noInternetText = "";

  String flashText = "";
  bool setup = false;
  bool settingsLoaded = false;
  String uploadingText;
  int peopleCurrentPage = 0;
  bool tipHandled = false;
  bool tipShown = true;

  @override
  void dispose() {
    // TODO: implement dispose
    WidgetsBinding.instance.removeObserver(this);
    for (var sub in subs) {
      sub.cancel();
    }
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    itemsLoaded = false;
    WidgetsBinding.instance.addObserver(this);
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      createUserListener();
      baseContext = context;
    });
    var subSub = subscriptionController.stream.listen((b) {
      if (!b) return;
      showMessage(context, Icons.check, green, "Congratulations!",
          "You are now a Premium User. Enjoy the benefits!");
    });
    var pageSub = pageSubController.stream.listen((int p) {
      setState(() {
        peopleCurrentPage = p;
      });
    });
    var uploadingSub = uploadingController.stream.listen((text) {
      setState(() {
        uploadingText = text;
      });
    });

    var storySub = storyController.stream.listen((models) async {
      if (models.isEmpty) {
        pushAndResult(
            context,
            Platform.isAndroid
                ? CameraArAndroid(
                    storyMode: true,
                  )
                : CameraPage(
                    storyMode: true,
                  ), result: (List<BaseModel> _) {
          if (_ == null || _.isEmpty) return;
          pushAndResult(
              context,
              PreviewPostImages(
                _,
                [],
                edittable: true,
                storyMode: true,
                indexOf: 0,
                heroTag: getRandomId(),
              ), result: (List<BaseModel> _) {
            if (_ == null || _.isEmpty) return;
            saveStories(_);
          });
        });
        return;
      }
      pushAndResult(
          context,
          PreviewPostImages(
            models,
            [],
            edittable: true,
            storyMode: true,
            indexOf: 0,
            heroTag: getRandomId(),
          ), result: (List<BaseModel> _) {
        if (_ == null || _.isEmpty) return;
        saveStories(_);
      });
    });

    /*var storiesSub = storiesController.stream.listen((camContext) {
      showMessage(context, LineIcons.warning, red, "Gallery Info!",
          "Selecting from gallery implies that your are sharing only to your stories.Videos recorded here are shared to your timeline",
          clickYesText: "Proceed", clickNoText: "Go Back", onClicked: (_) {
        if (_) {
          //openGallery(context, type: PickType.onlyImage, onPicked: (_) {});
          Navigator.pop(camContext);
          openGallery(context, type: PickType.onlyImage, maxSelection: 8,
              onPicked: (_) {
            if (_.isEmpty) return;
            Future.delayed(Duration(milliseconds: 100), () async {
              List files = [];
              for (var f in _) {
                //   File croppedFile = await ImageCropper.cropImage(
                //     sourcePath: f.file.path,
                //   );
                //   if (croppedFile != null) files.add(croppedFile);
                // }
                if (files.isEmpty) return;
                pushAndResult(context, ShowPhoto(files), result: (List models) {
                  saveStories(models);
                }, depend: false);
              }
            });
          });
        }
      });
    });*/
    var cameraSub = cameraController.stream.listen((event) async {
      saveCamVideo(event);
    });
    var contextSub = contextController.stream.listen((event) {
      for (var c in event) Navigator.pop(c);
    });

    //contextController.add([context,context]);

    subs.add(uploadingSub);
    subs.add(pageSub);
    subs.add(subSub);
    /*subs.add(storiesSub);*/
    subs.add(contextSub);
    subs.add(cameraSub);
    subs.add(storySub);
  }

  okLayout(bool manually) {
    itemsLoaded = true;
    if (mounted) setState(() {});
    Future.delayed(Duration(milliseconds: 1000), () {
//      if(manually)pageScrollController.add(-1);
      if (manually) peoplePageController.jumpToPage(peopleCurrentPage);
    });
  }

  Future<int> getSeenCount(String id) async {
    var pref = await SharedPreferences.getInstance();
    List<String> list = pref.getStringList(SHOWN) ?? [];
    int index = list.indexWhere((s) => s.contains(id));
    if (index != -1) {
      String item = list[index];
//      print(item);
      var parts = item.split(SPACE);
      int seenCount = int.parse(parts[1].trim());
      return seenCount;
    }
    return 0;
  }

  void createUserListener() async {
    User user = FirebaseAuth.instance.currentUser;
    if (user != null) {
      var userSub = FirebaseFirestore.instance
          .collection(USER_BASE)
          .doc(user.uid)
          .snapshots()
          .listen((shot) async {
        if (shot != null) {
          User user = FirebaseAuth.instance.currentUser;
          if (user == null) return;

          userModel = BaseModel(doc: shot);
          setState(() {});
          isAdmin = userModel.getBoolean(IS_ADMIN) ||
              userModel.getString(EMAIL) == "johnebere58@gmail.com" ||
              userModel.getString(EMAIL) == "ammaugost@gmail.com";
          loadBlocked();

          if (!settingsLoaded) {
            settingsLoaded = true;
            loadSettings();
          }
        }
      });
      subs.add(userSub);
    }
  }

  loadSettings() async {
    var settingsSub = FirebaseFirestore.instance
        .collection(APP_SETTINGS_BASE)
        .doc(APP_SETTINGS)
        .snapshots()
        .listen((shot) {
      if (shot != null) {
        appSettingsModel = BaseModel(doc: shot);
        chkUpdate();
        List banned = appSettingsModel.getList(BANNED);
        if (banned.contains(userModel.getObjectId()) ||
            banned.contains(userModel.getString(DEVICE_ID)) ||
            banned.contains(userModel.getEmail())) {
          io.exit(0);
        }

        String genMessage = appSettingsModel.getString(GEN_MESSAGE);
        int genMessageTime = appSettingsModel.getInt(GEN_MESSAGE_TIME);

        if (userModel.getInt(GEN_MESSAGE_TIME) != genMessageTime &&
            genMessageTime > userModel.getTime()) {
          userModel.put(GEN_MESSAGE_TIME, genMessageTime);
          userModel.updateItems();

          String title = !genMessage.contains("+")
              ? "Announcement!"
              : genMessage.split("+")[0].trim();
          String message = !genMessage.contains("+")
              ? genMessage
              : genMessage.split("+")[1].trim();
          showMessage(context, Icons.info, blue0, title, message);
        }

        if (!setup) {
          setup = true;
          blockedIds.addAll(userModel.getList(BLOCKED));
          setUpLocation();
          onResume();
          loadNotification();
          loadMessages();
          loadTopWinners();
          //setupPush();
          setUpPushOneSignal();
          loadBlocked();
          updatePackage();
          loadPosts();
          loadStories();
          listenForSShare();
          // loadAds();
          loadNearbyAds();
        }
      }
    });
    subs.add(settingsSub);
  }

  setUpLocation() async {
    print(permissionGranted);
    serviceEnabled = await location.serviceEnabled();
    if (!serviceEnabled) {
      serviceEnabled = await location.requestService();
      if (!serviceEnabled) return;
    }
    permissionGranted = await location.hasPermission();
    if (permissionGranted == PermissionStatus.denied) {
      permissionGranted = await location.requestPermission();
      if (permissionGranted != PermissionStatus.granted) return;
    }

    locationData = await location.getLocation();
    Geoflutterfire geo = Geoflutterfire();

    myLocation = geo.point(
        latitude: locationData.latitude, longitude: locationData.longitude);

    //PlaceMark
    // final place = await Geolocator()
    //     .placemarkFromCoordinates(myLocation.latitude, myLocation.longitude);

    userModel
      ..put(POSITION, myLocation.data)
      //..put(MY_LOCATION, place[0].name)
      //..put(COUNTRY, place[0].country)
      //..put(COUNTRY_CODE, place[0].isoCountryCode)
      ..updateItems();
  }

  loadNearbyAds() async {
    Geoflutterfire geo = Geoflutterfire();
    Map myPosition = userModel.getMap(POSITION);
    if (myPosition.isEmpty) {
      return;
    }
    GeoPoint myGeoPoint = myPosition["geopoint"];
    double lat = myGeoPoint.latitude;
    double lon = myGeoPoint.longitude;
    GeoFirePoint center = geo.point(latitude: lat, longitude: lon);
    final collectionReference = FirebaseFirestore.instance.collection(ADS_BASE);
    //.where(STATUS, isEqualTo: APPROVED);

    double radius = appSettingsModel.getDouble(NEARBY_RADIUS);
    var sub = geo
        .collection(collectionRef: collectionReference)
        .within(center: center, radius: radius, field: POSITION)
        .listen((event) async {
      for (DocumentSnapshot doc in event) {
        BaseModel model = BaseModel(doc: doc);
        bool hide = model.getInt(STATUS) != APPROVED;
        if (hide) {
          adsList.removeWhere((bm) => bm.getObjectId() == model.getObjectId());
          continue;
        }
        int p =
            adsList.indexWhere((bm) => bm.getObjectId() == model.getObjectId());
        if (p == -1) {
          adsList.add(model);
        } else {
          adsList[p] = model;
        }
      }
      //loadingSub.cancel();
      adsSetup = true;
      if (mounted) setState(() {});
    });
    subs.add(sub);
    if (mounted) setState(() {});
  }

  loadAds() async {
    var sub = FirebaseFirestore.instance
        .collection(ADS_BASE)
        .where(STATUS, isEqualTo: APPROVED)
        //.limit(1)
        .snapshots()
        .listen((shots) {
      for (var d in shots.docChanges) {
        BaseModel model = BaseModel(doc: d.doc);
        //bool hide = model.getInt(STATUS) == 123;
        int p =
            adsList.indexWhere((bm) => bm.getObjectId() == model.getObjectId());
        if (d.type == DocumentChangeType.removed /*|| hide*/) {
          nList.removeWhere((bm) => bm.getObjectId() == model.getObjectId());
          continue;
        }
        if (p == -1) {
          adsList.add(model);
        } else {
          adsList[p] = model;
        }
      }
      adsSetup = true;
      setState(() {});
    });
    subs.add(sub);
    if (mounted) setState(() {});
  }

  listenForSShare() async {
    var sub = FirebaseFirestore.instance
        .collection(SPARK_BASE)
        .where(PARTIES, arrayContains: userModel.getUserId())
        .limit(1)
        .orderBy(TIME, descending: true)
        .snapshots()
        .listen((shots) {
      for (var d in shots.docChanges) {
        BaseModel model = BaseModel(doc: d.doc);
        if (model.myItem()) continue;
        List parties = model.getList(PARTIES);
        if (!parties.contains(userModel.getUserId())) continue;
        List participants = model.getList(PARTICIPANTS);
        int participantIndex =
            parties.indexWhere((e) => e == userModel.getUserId());
        if (participantIndex == -1) continue;
        int index = participants[participantIndex];
        if (index > 0) continue;
        if (sparkInviteVisible) continue;
        if (!model.getBoolean(SESSION_ACTIVE)) continue;
        pauseController.add(true);
        pushAndResult(
            context,
            SparkInvitation(
              model: model,
            ), result: (_) {
          if (!_) return;
          pushAndResult(
              context,
              SparkShare(
                sparkShareId: model.getObjectId(),
              ));
        });
      }
    });
    subs.add(sub);
    if (mounted) setState(() {});
  }

  loadNotification() async {
    var sub = FirebaseFirestore.instance
        .collection(NOTIFY_BASE)
        .where(PARTIES, arrayContains: userModel.getUserId())
        // .where(TIME,
        //     isGreaterThan: (DateTime.now().millisecondsSinceEpoch -
        //         (Duration.millisecondsPerDay * 7)))
        .limit(30)
        .orderBy(TIME, descending: true)
        .snapshots()
        .listen((shots) {
      for (var d in shots.docChanges) {
        BaseModel model = BaseModel(doc: d.doc);
        if (model.getTime() >
            (DateTime.now().millisecondsSinceEpoch -
                (Duration.millisecondsPerDay * 7))) continue;

        int p =
            nList.indexWhere((bm) => bm.getObjectId() == model.getObjectId());
        if (d.type == DocumentChangeType.removed) {
          nList.removeWhere((bm) => bm.getObjectId() == model.getObjectId());
          continue;
        }
        if (p == -1) {
          nList.add(model);
        } else {
          nList[p] = model;
        }
        if (!model.getList(READ_BY).contains(userModel.getObjectId()) &&
            !model.myItem()) {
          showNewNotifyDot.add(model.getObjectId());
          setState(() {});
        }
      }
      nList.sort((bm1, bm2) => bm2.getInt(TIME).compareTo(bm1.getInt(TIME)));
    });

    subs.add(sub);
    notifySetup = true;
    if (mounted) setState(() {});
  }

  loadStory() async {
    //await Future.delayed(Duration(seconds: 2));
    /*var storySub =*/ FirebaseFirestore.instance
        .collection(STORY_BASE)
        .where(TIME,
            isGreaterThan: (DateTime.now().millisecondsSinceEpoch -
                (Duration.millisecondsPerDay * 1)))
        .get()
        .then((shots) {
      bool added = false;
      for (DocumentSnapshot shot in shots.docs) {
        if (!shot.exists) continue;
        BaseModel model = BaseModel(doc: shot);
        if (isBlocked(model)) continue;
        int index = allStories
            .indexWhere(((bm) => bm.getObjectId() == model.getObjectId()));
        if (index == -1) {
          allStories.add(model);
          added = true;
          if (!model.myItem() &&
              !model.getList(SHOWN).contains(userModel.getObjectId())) {
            if (!newStoryIds.contains(model.getObjectId()))
              newStoryIds.add(model.getObjectId());
          }
        } else {
          allStories[index] = model;
        }
      }
      homeRefreshController.add(true);
    });
    var myStorySub = FirebaseFirestore.instance
        .collection(STORY_BASE)
        .where(USER_ID, isEqualTo: userModel.getObjectId())
        .where(TIME,
            isGreaterThan: (DateTime.now().millisecondsSinceEpoch -
                (Duration.millisecondsPerDay * 1)))
        .snapshots()
        .listen((shots) {
      bool added = false;
      for (DocumentSnapshot shot in shots.docs) {
        if (!shot.exists) continue;
        BaseModel model = BaseModel(doc: shot);
        if (isBlocked(model)) continue;
        int index = allStories
            .indexWhere(((bm) => bm.getObjectId() == model.getObjectId()));
        if (index == -1) {
          allStories.add(model);
          added = true;
        } else {
          allStories[index] = model;
        }
      }
      homeRefreshController.add(true);
    });
//    subs.add(storySub);
    subs.add(myStorySub);
  }

  loadStories() async {
    //final arrayContains = userModel.myFollowers;
    //arrayContains.add(userModel.getUserId());

    var storySub = FirebaseFirestore.instance
        .collection(STORY_BASE)
        //.where(PARTIES, arrayContainsAny: arrayContains)
        .where(PARTIES, arrayContains: userModel.getUserId())
        .where(TIME,
            isGreaterThan: (DateTime.now().millisecondsSinceEpoch -
                (Duration.millisecondsPerDay * 1)))
        .snapshots()
        .listen((value) {
      for (var doc in value.docs) {
        BaseModel model = BaseModel(doc: doc);
        int p = allStories
            .indexWhere((e) => e.getObjectId() == model.getObjectId());
        if (p != -1) {
          allStories[p] = model;
        } else {
          allStories.add(model);
        }
        /* if (model.isVideo)
          VideoController.loadVideo(model, () {
            if (mounted) setState(() {});
          });*/
      }
      allStories.sort((bm1, bm2) => bm2.getTime().compareTo(bm1.getTime()));
      storiesReady = true;
    });
    subs.add(storySub);
  }

  loadPosts() async {
    FirebaseFirestore.instance
        .collection(POSTS_BASE)
        .orderBy(TIME, descending: true)
        .limit(10)
        .get()
        .then((value) {
      for (var doc in value.docs) {
        BaseModel md = BaseModel(doc: doc);
        if (md.getList(HIDDEN).contains(userModel.getUserId())) continue;
        //loadOtherPerson(md.getUserId());
        int p =
            allPostList.indexWhere((e) => e.getObjectId() == md.getObjectId());
        if (p == -1)
          allPostList.add(md);
        else
          allPostList[p] = md;
      }
      allPostSetup = true;
      //dumpAsJson(allPostList, POSTS_BASE);
      if (mounted) setState(() {});
    });
  }

  chkUpdate() async {
    int version = appSettingsModel
        .getInt(Platform.isAndroid ? ANDROID_VERSION : APPLE_VERSION);
    PackageInfo pack = await PackageInfo.fromPlatform();
    String v = pack.buildNumber;
    int myVersion = int.parse(v);
    print("My Version $myVersion App Version $version");
    if (myVersion < version) {
      pushAndResult(context, UpdateLayout(), opaque: false);
    }
  }

  handleMessage(var message) async {
    final dynamic data = Platform.isAndroid ? message['data'] : message;
    BaseModel model = BaseModel(items: data);
    String title = model.getString("title");
    String body = model.getString("message");

    if (data != null) {
      String type = data[TYPE];
      String id = data[OBJECT_ID];
      if (type != null) {
        if (type == PUSH_TYPE_CHAT && visibleChatId != id) {
//          pushAndResult(
//              context,
//              ChatMain(
//                id,
//                otherPerson: null,
//              ));
        }
      }
    }
  }

  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  /// Create a [AndroidNotificationChannel] for heads up notifications
  AndroidNotificationChannel channel = AndroidNotificationChannel(
    'high_importance_channel', // id
    'High Importance Notifications', // title
    'This channel is used for important notifications.', // description
    importance: Importance.high,
    enableVibration: true,
    playSound: true,
  );

//   setupPush() async {
//     if (Platform.isIOS) return;
//     await FirebaseMessaging.instance.requestPermission(
//       alert: true,
//       announcement: false,
//       badge: true,
//       carPlay: false,
//       criticalAlert: false,
//       provisional: false,
//       sound: true,
//     );
//
//     await FirebaseMessaging.instance
//         .setForegroundNotificationPresentationOptions(
//       alert: true, // Required to display a heads up notification
//       badge: true,
//       sound: true,
//     );
//
//     FirebaseMessaging.onBackgroundMessage(myBackgroundMessageHandler);
//
//     if (Platform.isAndroid) {
//       /// Create an Android Notification Channel.
//       ///
//       /// We use this channel in the `AndroidManifest.xml` file to override the
//       /// default FCM channel to enable heads up notifications.
//       await flutterLocalNotificationsPlugin
//           .resolvePlatformSpecificImplementation<
//               AndroidFlutterLocalNotificationsPlugin>()
//           ?.createNotificationChannel(channel);
//     }
//
//     /// Update the iOS foreground notification presentation options to allow
//     /// heads up notifications.
//     await FirebaseMessaging.instance
//         .setForegroundNotificationPresentationOptions(
//       alert: true,
//       badge: true,
//       sound: true,
//     );
//
//     FirebaseMessaging.onMessage.listen((RemoteMessage message) {
//       RemoteNotification notification = message.notification;
//       AndroidNotification android = message.notification?.android;
//       if (notification != null && android != null) {
//         flutterLocalNotificationsPlugin.show(
//             notification.hashCode,
//             notification.title,
//             notification.body,
//             NotificationDetails(
//               android: AndroidNotificationDetails(
//                 channel.id,
//                 channel.name,
//                 channel.description,
//                 // TODO add a proper drawable resource to android, for now using
//                 //      one that already exists in example app.
//                 icon: 'ic_notify',
//               ),
//             ));
//       }
//     });
//
//     FirebaseMessaging.instance.setAutoInitEnabled(true);
//     FirebaseMessaging.instance.subscribeToTopic('all');
//     FirebaseMessaging.instance.subscribeToTopic(userModel.getObjectId());
//     FirebaseMessaging.instance.getToken().then((String token) async {
//       if (userModel.getString(TOKEN).isEmpty)
//         userModel
//           ..put(TOKEN, token)
//           ..updateItems();
//     });
//     FirebaseMessaging.onMessage.listen((RemoteMessage message) {
//       handleMessage(message.data);
//     });
//
// /*    firebaseMessaging.configure(
//       onMessage: (Map<String, dynamic> message) async {
//         print("onMessage: $message");
//         //handleMessage(message);
//       },
//       onLaunch: (Map<String, dynamic> message) async {
//         print("onLaunch: $message");
//         handleMessage(message);
//       },
//       onResume: (Map<String, dynamic> message) async {
//         print("onResume: $message");
//         handleMessage(message);
//       },
//       //onBackgroundMessage: myBackgroundMessageHandler,
//     );
//     firebaseMessaging.requestNotificationPermissions(
//         const IosNotificationSettings(
//             sound: true, badge: true, alert: true, provisional: false));
//     firebaseMessaging.onIosSettingsRegistered
//         .listen((IosNotificationSettings settings) {
//       print("Settings registered: $settings");
//     });
//     firebaseMessaging.subscribeToTopic('all');
//     firebaseMessaging.subscribeToTopic(userModel.getObjectId());
//     firebaseMessaging.getToken().then((String token) async {
//       if (userModel.getString(TOKEN).isEmpty)
//         userModel
//           ..put(TOKEN, token)
//           ..updateItems();
//     });*/
//   }

  String _debugLabelString = "";
  String _emailAddress;
  String _externalUserId;
  bool _enableConsentButton = false;

  // CHANGE THIS parameter to true if you want to test GDPR privacy consent
  bool _requireConsent = true;

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> setUpPushOneSignal() async {
    if (!mounted) return;

    // if (Platform.isAndroid) return;
    _handlePromptForPushPermission();
    OneSignal.shared.setLogLevel(OSLogLevel.verbose, OSLogLevel.none);

    OneSignal.shared.setRequiresUserPrivacyConsent(_requireConsent);

    var settings = {
      OSiOSSettings.autoPrompt: false,
      OSiOSSettings.promptBeforeOpeningPushUrl: true
    };

    OneSignal.shared
        .setNotificationReceivedHandler((OSNotification notification) {
      this.setState(() {
        _debugLabelString =
            "Received notification: \n${notification.jsonRepresentation().replaceAll("\\n", "\n")}";
      });
    });

    OneSignal.shared
        .setNotificationOpenedHandler((OSNotificationOpenedResult result) {
      BaseModel model =
          BaseModel(items: result.notification.payload.additionalData);
      String db = model.getString(DATABASE_NAME);
      bool toProfile = db == USER_BASE;

      this.setState(() {
        _debugLabelString =
            "Opened notification: \n${result.notification.jsonRepresentation().replaceAll("\\n", "\n")}";
      });

      if (!toProfile) return;
      pushAndResult(
          context,
          ShowProfile(
            model: model,
          ));
    });

    OneSignal.shared
        .setInAppMessageClickedHandler((OSInAppMessageAction action) {
      this.setState(() {
        _debugLabelString =
            "In App Message Clicked: \n${action.jsonRepresentation().replaceAll("\\n", "\n")}";
      });
    });

    OneSignal.shared
        .setSubscriptionObserver((OSSubscriptionStateChanges changes) {
      print("SUBSCRIPTION STATE CHANGED: ${changes.jsonRepresentation()}");
    });

    OneSignal.shared.setPermissionObserver((OSPermissionStateChanges changes) {
      print("PERMISSION STATE CHANGED: ${changes.jsonRepresentation()}");
    });

    OneSignal.shared.setEmailSubscriptionObserver(
        (OSEmailSubscriptionStateChanges changes) {
      print("EMAIL SUBSCRIPTION STATE CHANGED ${changes.jsonRepresentation()}");
    });

    // NOTE: Replace with your own app ID from https://www.onesignal.com
    await OneSignal.shared.init(
        appSettingsModel
            .getString(Platform.isIOS ? ONE_SIGNAL_APPLE : ONE_SIGNAL_ANDROID),
        iOSSettings: settings);

    OneSignal.shared
        .setInFocusDisplayType(OSNotificationDisplayType.notification);

    bool requiresConsent = await OneSignal.shared.requiresUserPrivacyConsent();

    this.setState(() {
      _enableConsentButton = requiresConsent;
    });

    // Some examples of how to use In App Messaging public methods with OneSignal SDK
    oneSignalInAppMessagingTriggerExamples();

    // Some examples of how to use Outcome Events public methods with OneSignal SDK
    oneSignalOutcomeEventsExamples();
    _handleConsent();
    //_handleSetEmail();
    _handleGetPermissionSubscriptionState();
  }

  void _handleGetTags() {
    OneSignal.shared.getTags().then((tags) {
      if (tags == null) return;

      setState((() {
        _debugLabelString = "$tags";
      }));
    }).catchError((error) {
      setState(() {
        _debugLabelString = "$error";
      });
    });
  }

  void _handleSendTags() {
    print("Sending tags");
    OneSignal.shared.sendTag("test2", "val2").then((response) {
      print("Successfully sent tags with response: $response");
    }).catchError((error) {
      print("Encountered an error sending tags: $error");
    });
  }

  void _handlePromptForPushPermission() {
    print("Prompting for Permission");
    OneSignal.shared.promptUserForPushNotificationPermission().then((accepted) {
      print("Accepted permission: $accepted");
    });
  }

  void _handleGetPermissionSubscriptionState() async {
    // OneSignal.shared.send

    String playerId = (await OneSignal.shared.getPermissionSubscriptionState())
        .subscriptionStatus
        .userId;
    print('playerId $playerId');
    userModel
      ..put(ONE_SIGNAL_ID, playerId)
      ..updateItems();

    print("Getting permissionSubscriptionState");
    OneSignal.shared.getPermissionSubscriptionState().then((status) {
      String playerId = status.subscriptionStatus.userId;
      print('playerId $playerId');
      userModel
        ..put(ONE_SIGNAL_ID, playerId)
        ..updateItems();

      this.setState(() {
        _debugLabelString = status.jsonRepresentation();
      });
    });
  }

  void _handleSetEmail() {
    print("Setting email");
    OneSignal.shared.setEmail(email: userModel.getEmail()).whenComplete(() {
      print("Successfully set email");
    }).catchError((error) {
      print("Failed to set email with error: $error");
    });
  }

  void _handleLogoutEmail() {
    print("Logging out of email");
    OneSignal.shared.logoutEmail().then((v) {
      print("Successfully logged out of email");
    }).catchError((error) {
      print("Failed to log out of email: $error");
    });
  }

  void _handleConsent() {
    print("Setting consent to true");
    OneSignal.shared.consentGranted(true);

    print("Setting state");
    this.setState(() {
      _enableConsentButton = false;
    });
  }

  void _handleSetLocationShared() {
    print("Setting location shared to true");
    OneSignal.shared.setLocationShared(true);
  }

  void _handleDeleteTag() {
    print("Deleting tag");
    OneSignal.shared.deleteTag("test2").then((response) {
      print("Successfully deleted tags with response $response");
    }).catchError((error) {
      print("Encountered error deleting tag: $error");
    });
  }

  void _handleSetExternalUserId() {
    print("Setting external user ID");
    OneSignal.shared.setExternalUserId(_externalUserId).then((results) {
      if (results == null) return;

      this.setState(() {
        _debugLabelString = "External user id set: $results";
      });
    });
  }

  void _handleRemoveExternalUserId() {
    OneSignal.shared.removeExternalUserId().then((results) {
      if (results == null) return;

      this.setState(() {
        _debugLabelString = "External user id removed: $results";
      });
    });
  }

  void _handleSendNotification() async {
    var status = await OneSignal.shared.getPermissionSubscriptionState();
    var playerId = status.subscriptionStatus.userId;

    var imgUrlString =
        "http://cdn1-www.dogtime.com/assets/uploads/gallery/30-impossibly-cute-puppies/impossibly-cute-puppy-2.jpg";

    var notification = OSCreateNotification(
        playerIds: [playerId],
        content: "this is a test from OneSignal's Flutter SDK",
        heading: "Test Notification",
        iosAttachments: {"id1": imgUrlString},
        bigPicture: imgUrlString,
        buttons: [
          OSActionButton(text: "test1", id: "id1"),
          OSActionButton(text: "test2", id: "id2")
        ]);

    var response = await OneSignal.shared.postNotification(notification);

    this.setState(() {
      _debugLabelString = "Sent notification with response: $response";
    });
  }

  void _handleSendSilentNotification() async {
    var status = await OneSignal.shared.getPermissionSubscriptionState();

    var playerId = status.subscriptionStatus.userId;

    var notification = OSCreateNotification.silentNotification(
        playerIds: [playerId], additionalData: {'test': 'value'});

    var response = await OneSignal.shared.postNotification(notification);

    this.setState(() {
      _debugLabelString = "Sent notification with response: $response";
    });
  }

  oneSignalInAppMessagingTriggerExamples() async {
    /// Example addTrigger call for IAM
    /// This will add 1 trigger so if there are any IAM satisfying it, it
    /// will be shown to the user
    OneSignal.shared.addTrigger("trigger_1", "one");

    /// Example addTriggers call for IAM
    /// This will add 2 triggers so if there are any IAM satisfying these, they
    /// will be shown to the user
    Map<String, Object> triggers = new Map<String, Object>();
    triggers["trigger_2"] = "two";
    triggers["trigger_3"] = "three";
    OneSignal.shared.addTriggers(triggers);

    // Removes a trigger by its key so if any future IAM are pulled with
    // these triggers they will not be shown until the trigger is added back
    OneSignal.shared.removeTriggerForKey("trigger_2");

    // Get the value for a trigger by its key
    Object triggerValue =
        await OneSignal.shared.getTriggerValueForKey("trigger_3");
    print("'trigger_3' key trigger value: " + triggerValue.toString());

    // Create a list and bulk remove triggers based on keys supplied
    List<String> keys = ["trigger_1", "trigger_3"];
    OneSignal.shared.removeTriggersForKeys(keys);

    // Toggle pausing (displaying or not) of IAMs
    OneSignal.shared.pauseInAppMessages(false);
  }

  oneSignalOutcomeEventsExamples() async {
    // Await example for sending outcomes
    outcomeAwaitExample();

    // Send a normal outcome and get a reply with the name of the outcome
    OneSignal.shared.sendOutcome("normal_1");
    OneSignal.shared.sendOutcome("normal_2").then((outcomeEvent) {
      print(outcomeEvent.jsonRepresentation());
    });

    // Send a unique outcome and get a reply with the name of the outcome
    OneSignal.shared.sendUniqueOutcome("unique_1");
    OneSignal.shared.sendUniqueOutcome("unique_2").then((outcomeEvent) {
      print(outcomeEvent.jsonRepresentation());
    });

    // Send an outcome with a value and get a reply with the name of the outcome
    OneSignal.shared.sendOutcomeWithValue("value_1", 3.2);
    OneSignal.shared.sendOutcomeWithValue("value_2", 3.9).then((outcomeEvent) {
      print(outcomeEvent.jsonRepresentation());
    });
  }

  Future<void> outcomeAwaitExample() async {
    var outcomeEvent = await OneSignal.shared.sendOutcome("await_normal_1");
    print(outcomeEvent.jsonRepresentation());
  }

  // setupPush() async {
  //   firebaseMessaging.configure(
  //     onMessage: (Map<String, dynamic> message) async {
  //       print("onMessage: $message");
  //       //handleMessage(message);
  //     },
  //     onLaunch: (Map<String, dynamic> message) async {
  //       print("onLaunch: $message");
  //       handleMessage(message);
  //     },
  //     onResume: (Map<String, dynamic> message) async {
  //       print("onResume: $message");
  //       handleMessage(message);
  //     },
  //   );
  //   firebaseMessaging.requestNotificationPermissions(
  //       const IosNotificationSettings(sound: true, badge: true, alert: true));
  //   firebaseMessaging.onIosSettingsRegistered
  //       .listen((IosNotificationSettings settings) {
  //     print("Settings registered: $settings");
  //   });
  //   if (userModel.isAdminItem()) {
  //     firebaseMessaging.subscribeToTopic('admin');
  //   }
  //
  //   firebaseMessaging.subscribeToTopic('all');
  //   firebaseMessaging.subscribeToTopic(userModel.getUserId());
  //   firebaseMessaging.getToken().then((String token) async {
  //     List myTopics = List.from(userModel.getList(TOPICS));
  //
  //     if (userModel.isAdminItem() && !myTopics.contains('admin')) {
  //       myTopics.add('admin');
  //     }
  //     if (!myTopics.contains('all')) myTopics.add('all');
  //
  //     userModel.put(TOPICS, myTopics);
  //     userModel.put(TOKEN, token);
  //     userModel.updateItems();
  //   });
  //
  //   //local notifications
  // }

  void onPause() {
    if (userModel == null) return;
    int prevTimeOnline = userModel.getInt(TIME_ONLINE);
    int timeActive = (DateTime.now().millisecondsSinceEpoch) - timeOnline;
    int newTimeOnline = timeActive + prevTimeOnline;
    userModel.put(IS_ONLINE, false);
    userModel.put(TIME_ONLINE, newTimeOnline);
    userModel.put(TIME_PAUSED, DateTime.now().millisecondsSinceEpoch);
    userModel.updateItems();
    timeOnline = 0;
  }

  void onResume() async {
    if (userModel == null) return;
    final timePaused =
        DateTime.fromMillisecondsSinceEpoch(userModel.getInt(TIME_PAUSED));
    final nowTime = DateTime.now();

    timeOnline = DateTime.now().millisecondsSinceEpoch;
    userModel.put(IS_ONLINE, true);
    userModel.put(
        PLATFORM,
        Platform.isAndroid
            ? ANDROID
            : Platform.isIOS
                ? IOS
                : WEB);
    if (!userModel.getBoolean(NEW_APP)) {
      userModel.put(NEW_APP, true);
    }
    userModel.updateItems();
    print("Time Paused ${nowTime.difference(timePaused).inSeconds}");
    Future.delayed(Duration(seconds: 2), () {
//      if (nowTime.difference(timePaused).inMinutes > 2) {
//        showMessage(
//            context,
//            Icons.exit_to_app,
//            AppConfig.appColor,
//            "Auth Expired!",
//            "Please login again to Re-Authenticate your account",
//            cancellable: false, onClicked: (_) {
//          if (_) clickLogout(context);
//        });
//        return;
//      }
      checkLaunch();
    });
  }

  Future<void> checkLaunch() async {
    const platform = const MethodChannel("channel.john");
    try {
      Map response = await platform
          .invokeMethod('launch', <String, String>{'message': ""});
      int type = response[TYPE];
      String chatId = response[CHAT_ID];

//      toastInAndroid(type.toString());
//      toastInAndroid(chatId);

      if (type == LAUNCH_CHAT) {
//        pushAndResult(
//            context,
//            ChatMain(
//              chatId,
//              otherPerson: null,
//            ));
      }

      if (type == LAUNCH_REPORTS) {
//        pushAndResult(context, ReportMain());
      }

      //toastInAndroid(response);
    } catch (e) {
      //toastInAndroid(e.toString());
      //batteryLevel = "Failed to get what he said: '${e.message}'.";
    }
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // TODO: implement didChangeAppLifecycleState
    if (state == AppLifecycleState.paused) {
      onPause();
    }
    if (state == AppLifecycleState.resumed) {
      onResume();
    }

    super.didChangeAppLifecycleState(state);
  }

  loadTopWinners() async {
    FirebaseFirestore.instance.collection(POSTS_BASE).get().then((value) {
      for (var doc in value.docs) {
        BaseModel model = BaseModel(doc: doc);
        // bool complete = model.signUpCompleted;
        // if (!complete) continue;
        int p = winnersList.indexWhere(
            (element) => element.getObjectId() == model.getObjectId());
        if (p != -1) {
          winnersList[p] = model;
        } else {
          winnersList.add(model);
        }
        //addBirthDate(model.getUserId(), model);
      }
      winnersList.shuffle();
      winnersList
          .sort((a, b) => b.likesCount.length.compareTo(a.likesCount.length));
      //mostLiked = postList.first;

      if (mounted) setState(() {});
    });
    // subs.add(sub);
  }

  List loadedIds = [];
  loadMessages() async {
    return;
    var lock = Lock();
    await lock.synchronized(() async {
//      List<Map> myChats = List.from(userModel.getList(MY_CHATS));
      var sub = FirebaseFirestore.instance
          .collection(CHAT_IDS_BASE)
          .where(PARTIES, arrayContains: userModel.getObjectId())
          .snapshots()
          .listen((shots) {
        for (DocumentSnapshot doc in shots.docs) {
          BaseModel chatIdModel = BaseModel(doc: doc);
          String chatId = chatIdModel.getObjectId();
          if (userModel.getList(DELETED_CHATS).contains(chatId)) continue;
          if (loadedIds.contains(chatId)) {
            continue;
          }
          loadedIds.add(chatId);

          var sub = FirebaseFirestore.instance
              .collection(CHAT_BASE)
              .where(PARTIES, arrayContains: userModel.getUserId())
              .where(CHAT_ID, isEqualTo: chatId)
              .orderBy(TIME, descending: true)
              .limit(1)
              .snapshots()
              .listen((shots) async {
            if (shots.docs.isNotEmpty) {
              BaseModel cModel = BaseModel(doc: (shots.docs[0]));
              if (isBlocked(null, userId: getOtherPersonId(cModel))) {
                lastMessages.removeWhere(
                    (bm) => bm.getString(CHAT_ID) == cModel.getString(CHAT_ID));
                chatMessageController.add(true);
                return;
              }
            }
            if (stopListening.contains(chatId)) return;
            for (DocumentSnapshot doc in shots.docs) {
              BaseModel model = BaseModel(doc: doc);
              String chatId = model.getString(CHAT_ID);
              int index = lastMessages.indexWhere(
                  (bm) => bm.getString(CHAT_ID) == model.getString(CHAT_ID));
              if (index == -1) {
                lastMessages.add(model);
              } else {
                lastMessages[index] = model;
              }

              if (!model.getList(READ_BY).contains(userModel.getObjectId()) &&
                  !model.myItem() &&
                  visibleChatId != model.getString(CHAT_ID)) {
                try {
                  if (!showNewMessageDot.contains(chatId))
                    showNewMessageDot.add(chatId);
                  setState(() {});
                } catch (E) {
                  if (!showNewMessageDot.contains(chatId))
                    showNewMessageDot.add(chatId);
                  setState(() {});
                }
                countUnread(chatId);
              }
            }

            String otherPersonId = getOtherPersonId(chatIdModel);
            loadOtherPerson(otherPersonId);

            try {
              lastMessages
                  .sort((bm1, bm2) => bm2.getTime().compareTo(bm1.getTime()));
            } catch (E) {}
          });

          subs.add(sub);
        }
        chatSetup = true;
        if (mounted) setState(() {});
      });
      subs.add(sub);
    });
  }

  loadOtherPerson(String uId, {int delay = 0}) async {
    var lock = Lock();
    await lock.synchronized(() async {
      Future.delayed(Duration(seconds: delay), () async {
        FirebaseFirestore.instance
            .collection(USER_BASE)
            .doc(uId)
            .get()
            .then((doc) {
          if (doc == null) return;
          if (!doc.exists) return;
          BaseModel user = BaseModel(doc: doc);
          otherPeronInfo[uId] = user;
          if (mounted) setState(() {});
        });
      });
    }, timeout: Duration(seconds: 10));
  }

  countUnread(String chatId) async {
    var lock = Lock();
    lock.synchronized(() async {
      int count = 0;
      QuerySnapshot shots = await FirebaseFirestore.instance
          .collection(CHAT_BASE)
          .where(CHAT_ID, isEqualTo: chatId)
          .get();

      List list = [];
      for (DocumentSnapshot doc in shots.docs) {
        BaseModel model = BaseModel(doc: doc);
        if (!model.getList(READ_BY).contains(userModel.getObjectId()) &&
            !model.myItem()) {
          count++;
          list.add(model);
        }
      }
      if (list.isNotEmpty) unreadCounter[chatId] = list;
      chatMessageController.add(true);
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return WillPopScope(
      onWillPop: () {
        //backThings();
        io.exit(0);
        return;
      },
      child: Scaffold(
        backgroundColor: pageColor,
        body: page(),
      ),
    );
  }

  backThings() {
    if (userModel != null && !userModel.getBoolean(HAS_RATED)) {
      showMessage(context, Icons.star, blue0, "Rate Us",
          "Enjoying the App? Please support us with 5 stars",
          clickYesText: "RATE APP", clickNoText: "Later", onClicked: (_) {
        if (_ == true) {
          if (appSettingsModel == null ||
              appSettingsModel.getString(PACKAGE_NAME).isEmpty) {
            onPause();
            Future.delayed(Duration(seconds: 1), () {
              io.exit(0);
            });
          } else {
            rateApp();
          }
        } else {
          onPause();
          Future.delayed(Duration(seconds: 1), () {
            io.exit(0);
          });
        }
      });
      return;
    }
    onPause();
    Future.delayed(Duration(seconds: 1), () {
      io.exit(0);
    });
  }

  loadBlocked() async {
    var lock = Lock();
    lock.synchronized(() async {
      QuerySnapshot shots = await FirebaseFirestore.instance
          .collection(USER_BASE)
          .where(BLOCKED, arrayContains: userModel.getObjectId())
          .get();

      for (DocumentSnapshot doc in shots.docs) {
        BaseModel model = BaseModel(doc: doc);
        String uId = model.getObjectId();
        String deviceId = model.getString(DEVICE_ID);
        if (!blockedIds.contains(uId)) blockedIds.add(uId);
        if (deviceId.isNotEmpty) if (!blockedIds.contains(deviceId))
          blockedIds.add(deviceId);
      }
    }, timeout: Duration(seconds: 10));
  }

  saveCamVideo(BaseModel model, {bool thumbUpload = false}) async {
    String path = model.getString(VIDEO_PATH);
    String thumbPath = model.getString(THUMBNAIL_PATH);
    String id = model.getObjectId();
    int p = allPostList.indexWhere((e) => e.getObjectId() == id);

    uploadingController.add("Uploading Video");
    File file = File(path);

    print(path);
    print(await file.exists());
    print("uploading video...");

    uploadFile(file, (res, error) {
      print("uploaded video... res $res error $error");
      if (error != null) {
        saveCamVideo(model);
        return;
      }

      model.put(VIDEO_URL, res);
      model.put(IS_VIDEO, true);
      model.saveItem(POSTS_BASE, true, document: id, merged: true,
          onComplete: () {
        print("uploading thumbnail...");
        File thumbnail = File(thumbPath);
        uploadFile(thumbnail, (res, error) {
          print("uploaded thumb... res $res error $error");
          if (error != null) {
            saveCamVideo(model);
            return;
          }
          model.put(THUMBNAIL_URL, res);
          model.updateItems();
          if (p != -1) {
            allPostList[p] = model;
          } else {
            allPostList.add(model);
          }
          allPostList
              .sort((bm1, bm2) => bm2.getTime().compareTo(bm1.getTime()));
          insertVideoController.add(model);
          setState(() {});
          uploadingController.add("Upload Successful");
          profileStateController.add(model);
          Future.delayed(Duration(seconds: 1), () {
            uploadingController.add(null);
            //TODO Notify participants
            pushNotificationToUsers(
                notifyType: NOTIFY_TYPE_POST,
                notifyId: model.getObjectId(),
                userIds: userModel.followers);
          });
        });
      });
    });
  }

  saveStories(List models, {bool isThum = false}) async {
    if (models.isEmpty) {
      uploadingController.add("Uploading Successful");
      Future.delayed(Duration(seconds: 1), () {
        uploadingController.add(null);
        if (currentPage != 1) {
          vp.jumpToPage(1);
        }
      });
      return;
    }
    uploadingController.add("Uploading Story");

    BaseModel model = models[0];
    String id = model.getObjectId();
    bool isVideo = model.isVideo;
    String thumbPath = model.getString(THUMBNAIL_PATH);
    String imagePath = model.getString(IMAGE_PATH);
    List parties = [userModel.getUserId()];
    parties.addAll(userModel.myFollowers);
    model.put(PARTIES, parties);
    model.put(DATABASE_NAME, STORY_BASE);

    if (isThum) {
      uploadFile(File(thumbPath), (thumbRes, error) {
        if (error != null) {
          saveStories(models, isThum: true);
          return;
        }
        print("thumbPath url $thumbRes");
        model.put(THUMBNAIL_PATH, "");
        model.put(THUMBNAIL_URL, thumbRes);
        model.saveItem(STORY_BASE, true, merged: true, document: id);
        int p = allStories
            .indexWhere((e) => e.getObjectId() == model.getObjectId());
        if (p != -1) {
          allStories[p] = model;
        } else {
          allStories.add(model);
        }
        models.removeAt(0);
        saveStories(models);
      });
      return;
    }

    /*if (isVideo && thumbPath.isEmpty && imagePath.isEmpty) {
      models.removeAt(0);
      saveStories(models);
      return;
    }

    if (imagePath.isEmpty) {
      models.removeAt(0);
      saveStories(models);
      return;
    }*/

    uploadFile(File(imagePath), (res, error) {
      if (error != null) {
        saveStories(models);
        return;
      }

      model.put(IMAGE_PATH, "");
      model.put(IMAGE_URL, res);
      model.saveItem(STORY_BASE, true, document: id, merged: true);
      models[0] = model;

      if (isVideo) {
        uploadFile(File(thumbPath), (thumbRes, error) {
          if (error != null) {
            saveStories(models, isThum: true);
            return;
          }
          print("thumbPath url $thumbRes");
          model.put(THUMBNAIL_PATH, "");
          model.put(THUMBNAIL_URL, thumbRes);
          //model.saveItem(STORY_BASE, true, merged: true, document: id);
          model.updateItems();
          int p = allStories
              .indexWhere((e) => e.getObjectId() == model.getObjectId());
          if (p != -1) {
            allStories[p] = model;
          } else {
            allStories.add(model);
          }
          models.removeAt(0);
          saveStories(models);
        });
        return;
      }

      int p =
          allStories.indexWhere((e) => e.getObjectId() == model.getObjectId());
      if (p != -1) {
        allStories[p] = model;
      } else {
        allStories.add(model);
      }
      model.saveItem(STORY_BASE, true, document: id, merged: true);
      models.removeAt(0);
      saveStories(models);
    });
  }

  @override
  bool get wantKeepAlive => true;

  page() {
    return Stack(
      children: [
        PageView(
          controller: vp,
          onPageChanged: (p) {
            currentPage = p;
            globalPause = p == 0 ? false : true;
            print('globalPause $globalPause $p');
            //if (!globalPause)
            // pauseController.add(p > 0 ? false : true);
            if (p == 0) {
              pauseController.add(currentPageHomeItem == 1);
            } else {
              pauseController.add(true);
            }

            setState(() {});
          },
          physics: NeverScrollableScrollPhysics(),
          children: [
            Home(),
            Discovery(),
            MeetSB(),
            MyProfile(),
          ],
        ),
        bottomTabs(),
        AnimatedContainer(
          height: uploadingText == null ? 0 : 70,
          duration: Duration(milliseconds: 500),
          width: double.infinity,
          alignment: Alignment.center,
          child: SafeArea(
            bottom: false,
            minimum: EdgeInsets.only(bottom: 5),
            child: uploadingText == null
                ? Container()
                : Text(
                    uploadingText,
                    style: textStyle(true, 14, black),
                  ),
          ),
          color: AppConfig.appYellow,
          //padding: EdgeInsets.fromLTRB(35, 10, 10, 0),
        ),
      ],
    );
  }

  bottomTabs() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
          color: pageColor.withOpacity(1),
          alignment: Alignment.bottomCenter,
          padding: EdgeInsets.only(left: 15, right: 15, bottom: 15, top: 10),
          height: 90,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              btmTabItem(0),
              btmTabItem(1),
              Align(
                alignment: Alignment.topCenter,
                child: GestureDetector(
                  onTap: () {
                    pauseController.add(true);
                    pushAndResult(context,
                        Platform.isAndroid ? CameraArAndroid() : CameraPage());
                  },
                  onLongPress: () {
                    if (!isAdmin) return;
                    pauseController.add(true);
                    // pushAndResult(context, VideoListTest());
                    pushAndResult(context, NewSparkbuzzers());
                    //_handleSendNotification();
                  },
                  child: BeatAnimation(
                    scaleSize: 60,
                    currentPage: currentPage,
                    body: Container(
                      height: 50,
                      width: 50,
                      child: Image.asset(
                        "assets/sparkbuz/buzz_circle.png",
                      ),
                      decoration: BoxDecoration(
                        //color: red,
                        shape: BoxShape.circle,
                        border: Border.all(color: white, width: 1),
                      ),
                    ),
                  ),
                ),
              ),
              btmTabItem(2),
              btmTabItem(3),
            ],
          )),
    );
  }

  btmTabItem(int p) {
    bool active = currentPage == p;
    return Flexible(
      child: GestureDetector(
        onTap: () {
          //pauseController.add(p == 0 ? false : true);
          vp.jumpToPage(p);
        },
        child: Container(
          color: transparent,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Image.asset(
                getTabIcons(p),
                color: pageColorBg.withOpacity(active ? 1 : 0.5),
                height: 18,
                width: 18,
                fit: BoxFit.cover,
              ),
              addSpace(5),
              Text(
                getTabDescription(p).toString().toUpperCase(),
                style: textStyle(
                  active,
                  12,
                  pageColorBg.withOpacity(active ? 1 : 0.5),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Color get pageColor => currentPage == 0 ? black : white;
  Color get pageColorBg => currentPage == 0 ? white : black;

  getTabIcons(int p) {
    if (p == 0) return 'assets/icons/home.png';
    if (p == 1) return 'assets/icons/discover.png';
    if (p == 2) return 'assets/icons/team.png';
    return 'assets/icons/user.png';
  }

  getTabDescription(int p) {
    if (p == 0) return "Home";
    if (p == 1) return "Discover";
    if (p == 2) return "Meet SB";
    //if(p==1)
    return "Profile";
  }
}

class UpdateLayout extends StatelessWidget {
  BuildContext con;
  @override
  Widget build(BuildContext context) {
    String features = appSettingsModel.getString(NEW_FEATURE);
    if (features.isNotEmpty) features = "* $features";
    bool mustUpdate = appSettingsModel.getBoolean(MUST_UPDATE);
    con = context;
    return WillPopScope(
      onWillPop: () {},
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
              child: Container(
                color: black.withOpacity(.6),
              )),
          Container(
            padding: EdgeInsets.all(15),
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      if (isAdmin) {
                        Navigator.pop(con);
                      }
                    },
                    child: Image.asset(
                      ic_plain,
                      width: 60,
                      height: 60,
                      color: white,
                    ),
                  ),
                  addSpace(10),
                  Text(
                    "New Update Available",
                    style: textStyle(true, 22, white),
                    textAlign: TextAlign.center,
                  ),
                  addSpace(10),
                  Text(
                    features.isEmpty
                        ? "Please update your App to proceed"
                        : features,
                    style: textStyle(false, 16, white.withOpacity(.5)),
                    textAlign: TextAlign.center,
                  ),
                  addSpace(15),
                  Container(
                    height: 40,
                    width: 120,
                    child: FlatButton(
                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(25)),
                        color: blue3,
                        onPressed: () {
                          String appLink =
                              appSettingsModel.getString(APP_LINK_IOS);
                          if (Platform.isAndroid)
                            appLink =
                                appSettingsModel.getString(APP_LINK_ANDROID);
                          openLink(appLink);
                        },
                        child: Text(
                          "UPDATE",
                          style: textStyle(true, 14, white),
                        )),
                  ),
                  addSpace(15),
                  if (!mustUpdate)
                    Container(
                      height: 40,
                      child: FlatButton(
                          materialTapTargetSize:
                              MaterialTapTargetSize.shrinkWrap,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25)),
                          color: red0,
                          onPressed: () {
                            Navigator.pop(con);
                          },
                          child: Text(
                            "Later",
                            style: textStyle(true, 14, white),
                          )),
                    )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
