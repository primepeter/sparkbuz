import 'package:flutter_masked_text2/flutter_masked_text2.dart';
import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity/connectivity.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:line_icons/line_icons.dart';
import 'package:masked_controller/mask.dart';
import 'package:masked_controller/masked_controller.dart';
import 'package:sparkbuz/AppConfig.dart';
import 'package:sparkbuz/AppEngine.dart';
import 'package:sparkbuz/app/app.dart';
import 'package:sparkbuz/assets.dart';
import 'package:sparkbuz/auth/AuthOTP.dart';
import 'package:sparkbuz/auth/AuthUsername.dart';

class TextEditingControllerWorkaroud extends TextEditingController {
  TextEditingControllerWorkaroud({String text}) : super(text: text);

  void setTextAndPosition(String newText, {int caretPosition}) {
    int offset = caretPosition != null ? caretPosition : newText.length;
    value = value.copyWith(
        text: newText,
        selection: TextSelection.collapsed(offset: offset),
        composing: TextRange.empty);
  }
}

class AuthSignUp extends StatefulWidget {
  @override
  _AuthSignUpState createState() {
    return _AuthSignUpState();
  }
}

class _AuthSignUpState extends State<AuthSignUp> {
  //final mobileController = MaskedController(mask: Mask(mask: 'NNN-NNN-NNNNN'));
  // final mobileController = new MaskedTextController(mask: '000-000-00000');
  final mobileController =
      TextEditingControllerWorkaroud(text: country.countryCode);
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final cPasswordController = TextEditingController();

  FocusNode focusEmail = new FocusNode();
  FocusNode focusUserName = new FocusNode();
  FocusNode focusPassword = new FocusNode();
  FocusNode cFocusPassword = new FocusNode();
  FocusNode focusNumber = new FocusNode();
  bool passwordVisible = false;

  final vp = PageController();
  int currentPage = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    mobileController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext cc) {
    return Scaffold(
        backgroundColor: white, resizeToAvoidBottomInset: true, body: page());
  }

  bool rememberMe = false;

  page() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.only(top: 40),
          //height: 50,
          child: Stack(
            children: [
              Align(alignment: Alignment.centerLeft, child: BackButton()),
              Align(
                alignment: Alignment.center,
                child: Text(
                  "Sign Up",
                  style: textStyle(
                    true,
                    20,
                    black,
                  ),
                ),
              ),
            ],
          ),
        ),
        AnimatedContainer(
          duration: Duration(milliseconds: 500),
          width: double.infinity,
          height: errorText.isEmpty ? 0 : 40,
          color: red0,
          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: Center(
              child: Text(
            errorText,
            style: textStyle(true, 16, white),
          )),
        ),
        Container(
          height: 45,
          width: double.infinity,
          margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
          child: Card(
            color: default_white,
            elevation: 0,
            clipBehavior: Clip.antiAlias,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                side: BorderSide(color: black.withOpacity(.1), width: .5)),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(1, 1, 1, 1),
              child: Row(
                children: List.generate(1, (p) {
                  String title = "Email";
                  //String title = p == 0 ? "Phone" : "Email";
                  bool selected = p == currentPage;
                  return Flexible(
                    child: GestureDetector(
                      onTap: () {
                        vp.jumpToPage(p);
                      },
                      child: Container(
                          decoration: BoxDecoration(
                              color: selected ? white : transparent,
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(
                                  color: !selected
                                      ? transparent
                                      : black.withOpacity(.1),
                                  width: .5)),
                          child: Center(
                              child: Text(
                            title,
                            style: textStyle(selected, 14,
                                selected ? black : (black.withOpacity(.5))),
                            textAlign: TextAlign.center,
                          ))),
                    ),
                    fit: FlexFit.tight,
                  );
                }),
              ),
            ),
          ),
        ),
        Flexible(
          child: PageView(
            //physics: NeverScrollableScrollPhysics(),
            controller: vp,
            onPageChanged: (p) {
              setState(() {
                currentPage = p;
              });
            },
            children: [
              //authPagePhone(),
              authPageEmail(),
            ],
          ),
        ),
      ],
    );
  }

  authPagePhone() {
    return ListView(
      padding: EdgeInsets.only(top: 0),
      children: <Widget>[
        textInputField(
            controller: mobileController,
            focusNode: focusNumber,
            title: "",
            hint: "Phone Number",
            asset: "null",
            isPhone: true,
            icon: LineIcons.phone,
            onPhoneTap: () {
              pushAndResult(context, CountryChooser(), result: (Countries _) {
                setState(() {
                  country = _;
                  mobileController.text = country.countryCode;
                });
              }, depend: false);
            },
            onSearching: (s) {
              print(s.isEmpty);
              if (s.isNotEmpty) return;
              String code = country.countryCode;
              mobileController.setTextAndPosition(code);
              setState(() {});
            }),
        termsAndConditionView(),
        Container(
          padding: EdgeInsets.all(15),
          child: RawMaterialButton(
            onPressed: () {
              handleSignUp("phone");
            },
            fillColor: AppConfig.appColor,
            padding: EdgeInsets.all(15),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            elevation: 10,
            child: Center(
                child: Text(
              "SEND CODE",
              style: textStyle(true, 18, white),
            )),
          ),
        ),
      ],
    );
  }

  authPageEmail() {
    return ListView(
      padding: EdgeInsets.only(top: 0),
      children: <Widget>[
        textInputField(
            controller: emailController,
            focusNode: focusEmail,
            title: "",
            hint: "johndoe@email.com",
            asset: "null",
            icon: LineIcons.envelope),
        textInputField(
            controller: passwordController,
            focusNode: focusPassword,
            title: "",
            hint: "Password",
            asset: "null",
            icon: LineIcons.lock,
            isPass: true,
            refresh: () => setState(() {})),
        textInputField(
            controller: cPasswordController,
            focusNode: cFocusPassword,
            title: "",
            hint: "Confirm Password",
            asset: "null",
            icon: LineIcons.lock,
            isPass: true,
            refresh: () => setState(() {})),
        termsAndConditionView(),
        Container(
          padding: EdgeInsets.all(15),
          child: RawMaterialButton(
            onPressed: () {
              handleSignUp("email");
            },
            fillColor: AppConfig.appColor,
            padding: EdgeInsets.all(15),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            elevation: 10,
            child: Center(
                child: Text(
              "NEXT",
              style: textStyle(true, 18, white),
            )),
          ),
        ),
      ],
    );
  }

  handleSignUp(String type) async {
    var result = await (Connectivity().checkConnectivity());
    if (result == ConnectivityResult.none) {
      showError("No internet connectivity");
      return;
    }

    String email = emailController.text.toLowerCase().trim();
    String password = passwordController.text;
    String cPassword = cPasswordController.text;

    String number = mobileController.text;
    number = number.replaceAll(" ", "");

    // number = number.replaceAll("-", "");
    // if (number.startsWith("0")) number = number.substring(1);
    // number = (country.countryCode + number);

    if (type == "email" && email.isEmpty) {
      FocusScope.of(context).requestFocus(focusEmail);
      showError("Email or Username is required!");
      return;
    }

    if (type == "email" && password.isEmpty) {
      FocusScope.of(context).requestFocus(focusPassword);
      showError("Password is required!");
      return;
    }

    if (type == "email" && cPassword.isEmpty) {
      FocusScope.of(context).requestFocus(cFocusPassword);
      showError("Confirm Password is required!");
      return;
    }

    if (type == "email" && password != cPassword) {
      FocusScope.of(context).requestFocus(focusPassword);
      showError("Password's dont match!");
      return;
    }

    if (type == "phone" && number.isEmpty) {
      FocusScope.of(context).requestFocus(focusNumber);
      showError("Phone number is required!");
      return;
    }

    if (type == "phone") {
      pushAndResult(context, AuthOtp(type: 0, phoneNumber: number),
          depend: false);
    } else {
      showProgress(true, context, msg: "Creating Account...");

      var doc = await FirebaseFirestore.instance
          .collection(USER_BASE)
          .where(EMAIL, isEqualTo: email)
          .limit(1)
          .get();

      print(doc.size);

      if (null != doc && doc.size > 0) {
        print(doc.docs[0].data());

        showError("Email address already exists!", wasLoading: true);
        return;
      }

      //showProgress(true, context, msg: "Creating Account...");
      FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: email, password: password)
          .then((value) {
        final account = value.user;
        userModel
          ..put(EMAIL, email)
          ..put(PASSWORD, password)
          ..put(PHONE_NUMBER, password)
          ..saveItem(USER_BASE, false, document: account.uid, onComplete: () {
            pushAndResult(context, AuthUsername(), depend: false);
          });
      }).catchError((e) {
        showError(e.toString(), wasLoading: true);
      });
    }
  }

  String errorText = "";
  showError(String text, {bool wasLoading = false}) {
    if (wasLoading) showProgress(false, context);
    errorText = text;
    setState(() {});
    Future.delayed(Duration(seconds: 3), () {
      errorText = "";
      setState(() {});
    });
  }
}
