import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:sparkbuz/AppEngine.dart';
import 'package:sparkbuz/AppConfig.dart';
import 'package:sparkbuz/assets.dart';

import 'AuthSignUp.dart';

class AuthPassword extends StatefulWidget {
  @override
  _AuthPasswordState createState() {
    return _AuthPasswordState();
  }
}

class _AuthPasswordState extends State<AuthPassword> {
  final passwordController = TextEditingController();
  final cPasswordController = TextEditingController();
  FocusNode focusPassword = new FocusNode();
  FocusNode cFocusPassword = new FocusNode();
  bool passwordVisible = false;
  bool isSignUp = false;

  final vp = PageController();
  int currentPage = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext cc) {
    return Scaffold(
        backgroundColor: white, resizeToAvoidBottomInset: true, body: page());
  }

  bool rememberMe = false;

  page() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.only(top: 40),
          //height: 50,
          child: Stack(
            children: [
              Align(alignment: Alignment.centerLeft, child: BackButton()),
              Align(
                alignment: Alignment.center,
                child: Text(
                  "Sign Up",
                  style: textStyle(
                    true,
                    20,
                    black,
                  ),
                ),
              ),
            ],
          ),
        ),
        AnimatedContainer(
          duration: Duration(milliseconds: 500),
          width: double.infinity,
          height: errorText.isEmpty ? 0 : 40,
          color: red0,
          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: Center(
              child: Text(
            errorText,
            style: textStyle(true, 16, white),
          )),
        ),
        Container(
          padding: EdgeInsets.only(right: 15, left: 15),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              addSpace(15),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Create password",
                  style: textStyle(
                    true,
                    25,
                    black,
                  ),
                ),
              ),
              addSpace(5),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Use a password you can remember and ensure it's not less than 6 charaters in length",
                  style: textStyle(
                    false,
                    14,
                    black.withOpacity(.6),
                  ),
                ),
              ),
            ],
          ),
        ),
        Flexible(
          child: ListView(
            padding: EdgeInsets.only(top: 0),
            children: <Widget>[
              textInputField(
                  controller: passwordController,
                  focusNode: focusPassword,
                  title: "Password",
                  hint: "*********",
                  asset: "null",
                  isPass: true,
                  icon: Icons.lock,
                  refresh: () => setState(() {})),
              textInputField(
                  controller: cPasswordController,
                  focusNode: cFocusPassword,
                  title: "Confirm Password",
                  hint: "*********",
                  asset: "null",
                  isPass: true,
                  icon: Icons.lock,
                  refresh: () => setState(() {})),
              Container(
                padding: EdgeInsets.all(15),
                child: RawMaterialButton(
                  onPressed: () {
                    handleSignUp();
                  },
                  fillColor: AppConfig.appColor,
                  padding: EdgeInsets.all(15),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  elevation: 10,
                  child: Center(
                      child: Text(
                    "NEXT",
                    style: textStyle(true, 18, white),
                  )),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  textInputField({
    @required TextEditingController controller,
    @required String title,
    @required String hint,
    @required String asset,
    bool isPass = false,
    UniqueStatus unique,
    FocusNode focusNode,
    IconData icon,
    void Function() refresh,
    void Function(String) onSearching,
  }) {
    return Container(
      padding: EdgeInsets.only(left: 15, right: 15),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          addSpace(5),
          Text(
            title,
            style: textStyle(false, 14, AppConfig.appColor),
          ),
          addSpace(4),
          Container(
            padding: EdgeInsets.only(left: 15, right: 15),
            decoration: BoxDecoration(
                color: white,
                border: Border.all(
                  color: black.withOpacity(.2),
                ),
                borderRadius: BorderRadius.circular(10)),
            child: Row(
              children: [
                Icon(
                  icon,
                  color: AppConfig.appColor,
                ),
                Container(
                  height: 20,
                  width: 1,
                  color: black.withOpacity(.1),
                  margin: EdgeInsets.only(left: 5, right: 15),
                ),
                Flexible(
                  child: TextField(
                    controller: controller,
                    obscureText: isPass && !passwordVisible,
                    onChanged: unique == null ? null : onSearching,
                    decoration: InputDecoration(
                        suffix: !isPass
                            ? null
                            : GestureDetector(
                                onTap: () {
                                  passwordVisible = !passwordVisible;
                                  if (refresh != null) refresh();
                                },
                                child: Text(
                                  passwordVisible ? "HIDE" : "SHOW",
                                  style: textStyle(
                                      false, 12, black.withOpacity(.5)),
                                )),
                        hintText: hint,
                        border: InputBorder.none),
                  ),
                ),
                if (null != unique)
                  Container(
                    height: 20,
                    width: 20,
                    alignment: Alignment.center,
                    child: uniqueBuilder(unique),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: unique == UniqueStatus.none
                            ? transparent
                            : unique == UniqueStatus.searching
                                ? transparent
                                : unique == UniqueStatus.available
                                    ? green
                                    : red),
                  )
              ],
            ),
          ),
        ],
      ),
    );
  }

  uniqueBuilder(UniqueStatus status) {
    switch (status) {
      case UniqueStatus.searching:
        // TODO: Handle this case.
        return CircularProgressIndicator(
          strokeWidth: 2,
          //valueColor: AlwaysStoppedAnimation(AppConfig.appColor),
        );
        break;
      case UniqueStatus.available:
        // TODO: Handle this case.
        return Icon(
          Icons.check,
          color: white,
          size: 15,
        );
        break;
      case UniqueStatus.failed:
        // TODO: Handle this case.
        return Icon(
          Icons.error_outline,
          color: white,
          size: 15,
        );
        break;
      case UniqueStatus.none:
        // TODO: Handle this case.
        return Container();
        break;
    }
  }

  selectorField({
    @required String value,
    @required String title,
    @required String hint,
    @required String asset,
    IconData icon,
    void Function() onTap,
  }) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.only(left: 15, right: 15),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            addSpace(5),
            Text(
              title,
              style: textStyle(false, 14, AppConfig.appColor),
            ),
            addSpace(4),
            Container(
              padding: EdgeInsets.all(15),
              decoration: BoxDecoration(
                  color: white,
                  border: Border.all(
                    color: black.withOpacity(.2),
                  ),
                  borderRadius: BorderRadius.circular(10)),
              child: Row(
                children: [
                  if (null == icon)
                    Image.asset(
                      asset,
                      height: 20,
                      width: 20,
                      fit: BoxFit.cover,
                      color: AppConfig.appColor,
                    )
                  else
                    Icon(
                      icon,
                      color: AppConfig.appColor,
                    ),
                  Container(
                    height: 20,
                    width: 1,
                    color: black.withOpacity(.1),
                    margin: EdgeInsets.only(left: 5, right: 15),
                  ),
                  Text(
                    value ?? hint,
                    style: textStyle(
                        false, 14, black.withOpacity(value == null ? 0.5 : 1)),
                  ),
                  Spacer(),
                  Icon(
                    Icons.arrow_drop_down_circle,
                    color: black.withOpacity(.5),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  handleSignUp() async {
    var result = await (Connectivity().checkConnectivity());
    if (result == ConnectivityResult.none) {
      showError("No internet connectivity");
      return;
    }
    //userModel.put(BIRTH_DATE, birthDate);
    pushAndResult(context, AuthSignUp(), depend: false);
  }

  String errorText = "";
  showError(String text, {bool wasLoading = false}) {
    if (wasLoading) showProgress(false, context);
    errorText = text;
    setState(() {});
    Future.delayed(Duration(seconds: 3), () {
      errorText = "";
      setState(() {});
    });
  }
}
