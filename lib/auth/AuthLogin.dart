import 'package:flutter_masked_text2/flutter_masked_text2.dart';

import 'dart:async';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity/connectivity.dart';
import 'package:device_info/device_info.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:line_icons/line_icons.dart';
import 'package:masked_controller/mask.dart';
import 'package:masked_controller/masked_controller.dart';
import 'package:sparkbuz/AppConfig.dart';
import 'package:sparkbuz/AppEngine.dart';
import 'package:sparkbuz/app/app.dart';
import 'package:sparkbuz/app/navigation.dart';
import 'package:sparkbuz/assets.dart';
import 'package:sparkbuz/auth/AuthOTP.dart';
import 'package:sparkbuz/basemodel.dart';

import '../MainAdmin.dart';
import 'AuthSignUp.dart';

class AuthLogin extends StatefulWidget {
  @override
  _AuthLoginState createState() {
    return _AuthLoginState();
  }
}

class _AuthLoginState extends State<AuthLogin> {
  // final mobileController = MaskedController(mask: Mask(mask: 'NNN-NNN-NNNNN'));
  // final mobileController = new MaskedTextController(mask: '000-000-00000');
  final mobileController =
      TextEditingControllerWorkaroud(text: country.countryCode);
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  FocusNode focusEmail = new FocusNode();
  FocusNode focusPassword = new FocusNode();
  FocusNode focusNumber = new FocusNode();

  final vp = PageController();
  int currentPage = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext cc) {
    return Scaffold(
        backgroundColor: white, resizeToAvoidBottomInset: true, body: page());
  }

  page() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.only(top: 40),
          //height: 50,
          child: Stack(
            children: [
              Align(alignment: Alignment.centerLeft, child: BackButton()),
              Align(
                alignment: Alignment.center,
                child: Text(
                  "Log In",
                  style: textStyle(
                    true,
                    20,
                    black,
                  ),
                ),
              ),
            ],
          ),
        ),
        AnimatedContainer(
          duration: Duration(milliseconds: 500),
          width: double.infinity,
          height: errorText.isEmpty ? 0 : 40,
          color: red0,
          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: Center(
              child: Text(
            errorText,
            style: textStyle(true, 16, white),
          )),
        ),
        Container(
          height: 45,
          width: double.infinity,
          margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
          child: Card(
            color: default_white,
            elevation: 0,
            clipBehavior: Clip.antiAlias,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                side: BorderSide(color: black.withOpacity(.1), width: .5)),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(1, 1, 1, 1),
              child: Row(
                children: List.generate(1, (p) {
                  String title = "Email";
                  // String title = p == 0 ? "Phone" : "Email";
                  bool selected = p == currentPage;
                  return Flexible(
                    child: GestureDetector(
                      onTap: () {
                        vp.jumpToPage(p);
                      },
                      child: Container(
                          decoration: BoxDecoration(
                              color: selected ? white : transparent,
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(
                                  color: !selected
                                      ? transparent
                                      : black.withOpacity(.1),
                                  width: .5)),
                          child: Center(
                              child: Text(
                            title,
                            style: textStyle(selected, 14,
                                selected ? black : (black.withOpacity(.5))),
                            textAlign: TextAlign.center,
                          ))),
                    ),
                    fit: FlexFit.tight,
                  );
                }),
              ),
            ),
          ),
        ),
        Flexible(
          child: PageView(
            //physics: NeverScrollableScrollPhysics(),
            controller: vp,
            onPageChanged: (p) {
              setState(() {
                currentPage = p;
              });
            },
            children: [
              //authPagePhone(),
              authPageEmail(),
            ],
          ),
        ),
      ],
    );
  }

  authPagePhone() {
    return ListView(
      padding: EdgeInsets.only(top: 0),
      children: <Widget>[
        textInputField(
            controller: mobileController,
            focusNode: focusNumber,
            title: "",
            hint: "Phone Number",
            asset: "null",
            isPhone: true,
            icon: LineIcons.phone,
            onPhoneTap: () {
              pushAndResult(context, CountryChooser(), result: (Countries _) {
                setState(() {
                  country = _;
                  mobileController.text = country.countryCode;
                });
              }, depend: false);
            },
            onSearching: (s) {
              print(s.isEmpty);
              if (s.isNotEmpty) return;
              String code = country.countryCode;
              mobileController.setTextAndPosition(code);
              setState(() {});
            }),
        Container(
          padding: EdgeInsets.all(15),
          child: RawMaterialButton(
            onPressed: () {
              handleSignUp("phone");
            },
            fillColor: AppConfig.appColor,
            padding: EdgeInsets.all(15),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            elevation: 10,
            child: Center(
                child: Text(
              "SEND CODE",
              style: textStyle(true, 18, white),
            )),
          ),
        ),
      ],
    );
  }

  authPageEmail() {
    return ListView(
      padding: EdgeInsets.only(top: 0),
      children: <Widget>[
        textInputField(
            controller: emailController,
            focusNode: focusEmail,
            title: "",
            hint: "Email or Username",
            asset: "null",
            icon: LineIcons.envelope),
        textInputField(
            controller: passwordController,
            focusNode: focusPassword,
            title: "",
            hint: "Password",
            asset: "null",
            icon: LineIcons.lock,
            isPass: true,
            refresh: () => setState(() {})),
        Container(
          padding: EdgeInsets.all(15),
          child: RawMaterialButton(
            onPressed: () {
              handleSignUp("email");
            },
            fillColor: AppConfig.appColor,
            padding: EdgeInsets.all(15),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            elevation: 10,
            child: Center(
                child: Text(
              "LOGIN",
              style: textStyle(true, 18, white),
            )),
          ),
        ),
      ],
    );
  }

  handleSignUp(String type) async {
    var result = await (Connectivity().checkConnectivity());
    if (result == ConnectivityResult.none) {
      showError("No internet connectivity");
      return;
    }

    String emailOrUsername = emailController.text.toLowerCase().trim();
    String password = passwordController.text;
    String number = mobileController.text;
    number = number.replaceAll(" ", "");
    // if (number.startsWith("0")) number = number.substring(1);
    // number = (country.countryCode + number);

    if (type == "email" && emailOrUsername.isEmpty) {
      FocusScope.of(context).requestFocus(focusEmail);
      showError("Email or Username is required!");
      return;
    }

    if (type == "email" && password.isEmpty) {
      FocusScope.of(context).requestFocus(focusPassword);
      showError("Password is required!");
      return;
    }

    if (type == "phone" && number.isEmpty) {
      showError("Phone number is required!");
      return;
    }

    if (type == "email") {
      showProgress(true, context, msg: "Loggin In");
      loginIntoApp(emailOrUsername, password);
    }

    if (type == "phone") {
      pushAndResult(context, AuthOtp(type: 1, phoneNumber: number),
          depend: false);
    }
  }

  loginIntoApp(String email, String password) async {
    DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
    String deviceId;
    if (Platform.isIOS) {
      final deviceInfo = await deviceInfoPlugin.iosInfo;
      deviceId = deviceInfo.identifierForVendor;
    } else {
      final deviceInfo = await deviceInfoPlugin.androidInfo;
      deviceId = deviceInfo.androidId;
    }

    FirebaseAuth.instance
        .signInWithEmailAndPassword(email: email, password: password)
        .then((value) {
      final account = value.user;
      FirebaseFirestore.instance
          .collection(USER_BASE)
          .doc(account.uid)
          .get()
          .then((doc) {
        if (!doc.exists) {
          userModel
            ..put(USER_ID, account.uid)
            ..put(EMAIL, account.email)
            ..put(USER_IMAGE, account.photoURL)
            ..put(NAME, account.displayName)
            ..putInList(DEVICE_ID, deviceId, true)
            ..saveItem(USER_BASE, false, document: account.uid);
        }
        userModel = BaseModel(doc: doc);
        popUpUntil(context, MainAdmin());
      }).catchError((e) {
        showError(e.toString(), wasLoading: true);
      });
    }).catchError((e) {
      if (e.toString().contains("email address is badly formatted")) {
        FirebaseFirestore.instance
            .collection(USER_BASE)
            .where(USERNAME, isEqualTo: email)
            .limit(1)
            .get()
            .then((value) {
          if (value.docs.isEmpty) {
            showError("No record found matching the login credentials!",
                wasLoading: true);
            return;
          }
          BaseModel model = BaseModel(doc: value.docs[0]);
          loginIntoApp(model.getEmail(), password);
        });
        return;
      }
      print(e);
      showError(e.toString().replaceAll('firebase_auth', ''), wasLoading: true);
    });
  }

  String errorText = "";
  showError(String text, {bool wasLoading = false}) {
    if (wasLoading) {
      Future.delayed(Duration(milliseconds: 200), () {
        showProgress(false, context);
      });
    }
    errorText = text;
    setState(() {});
    Future.delayed(Duration(seconds: 4), () {
      errorText = "";
      setState(() {});
    });
  }
}
