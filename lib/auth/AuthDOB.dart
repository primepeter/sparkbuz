import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:sparkbuz/AppEngine.dart';
import 'package:sparkbuz/AppConfig.dart';
import 'package:sparkbuz/assets.dart';
import 'package:sparkbuz/auth/AuthUsername.dart';
import 'package:sparkbuz/date_picker/flutter_datetime_picker.dart';

import 'AuthSignUp.dart';

class AuthDOB extends StatefulWidget {
  final bool toUsername;

  const AuthDOB({Key key, this.toUsername = false}) : super(key: key);
  @override
  _AuthDOBState createState() {
    return _AuthDOBState();
  }
}

class _AuthDOBState extends State<AuthDOB> {
  String birthDate;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext cc) {
    return Scaffold(
        backgroundColor: white, resizeToAvoidBottomInset: true, body: page());
  }

  bool rememberMe = false;

  page() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.only(top: 40),
          //height: 50,
          child: Stack(
            children: [
              Align(alignment: Alignment.centerLeft, child: BackButton()),
              Align(
                alignment: Alignment.center,
                child: Text(
                  "Sign Up",
                  style: textStyle(
                    true,
                    20,
                    black,
                  ),
                ),
              ),
            ],
          ),
        ),
        AnimatedContainer(
          duration: Duration(milliseconds: 500),
          width: double.infinity,
          height: errorText.isEmpty ? 0 : 40,
          color: red0,
          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: Center(
              child: Text(
            errorText,
            style: textStyle(true, 16, white),
          )),
        ),
        Container(
          padding: EdgeInsets.only(right: 15, left: 15),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              addSpace(15),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  "When's your birthday?",
                  style: textStyle(
                    true,
                    25,
                    black,
                  ),
                ),
              ),
              addSpace(5),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Providing your date of birth allows us to make sure that you are old enouh to use SparkBux.",
                  style: textStyle(
                    false,
                    14,
                    black.withOpacity(.6),
                  ),
                ),
              ),
            ],
          ),
        ),
        Flexible(
          child: ListView(
            //mainAxisSize: MainAxisSize.min,
            //mainAxisAlignment: MainAxisAlignment.center,
            padding: EdgeInsets.only(top: 0),
            children: <Widget>[
              selectorField(
                  value: birthDate,
                  title: "",
                  hint: "Date of Birth",
                  icon: Icons.event,
                  asset: "assets/icons/gender.png",
                  onTap: () {
                    bool empty = null == birthDate || birthDate.isEmpty;
                    int year;
                    int month;
                    int day;
                    if (!empty) {
                      var birthDay = birthDate.split("-");
                      year = num.parse(birthDay[0]);
                      month = num.parse(birthDay[1]);
                      day = num.parse(birthDay[2]);
                    }
                    DatePicker.showDatePicker(
                      context,
                      showTitleActions: true,
                      minTime: DateTime(1930, 12, 31),
                      maxTime: DateTime(2040, 12, 31),
                      onChanged: (date) {},
                      onConfirm: (date) {
                        setState(() {
                          int year = date.year;
                          int month = date.month;
                          int day = date.day;
                          birthDate =
                              "$year-${formatDOB(month)}-${formatDOB(day)}";
                        });
                      },
                      currentTime: empty ? null : DateTime(year, month, day),
                    );
                  }),
              Container(
                padding: EdgeInsets.all(15),
                child: RawMaterialButton(
                  onPressed: () {
                    handleSignUp();
                  },
                  fillColor: AppConfig.appColor,
                  padding: EdgeInsets.all(15),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  elevation: 10,
                  child: Center(
                      child: Text(
                    "NEXT",
                    style: textStyle(true, 18, white),
                  )),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  handleSignUp() async {
    var result = await (Connectivity().checkConnectivity());
    if (result == ConnectivityResult.none) {
      showError("No internet connectivity");
      return;
    }

    int minAge = 18;
    int maxAge = 80;

    if (birthDate == null) {
      showError("What's your date of birth?");
      return;
    }

    int age = getAge(DateTime.parse(birthDate));

    if (minAge > age) {
      showError("Sorry, You must be up to 18 years");
      return;
    }

    if (maxAge < age) {
      showError("Sorry, You can't be above 80 years");
      return;
    }

    userModel.put(BIRTH_DATE, birthDate);
    pushAndResult(context, widget.toUsername ? AuthUsername() : AuthSignUp(),
        depend: false);
  }

  String errorText = "";
  showError(String text, {bool wasLoading = false}) {
    if (wasLoading) showProgress(false, context);
    errorText = text;
    setState(() {});
    Future.delayed(Duration(seconds: 3), () {
      errorText = "";
      setState(() {});
    });
  }
}
