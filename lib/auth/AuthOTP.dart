import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:nexmo_verify/nexmo_sms_verify.dart';
import 'package:otp/otp.dart';
import 'package:sparkbuz/AppConfig.dart';
import 'package:sparkbuz/AppEngine.dart';
import 'package:sparkbuz/VonageApi.dart';
import 'package:sparkbuz/app/app.dart';
import 'package:sparkbuz/app/navigation.dart';
import 'package:sparkbuz/assets.dart';
import 'package:sparkbuz/basemodel.dart';
import 'package:sparkbuz/pin_codes/pin_code_fields.dart';
import 'package:sparkbuz/pin_codes/pin_theme.dart';
// import 'package:twilio_flutter/twilio_flutter.dart';

import '../MainAdmin.dart';
import '../assets.dart';
import 'AuthUsername.dart';

const String TWILIO_SSID = 'ACfd192b68b73bdbeb942d15d78787ea16';
const String TWILIO_TOKEN = '8a4c61213db6d9fd1471e57533dcdcbe';

// TwilioFlutter twilioFlutter = TwilioFlutter(
//     accountSid: "ACfd192b68b73bdbeb942d15d78787ea16",
//     authToken: "8a4c61213db6d9fd1471e57533dcdcbe",
//     twilioNumber: '+12057406266' //
//     );

String generateOTP() {
  return OTP.generateTOTPCodeString(
      "JBSWY3DPEHPK3PXP", DateTime.now().millisecondsSinceEpoch);
}

class AuthOtp extends StatefulWidget {
  final String phoneNumber;
  final int type;

  const AuthOtp({Key key, @required this.type, @required this.phoneNumber})
      : super(key: key);
  @override
  _AuthOtpState createState() => _AuthOtpState();
}

class _AuthOtpState extends State<AuthOtp> with AutomaticKeepAliveClientMixin {
  int time = 0;
  String timeText = "";
  int forceResendingToken = 0;
  bool verifying = false;
  String verificationId = "";
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final otpController = TextEditingController();
  final errorController = StreamController<ErrorAnimationType>.broadcast();
  String currentOTP = '';

  // NexmoSmsVerificationUtil _nexmoSmsVerificationUtil =
  //     NexmoSmsVerificationUtil();
  String nexmoKey = appSettingsModel.getString(NEXMO_KEY);
  String nexmoSecret = appSettingsModel.getString(NEXMO_SECRET);
  String nexmoBrand = appSettingsModel.getString(NEXMO_BRAND);

  VonageApi vonageApi = VonageApi(
      brandName: appSettingsModel.getString(NEXMO_BRAND),
      apiKey: appSettingsModel.getString(NEXMO_KEY),
      apiSecret: appSettingsModel.getString(NEXMO_SECRET));

  @override
  void initState() {
    super.initState();
    //_nexmoSmsVerificationUtil.initNexmo(nexmoKey, nexmoSecret);
    // _nexmoSmsVerificationUtil.initNexmo("421bfb47", "a28b0736");

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      clickVerify();
    });
  }

//  @override
//  void didChangeDependencies() {
//    // TODO: implement didChangeDependencies
//    super.didChangeDependencies();
//    Future.delayed(Duration(seconds: 1), () {
//      clickVerify();
//    });
//  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(backgroundColor: white, key: scaffoldKey, body: page());
  }

  page() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
            padding: EdgeInsets.only(top: 25, left: 10), child: BackButton()),
        AnimatedContainer(
          duration: Duration(milliseconds: 500),
          width: double.infinity,
          height: errorText.isEmpty ? 0 : 40,
          color: red0,
          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: Center(
              child: Text(
            errorText,
            style: textStyle(true, 16, white),
          )),
        ),
        Flexible(
          child: ListView(
            padding: EdgeInsets.all(25),
            children: [
              Text.rich(TextSpan(children: [
                TextSpan(
                    text: "Please enter the ",
                    style: textStyle(false, 25, black)),
                TextSpan(text: "Code ", style: textStyle(true, 25, black)),
                TextSpan(
                    text: "to Verify ", style: textStyle(false, 25, black)),
                TextSpan(
                    text: "Your Phone Number",
                    style: textStyle(true, 25, black)),
              ])),
              addSpace(15),
              Text(
                "We have sent you an SMS with a code to the number ${widget.phoneNumber}",
                style: textStyle(false, 14, black.withOpacity(.6)),
              ),
              addSpace(10),
              Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  //color: black.withOpacity(.05),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: PinCodeTextField(
                  length: 6,
                  obsecureText: false,
                  animationType: AnimationType.fade,
                  textInputType: TextInputType.number,
                  autoDisposeControllers: false,
                  errorAnimationController: errorController,
                  pinTheme: PinTheme(
                      shape: PinCodeFieldShape.circle,
                      borderRadius: BorderRadius.circular(25),
                      fieldHeight: 40,
                      fieldWidth: 40,
                      inactiveFillColor: black.withOpacity(.3),
                      disabledColor: black.withOpacity(.02),
                      inactiveColor: black.withOpacity(.02),
                      selectedColor: black.withOpacity(.02),
                      activeFillColor: black.withOpacity(.02),
                      activeColor: black.withOpacity(.02),
                      selectedFillColor: black.withOpacity(.5)),
                  animationDuration: Duration(milliseconds: 300),
                  backgroundColor: transparent,
                  enableActiveFill: true,
                  controller: otpController,
                  onCompleted: (v) {},
                  onChanged: (value) {},
                  beforeTextPaste: (text) {
                    return true;
                  },
                ),
              ),
              //addSpace(10),
              Container(
                alignment: Alignment.center,
                child: Opacity(
                  opacity: timeText.isEmpty ? 1 : (.5),
                  child: Container(
                    height: 40,
                    //width: 105,
                    child: FlatButton(
                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(25),
                            side: BorderSide(
                                color: white.withOpacity(.5), width: .5)),
                        color: blue3,
                        onPressed: () {
                          if (timeText.isEmpty) {
                            // showProgress(true, context,
                            //     msg: "Verifying Number");
                            clickVerify();
                          }
                        },
                        child: Column(
                          // crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Text(
                              "Send a new code",
                              style: textStyle(true, 12, white),
                            ),
                            if (timeText.isNotEmpty)
                              Text(
                                timeText,
                                style: textStyle(false, 10, white),
                              ),
                          ],
                        )),
                  ),
                ),
              ),
              addSpace(20),
              FlatButton(
                  onPressed: () {
                    checkCode();
                  },
                  padding: EdgeInsets.all(15),
                  color: AppConfig.appColor,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  child: Center(
                      child: Text("VERIFY", style: textStyle(true, 16, white))))
            ],
          ),
        ),
      ],
    );
  }

  clickVerify() {
    print(widget.phoneNumber);
    showProgress(true, context, msg: "Sending OTP");
    currentOTP = generateOTP();
    setState(() {});
    vonageApi.sendSMS(
        number: widget.phoneNumber,
        text: "Your SparkBuz Verification Code $currentOTP",
        onResponse: (s) {
          showProgress(false, context);
          createTimer(true);
        });

    return;
    FirebaseAuth.instance
        .verifyPhoneNumber(
      phoneNumber: widget.phoneNumber,
      timeout: const Duration(seconds: 60),
      verificationCompleted: (cred) {
        showProgress(false, context);
        if (widget.type == 0)
          signUpIntoAccount(cred: cred);
        else
          loginIntoAccount(cred: cred);
      },
      codeSent: (s, [x]) {
        createTimer(true);
        showProgress(false, context);
        verificationId = s;
        print("verification $s x $x");
        forceResendingToken = x;
        if (mounted) setState(() {});
      },
      codeAutoRetrievalTimeout: (s) {},
      verificationFailed: (e) {
        showProgress(false, context);
        showMessage(
          context,
          Icons.error_outline,
          red,
          "Oops",
          e.toString(),
          delayInMilli: 1000,
        );
      },
    )
        .catchError((e) {
      showProgress(false, context);
      showMessage(
        context,
        Icons.error_outline,
        red,
        "Oops",
        e.toString(),
        delayInMilli: 1000,
      );
    });
  }

  createTimer(bool create) {
    if (create) {
      time = 60;
    }
    if (time < 0) {
      if (mounted)
        setState(() {
          timeText = "";
        });
      return;
    }
    timeText = getTimeText();
    Future.delayed(Duration(seconds: 1), () {
      time--;
      if (mounted) setState(() {});
      createTimer(false);
    });
  }

  getTimeText() {
    if (time <= 0) return "";
    return "${time >= 60 ? "01" : "00"}:${time % 60}";
  }

  checkCode() async {
    String code = otpController.text.replaceAll(" ", "");
    if (code.isEmpty) {
      errorController.add(ErrorAnimationType.shake);
      showError(
        "Enter OTP Code",
      );
      return;
    }

    showProgress(true, context, msg: "Verifying OTP");
    /* _nexmoSmsVerificationUtil.verifyOtp(code).then((value) {
      if (widget.type == 0)
        signUpIntoAccount();
      else
        loginIntoAccount();
    }).catchError((e) {
      showError(e.toString(), wasLoading: true);
    });

    return;*/

    if (code != currentOTP) {
      otpController.clear();
      showError(
        "OTP code entered is Invalid.",
      );
      return;
    }
    showProgress(true, context, msg: "Verifying OTP");

    if (appSettingsModel.getBoolean(USE_FIREBASE_OTP)) {
      AuthCredential cred = PhoneAuthProvider.credential(
          verificationId: verificationId, smsCode: code);
      showProgress(false, context);
      if (widget.type == 0)
        signUpIntoAccount(cred: cred);
      else
        loginIntoAccount(cred: cred);
      return;
    }

    if (widget.type == 0)
      signUpIntoAccount();
    else
      loginIntoAccount();
  }

  loginIntoAccount({AuthCredential cred}) {
    if (null != cred) {
      FirebaseAuth.instance.signInWithCredential(cred).then((value) {
        FirebaseFirestore.instance
            .collection(USER_BASE)
            .doc(value.user.uid)
            .get()
            .then((doc) {
          //TODO catch for error in account
          if (!doc.exists) {
            showProgress(false, context);
            showMessage(
                context,
                Icons.error_outline,
                red,
                "Oops",
                "This phone number isn't registered"
                    "yet.Create account with this number?",
                clickYesText: "Create Account",
                delayInMilli: 1000,
                //cancellable: false,
                clickNoText: "Go Back", onClicked: (_) {
              if (_) {
                showProgress(true, context, msg: "Create Account...");
                signUpIntoAccount();
              }
            });
            return;
          }
          userModel = BaseModel(doc: doc);
          popUpUntil(context, MainAdmin());
        }).catchError((e) {
          showError(e.toString(), wasLoading: true);
        });
      }).catchError((e) {
        showError(e.toString(), wasLoading: true);
      });
      return;
    }

    String authKey = widget.phoneNumber + "@sparkbuz.com";
    FirebaseAuth.instance
        .signInWithEmailAndPassword(email: authKey, password: authKey)
        .then((value) {
      FirebaseFirestore.instance
          .collection(USER_BASE)
          .doc(value.user.uid)
          .get()
          .then((doc) {
        //TODO catch for error in account
        if (!doc.exists) {
          showProgress(false, context);
          showMessage(
              context,
              Icons.error_outline,
              red,
              "Oops",
              "This phone number isn't registered"
                  "yet.Create account with this number?",
              clickYesText: "Create Account",
              delayInMilli: 1000,
              //cancellable: false,
              clickNoText: "Go Back", onClicked: (_) {
            if (_) {
              showProgress(true, context, msg: "Create Account...");
              signUpIntoAccount();
            }
          });
          return;
        }
        userModel = BaseModel(doc: doc);
        popUpUntil(context, MainAdmin());
      }).catchError((e) {
        showError(e.toString(), wasLoading: true);
      });
    }).catchError((e) {
      if (e.message.contains("no user record")) {
        showProgress(false, context);
        showMessage(
            context,
            Icons.error_outline,
            red,
            "Oops",
            "This phone number isn't registered"
                " yet.Do you want to create account with this number?",
            clickYesText: "Create Account",
            delayInMilli: 1000,
            clickNoText: "Go Back",
            //cancellable: false,
            onClicked: (_) {
          if (_) {
            showProgress(true, context, msg: "Create Account...");
            signUpIntoAccount();
          } else
            Navigator.pop(context);
        });
        return;
      }
      showError(e.toString(), wasLoading: true);
    });
  }

  signUpIntoAccount({AuthCredential cred}) {
    if (null != cred) {
      FirebaseAuth.instance.signInWithCredential(cred).then((result) {
        userModel
          ..put(USER_ID, result.user.uid)
          ..put(OBJECT_ID, result.user.uid)
          ..put(COUNTRY, country.countryName)
          ..put(COUNTRY_CODE, country.countryCode)
          ..put(PHONE_NUMBER, widget.phoneNumber)
          ..saveItem(USER_BASE, false, document: result.user.uid);
        pushReplacementAndResult(context, AuthUsername(), depend: false);
      }).catchError((e) {
        showError(e.toString(), wasLoading: true);
      });
      return;
    }

    String authKey = widget.phoneNumber + "@sparkbuz.com";
    FirebaseAuth.instance
        .createUserWithEmailAndPassword(email: authKey, password: authKey)
        .then((result) {
      print(result.user.uid);

      userModel
        ..put(USER_ID, result.user.uid)
        ..put(OBJECT_ID, result.user.uid)
        ..put(COUNTRY, country.countryName)
        ..put(COUNTRY_CODE, country.countryCode)
        ..put(PHONE_NUMBER, widget.phoneNumber)
        ..saveItem(USER_BASE, false, document: result.user.uid);
      pushReplacementAndResult(context, AuthUsername(), depend: false);
    }).catchError((e) {
      showError(e.toString(), wasLoading: true);
    });
  }

  String errorText = "";
  showError(String text, {bool wasLoading = false}) {
    if (wasLoading) showProgress(false, context);
    errorText = text;
    setState(() {});
    Future.delayed(Duration(seconds: 3), () {
      errorText = "";
      setState(() {});
    });
  }

  @override
  bool get wantKeepAlive => true;
}
