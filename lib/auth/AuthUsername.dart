import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:line_icons/line_icons.dart';
import 'package:sparkbuz/AppConfig.dart';
import 'package:sparkbuz/AppEngine.dart';
import 'package:sparkbuz/app/navigation.dart';
import 'package:sparkbuz/assets.dart';

import '../MainAdmin.dart';
import '../NewSparkbuzzers.dart';
import '../basemodel.dart';

class AuthUsername extends StatefulWidget {
  @override
  _AuthUsernameState createState() {
    return _AuthUsernameState();
  }
}

class _AuthUsernameState extends State<AuthUsername> {
  // UniqueStatus unique = UniqueStatus.none;
  // final userNameController = TextEditingController();
  FocusNode focusUserName = new FocusNode();
  bool isSignUp = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    usernameController.addListener(() async {
      String check = usernameController.text;
      //usernameController.text = check.replaceAll(" ", "");

      if (check.isEmpty) {
        usernameState = UsernameState.none;
        if (mounted) setState(() {});
        return;
      }

      if (check.startsWith('@')) {
        showError('Username cannot start with @');
        return;
      }

      if (check.contains(' ')) {
        showError('Username cannot contain space!');
        return;
      }

      if (check.isNotEmpty && check.length > 4) {
        usernameState = UsernameState.checking;
        checkUsernameAvailability(check.toLowerCase());
      }
      if (mounted) setState(() {});
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext cc) {
    return Scaffold(
        backgroundColor: white, resizeToAvoidBottomInset: true, body: page());
  }

  page() {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.only(top: 40),
          //height: 50,
          child: Stack(
            children: [
              Align(alignment: Alignment.centerLeft, child: BackButton()),
              Align(
                alignment: Alignment.center,
                child: Text(
                  "Sign Up",
                  style: textStyle(
                    true,
                    20,
                    black,
                  ),
                ),
              ),
            ],
          ),
        ),
        AnimatedContainer(
          duration: Duration(milliseconds: 500),
          width: double.infinity,
          height: errorText.isEmpty ? 0 : 40,
          color: red0,
          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: Center(
              child: Text(
            errorText,
            style: textStyle(true, 16, white),
          )),
        ),
        Container(
          padding: EdgeInsets.only(right: 15, left: 15),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              addSpace(15),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Create username",
                  style: textStyle(
                    true,
                    25,
                    black,
                  ),
                ),
              ),
              addSpace(5),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  "You can always change this later.",
                  style: textStyle(
                    false,
                    14,
                    black.withOpacity(.6),
                  ),
                ),
              ),
            ],
          ),
        ),
        Flexible(
          child: ListView(
            //mainAxisSize: MainAxisSize.min,
            //mainAxisAlignment: MainAxisAlignment.center,
            padding: EdgeInsets.only(top: 0),
            children: <Widget>[
              textInputField(
                controller: usernameController,
                focusNode: focusUserName,
                title: "",
                hint: "Username",
                asset: "null",
                icon: LineIcons.user,
                //unique: unique,
                /*onSearching: (s) async {
                    unique = UniqueStatus.searching;
                    setState(() {});
                    await checkUniqueness(s);
                  }*/
              ),
              if (usernameState != UsernameState.none)
                Container(
                  padding: EdgeInsets.only(top: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            color: black.withOpacity(.02),
                            border: Border.all(
                                width: 2, color: getAvailabilityColor),
                            borderRadius: BorderRadius.circular(5)),
                        padding: EdgeInsets.all(5),
                        alignment: Alignment.centerLeft,
                        child: Row(
                          children: [
                            Container(
                              width: 25,
                              height: 25,
                              padding: EdgeInsets.all(2),
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: getAvailabilityColor,
                              ),
                              alignment: Alignment.center,
                              child: getAvailabilityIcon,
                            ),
                            addSpaceWidth(10),
                            Expanded(
                              child: Container(
                                //color: red0,
                                child: Text(
                                  getAvailabilityTxt,
                                  style: textStyle(false, 14, black),
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              Container(
                padding: EdgeInsets.all(15),
                child: RawMaterialButton(
                  onPressed: () {
                    handleSignUp();
                  },
                  fillColor: AppConfig.appColor,
                  padding: EdgeInsets.all(15),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  elevation: 10,
                  child: Center(
                      child: Text(
                    "SIGN UP",
                    style: textStyle(true, 18, white),
                  )),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  String objectId = getRandomId();
  BaseModel model = BaseModel();
  UsernameState usernameState = UsernameState.none;
  final usernameController = new TextEditingController();

  checkUsernameAvailability(String name) async {
    print(name);
    var doc = await FirebaseFirestore.instance
        .collection(USER_BASE)
        .where(USERNAME, isEqualTo: name)
        .limit(1)
        .get();

    print(doc.size);

    if (null == doc || doc.size == 0) {
      usernameState = UsernameState.available;
    } else {
      usernameState = UsernameState.notAvailable;
      // model = BaseModel(doc: doc.docs[0]);
      // objectId = model.getObjectId();
    }

    if (mounted) setState(() {});
  }

  get getAvailabilityTxt {
    switch (usernameState) {
      case UsernameState.none:
        // TODO: Handle this case.
        return '';
        break;
      case UsernameState.checking:
        // TODO: Handle this case.
        return 'Checking username availability';
        break;
      case UsernameState.available:
        // TODO: Handle this case.
        return 'Username is available';
        break;
      case UsernameState.notAvailable:
        // TODO: Handle this case.
        return 'Username is not available';
        break;
      case UsernameState.error:
        // TODO: Handle this case.
        return 'Username contains space!';
        break;
    }
  }

  get getAvailabilityColor {
    switch (usernameState) {
      case UsernameState.none:
        // TODO: Handle this case.
        return transparent;
        break;
      case UsernameState.checking:
        // TODO: Handle this case.
        return transparent;
        break;
      case UsernameState.available:
        // TODO: Handle this case.
        return dark_green0;
        break;
      case UsernameState.notAvailable:
        // TODO: Handle this case.
        return red0;
        break;
      case UsernameState.error:
        // TODO: Handle this case.
        return red0;
        break;
    }
  }

  get getAvailabilityIcon {
    switch (usernameState) {
      case UsernameState.none:
        // TODO: Handle this case.
        return null;
        break;
      case UsernameState.checking:
        // TODO: Handle this case.
        return CircularProgressIndicator(
          //valueColor: AlwaysStoppedAnimation(white),
          strokeWidth: 2,
        );
        break;
      case UsernameState.available:
        // TODO: Handle this case.
        return Icon(Icons.check, color: white, size: 20);
        break;
      case UsernameState.notAvailable:
        // TODO: Handle this case.
        return Icon(Icons.close, color: white, size: 20);
        break;
      case UsernameState.error:
        // TODO: Handle this case.
        return Icon(Icons.close, color: white, size: 20);
        break;
    }
  }

  handleSignUp() async {
    var result = await (Connectivity().checkConnectivity());
    if (result == ConnectivityResult.none) {
      showError("No internet connectivity");
      return;
    }

    String username = usernameController.text.trim().toLowerCase();
    username = username.replaceAll(" ", "");
    if (username.isEmpty) {
      showError("Enter your username!");
      return;
    }

    if (usernameState != UsernameState.available) {
      showError(getAvailabilityTxt);
      return;
    }

    final search = getSearchString(username);
    userModel
      ..put(USERNAME, username)
      ..put(SEARCH, search)
      ..put(SIGNUP_COMPLETED, true)
      ..updateItems();
    showMessage(context, Icons.check, green, "Successful",
        "Your account has been successfully created!",
        cancellable: false, clickYesText: "Continue", onClicked: (_) {
      popUpUntil(context, MainAdmin());
    });
  }

  String errorText = "";
  showError(String text, {bool wasLoading = false}) {
    if (wasLoading) showProgress(false, context);
    errorText = text;
    setState(() {});
    Future.delayed(Duration(seconds: 3), () {
      errorText = "";
      setState(() {});
    });
  }
}
