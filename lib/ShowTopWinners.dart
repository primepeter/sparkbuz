import 'package:timeago/timeago.dart' as timeAgo;

import 'dart:io';
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'AppEngine.dart';
import 'MainAdmin.dart';
import 'assets.dart';
import 'basemodel.dart';
import 'main_pages/ShowProfile.dart';

class ShowTopWinners extends StatefulWidget {
  @override
  _ShowTopWinnersState createState() => _ShowTopWinnersState();
}

class _ShowTopWinnersState extends State<ShowTopWinners>
    with TickerProviderStateMixin {
  // List<BaseModel> people = List();
  // List ids;
  String title;
  String emptyText;
  bool setup = false;
  RefreshController refreshController = RefreshController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setup = winnersList.isNotEmpty;
    // Future.delayed(Duration(seconds: 1), () {
    //   loadPeople();
    // });
  }

  List loadedIds = [];

  // loadPeople() async {
  //   int loadMax = 20;
  //   int loadCount = 0;
  //   for (String id in ids) {
  //     if (loadedIds.contains(id)) continue;
  //     loadedIds.add(id);
  //     if (isBlocked(null, userId: id)) continue;
  //     if (appSettingsModel.getList(DISABLED).contains(id)) continue;
  //     if (appSettingsModel.getList(BANNED).contains(id)) continue;
  //
  //     DocumentSnapshot doc =
  //         await FirebaseFirestore.instance.collection(USER_BASE).doc(id).get();
  //
  //     print(doc.data());
  //     print(ids);
  //     print(widget.ids);
  //
  //     if (doc == null) continue;
  //     if (!doc.exists) continue;
  //     BaseModel user = BaseModel(doc: doc);
  //     people.add(user);
  //     loadCount++;
  //     if (loadCount == (loadMax / 2)) {
  //       setup = true;
  //       setState(() {});
  //     }
  //     if (loadCount >= loadMax) break;
  //   }
  //
  //   setup = true;
  //   try {
  //     refreshController.loadComplete();
  //   } catch (e) {}
  //   ;
  //   if (mounted) setState(() {});
  // }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.pop(context, "");
        return false;
      },
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: transparent,
        body: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                child: Container(
                  color: black.withOpacity(.8),
                )),
            page()
          ],
        ),
      ),
    );
  }

  BuildContext con;

  Builder page() {
    return Builder(builder: (context) {
      this.con = context;
      return new Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          addSpace(30),
          new Container(
            width: double.infinity,
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                InkWell(
                    onTap: () {
                      Navigator.of(context).pop("");
                    },
                    child: Container(
                      width: 50,
                      height: 50,
                      child: Center(
                          child: Icon(
                        Icons.keyboard_backspace,
                        color: white,
                        size: 25,
                      )),
                    )),
                Flexible(
                  fit: FlexFit.tight,
                  flex: 1,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text(
                        "Top Winners",
                        style: textStyle(true, 22, white),
                      ),
                    ],
                  ),
                ),
                addSpaceWidth(15),
              ],
            ),
          ),
//          addLine(1, black.withOpacity(.1), 0, 0, 0, 0),
          new Expanded(
            flex: 1,
            child: !setup
                ? loadingLayout(trans: true)
                : setup && winnersList.isEmpty
                    ? emptyLayout(Icons.person, "No Top Winners Yet", "",
                        trans: true)
                    : Container(
//                        color: white,
                        child: SmartRefresher(
                        controller: refreshController,
                        enablePullDown: false,
                        enablePullUp: true,
                        header: Platform.isIOS
                            ? WaterDropHeader()
                            : WaterDropMaterialHeader(),
                        footer: ClassicFooter(
                          idleText: "",
                          idleIcon:
                              Icon(Icons.arrow_drop_down, color: transparent),
                        ),
                        onLoading: () {
                          // loadPeople();
                        },
                        onOffsetChange: (_, d) {},
                        child: new ListView.builder(
                          shrinkWrap: true,
                          padding: EdgeInsets.all(0),
                          itemBuilder: (c, p) {
                            return peopleItem(p);
                          },
                          itemCount: winnersList.length,
                        ),
                      )),
          ),
        ],
      );
    });
  }

  peopleItem(int p) {
    var user = winnersList[p];
    int now = DateTime.now().millisecondsSinceEpoch;
    int lastUpdated = user.getInt(TIME_UPDATED);
    bool notOnline =
        ((now - lastUpdated) > (Duration.millisecondsPerMinute * 10));
    bool isOnline = user.getBoolean(IS_ONLINE) && (!notOnline);
    int gender = user.getInt(GENDER);
    String username = user.getUserName();
    if (username.isEmpty) username = user.getString(NAME);
    //bool following = user.followers.contains(userModel.getUserId());

    List followersIds = user.getList(FOLLOWERS);
    bool following = followersIds.contains(userModel.getUserId());

    String likesCount = user.likesCount.length.toString();
    int age = 0;

    if (user.birthDate.isNotEmpty) {
      age = getAge(DateTime.parse(user.birthDate));
    }

    bool dontChat = false;
    /*!user.getList(LOVE_IDS).contains(userModel.getObjectId()) &&
      !userModel.getList(PAID_CHATS).contains(user.getObjectId());*/
    return GestureDetector(
      onTap: () {
        pushAndResult(
            context,
            ShowProfile(
              model: user,
              showCloseBtn: true,
              profileId: user.getUserId(),
            ));
      },
      child: Container(
        color: transparent,
        padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new Stack(
                  //fit: StackFit.expand,
                  children: <Widget>[
                    new Card(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          side: BorderSide(color: blue09, width: 1)),
                      clipBehavior: Clip.antiAlias,
                      color: white,
                      elevation: .5,
                      child: Stack(
                        children: <Widget>[
                          Container(
                            width: 70,
                            height: 100,
                            color: black.withOpacity(.05),
                            child: Icon(LineIcons.user),
                          ),
                          CachedNetworkImage(
                            width: 70,
                            height: 100,
                            imageUrl: user.getString(USER_IMAGE),
                            fit: BoxFit.cover,
                            placeholder: (c, s) {
                              return Container(
                                width: 70,
                                height: 100,
                                color: black.withOpacity(.05),
                                child: Icon(LineIcons.user),
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                    !isOnline
                        ? Container()
                        : Container(
                            width: 10,
                            height: 10,
                            margin: EdgeInsets.fromLTRB(5, 5, 0, 0),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(color: white, width: 2),
                              color: red0,
                            ),
                          ),
                  ],
                ),
                addSpaceWidth(10),
                Flexible(
                  flex: 1,
                  fit: FlexFit.tight,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        username,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: textStyle(true, 18, white),
                      ),
                      Text(
                        "Age: " + age.toString(),
                        style: textStyle(false, 12, white.withOpacity(.8)),
                      ),
                      Text(
                        isOnline
                            ? "Online now"
                            : "Last seen ${timeAgo.format(DateTime.fromMillisecondsSinceEpoch(user.getInt(TIME_UPDATED)), locale: "en_short")}",
                        style: textStyle(
                          false,
                          12,
                          white.withOpacity(.5),
                        ),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                ),
                addSpaceWidth(10),
                FlatButton(
                  onPressed: () {
                    onFollow(p, following);
                    return;

                    user
                      ..putInList(FOLLOWERS, userModel.getUserId(), !following)
                      ..updateItems();
                    userModel
                      ..putInList(FOLLOWING, user.getUserId(), !following)
                      ..updateItems();

                    if (following) {
                      pushNotificationToUsers(
                        notifyType: NOTIFY_TYPE_FOLLOW,
                        notifyId: user.getObjectId(),
                        userIds: [user.getUserId()],
                      );
                    }

                    //if (null != setState) setState();
                  },
                  color: appYellow,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  child: Text(
                    following ? "Following" : "Follow",
                    style: textStyle(following, 14, black),
                  ),
                ),
              ],
            ),
            addSpace(5),
            addLine(.5, white.withOpacity(.1), 0, 5, 0, 0)
          ],
        ),
      ),
    );
  }

  onFollow(int p, bool following) {
    BaseModel theUser = winnersList[p];
    String userId = userModel.getObjectId();
    String personId = theUser.getObjectId();
    List followersIds = theUser.getList(FOLLOWERS);

    //bool following = followersIds.contains(userId);

    List followings = userModel.getList(FOLLOWING);
    bool hasFollowed = followings.contains(personId);

    if (following) {
      followersIds.remove(userId);
    } else {
      followersIds.add(userId);
    }

    if (hasFollowed) {
      followings.remove(personId);
    } else {
      followings.add(personId);
    }

    theUser.put(FOLLOWERS, followersIds);
    theUser.updateItems();

    userModel.put(FOLLOWING, followings);
    userModel.updateItems();

    following = !following;
    winnersList[p] = theUser;
    print(followersIds);
    setState(() {});

    // return;
    addToStatusParty(theUser, add: !following);
    pushNotificationToUsers(
      notifyType: following ? NOTIFY_TYPE_FOLLOW : NOTIFY_TYPE_UNFOLLOW,
      notifyId: personId,
      userIds: [personId],
    );
  }
}
