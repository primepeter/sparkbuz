import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:line_icons/line_icons.dart';

import 'AppConfig.dart';
import 'AppEngine.dart';
import 'assets.dart';

class ShowFilterFonts extends StatefulWidget {
  @override
  _ShowFilterFontsState createState() => _ShowFilterFontsState();
}

class _ShowFilterFontsState extends State<ShowFilterFonts> {
  final searchController = TextEditingController();
  bool showCancel = false;
  bool searching = false;
  List<String> result = GoogleFonts.asMap().keys.toList();
  List<String> appCategories = GoogleFonts.asMap().keys.toList();

  @override
  initState() {
    super.initState();
    searchController.addListener(listener);
    result.sort((a, b) => a.compareTo(b));
  }

  listener() async {
    String text = searchController.text.trim().toLowerCase();
    if (text.isEmpty) {
      result = appCategories;
      showCancel = false;
      searching = false;
      result.sort((a, b) => a.compareTo(b));
      if (mounted) setState(() {});
      return;
    }
    showCancel = true;
    searching = true;
    if (mounted) setState(() {});

    result =
        appCategories.where((b) => b.toLowerCase().startsWith(text)).toList();

    searching = false;
    if (mounted) setState(() {});
  }

  @override
  dispose() {
    super.dispose();
    searchController?.removeListener(listener);
    searchController?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: widget.popResult ? transparent : white,
      backgroundColor: black.withOpacity(.5),
      body: page(),
    );
  }

  page() {
    return Stack(
      children: [
        BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
            child: Container(
              color: black.withOpacity(.6),
            )),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              padding: EdgeInsets.only(top: 30, right: 10, left: 10, bottom: 0),
              child: Row(
                children: [
                  BackButton(
                    color: white,
                  ),
                  Text(
                    "Choose Font",
                    style: textStyle(true, 16, white),
                  ),
                  Spacer(),
                  //if (isAdmin)
                  RaisedButton(
                    color: AppConfig.appYellow,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25)),
                    child: Text(
                      "Apply",
                      style: textStyle(true, 14, black),
                    ),
                    onPressed: () {},
                  ),
                  addSpaceWidth(5),
                ],
              ),
            ),
            addSpace(5),
            Container(
              margin: EdgeInsets.only(left: 15, right: 15, bottom: 10),
              child: Container(
                  padding: EdgeInsets.only(left: 15, right: 15),
                  decoration: BoxDecoration(
                      color: white.withOpacity(.04),
                      border: Border.all(
                        color: white.withOpacity(.09),
                      ),
                      borderRadius: BorderRadius.circular(10)),
                  child: Row(
                    children: [
                      Icon(
                        Icons.search,
                        color: white.withOpacity(.4),
                        size: 20,
                      ),
                      addSpaceWidth(5),
                      Expanded(
                        child: TextField(
                          controller: searchController,
                          cursorColor: white,
                          style: textStyle(false, 16, white),
                          decoration: InputDecoration(
                              hintText: "Search in Fonts ",
                              hintStyle:
                                  textStyle(false, 14, white.withOpacity(.5)),
                              border: InputBorder.none),
                        ),
                      ),
                      if (showCancel)
                        GestureDetector(
                          onTap: () {
                            searchController.clear();
                            result = appCategories;
                            showCancel = false;
                            searching = false;
                            result.sort((a, b) => a.compareTo(b));
                            setState(() {});
                          },
                          child: Icon(
                            LineIcons.close,
                            color: black.withOpacity(.5),
                          ),
                        ),
                    ],
                  )),
            ),
            AnimatedContainer(
              duration: Duration(milliseconds: 400),
              child: LinearProgressIndicator(
                valueColor: AlwaysStoppedAnimation(orange03),
              ),
              height: searching ? 2 : 0,
              margin: EdgeInsets.only(bottom: searching ? 5 : 0),
            ),
            Expanded(
              child: GridView.builder(
                padding: EdgeInsets.all(0),
                itemCount: result.length, //GoogleFonts.asMap().keys.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 4,
                  //childAspectRatio: 0.9,
                ),
                itemBuilder: (c, p) {
                  return resultItem(p);
                },
              ),
            )
          ],
        ),
      ],
    );
  }

  resultItem(int index) {
    String fontName = result[index];
    final textStyle = GoogleFonts.asMap()[fontName];

    return InkWell(
      onTap: () {
        Navigator.pop(context, fontName);
      },
      child: Container(
        padding: EdgeInsets.all(5),
        child: Column(
          children: [
            Text(
              fontName,
              style: filterTextStyle(fontName, true, 16, white),
            )
          ],
        ),
      ),
    );
  }
}

TextStyle filterTextStyle(
    String fontName, bool bold, double size, Color color) {
  //if (null == color) color = white.withOpacity(.5);
  if (fontName.isEmpty) fontName = 'Baloo';
  final textStyle = GoogleFonts.asMap()[fontName];
  return textStyle(
    color: color,
    fontSize: size,
    fontWeight: bold ? FontWeight.bold : FontWeight.normal,
  );
}
