/*
import 'package:cached_video_player/cached_video_player.dart';
import 'package:flutter/material.dart';

import 'assets.dart';

typedef void PlayScreenCallback(bool playing);

class PlayTapScreen extends StatelessWidget {
  final bool isPlaying, showPlayIcon;
  final CachedVideoPlayerController controller;
  final PlayScreenCallback playCallback;

  const PlayTapScreen(
      {Key key,
      @required this.isPlaying,
      @required this.showPlayIcon,
      @required this.controller,
      @required this.playCallback})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        bool isPlaying = controller.value.isPlaying;
        if (isPlaying) {
          controller.pause();
        } else {
          controller.play();
        }
        playCallback(!isPlaying);
      },
      child: Container(
        color: transparent,
        child: Center(
          child: showPlayIcon
              ? Container(
                  height: 50,
                  width: 50,
                  child: Icon(
                    controller.value.isPlaying ? Icons.pause : Icons.play_arrow,
                    color: Colors.white,
                  ),
                  decoration: BoxDecoration(
                      color: Colors.black.withOpacity(0.8),
                      border: Border.all(color: Colors.white, width: 1.5),
                      shape: BoxShape.circle),
                )
              : null,
        ),
      ),
    );
  }
}
*/
