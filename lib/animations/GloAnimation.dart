import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:sparkbuz/AppConfig.dart';

class GloAnimation extends StatefulWidget {
  final Widget body;
  final double scaleSize;
  final double initialSize;
  final bool animate;
  GloAnimation({this.body, this.scaleSize, this.initialSize, this.animate});

  @override
  _GloAnimationState createState() => _GloAnimationState(
        body: this.body,
        scaleSize: scaleSize,
      );
}

class _GloAnimationState extends State<GloAnimation>
    with SingleTickerProviderStateMixin {
  final Widget body;
  final double scaleSize;
  double initialSize;
  bool animate;
  _GloAnimationState({
    this.body,
    this.scaleSize,
    this.initialSize,
    this.animate = false,
  });

  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(milliseconds: 2000),
      vsync: this,
    ) /*..repeat(reverse: true)*/;
    _controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _controller.reverse();
      } else if (status == AnimationStatus.dismissed) {
        _controller.forward();
      }
    });
    if (animate) _controller.forward();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  void didUpdateWidget(GloAnimation oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
    if (oldWidget.animate != widget.animate) {
      animate = widget.animate;
      //if (animate) _controller.forward();
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _controller,
      child: body,
      builder: (BuildContext context, Widget child) {
        final scaleTo = _controller.value * scaleSize;
        return Container(
          height: animate ? initialSize : scaleSize,
          width: animate ? initialSize : scaleSize,
          child: Stack(
            alignment: Alignment.center,
            children: [
              Transform.scale(
                scale: _controller.value,
                child: Container(
                  height: scaleTo,
                  width: scaleTo,
                  decoration: BoxDecoration(
                      color: AppConfig.appColor, shape: BoxShape.circle),
                ),
                alignment: Alignment.center,
                origin: Offset.zero,
              ),
              child
            ],
          ),
        );
      },
    );
  }
}

class RandomColor {
  static final Random _random = new Random();

  /// Returns a random color.
  static Color next() {
    return new Color(0xFF000000 + _random.nextInt(0x00FFFFFF));
  }
}

/// A Container Widget that takes up a given [width] and [height] and paints itself with a
/// random color.
class RandomContainer extends StatefulWidget {
  final double width;
  final double height;
  final Widget child;
  final bool changeOnRedraw;

  RandomContainer(
      {this.width, this.height, this.child, this.changeOnRedraw = true});

  @override
  _RandomContainerState createState() => new _RandomContainerState();
}

class _RandomContainerState extends State<RandomContainer> {
  Color randomColor;

  @override
  void initState() {
    super.initState();
    randomColor = RandomColor.next();
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      width: widget.width,
      height: widget.height,
      color: widget.changeOnRedraw == true ? RandomColor.next() : randomColor,
      child: widget.child,
    );
  }
}
