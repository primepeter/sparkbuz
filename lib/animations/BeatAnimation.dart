import 'dart:math' as math;
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:sparkbuz/AppConfig.dart';
import 'package:sparkbuz/assets.dart';

class BeatAnimation extends StatefulWidget {
  final Widget body;
  final double scaleSize;
  final int currentPage;
  BeatAnimation({this.body, this.scaleSize, this.currentPage = 0});

  @override
  _BeatAnimationState createState() => _BeatAnimationState(
      body: this.body, scaleSize: scaleSize, currentPage: this.currentPage);
}

class _BeatAnimationState extends State<BeatAnimation>
    with SingleTickerProviderStateMixin {
  final Widget body;
  final double scaleSize;
  int currentPage;
  _BeatAnimationState({
    this.body,
    this.scaleSize,
    this.currentPage,
  });

  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(milliseconds: 2000),
      vsync: this,
    ) /*..repeat(reverse: true)*/;
    _controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _controller.reverse();
      } else if (status == AnimationStatus.dismissed) {
        _controller.forward();
      }
    });
    _controller.forward();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  void didUpdateWidget(BeatAnimation oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
    if (oldWidget.currentPage != widget.currentPage) {
      currentPage = widget.currentPage;
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _controller,
      child: body,
      builder: (BuildContext context, Widget child) {
        final scaleTo = _controller.value * scaleSize;
        return Container(
          height: scaleSize,
          width: scaleSize,
          child: Stack(
            alignment: Alignment.center,
            children: [
              Transform.scale(
                scale: _controller.value,
                child: Container(
                  height: scaleTo,
                  width: scaleTo,
                  decoration: BoxDecoration(
                      color: currentPage == 0 ? white : AppConfig.appColor,
                      shape: BoxShape.circle),
                ),
                alignment: Alignment.center,
                origin: Offset.zero,
              ),
              child
            ],
          ),
        );
        return Transform.rotate(
          angle: _controller.value * 2.0 * math.pi,
          child: child,
        );
      },
    );
  }
}

class RandomColor {
  static final Random _random = new Random();

  /// Returns a random color.
  static Color next() {
    return new Color(0xFF000000 + _random.nextInt(0x00FFFFFF));
  }
}

/// A Container Widget that takes up a given [width] and [height] and paints itself with a
/// random color.
class RandomContainer extends StatefulWidget {
  final double width;
  final double height;
  final Widget child;
  final bool changeOnRedraw;

  RandomContainer(
      {this.width, this.height, this.child, this.changeOnRedraw = true});

  @override
  _RandomContainerState createState() => new _RandomContainerState();
}

class _RandomContainerState extends State<RandomContainer> {
  Color randomColor;

  @override
  void initState() {
    super.initState();
    randomColor = RandomColor.next();
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      width: widget.width,
      height: widget.height,
      color: widget.changeOnRedraw == true ? RandomColor.next() : randomColor,
      child: widget.child,
    );
  }
}
