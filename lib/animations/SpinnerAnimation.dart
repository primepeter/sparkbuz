import 'dart:math' as math;

import 'package:flutter/material.dart';

class SpinnerAnimation extends StatefulWidget {
  final Widget body;
  final bool playing;
  SpinnerAnimation({this.body, this.playing = false});

  @override
  _SpinnerAnimationState createState() =>
      _SpinnerAnimationState(body: this.body, playing: playing);
}

class _SpinnerAnimationState extends State<SpinnerAnimation>
    with SingleTickerProviderStateMixin {
  final Widget body;
  bool playing;
  _SpinnerAnimationState({this.body, this.playing = false});

  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(seconds: 5),
      vsync: this,
    )..repeat();
    // if (playing)
    //   _controller.forward();
    // else
    //   _controller.stop(canceled: false);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  void didUpdateWidget(covariant SpinnerAnimation oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
    if (oldWidget.playing != playing) {
      playing = widget.playing;
      if (playing)
        _controller.forward();
      else
        _controller.stop(canceled: false);
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _controller,
      child: body,
      builder: (BuildContext context, Widget child) {
        return Transform.rotate(
          angle: _controller.value * 2.0 * math.pi,
          child: child,
        );
      },
    );
  }
}
