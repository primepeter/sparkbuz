import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_size/flutter_keyboard_size.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:line_icons/line_icons.dart';
import 'package:sparkbuz/main_pages/ShowProfile.dart';
import 'package:sparkbuz/main_pages/ShowVideoPost.dart';

import 'AppEngine.dart';
import 'NewSparkbuzzers.dart';
import 'assets.dart';
import 'basemodel.dart';

class SearchPeople extends StatefulWidget {
  @override
  _SearchPeopleState createState() => _SearchPeopleState();
}

class _SearchPeopleState extends State<SearchPeople> {
  final searchController = TextEditingController();
  List<BaseModel> result = [];
  bool showCancel = false;
  bool searching = false;
  FocusNode focusNode = FocusNode();

  int clampSize(String value) {
    return (searchController.text.length - 1).clamp(0, value.length);
  }

  @override
  initState() {
    super.initState();
    searchController.addListener(listener);
    //applySearchFix();
  }

  @override
  didChangeDependencies() {
    super.didChangeDependencies();
    focusNode.requestFocus();
  }

  @override
  dispose() {
    super.dispose();
    searchController?.removeListener(listener);
    searchController?.dispose();
    focusNode?.dispose();
  }

  applySearchFix() {
    print("<<<done>>> fixing!");

    FirebaseFirestore.instance.collection(USER_BASE).get().then((value) {
      for (var doc in value.docs) {
        BaseModel model = BaseModel(doc: doc);
        String token = model.getString(TOKEN);
        String userId = model.getUserId();
        FirebaseFirestore.instance
            .collection(POSTS_BASE)
            .where(USER_ID, isEqualTo: userId)
            .get()
            .then((value) {
          for (var doc in value.docs) {
            BaseModel model = BaseModel(doc: doc);
            model
              ..put(TOKEN, token)
              ..updateItems(delaySeconds: 2);
          }
          print("<<<done>>> fix applied!");
        });

        // String username = model.getUserName().toLowerCase();
        // String name = model.getName().toLowerCase();
        //
        // String description = model.getString(DESCRIPTION).toLowerCase();
        // String category = model.getString(CATEGORY).toLowerCase();
        //
        // //final search = getSearchString('$username $name');
        // final search = getSearchString('$description $category');
        // model
        //   ..put(SEARCH, search)
        //   ..updateItems(delaySeconds: 2);
      }
      //print("<<<done>>> fix applied!");
    });
  }

  listener() async {
    List<DocumentSnapshot> docList = [];
    String text = searchController.text.trim().toLowerCase();
    result.clear();
    setState(() {});
    if (text.isEmpty) {
      result.clear();
      showCancel = false;
      searching = false;
      if (mounted) setState(() {});
      return;
    }
    showCancel = true;
    searching = true;
    if (mounted) setState(() {});
    QuerySnapshot shots = await FirebaseFirestore.instance
        .collection(USER_BASE)
        .where(SEARCH, arrayContains: text.trim())
        .limit(30)
        .get();

    if (shots != null && shots.size > 0) {
      docList.addAll(shots.docs);
    }

    QuerySnapshot shots2 = await FirebaseFirestore.instance
        .collection(POSTS_BASE)
        .where(SEARCH, arrayContains: text.trim())
        .limit(30)
        .get();

    if (shots2 != null && shots2.size > 0) {
      docList.addAll(shots2.docs);
    }

    for (var doc in docList) {
      BaseModel model = BaseModel(doc: doc);
      int p = result.indexWhere((e) => e.getObjectId() == model.getObjectId());
      if (p != -1)
        result[p] = model;
      else
        result.add(model);
      searching = false;
      if (mounted) setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return KeyboardDismisser(
      gestures: [
        GestureType.onTap,
        GestureType.onPanUpdateDownDirection,
      ],
      child: KeyboardSizeProvider(
        child: KeyboardSizeProvider(
          smallSize: 500.0,
          child: Consumer<ScreenHeight>(
              builder: (context, ScreenHeight sh, child) {
            return Scaffold(
              backgroundColor: white,
              body: page(),
            );
          }),
        ),
      ),
    );
  }

  page() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          padding: EdgeInsets.only(top: 30, right: 10, left: 10, bottom: 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              BackButton(),
            ],
          ),
        ),
        Container(
            margin: EdgeInsets.only(left: 15, right: 15, bottom: 10),
            padding: EdgeInsets.only(left: 15, right: 15),
            decoration: BoxDecoration(
                color: black.withOpacity(.04),
                border: Border.all(
                  color: black.withOpacity(.09),
                ),
                borderRadius: BorderRadius.circular(10)),
            child: Row(
              children: [
                Icon(
                  Icons.search,
                  color: black.withOpacity(.4),
                  size: 20,
                ),
                addSpaceWidth(5),
                Expanded(
                  child: TextField(
                    controller: searchController,
                    focusNode: focusNode,
                    autofocus: true,
                    cursorColor: black,
                    decoration: InputDecoration(
                        hintText: "Search ", border: InputBorder.none),
                  ),
                ),
                if (showCancel)
                  GestureDetector(
                    onTap: () {
                      searchController.clear();
                      result.clear();
                      showCancel = false;
                      setState(() {});
                    },
                    child: Icon(
                      LineIcons.close,
                      color: black.withOpacity(.5),
                    ),
                  ),
              ],
            )),
        AnimatedContainer(
          duration: Duration(milliseconds: 400),
          child: LinearProgressIndicator(
            valueColor: AlwaysStoppedAnimation(orange03),
          ),
          height: searching ? 2 : 0,
          margin: EdgeInsets.only(bottom: searching ? 5 : 0),
        ),
        Expanded(
          child: ListView(
            padding: EdgeInsets.all(0),
            children: List.generate(result.length, (index) {
              return resultItem(index);
            }),
          ),
        )
      ],
    );
  }

  resultItem(int index) {
    BaseModel model = result[index];
    String username = model.getUserName();

    // bool followers = model.followings.contains(userModel.getUserId());
    //bool following = model.followings.contains(model.getUserId());

    List followersIds = model.getList(FOLLOWERS);
    bool following = followersIds.contains(userModel.getUserId());

    bool isPost = model.getString(DATABASE_NAME) == POSTS_BASE;

    String description = model.getString(DESCRIPTION);
    String category = model.getString(CATEGORY);
    String thumbnail = model.getString(THUMBNAIL_URL);

    //print(username);

    return InkWell(
      onTap: () {
        if (isPost) {
          pushAndResult(
              context,
              ShowVideoPost(
                videoModel: model,
              ));
          return;
        }
        pushAndResult(
            context,
            ShowProfile(
              model: model,
              showCloseBtn: true,
              profileId: model.getUserId(),
            ));
      },
      onLongPress: () {
        if (!isAdmin) return;
        showListDialog(context, ["Add to SparkBuzzers", "Delete"], (_) {
          if (_ == 0) {
            if (isPost) return;
            pushAndResult(
                context,
                NewSparkbuzzers(
                  model: model,
                ));
          }
          if (_ == 1) {
            model.deleteItem();
            result.removeWhere((e) => e.getObjectId() == model.getObjectId());
            setState(() {});
          }
        });
      },
      child: Container(
        padding: EdgeInsets.all(5),
        child: Row(
          children: [
            if (isPost) ...[
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Container(
                  height: 80,
                  width: 80,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(color: black.withOpacity(.1))),
                  child: Stack(
                    children: [
                      CachedNetworkImage(
                        imageUrl: thumbnail,
                        height: 80,
                        width: 80,
                        fit: BoxFit.cover,
                        placeholder: (c, s) {
                          return Container(
                            height: 50,
                            width: 50,
                            color: black.withOpacity(.09),
                            child: Icon(LineIcons.user),
                          );
                        },
                      ),
                      Container(
                        width: double.infinity,
                        height: double.infinity,
                        color: black.withOpacity(.09),
                        child: Center(
                          child: Container(
                            height: 25,
                            width: 25,
                            child: Icon(
                              Icons.play_arrow,
                              color: Colors.white,
                              size: 14,
                            ),
                            decoration: BoxDecoration(
                                color: black.withOpacity(0.8),
                                border:
                                    Border.all(color: Colors.white, width: 1.5),
                                shape: BoxShape.circle),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              addSpaceWidth(10),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text.rich(TextSpan(children: [
                      // TextSpan(
                      //     text: category.substring(
                      //         0, searchController.text.length),
                      //     style: textStyle(true, 14, black)),
                      TextSpan(
                          text:
                              category.substring(searchController.text.length),
                          style: textStyle(true, 14, black))
                    ])),
                    addSpace(5),
                    Text.rich(TextSpan(children: [
                      TextSpan(
                          text: description.substring(
                              0, searchController.text.length),
                          style: textStyle(true, 15, black)),
                      TextSpan(
                          text: description
                              .substring(searchController.text.length),
                          style: textStyle(false, 15, black.withOpacity(.5)))
                    ])),
                    Container(
                      decoration: BoxDecoration(
                          color: black.withOpacity(.02),
                          border: Border.all(color: black.withOpacity(.0)),
                          borderRadius: BorderRadius.circular(15)),
                      padding: EdgeInsets.all(4),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(25),
                            child: CachedNetworkImage(
                              imageUrl: model.userImage,
                              height: 25,
                              width: 25,
                              fit: BoxFit.cover,
                              placeholder: (c, s) {
                                return Container(
                                  height: 25,
                                  width: 25,
                                  color: black.withOpacity(.09),
                                  child: Icon(LineIcons.user),
                                );
                              },
                            ),
                          ),
                          addSpaceWidth(5),
                          Text.rich(TextSpan(children: [
                            TextSpan(
                                text: username,
                                style:
                                    textStyle(false, 14, black.withOpacity(1)))
                          ])),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ] else ...[
              ClipRRect(
                borderRadius: BorderRadius.circular(50),
                child: CachedNetworkImage(
                  imageUrl: model.userImage,
                  height: 50,
                  width: 50,
                  fit: BoxFit.cover,
                  placeholder: (c, s) {
                    return Container(
                      height: 50,
                      width: 50,
                      color: black.withOpacity(.09),
                      child: Icon(LineIcons.user),
                    );
                  },
                ),
              ),
              addSpaceWidth(10),
              Expanded(
                child: Text.rich(TextSpan(children: [
                  TextSpan(
                      text: username.substring(0, clampSize(username)),
                      style: textStyle(true, 14, black)),
                  TextSpan(
                      text: username.substring(clampSize(username)),
                      style: textStyle(false, 14, black.withOpacity(.5)))
                ])),
              ),
              FlatButton(
                onPressed: () {
                  onFollow(index, following);
                  return;
                  // model
                  //   ..putInList(FOLLOWERS, userModel.getUserId(), !followers)
                  //   ..updateItems();
                  // result[index] = model;

                  print(model.followers);

                  setState(() {});
                  return;

                  userModel
                    ..putInList(FOLLOWING, model.getUserId(), !following)
                    ..updateItems();
                  addToStatusParty(model, add: !following);
                  // result[index] = model;
                  setState(() {});
                  pushNotificationToUsers(
                    notifyType:
                        following ? NOTIFY_TYPE_FOLLOW : NOTIFY_TYPE_UNFOLLOW,
                    notifyId: model.getObjectId(),
                    userIds: [model.getUserId()],
                  );
                },
                color: appColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                child: Text(
                  following ? "Following" : "Follow",
                  style: textStyle(true, 14, white),
                ),
              ),
            ]
          ],
        ),
      ),
    );
  }

  // List followersIds = [];
  // bool following = false;

  onFollow(int p, bool following) {
    BaseModel theUser = result[p];
    String userId = userModel.getObjectId();
    String personId = theUser.getObjectId();
    List followersIds = theUser.getList(FOLLOWERS);

    //bool following = followersIds.contains(userId);

    List followings = userModel.getList(FOLLOWING);
    bool hasFollowed = followings.contains(personId);

    if (following) {
      followersIds.remove(userId);
    } else {
      followersIds.add(userId);
    }

    if (hasFollowed) {
      followings.remove(personId);
    } else {
      followings.add(personId);
    }

    theUser.put(FOLLOWERS, followersIds);
    theUser.updateItems();

    userModel.put(FOLLOWING, followings);
    userModel.updateItems();

    following = !following;
    result[p] = theUser;
    print(followersIds);
    setState(() {});

    // return;
    addToStatusParty(theUser, add: !following);
    pushNotificationToUsers(
      notifyType: following ? NOTIFY_TYPE_FOLLOW : NOTIFY_TYPE_UNFOLLOW,
      notifyId: personId,
      userIds: [personId],
    );
  }
}
