import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:sparkbuz/basemodel.dart';

import 'AppConfig.dart';
import 'AppEngine.dart';
import 'assets.dart';
import 'basemodel.dart';

class ShowFollowers extends StatefulWidget {
  @override
  _ShowFollowersState createState() => _ShowFollowersState();
}

class _ShowFollowersState extends State<ShowFollowers> {
  final searchController = TextEditingController();
  FocusNode focusNode = FocusNode();
  List<BaseModel> result = [];
  List<BaseModel> videoShare = [];
  bool showCancel = false;
  bool searching = true;
  bool ready = false;

  @override
  initState() {
    super.initState();
    searchController.addListener(listener);
    loadUsers();
  }

  loadUsers() {
    print("<<<done>>> fixing!");
    FirebaseFirestore.instance
        .collection(USER_BASE)
        .where(FOLLOWERS, arrayContains: userModel.getUserId())
        .get()
        .then((value) {
      for (var doc in value.docs) {
        BaseModel model = BaseModel(doc: doc);
        if (model.myItem()) continue;
        int p =
            result.indexWhere((e) => e.getObjectId() == model.getObjectId());
        if (p != -1)
          result[p] = model;
        else
          result.add(model);
      }
      searching = false;
      ready = true;
      if (mounted) setState(() {});
      print("<<<done>>> fix applied!");
    });
  }

  listener() async {
    String text = searchController.text.trim().toLowerCase();
    if (text.isEmpty) {
      result.clear();
      showCancel = false;
      searching = false;
      if (mounted) setState(() {});
      return;
    }
    showCancel = true;
    searching = true;
    if (mounted) setState(() {});
    QuerySnapshot shots = await FirebaseFirestore.instance
        .collection(USER_BASE)
        .where(SEARCH, arrayContains: text.trim())
        .limit(30)
        .get();

    // QuerySnapshot shots2 = await Firestore.instance
    //     .collection(POSTS_BASE)
    //     .where(SEARCH, arrayContains: text.trim())
    //     .limit(30)
    //     .getDocuments();

    final responses = shots.docs;
    // responses.addAll(shots2.documents);

    for (var doc in responses) {
      BaseModel model = BaseModel(doc: doc);
      bool following = model.followers.contains(userModel.getUserId());
      if (!following) continue;
      int p = result.indexWhere((e) => e.getObjectId() == model.getObjectId());
      if (p != -1)
        result[p] = model;
      else
        result.add(model);
    }
    searching = false;
    if (mounted) setState(() {});
  }

  @override
  dispose() {
    super.dispose();
    searchController?.removeListener(listener);
    searchController?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      body: page(),
    );
  }

  page() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          padding: EdgeInsets.only(top: 30, right: 10, left: 10, bottom: 0),
          child: Row(
            children: [
              BackButton(),
              Container(
                margin: EdgeInsets.only(left: 15),
                child: Text(
                  "Followers",
                  style: textStyle(true, 25, black),
                ),
              ),
              Spacer(),
              // IconButton(
              //   onPressed: () {},
              //   icon: Icon(
              //     Icons.search,
              //     color: black.withOpacity(.4),
              //     //size: 20,
              //   ),
              // )
            ],
          ),
        ),
        Container(
            margin: EdgeInsets.only(left: 15, right: 15, bottom: 10),
            padding: EdgeInsets.only(left: 15, right: 15),
            decoration: BoxDecoration(
                color: black.withOpacity(.04),
                border: Border.all(
                  color: black.withOpacity(.09),
                ),
                borderRadius: BorderRadius.circular(10)),
            child: Row(
              children: [
                Icon(
                  Icons.search,
                  color: black.withOpacity(.4),
                  size: 20,
                ),
                addSpaceWidth(5),
                Expanded(
                  child: TextField(
                    controller: searchController,
                    focusNode: focusNode,
                    autofocus: true,
                    cursorColor: black,
                    decoration: InputDecoration(
                        hintText: "Search ", border: InputBorder.none),
                  ),
                ),
                if (showCancel)
                  GestureDetector(
                    onTap: () {
                      searchController.clear();
                      result.clear();
                      showCancel = false;
                      setState(() {});
                    },
                    child: Icon(
                      LineIcons.close,
                      color: black.withOpacity(.5),
                    ),
                  ),
              ],
            )),
        AnimatedContainer(
          duration: Duration(milliseconds: 400),
          child: LinearProgressIndicator(
            valueColor: AlwaysStoppedAnimation(orange03),
          ),
          height: searching ? 2 : 0,
          margin: EdgeInsets.only(bottom: searching ? 5 : 0),
        ),
        Expanded(
          child: Builder(
            builder: (c) {
              if (!ready) return loadingLayout();

              if (result.isEmpty)
                return emptyLayout('assets/icons/follow.png', "No Followers",
                    "You have no followers yet.",
                    nullColor: true);

              return ListView(
                padding: EdgeInsets.all(0),
                children: List.generate(result.length, (index) {
                  return resultItem(index);
                }),
              );
            },
          ),
        ),
        if (videoShare.length > 0)
          Container(
            padding: EdgeInsets.all(10),
            child: FlatButton(
                onPressed: () {
                  Navigator.pop(context, videoShare);
                },
                color: AppConfig.appYellow,
                padding: EdgeInsets.all(15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Let's Share"),
                    addSpaceWidth(5),
                    Icon(LineIcons.user)
                  ],
                )),
          )
      ],
    );
  }

  resultItem(int index) {
    BaseModel model = result[index];
    String username = model.getUserName();
    if (username.isEmpty) username = model.getName();
    bool sharing = videoShare.contains(model);
    bool isPost = model.getString(DATABASE_NAME) == POSTS_BASE;

    String description = model.getString(DESCRIPTION);
    String category = model.getString(CATEGORY);
    String thumbnail = model.getString(THUMBNAIL_URL);

    return InkWell(
      onTap: () {},
      child: Container(
        padding: EdgeInsets.all(5),
        child: Row(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(50),
              child: CachedNetworkImage(
                imageUrl: model.userImage,
                height: 50,
                width: 50,
                fit: BoxFit.cover,
                placeholder: (c, s) {
                  return Container(
                    height: 50,
                    width: 50,
                    color: black.withOpacity(.09),
                    child: Icon(LineIcons.user),
                  );
                },
              ),
            ),
            addSpaceWidth(10),
            Expanded(
              child: Text.rich(TextSpan(children: [
                // TextSpan(
                //     text: username.substring(0, searchController.text.length),
                //     style: textStyle(true, 14, black)),
                TextSpan(
                    text: username,
                    style: textStyle(false, 14, black.withOpacity(1)))
              ])),
            ),
            FlatButton(
              onPressed: () {
                if (sharing)
                  videoShare.remove(model);
                else if (videoShare.length < 4) videoShare.add(model);
                setState(() {});
              },
              color: sharing ? AppConfig.appColor : AppConfig.appYellow,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              child: Text(
                sharing ? "Remove" : "Add",
                style: textStyle(false, 14, black),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
