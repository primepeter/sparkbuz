import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'AppEngine.dart';
import 'MainAdmin.dart';
import 'StoryItem.dart';
import 'assets.dart';
import 'basemodel.dart';

class StoriesPage extends StatefulWidget {
  final List<List<BaseModel>> stories;
  final int position;
  final storyIndex;
  StoriesPage(this.stories, {this.position = 0, this.storyIndex = 0});
  @override
  _StoriesPageState createState() => _StoriesPageState();
}

class _StoriesPageState extends State<StoriesPage>
    with TickerProviderStateMixin {
  PageController storyPageController;
  List<List<BaseModel>> stories;
  int currentPage = 0;
  int storyIndex = 0;

  List<StreamSubscription> subs = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    var storyPageChange = storyPageChangeController.stream.listen((event) {
      bool lastStoryPageUp = currentPage == 0;
      bool lastStoryPageDown = currentPage == stories.length - 1;

      if ((stories.length - 1) == 0) {
        //page has to close because it has only one story available
        print("page has to close because it has only one story available");
        closeStoryPageController.add(true);
        return;
      }

      if ((stories.length - 1) == currentPage) {
        //page has close because its at the last story
        print("page has close because its at the last story");
        closeStoryPageController.add(true);
        return;
      }

      print("changing Page $currentPage");

      storyPageController.nextPage(
          duration: Duration(milliseconds: 800), curve: Curves.ease);
    });

    var closePageChange = closeStoryPageController.stream.listen((event) {
      stopStoriesController.add(true);
      Navigator.pop(context);
    });

    var deleteStory = deleteStoryAtController.stream.listen((index) {
      //Navigator.pop(context);
      stories[currentPage].removeAt(index);
      if (stories[currentPage].isEmpty) stories.removeAt(currentPage);
      if (mounted) setState(() {});
    });

    subs.add(deleteStory);
    subs.add(closePageChange);
    subs.add(storyPageChange);
    stories = widget.stories;
    storyIndex = widget.storyIndex;
    currentPage = widget.position;
    setState(() {});
    storyPageController = PageController(initialPage: widget.position);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    storyPageController?.dispose();
    for (var s in subs) s?.cancel();
  }

  bool hasStarted = false;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        stopStoriesController.add(true);
        Navigator.pop(context);
        return false;
      },
      child: Scaffold(
        body: !storiesReady ? loadingLayout() : page(),
        backgroundColor: black,
      ),
    );
  }

  page() {
    return Stack(
      alignment: Alignment.center,
      children: [
        PageView.builder(
          controller: storyPageController,
          scrollDirection: Axis.vertical,
          //physics: NeverScrollableScrollPhysics(),
          onPageChanged: (p) async {
            currentPage = p;
            stopStoriesController.add(true);
            setState(() {});
          },
          itemCount: stories.length,
          itemBuilder: (c, p) {
            String keyId = stories[p][0].getObjectId();
            return StoryItem(
                key: Key(keyId),
                storyPage: p,
                storyIndex: storyIndex,
                maxStories: stories.length,
                stories: stories[p]);
          },
        ),
        Align(
          alignment: Alignment.topRight,
          child: Container(
            margin: EdgeInsets.only(
              top: 60,
            ),
            child: FlatButton(
              color: Colors.red,
              onPressed: () {
                stopStoriesController.add(true);
                Navigator.pop(context);
              },
              child: Icon(
                Icons.close,
                color: white,
              ),
              shape: CircleBorder(),
            ),
          ),
        )
      ],
    );
  }
}
