import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';

import 'AppEngine.dart';
import 'assets.dart';
import 'basemodel.dart';

//import 'package:Strokes/photo_picker/photo.dart';

enum UsernameState { none, checking, available, notAvailable, error }

class NewSparkbuzzers extends StatefulWidget {
  final BaseModel model;

  const NewSparkbuzzers({Key key, this.model}) : super(key: key);
  @override
  _NewSparkbuzzersState createState() => _NewSparkbuzzersState();
}

class _NewSparkbuzzersState extends State<NewSparkbuzzers> {
  String objectId = getRandomId();
  UsernameState usernameState = UsernameState.none;
  TextEditingController usernameController = new TextEditingController();

  TextEditingController featuresController = new TextEditingController();

  bool mustUpdate = false;
  int clickBack = 0;
  BaseModel model = BaseModel();
  String linkedImage = '';

  @override
  void initState() {
    // TODO: implement initState
    if (widget.model != null) {
      model = widget.model;
      usernameController.text = model.getString(USERNAME);
      linkedImage = model.getString(USER_IMAGE);
      objectId = model.getObjectId();
    }

    usernameController.addListener(() async {
      String check = usernameController.text;

      if (check.isEmpty) {
        usernameState = UsernameState.none;
        if (mounted) setState(() {});
        return;
      }

      if (check.startsWith('@')) {
        showError('Username cannot start with @');
        return;
      }

      if (check.contains(' ')) {
        showError('Username cannot contain space!');
        return;
      }

      if (check.isNotEmpty && check.length > 4) {
        usernameState = UsernameState.checking;
        checkUsernameAvailability(check.toLowerCase());
      }
      if (mounted) setState(() {});
    });
    super.initState();
  }

  checkUsernameAvailability(String name) async {
    print(name);
    var doc = await FirebaseFirestore.instance
        .collection(USER_BASE)
        .where(USERNAME, isEqualTo: name)
        .limit(1)
        .get();

    print(doc.size);

    if (doc.size == 0) {
      usernameState = UsernameState.notAvailable;
    } else {
      usernameState = UsernameState.available;
      model = BaseModel(doc: doc.docs[0]);
      objectId = model.getObjectId();
    }

    if (mounted) setState(() {});
  }

  get getAvailabilityTxt {
    switch (usernameState) {
      case UsernameState.none:
        // TODO: Handle this case.
        return '';
        break;
      case UsernameState.checking:
        // TODO: Handle this case.
        return 'Checking username availability';
        break;
      case UsernameState.available:
        // TODO: Handle this case.
        return 'Username is available';
        break;
      case UsernameState.notAvailable:
        // TODO: Handle this case.
        return 'Username is not available';
        break;
      case UsernameState.error:
        // TODO: Handle this case.
        return 'Username contains space!';
        break;
    }
  }

  get getAvailabilityColor {
    switch (usernameState) {
      case UsernameState.none:
        // TODO: Handle this case.
        return transparent;
        break;
      case UsernameState.checking:
        // TODO: Handle this case.
        return transparent;
        break;
      case UsernameState.available:
        // TODO: Handle this case.
        return dark_green0;
        break;
      case UsernameState.notAvailable:
        // TODO: Handle this case.
        return red0;
        break;
      case UsernameState.error:
        // TODO: Handle this case.
        return red0;
        break;
    }
  }

  get getAvailabilityIcon {
    switch (usernameState) {
      case UsernameState.none:
        // TODO: Handle this case.
        return null;
        break;
      case UsernameState.checking:
        // TODO: Handle this case.
        return CircularProgressIndicator(
          //valueColor: AlwaysStoppedAnimation(white),
          strokeWidth: 2,
        );
        break;
      case UsernameState.available:
        // TODO: Handle this case.
        return Icon(Icons.check, color: white, size: 20);
        break;
      case UsernameState.notAvailable:
        // TODO: Handle this case.
        return Icon(Icons.close, color: white, size: 20);
        break;
      case UsernameState.error:
        // TODO: Handle this case.
        return Icon(Icons.close, color: white, size: 20);
        break;
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    usernameController?.dispose();
    super.dispose();
  }

  BuildContext context;
  @override
  Widget build(BuildContext c) {
    context = c;
    return WillPopScope(
        onWillPop: () {
          int now = DateTime.now().millisecondsSinceEpoch;
          if ((now - clickBack) > 5000) {
            clickBack = now;
            toastInAndroid("Click back again to exit");
            return;
          }
          Navigator.pop(context);
        },
        child: Scaffold(
            resizeToAvoidBottomInset: true,
            backgroundColor: white,
            body: page()));
  }

  BuildContext con;

  String errorText = "";
  bool showSuccess = false;
  showError(String text, {bool wasLoading = false, bool success = false}) {
    if (wasLoading) showProgress(false, context);
    errorText = text;
    showSuccess = success;
    if (mounted) setState(() {});

    Future.delayed(Duration(seconds: 3), () {
      errorText = "";
      showSuccess = false;
      if (mounted) setState(() {});
    });
  }

  Builder page() {
    return Builder(builder: (context) {
      this.con = context;
      return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          addSpace(30),
          new Container(
            width: double.infinity,
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                InkWell(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Container(
                      width: 50,
                      height: 50,
                      child: Center(
                          child: Icon(
                        Icons.keyboard_backspace,
                        color: black,
                        size: 25,
                      )),
                    )),
                Flexible(
                  fit: FlexFit.tight,
                  flex: 1,
                  child: new Text(
                    "New Sparkbuzzers",
                    style: textStyle(true, 17, black),
                  ),
                ),
                addSpaceWidth(10),
                FlatButton(
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25)),
                    color: blue3,
                    onPressed: () {
                      post();
                    },
                    child: Text(
                      "CREATE",
                      style: textStyle(true, 14, white),
                    )),
                addSpaceWidth(15)
              ],
            ),
          ),
          AnimatedContainer(
            duration: Duration(milliseconds: 500),
            width: double.infinity,
            height: errorText.isEmpty ? 0 : 40,
            color: showSuccess ? dark_green0 : red0,
            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: Center(
                child: Text(
              errorText,
              style: textStyle(true, 16, white),
            )),
          ),
          Expanded(
            flex: 1,
            child: Scrollbar(
              child: SingleChildScrollView(
                padding: EdgeInsets.all(0),
                child: new Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                      child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              alignment: Alignment.center,
                              child: GestureDetector(
                                onTap: () async {
                                  final picker = await ImagePicker.platform
                                      .pickImage(source: ImageSource.gallery);
                                  if (null == picker) return;
                                  linkedImage = picker.path;
                                  setState(() {});
                                },
                                child: Container(
                                  height: 150,
                                  width: 150,
                                  child: Stack(
                                    children: [
                                      ClipRRect(
                                        borderRadius: BorderRadius.circular(75),
                                        child: CachedNetworkImage(
                                          imageUrl: linkedImage,
                                          height: 150,
                                          width: 150,
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                      if (linkedImage.isEmpty)
                                        Container(
                                          padding: EdgeInsets.all(4),
                                          //margin: EdgeInsets.all(5),
                                          height: 150,
                                          width: 150,
                                          decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                          ),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Icon(
                                                Icons.add_circle_outline,
                                                size: 30,
                                                color: black.withOpacity(.5),
                                              ),
                                              Text("Add")
                                            ],
                                          ),
                                        ),
                                      Align(
                                        alignment: Alignment.bottomRight,
                                        child: Container(
                                          decoration: BoxDecoration(
                                              border: Border.all(
                                                  color:
                                                      black.withOpacity(.09)),
                                              color: blue3,
                                              shape: BoxShape.circle),
                                          height: 40,
                                          width: 40,
                                          child: Center(
                                            child: Image.asset(
                                              'assets/icons/upload.png',
                                              height: 25,
                                              width: 25,
                                              fit: BoxFit.cover,
                                              color: white,
                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                  decoration:
                                      BoxDecoration(shape: BoxShape.circle),
                                ),
                              ),
                            ),
                            addSpace(10),
                            Text(
                              "Username",
                              style: textStyle(true, 14, blue0),
                            ),
                            addSpace(10),
                            Container(
                              //height: 45,
                              margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                              decoration: BoxDecoration(
                                  color: blue09,
                                  borderRadius: BorderRadius.circular(10),
                                  border: Border.all(
                                      color: black.withOpacity(.1), width: .5)),
                              child: new TextField(
                                onSubmitted: (_) {
                                  //post();
                                },
                                textInputAction: TextInputAction.done,

                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    contentPadding:
                                        EdgeInsets.fromLTRB(10, 10, 10, 10),
                                    hintText: "",
                                    hintStyle: textStyle(
                                        false, 16, black.withOpacity(.2))),
                                style: textStyle(
                                  false,
                                  16,
                                  black,
                                ),
                                controller: usernameController,
                                cursorColor: black,
                                cursorWidth: 1,
//                          maxLength: 50,
                                maxLines: 1,
                                //keyboardType: TextInputType.number,
                                scrollPadding: EdgeInsets.all(0),
                              ),
                            ),
                            if (usernameState != UsernameState.none)
                              Container(
                                padding: EdgeInsets.only(top: 10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      decoration: BoxDecoration(
                                          color: black.withOpacity(.02),
                                          border: Border.all(
                                              width: 2,
                                              color: getAvailabilityColor),
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                      padding: EdgeInsets.all(5),
                                      alignment: Alignment.centerLeft,
                                      child: Row(
                                        children: [
                                          Container(
                                            width: 25,
                                            height: 25,
                                            padding: EdgeInsets.all(2),
                                            decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: getAvailabilityColor,
                                            ),
                                            alignment: Alignment.center,
                                            child: getAvailabilityIcon,
                                          ),
                                          addSpaceWidth(10),
                                          Expanded(
                                            child: Container(
                                              //color: red0,
                                              child: Text(
                                                getAvailabilityTxt,
                                                style:
                                                    textStyle(false, 14, black),
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            addSpace(10),
                          ]),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      );
    });
  }

  savePhoto() {
    showProgress(true, context, msg: "Uploading Photo...");
    uploadFile(File(linkedImage), (res, error) {
      showProgress(false, context);
      Future.delayed(Duration(milliseconds: 500), () {
        if (error != null) {
          showProgress(false, context);
          Future.delayed(Duration(seconds: 1), () {
            showErrorDialog(context, error);
          });
          return;
        }
        linkedImage = res;
        model.put(LINK_IMAGE, res);
        setState(() {});
        post();
      });
    });
  }

  void post() {
    String username = usernameController.text.trim();
    FocusScope.of(context).requestFocus(FocusNode());

    if (linkedImage.isEmpty) {
      showError("Add Profile Photo!");
      return;
    }

    if (usernameState != UsernameState.available) {
      showError(getAvailabilityTxt);
      return;
    }

    if (!fromOnline(linkedImage)) {
      savePhoto();
      return;
    }

    //model.put(PROFILE_TO_LINK, true);
    //model.put(USERNAME, username);

    model.put(LINK_IMAGE, linkedImage);
    model.put(OBJECT_ID, objectId);
    model.put(USER_ID, objectId);
    model.saveItem(LINK_BASE, false, document: objectId, merged: true,
        onComplete: () {
      showMessage(context, Icons.check, green, "SparkBuzzer Created",
          'You have successfully added a new SparkBuzzer Profile.',
          cancellable: false, delayInMilli: 1000, onClicked: (_) {
        Navigator.pop(context);
      });
    });
  }
}
