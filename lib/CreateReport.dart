import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'AppEngine.dart';
import 'MainAdmin.dart';
import 'assets.dart';
import 'basemodel.dart';

class CreateReport extends StatefulWidget {
  final BaseModel item;
  const CreateReport({Key key, this.item}) : super(key: key);
  @override
  _CreateReportState createState() => _CreateReportState();
}

class _CreateReportState extends State<CreateReport> {
  String selectedCategory = "";
  List reportCategories = appSettingsModel.getList(APP_REPORT_CATEGORIES);
  final reasonController = TextEditingController();
  BaseModel item;
  bool isUser = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    item = widget.item;
    isUser = item.getString(DATABASE_NAME) == USER_BASE;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      body: page(),
    );
  }

  page() {
    return Column(
      children: [
        addSpace(30),
        Padding(
          padding: const EdgeInsets.only(left: 0),
          child: Row(
            children: <Widget>[
              InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Container(
                    width: 50,
                    height: 50,
                    child: Center(
                        child: Icon(
                      Icons.keyboard_backspace,
                      color: black,
                      size: 25,
                    )),
                  )),
              Expanded(
                  child: Text(
                "Report item",
                style: textStyle(true, 20, black),
              )),
              //if (selections.isNotEmpty)
              FlatButton(
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25)),
                  color: appColor,
                  onPressed: () {
                    handleReport();
                  },
                  child: Text(
                    "Submit",
                    style: textStyle(true, 14, white),
                  )),
              addSpaceWidth(10)
            ],
          ),
        ),
        AnimatedContainer(
          duration: Duration(milliseconds: 500),
          width: double.infinity,
          height: errorText.isEmpty ? 0 : 40,
          color: showSuccess ? dark_green0 : red0,
          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: Center(
              child: Text(
            errorText,
            style: textStyle(true, 16, white),
          )),
        ),
        pageList()
      ],
    );
  }

  pageList() {
    return Expanded(
      child: SingleChildScrollView(
        padding: EdgeInsets.all(15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Please select a problem to continue",
              style: textStyle(true, 20, black),
            ),
            Text(
              "You can only make a report after selecting the problem",
              style: textStyle(false, 14, black.withOpacity(.5)),
            ),
            addSpace(10),
            Container(
              width: double.infinity,
              child: Wrap(
                //alignment: WrapAlignment.start,
                children: reportCategories.map((e) {
                  bool selected = selectedCategory == e;

                  return GestureDetector(
                    onTap: () {
                      if (mounted)
                        setState(() {
                          selectedCategory = e;
                        });
                    },
                    child: Container(
                      padding: EdgeInsets.only(
                          left: 10, right: 10, top: 4, bottom: 4),
                      margin: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                          color: appColor.withOpacity(selected ? 1 : .1),
                          borderRadius: BorderRadius.circular(8)),
                      child: Text(
                        e,
                        style:
                            textStyle(selected, 16, selected ? white : black),
                      ),
                    ),
                  );
                }).toList(),
              ),
            ),
            addSpace(10),
            inputTextView("Describe the issue", reasonController,
                maxLine: 5, isNum: false),
          ],
        ),
      ),
    );
  }

  String errorText = "";
  bool showSuccess = false;
  showError(String text, {bool wasLoading = false, bool success = false}) {
    if (wasLoading) showProgress(false, context);
    errorText = text;
    showSuccess = success;
    if (mounted) setState(() {});

    Future.delayed(Duration(seconds: 3), () {
      errorText = "";
      showSuccess = false;
      if (mounted) setState(() {});
    });
  }

  handleReport() async {
    String reason = reasonController.text;

    if (selectedCategory.isEmpty) {
      showError("Choose what the problem is!");
      return;
    }
    if (reason.isEmpty) {
      showError("Describe the issue with the item");
      return;
    }

    if (!await isConnected()) {
      showErrorDialog(context, "No Internet Connectivity");
      return;
    }

    Map<String, dynamic> reportData = {
      REPORTS: FieldValue.arrayUnion([
        {
          NAME: userModel.getString(USERNAME).isEmpty
              ? userModel.getString(NAME)
              : userModel.getString(USERNAME),
          USER_ID: userModel.getObjectId(),
          TITLE: reason,
          MESSAGE: reason,
          TIME: DateTime.now().millisecondsSinceEpoch
        }
      ]),
      ITEM_NAME: item.getString(isUser
          ? item.getString(USERNAME).isEmpty
              ? item.getString(NAME)
              : item.getString(USERNAME)
          : MESSAGE),
      ITEM_ID: item.getObjectId(),
      ITEM_DB: item.getString(DATABASE_NAME),
      STATUS: PENDING,
      DATABASE_NAME: REPORT_BASE,
      OBJECT_ID: item.getObjectId(),
      TIME_UPDATED: DateTime.now().millisecondsSinceEpoch
    };

    DocumentReference doc = FirebaseFirestore.instance
        .collection(REPORT_BASE)
        .doc(item.getObjectId());
    doc.update(reportData).catchError((e) {
      if (e.toString().toLowerCase().contains("found")) {
        reportData[TIME] = DateTime.now().millisecondsSinceEpoch;
        doc.set(reportData);
      }
    });

    /* NotificationService.sendPush(
      topic: "admin",
      title: "Report",
      body: "Some reports are pending approval",
    );*/
    item
      ..putInList(HIDDEN, userModel.getUserId())
      ..updateItems();
    deleteVideoController.add(item);
    await Future.delayed(Duration(seconds: 2));
    showProgress(false, context);
    await Future.delayed(Duration(milliseconds: 500));
    showMessage(context, Icons.check, pink3, "Report Submitted",
        "Thank you for keeping our platform safe", cancellable: false,
        onClicked: (_) {
      Navigator.pop(context, true);
    });
  }
}
