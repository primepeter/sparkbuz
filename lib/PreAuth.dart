import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:device_info/device_info.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_login_facebook/flutter_login_facebook.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:line_icons/line_icons.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';
import 'package:sparkbuz/AppEngine.dart';
import 'package:sparkbuz/MainAdmin.dart';

import 'AppConfig.dart';
import 'AppEngine.dart';
import 'app/navigation.dart';
import 'assets.dart';
import 'auth/AuthDOB.dart';
import 'auth/AuthLogin.dart';
import 'basemodel.dart';

class PreAuth extends StatefulWidget {
  final int position;
  const PreAuth({Key key, @required int p})
      : position = p,
        super(key: key);
  @override
  _PreAuthState createState() => _PreAuthState();
}

class _PreAuthState extends State<PreAuth> {
  int currentPage = 0;
  PageController vp = PageController();

  @override
  void initState() {
    super.initState();
    currentPage = widget.position;
    vp = PageController(initialPage: widget.position);
    //loadVideo();
  }

  @override
  Widget build(BuildContext context) {
    return page();
  }

  page() {
    return Material(
      color: black.withOpacity(.1),
      child: Container(
        margin: EdgeInsets.only(top: 80),
        decoration: BoxDecoration(
            color: white,
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(25),
              topLeft: Radius.circular(25),
            )),
        child: Column(
          //mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
                padding: EdgeInsets.only(top: 10, right: 15, left: 15),
                child: Row(
                  children: [
                    CloseButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                    Spacer(),
                    // IconButton(
                    //   onPressed: () {},
                    //   icon: Icon(LineIcons.question_circle),
                    // )
                  ],
                )),
            addSpace(10),
            Expanded(
              child: PageView.builder(
                  itemCount: 2,
                  controller: vp,
                  onPageChanged: (p) {
                    currentPage = p;
                    setState(() {});
                  },
                  physics: NeverScrollableScrollPhysics(),
                  itemBuilder: (ctx, p) {
                    return authItem(p);
                  }),
            ),
            Container(
              padding: EdgeInsets.all(15),
              height: 100,
              child: currentPage == 1
                  ? null
                  : Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                              text:
                                  'By Clicking on "CONTINUE WITH", You hereby agree to our ',
                              style:
                                  textStyle(false, 14, black.withOpacity(.6))),
                          TextSpan(
                              text: 'Terms of Service',
                              recognizer: new TapGestureRecognizer()
                                ..onTap = () => openLink(
                                    appSettingsModel.getString(TERMS_LINK)),
                              style:
                                  textStyle(true, 14, black, underlined: true)),
                          TextSpan(
                              text: ' and ',
                              style:
                                  textStyle(false, 14, black.withOpacity(.6))),
                          TextSpan(
                              text: 'Privacy Policy',
                              recognizer: new TapGestureRecognizer()
                                ..onTap = () => openLink(
                                    appSettingsModel.getString(PRIVACY_LINK)),
                              style:
                                  textStyle(true, 14, black, underlined: true)),
                          TextSpan(
                              text: ' Binding our community.',
                              style:
                                  textStyle(false, 14, black.withOpacity(.6))),
                        ],
                      ),
                      textAlign: TextAlign.center,
                    ),
            ),
            GestureDetector(
              onTap: () {
                int jumpTo = currentPage == 0 ? 1 : 0;
                vp.jumpToPage(jumpTo);
              },
              child: Container(
                padding: EdgeInsets.all(10),
                color: black.withOpacity(.03),
                alignment: Alignment.center,
                child: Text.rich(TextSpan(children: [
                  TextSpan(
                    text: currentPage == 0
                        ? "Already have an account? "
                        : "Don't have an account? ",
                    style: textStyle(false, 16, black),
                  ),
                  TextSpan(
                    text: currentPage == 0 ? "Login" : "Sign up",
                    style: textStyle(true, 16, red),
                  )
                ])),
              ),
            )
          ],
        ),
      ),
    );
  }

  handleSignIn(String type) async {
    showProgress(true, context, msg: "Loggin In");
    if (type == "google") {
      GoogleSignIn googleSignIn = GoogleSignIn();
      googleSignIn.signIn().then((account) async {
        account.authentication.then((googleAuth) {
          final credential = GoogleAuthProvider.credential(
            accessToken: googleAuth.accessToken,
            idToken: googleAuth.idToken,
          );
          loginIntoApp(credential);
        }).catchError((e) {
          onError("Error 001", e);
        });
      }).catchError((e) {
        onError("Error 01", "Authentication was terminated by you.");
      });
    }

    if (type == "facebook") {
      final fb = FacebookLogin();

      fb.logIn(permissions: [
        FacebookPermission.publicProfile,
        FacebookPermission.email,
        FacebookPermission.userFriends,
      ]).then((account) {
        final credential = FacebookAuthProvider.credential(
          account.accessToken.token,
        );
        loginIntoApp(credential);
      }).catchError((e) {
        onError("Error 02", "Authentication was terminated by you.");
      });
    }

    if (type == "apple") {
      SignInWithApple.getAppleIDCredential(
        scopes: [
          AppleIDAuthorizationScopes.email,
          AppleIDAuthorizationScopes.fullName,
        ],
      ).then((value) {
        final credential = OAuthProvider('apple.com').credential(
          accessToken: value.authorizationCode,
          idToken: value.identityToken,
        );
        loginIntoApp(credential);
      }).catchError((e) {
        onError("Error 04", "Authentication was terminated by you.");
      });
    }
  }

  loginIntoApp(
    AuthCredential credential, {
    String email,
    String pass,
  }) async {
    DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
    String deviceId;
    if (Platform.isIOS) {
      final deviceInfo = await deviceInfoPlugin.iosInfo;
      deviceId = deviceInfo.identifierForVendor;
    } else {
      final deviceInfo = await deviceInfoPlugin.androidInfo;
      deviceId = deviceInfo.androidId;
    }

    (null != credential
            ? FirebaseAuth.instance.signInWithCredential(credential)
            : FirebaseAuth.instance
                .signInWithEmailAndPassword(email: email, password: pass))
        .then((value) {
      final account = value.user;

      FirebaseFirestore.instance
          .collection(USER_BASE)
          .doc(account.uid)
          .get()
          .then((doc) {
        if (!doc.exists) {
          userModel
            ..put(USER_ID, account.uid)
            ..put(EMAIL, account.email)
            ..put(USER_IMAGE, account.photoURL)
            ..put(NAME, account.displayName)
            ..putInList(DEVICE_ID, deviceId, true)
            ..saveItem(USER_BASE, false, document: account.uid);
        } else {
          userModel = BaseModel(doc: doc);
        }
        popUpUntil(
            context,
            userModel.signUpCompleted
                ? MainAdmin()
                : AuthDOB(
                    toUsername: true,
                  ));
      }).catchError((e) {
        onError("Error 04", e);
      });
    }).catchError((e) {
      onError("Error 03", e);
    });
  }

  onError(String type, e) {
    showProgress(false, context);
    showMessage(context, Icons.error, red0, type, e.toString(),
        delayInMilli: 1200, cancellable: true);
  }

  authItem(int p) {
    String headerText = "Sign up for SparkBuz!";
    String description =
        "Create a profile, folow other accounts, make your own videos and more...!";

    if (p == 1) {
      headerText = "Log in to SparkBuz!";
      description =
          "Manage your account, check notifications, comment on videos and more...!";
    }

    return Center(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              //height: 150,
              padding: EdgeInsets.only(left: 40, right: 40, bottom: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    headerText,
                    style: textStyle(true, 25, black),
                  ),
                  addSpace(10),
                  Text(
                    description,
                    textAlign: TextAlign.center,
                    style: textStyle(false, 16, black.withOpacity(.5)),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: 20, right: 20),
              //width: 160,
              child: FlatButton(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  //mainAxisSize: MainAxisSize.min,
                  children: [
                    Icon(
                      LineIcons.user,
                      color: white,
                    ),
                    addSpaceWidth(5),
                    // Text('USE PHONE,EMAIL OR USERNAME',
                    Text('USE EMAIL OR USERNAME',
                        style: textStyle(true, 14, white)),
                  ],
                ),
                onPressed: () {
                  pushAndResult(context, p == 0 ? AuthDOB() : AuthLogin(),
                      depend: false);
                },
                padding: EdgeInsets.all(16),
                shape: RoundedRectangleBorder(
                    //side: BorderSide(color: white.withOpacity(.4), width: 2),
                    borderRadius: BorderRadius.circular(8)),
                color: AppConfig.appColor,
              ),
            ),
            addSpace(10),
            if (Platform.isIOS)
              Container(
                padding: EdgeInsets.only(left: 20, right: 20),
                //width: 160,
                child: FlatButton(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    //mainAxisSize: MainAxisSize.min,
                    children: [
                      Image.asset(
                        "assets/icons/apple.png",
                        height: 20,
                        width: 20,
                        color: white,
                      ),
                      addSpaceWidth(5),
                      Text('CONTINUE WITH APPLE',
                          style: textStyle(true, 14, white)),
                    ],
                  ),
                  onPressed: () {
                    handleSignIn("apple");
                  },
                  padding: EdgeInsets.all(16),
                  shape: RoundedRectangleBorder(
                      //side: BorderSide(color: white.withOpacity(.4), width: 2),
                      borderRadius: BorderRadius.circular(8)),
                  color: black,
                ),
              )
            else
              Container(
                padding: EdgeInsets.only(
                    left: 20, right: 20), //width: double.infinity,
                child: FlatButton(
                  color: white,
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Image.asset(
                          "assets/icons/google.png",
                          height: 20,
                          width: 20,
                          //color: white,
                        ),
                        addSpaceWidth(5),
                        Text(
                          'CONTINUE WITH GOOGLE',
                          style: textStyle(true, 14, black),
                        ),
                      ],
                    ),
                  ),
                  onPressed: () {
                    handleSignIn("google");
                  },
                  padding: EdgeInsets.all(16),
                  shape: RoundedRectangleBorder(
                      side: BorderSide(color: black.withOpacity(0.5), width: 1),
                      borderRadius: BorderRadius.circular(8)),
                  //color: Color(0xFFf4c20d),
                ),
              ),
            addSpace(10),
            Container(
              padding: EdgeInsets.only(left: 20, right: 20),
              //width: 160,
              child: FlatButton(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  //mainAxisSize: MainAxisSize.min,
                  children: [
                    Image.asset(
                      "assets/icons/facebook.png",
                      height: 20,
                      width: 20,
                      color: white,
                    ),
                    addSpaceWidth(5),
                    Text('CONTINUE WITH FACEBOOK',
                        style: textStyle(true, 14, white)),
                  ],
                ),
                onPressed: () {
                  handleSignIn("facebook");
                },
                padding: EdgeInsets.all(16),
                shape: RoundedRectangleBorder(
                    //side: BorderSide(color: white.withOpacity(.4), width: 2),
                    borderRadius: BorderRadius.circular(8)),
                color: Color(0xFF4267B2),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
