import 'dart:io';

import 'package:cached_video_player/cached_video_player.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:extended_image/extended_image.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:path_provider/path_provider.dart';
import 'package:video_player/video_player.dart';

// import 'package:video_trimmer/video_trimmer.dart';

import 'AppEngine.dart';
import 'assets.dart';
import 'basemodel.dart';
import 'basic/Tapped.dart';

List<String> upOrDown = List();
List<String> noFileFound = List();
List<String> fileThatExists = List();

class VideoManager {
  final CachedVideoPlayerController controller;
  final String id;

  VideoManager(this.controller, this.id);
}

typedef void PreviewCallBack(List<BaseModel> photos, List selectedPhotos);

class PreviewPostImages extends StatefulWidget {
  final List<BaseModel> photos;
  final List selectedPhotos;
  final int indexOf;
  final bool edittable;
  final String heroTag;
  final bool storyMode;
  PreviewPostImages(this.photos, this.selectedPhotos,
      {this.indexOf = 0,
      this.edittable = false,
      this.heroTag = "heroTag",
      this.storyMode = false});

  @override
  _PreviewPostImagesState createState() => _PreviewPostImagesState();
}

class _PreviewPostImagesState extends State<PreviewPostImages> {
  int currentPage;
  PageController controller;
  VideoPlayerController videoController;
  List<BaseModel> photos = [];
  List selectedPhotos = [];
  ScrollController imageScroll = ScrollController();
  bool keyboardVisible = false;
  final messageController = TextEditingController();

  List<VideoManager> videoManager = [];
  List<CachedVideoPlayerController> controllers = [];
  bool editMode = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller = PageController(initialPage: widget.indexOf);
    currentPage = widget.indexOf;
    photos = widget.photos;
    selectedPhotos = widget.selectedPhotos;
    editMode = widget.edittable;
    //stopPlaying = false;
    loadVideosPlus();
    KeyboardVisibility.onChange.listen((bool visible) {
      keyboardVisible = visible;
      print("Keyboard Visible $visible");
      if (mounted) setState(() {});
    });

    if (widget.storyMode) {
      messageController.addListener(() {
        String text = messageController.text;
        if (text.isEmpty) return;
        BaseModel model = photos[currentPage];
        model.put(MESSAGE, text);
        photos[currentPage] = model;
        if (mounted) setState(() {});
      });
    }

    Future.delayed(Duration(seconds: 1), () {
      try {
        imageScroll.animateTo((widget.indexOf * 80).toDouble(),
            duration: Duration(milliseconds: 500), curve: Curves.ease);
      } catch (e) {}
    });
  }

  @override
  dispose() {
    disposeVideo();
    super.dispose();
  }

  loadVideosPlus() {
    photos = widget.photos;
    for (int p = 0; p < photos.length; p++) {
      BaseModel bm = photos[p];
      CachedVideoPlayerController control;
      bool playThis = widget.indexOf == p;
      String objectId = bm.getObjectId();
      String path = bm.getString(IMAGE_PATH);
      String videoUrl = bm.getString(IMAGE_URL);

      if (!bm.isVideo) {
        print("null p $p not a video");
        videoManager.add(VideoManager(null, objectId));
        continue;
      }

      if (editMode)
        control = CachedVideoPlayerController.file(File(path));
      else
        control = CachedVideoPlayerController.network(videoUrl);
      control.initialize().then((value) {
        int index = videoManager.indexWhere((e) => e.id == objectId);

        if (index == -1) {
          videoManager.add(VideoManager(control, objectId));
        } else {
          videoManager[index] = VideoManager(control, objectId);
        }
        if (playThis && !widget.storyMode) {
          control.seekTo(Duration(seconds: 1));
          control.play();
        }
        if (mounted) setState(() {});
        print("Has been reopened video but needs to be re-initialized!!!");
      });
      int index = videoManager.indexWhere((e) => e.id == objectId);

      if (index == -1) {
        videoManager.add(VideoManager(control, objectId));
      } else {
        videoManager[index] = VideoManager(control, objectId);
      }
      if (mounted) setState(() {});
    }
  }

  bool disposed = false;

  pauseAnyVideo() async {
    for (int p = 0; p < videoManager.length; p++) {
      final manager = videoManager[p];
      if (null == manager.controller) {
        continue;
      }
      try {
        manager.controller.pause();
        videoManager[p] = VideoManager(manager.controller, manager.id);
        if (mounted) setState(() {});
        await Future.delayed(Duration(seconds: 1));
      } catch (e) {
        print(e);
      }
    }
  }

  disposeVideo() async {
    for (int p = 0; p < videoManager.length; p++) {
      final manager = videoManager[p];
      if (null == manager.controller) {
        continue;
      }
      try {
        manager.controller.pause();
        videoManager[p] = VideoManager(null, manager.id);
        if (mounted) setState(() {});
        await Future.delayed(Duration(seconds: 1));
        await manager.controller.dispose();
      } catch (e) {
        print(e);
      }
    }
  }

  playVideoInPage(int p, {bool pause = false}) {
    final manager = videoManager[p];
    if (manager == null || manager.controller == null) {
      print("show error failed to load video!");
      return;
    }
    if (pause)
      manager.controller?.pause();
    else
      manager.controller?.play();
    if (mounted) setState(() {});
  }

  String errorText = "";
  bool showSuccess = false;
  showError(String text, {bool wasLoading = false, bool success = false}) {
    if (wasLoading) showProgress(false, context);
    errorText = text;
    showSuccess = success;
    if (mounted) setState(() {});

    Future.delayed(Duration(seconds: 3), () {
      errorText = "";
      showSuccess = false;
      if (mounted) setState(() {});
    });
  }

  bool showPlayButton = false;
  showPlayBtn() {
    showPlayButton = true;
    if (mounted) setState(() {});
    Future.delayed(Duration(seconds: 3), () {
      showPlayButton = false;
      if (mounted) setState(() {});
    });
  }

  downloadFile(BaseModel model, onComplete) async {
    bool isVideo = model.isVideo;
    String extensions = model.isVideo ? "mp4" : "png";
    String fileName = "${model.getString(IMAGE_URL).hashCode}.$extensions";

    String path = "/storage/emulated/0/Download/$fileName";
    if (Platform.isIOS)
      path = (await getApplicationDocumentsDirectory()).path + "/$fileName";
    File file = File(path);
    await file.create();

    upOrDown.add(model.getObjectId());
    showError("Downloading ${isVideo ? "Video" : "Photo"}", success: true);
    QuerySnapshot shots = await FirebaseFirestore.instance
        .collection(REFERENCE_BASE)
        .where(FILE_URL, isEqualTo: model.getString(IMAGE_URL))
        .limit(1)
        .get();
    if (shots.docs.isEmpty) {
      upOrDown.removeWhere((s) => s == model.getObjectId());
      showError("File not found on the server");
      //onComplete();
    } else {
      for (DocumentSnapshot doc in shots.docs) {
        if (!doc.exists || doc.data().isEmpty) continue;
        BaseModel model = BaseModel(doc: doc);
        String ref = model.getString(REFERENCE);
        Reference storageReference = FirebaseStorage.instance.ref().child(ref);
        storageReference.writeToFile(file).then((_) {
          showError("Download Complete", success: true);
          print(file.path);
          upOrDown.removeWhere((s) => s == model.getObjectId());
          onComplete();
        }, onError: (error) {
          upOrDown.removeWhere((s) => s == model.getObjectId());
          showError("File not found on the server");
          //onComplete();
        }).catchError((error) {
          upOrDown.removeWhere((s) => s == model.getObjectId());
          showError("File not found on the server");
          //onComplete();
        });

        break;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        disposeVideo();
        Navigator.of(context).pop();
        return false;
      },
      child: Scaffold(
          backgroundColor: black,
          body: Column(children: [
            addSpace(30),
            Padding(
              padding: const EdgeInsets.only(left: 0),
              child: Row(
                children: <Widget>[
                  InkWell(
                      onTap: () {
                        //if (!widget.edittable)
                        disposeVideo();
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        width: 50,
                        height: 50,
                        child: Center(
                            child: Icon(
                          Icons.keyboard_backspace,
                          color: white,
                          size: 25,
                        )),
                      )),
                  Expanded(
                      child: Text(
                    "Preview",
                    style: textStyle(true, 20, white),
                  )),
                  if (!photos[currentPage].isVideo &&
                      widget.edittable &&
                      !photos[currentPage]
                          .getString(IMAGE_URL)
                          .startsWith("http"))
                    IconButton(
                        icon: Icon(
                          Icons.edit,
                          color: white,
                        ),
                        onPressed: () async {
                          BaseModel bm = photos[currentPage];
                          String imageUrl = bm.getString(IMAGE_URL);
                          String imagePath = bm.getString(IMAGE_PATH);
                          String thumbPath = bm.getString(THUMBNAIL_PATH);
                          String thumbUrl = bm.getString(THUMBNAIL_URL);
                          bool onlineImage = imageUrl.startsWith("http");
                          bool isVideo = bm.isVideo;

                          if (isVideo) {
                            // pauseAnyVideo();
                            // await Future.delayed(Duration(seconds: 1));
                            // final Trimmer _trimmer = Trimmer();
                            // await _trimmer.loadVideo(
                            //     videoFile: File(imagePath));
                            // pushReplacementAndResult(
                            //     context,
                            //     TrimmerView(
                            //       _trimmer,
                            //       isPost: !widget.storyMode,
                            //     ), result: (_) async {
                            //   if (null == _) return;
                            //   String thumbnailPath =
                            //       await getVideoThumbnail(_[0]);
                            //   bm.put(THUMBNAIL_PATH, thumbnailPath);
                            //   bm.put(IMAGE_PATH, _[0]);
                            //   photos[currentPage] = bm;
                            //   if (mounted) setState(() {});
                            // });
                          } else {
                            if (imagePath.startsWith("http")) return;
                            final crop = await ImageCropper.cropImage(
                                sourcePath: imagePath);
                            if (null == crop) return;
                            bm.put(IMAGE_PATH, crop.path);
                            photos[currentPage] = bm;
                            if (mounted) setState(() {});
                          }
                        }),
                  if (widget.edittable)
                    IconButton(
                        icon: Icon(
                          Icons.delete,
                          color: white,
                        ),
                        onPressed: () {
                          photos.removeAt(currentPage);
                          if (photos.isEmpty) {
                            disposeVideo();
                            Navigator.pop(context, []);
                          } else {
                            setState(() {});
                          }
                        }),
                  if (!widget.edittable ||
                      photos[currentPage]
                          .getString(IMAGE_URL)
                          .startsWith("http"))
                    IconButton(
                        icon: Icon(
                          Icons.cloud_download_outlined,
                          color: white,
                        ),
                        onPressed: () {
                          // showError("Downloading...", success: true);
                          BaseModel bm = photos[currentPage];
                          downloadFile(bm, () {
                            //showError("File is saved!...", success: true);
                          });
                        })
                ],
              ),
            ),
            AnimatedContainer(
              duration: Duration(milliseconds: 500),
              width: double.infinity,
              height: errorText.isEmpty ? 0 : 40,
              color: showSuccess ? dark_green0 : red0,
              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: Center(
                  child: Text(
                errorText,
                style: textStyle(true, 16, white),
              )),
            ),
            pages()
          ])),
    );
  }

  pages() {
    return Expanded(
      child: Stack(
        children: [
          PageView.builder(
            controller: controller,
            itemCount: photos.length,
            onPageChanged: (p) {
              pauseAnyVideo();
              currentPage = p;

              if (widget.storyMode) {
                BaseModel model = photos[p];
                String text = model.getString(MESSAGE);
                messageController.text = text;
              }

              try {
                imageScroll.animateTo((p * 80).toDouble(),
                    duration: Duration(milliseconds: 500), curve: Curves.ease);
                BaseModel bm = photos[(p - 1).clamp(0, photos.length - 1)];
                if (bm.isVideo) {
                  playVideoInPage(p);
                }
              } catch (e) {}
              setState(() {});
            },
            itemBuilder: (c, p) {
              BaseModel bm = photos[p];
              String objectId = bm.getObjectId();
              String imageUrl = bm.getString(IMAGE_URL);
              if (objectId.isEmpty) objectId = imageUrl;
              String imagePath = bm.getString(IMAGE_PATH);
              String thumbPath = bm.getString(THUMBNAIL_PATH);
              String thumbUrl = bm.getString(THUMBNAIL_URL);
              bool onlineImage = imageUrl.startsWith("http");
              bool isVideo = bm.isVideo;

              return GestureDetector(
                onTap: () async {
                  if (!widget.edittable) return;
                  if (!isVideo) return;

                  // videoManager[p].controller.play();
                  playVideoInPage(p);
                  // final Trimmer _trimmer = Trimmer();
                  // _trimmer.loadVideo(videoFile: File(imagePath)).then((value) {
                  //   pushAndResult(context, TrimmerView(_trimmer),
                  //       result: (_) async {
                  //     if (null == _) return;
                  //     String thumbnailPath = await getVideoThumbnail(_[0]);
                  //     bm.put(THUMBNAIL_PATH, thumbnailPath);
                  //     bm.put(IMAGE_PATH, _[0]);
                  //     bm.put(VIDEO_LENGTH, _[1]);
                  //
                  //     print("url $_");
                  //
                  //     photos[p] = bm;
                  //     setState(() {});
                  //   });
                  // });
                },
                child: Builder(
                  builder: (c) {
                    if (isVideo) return videoItem(p);
                    return imageItem(p);
                  },
                ),
              );
            },
          ),
          if (photos.length > 0)
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                color: black.withOpacity(.8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    addSpace(10),
                    Row(
                      children: [
                        if (widget.storyMode)
                          Expanded(
                              child: Container(
                            padding: EdgeInsets.only(left: 10, right: 10),
                            decoration: BoxDecoration(
                                color: white,
                                borderRadius: BorderRadius.circular(10)),
                            child: TextField(
                              controller: messageController,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: "Add caption"),
                            ),
                          )),
                        if (widget.edittable)
                          Container(
                            margin: EdgeInsets.all(20),
                            child: FloatingActionButton(
                              onPressed: () {
                                disposeVideo();
                                Navigator.pop(context, photos);
                              },
                              heroTag: "p33",
                              clipBehavior: Clip.antiAlias,
                              backgroundColor: light_green3,
                              shape: CircleBorder(),
                              child: Icon(
                                Icons.check,
                                color: white,
                                size: 30,
                              ),
                            ),
                          ),
                      ],
                    ),
                    if (!keyboardVisible)
                      new Container(
                        height: 70,
                        width: double.infinity,
                        child: ListView.builder(
                          itemBuilder: (c, p) {
                            BaseModel bm = photos[p];
                            String imageUrl = bm.getString(IMAGE_URL);
                            String imagePath = bm.getString(IMAGE_PATH);
                            String thumbPath = bm.getString(THUMBNAIL_PATH);
                            String thumbUrl = bm.getString(THUMBNAIL_URL);
                            bool onlineImage = imageUrl.startsWith("http");
                            bool isVideo = bm.isVideo;
                            return GestureDetector(
                              onTap: () {
                                controller.animateToPage(p,
                                    duration: Duration(milliseconds: 500),
                                    curve: Curves.ease);
                              },
                              child: Row(
                                children: [
                                  Container(
                                    width: 70,
                                    height: 70,
                                    margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
                                    child: Card(
                                      clipBehavior: Clip.antiAlias,
                                      shape: CircleBorder(
                                          side: BorderSide(
                                              color: p == currentPage
                                                  ? light_green3
                                                  : transparent,
                                              width: 3)),
                                      child: onlineImage
                                          ? CachedNetworkImage(
                                              imageUrl:
                                                  isVideo ? thumbUrl : imageUrl,
                                              height: MediaQuery.of(context)
                                                  .size
                                                  .height,
                                              fit: BoxFit.cover,
                                            )
                                          : Image.file(
                                              File(isVideo
                                                  ? thumbPath
                                                  : imagePath),
                                              fit: BoxFit.cover,
                                              width: double.infinity,
                                            ),
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                          shrinkWrap: true,
                          padding: EdgeInsets.all(0),
                          itemCount: photos.length,
                          scrollDirection: Axis.horizontal,
                          controller: imageScroll,
                        ),
                      ),
                    addSpace(10),
                  ],
                ),
              ),
            ),
        ],
      ),
    );
  }

  imageItem(int p) {
    BaseModel bm = photos[p];
    String imageUrl = bm.getString(IMAGE_URL);
    String imagePath = bm.getString(IMAGE_PATH);
    bool onlineImage = imageUrl.contains("http");

    if (onlineImage) {
      return ExtendedImage.network(imageUrl,
          fit: BoxFit.cover,
          cache: true,
          alignment: Alignment.center,
          mode: ExtendedImageMode.gesture,
          loadStateChanged: (ExtendedImageState state) {
        switch (state.extendedImageLoadState) {
          case LoadState.loading:
            return Center(
              child: Container(
                width: 30,
                height: 30,
                child: CircularProgressIndicator(
                  //value: 20,
                  valueColor: AlwaysStoppedAnimation<Color>(white),
                  strokeWidth: 2,
                ),
              ),
            );
            break;

          ///if you don't want override completed widget
          ///please return null or state.completedWidget
          //return null;
          case LoadState.completed:
            return state.completedWidget;
            break;
          case LoadState.failed:
            return Container(
              color: transparent,
            );

          default:
            return Container();
        }
      });
    }
    return ExtendedImage.file(File(imagePath),
        fit: BoxFit.cover,
        //cache: true,
        alignment: Alignment.center,
        mode: ExtendedImageMode.gesture,
        loadStateChanged: (ExtendedImageState state) {
      switch (state.extendedImageLoadState) {
        case LoadState.loading:
          return Center(
            child: Container(
              width: 30,
              height: 30,
              child: CircularProgressIndicator(
                //value: 20,
                valueColor: AlwaysStoppedAnimation<Color>(white),
                strokeWidth: 2,
              ),
            ),
          );
          break;

        ///if you don't want override completed widget
        ///please return null or state.completedWidget
        //return null;
        case LoadState.completed:
          return state.completedWidget;
          break;
        case LoadState.failed:
          return Container(
            color: transparent,
          );

        default:
          return Container();
      }
    });
  }

  videoItem(
    int p,
  ) {
    if (videoManager.isEmpty) return videoBuffering(p);
    final controller = videoManager[p].controller;
    if (null == controller) return videoBuffering(p);
    if (!controller.value.initialized) return videoBuffering(p);
    return Stack(
      alignment: Alignment.center,
      children: [
        Center(
          child: AspectRatio(
            aspectRatio: controller.value.aspectRatio,
            child: CachedVideoPlayer(controller),
          ),
        ),
        Tapped(
          showIcon: showPlayButton,
          child: Container(
            color: transparent,
            alignment: Alignment.center,
            child: AnimatedContainer(
                duration: Duration(milliseconds: 400),
                height: 50,
                width: 50,
                decoration: BoxDecoration(
                    border: Border.all(color: white, width: 1),
                    color: black.withOpacity(.5),
                    shape: BoxShape.circle),
                child: Icon(
                  controller.value.isPlaying ? Icons.pause : Icons.play_arrow,
                  color: white,
                  size: 20,
                )),
          ),
          onTap: () async {
            showPlayBtn();
            final playerPosition = controller.value.position;
            final videoDuration = controller.value.duration;
            bool hasEnded = playerPosition == videoDuration;
            if (controller.value.isPlaying)
              controller.pause();
            else {
              if (hasEnded) controller.seekTo(Duration(seconds: 0));
              controller.play();
            }
            setState(() {});
          },
        ),
      ],
    );
  }

  videoBuffering(int p) {
    BaseModel bm = photos[p];
    String thumbPath = bm.getString(THUMBNAIL_PATH);
    String thumbUrl = bm.getString(THUMBNAIL_URL);
    bool onlineImage = thumbUrl.contains("http");

    return Stack(
      alignment: Alignment.center,
      children: [
        if (onlineImage)
          Container(
            margin: EdgeInsets.all(1.2),
            child: Image.network(
              thumbUrl,
              fit: BoxFit.cover,
              //width: double.infinity,
            ),
          )
        else
          Container(
            margin: EdgeInsets.all(1.2),
            child: Image.file(
              File(thumbPath),
              fit: BoxFit.cover,
              //width: double.infinity,
            ),
          ),
        videoLoading,
        Container(
          child: Center(
            child: Container(
              height: 60,
              width: 60,
              padding: EdgeInsets.all(3),
              margin: EdgeInsets.all(6),
              decoration: BoxDecoration(
                color: black.withOpacity(.5),
                border: Border.all(color: white, width: 2),
                shape: BoxShape.circle,
              ),
              alignment: Alignment.center,
              child: Icon(
                Icons.play_arrow,
                color: white,
                size: 20,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
