import 'dart:convert';

import 'package:http/http.dart';

class NotificationService {
  static final Client client = Client();

  static const String serverKey =
      "AAAAVOAdcXo:APA91bEdB9_Jx1FlAyuCosUTzumU5MlIiVrdF-9TaWU_o4DJznr-PVDLcVSdnWIt6vpwszO0yAwL7rLVSye51R61tIpIaGJqtXUPrNRDERT4gca_yxSSJfh9zhYcwuv5hLT1QMCHImon";

  static sendPush({
    String topic,
    String token,
    int liveTimeInSeconds = (Duration.secondsPerDay * 7),
    String title,
    String body,
    String image,
    Map data,
    String tag,
  }) async {
    String fcmToken = topic != null ? '/topics/$topic' : token;
    data = data ?? Map();
    data['click_action'] = 'FLUTTER_NOTIFICATION_CLICK';
    data['id'] = '1';
    data['status'] = 'done';
    client.post(
      Uri.parse('https://fcm.googleapis.com/fcm/send'),
      body: json.encode({
        'notification': {
          'body': body,
          'title': title,
          'image': image,
          'icon': "ic_notify",
          'color': "#ffffff",
          'tag': tag
        },
        'data': data,

        // Set Android priority to "high"
        'android': {
          'priority': "high",
        },
        // Add APNS (Apple) config
        'apns': {
          'payload': {
            'aps': {
              'contentAvailable': true,
            },
          },
          'headers': {
            "apns-push-type": "background",
            "apns-priority":
                "5", // Must be `5` when `contentAvailable` is set to true.
            "apns-topic": "com.sparkbuz", // bundle identifier
          },
        },
        'to': fcmToken,
        'time_to_live': liveTimeInSeconds
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'key=$serverKey',
      },
    );
  }
}
