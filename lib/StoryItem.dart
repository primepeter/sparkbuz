import 'dart:async';
import 'dart:io';

import 'package:cached_video_player/cached_video_player.dart';
import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';

import 'AppEngine.dart';
import 'MainAdmin.dart';
import 'assets.dart';
import 'basemodel.dart';

class VideoManager {
  final CachedVideoPlayerController controller;
  final String id;

  VideoManager(this.controller, this.id);
}

class StoryItem extends StatefulWidget {
  final List<BaseModel> stories;
  final int storyPage, storyIndex, maxStories;
  const StoryItem(
      {Key key,
      @required this.stories,
      this.storyPage = 0,
      this.storyIndex = 0,
      @required this.maxStories})
      : super(key: key);
  @override
  _StoryItemState createState() => _StoryItemState();
}

class _StoryItemState extends State<StoryItem> with TickerProviderStateMixin {
  AnimationController animController;
  Animation<double> animation;

  int seconds = 6;
  double resumeSecond = 0;
  double progressValue = 0;

  List<BaseModel> stories = [];
  PageController pageController = PageController();
  List<CachedVideoPlayerController> controllers = [];
  List<VideoManager> videoManager = [];

  int storyPage = 0;
  int maxStories = 0;
  int currentPage = 0;

  BaseModel pageStory;
  String fullName = "";
  String imageUrl = "";
  List taggedPersons = [];
  String message = "";
  bool myPost = false;
  String time = "";

  bool tagShowing = false;
  bool tappedDown = false;
  int tappedUpTime = 0;
  bool pagePaused = false;
  bool lastRightStory = false;
  List<StreamSubscription> subs = [];
  int changeDelay = 0;
  bool disposed = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    var stopStories = stopStoriesController.stream.listen((event) {
      animController?.repeat();
      animController?.stop();
      disposeVideo();
      print("page changed @this point....");
      changeDelay = 2;
    });

    subs.add(stopStories);

    var refreshAnimation = refreshAnimationController.stream.listen((event) {
      animController?.stop();
      pauseAnyVideo();
      disposeVideo();
      resumeSecond = progressValue = 0;
      seconds = event;
      setState(() {});
      loadAnimation();
    });
    subs.add(refreshAnimation);

    maxStories = widget.maxStories;
    stories = widget.stories;
    storyPage = widget.storyPage;
    currentPage = widget.storyIndex;
    pageController = PageController(initialPage: currentPage);
    pageController.addListener(pageListener);
    displayCurrentProfile(0);
    loadVideosPlus();
    loadAnimation();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    disposed = true;
    disposeVideo();
    pageController.removeListener(pageListener);
    animation?.removeListener(pageListener);
    animController?.dispose();
    for (var s in subs) s?.cancel();
    super.dispose();
  }

  loadVideosPlus() async {
    stories = widget.stories;
    for (int p = 0; p < stories.length; p++) {
      BaseModel bm = stories[p];
      CachedVideoPlayerController control;
      bool playThis = widget.storyIndex == p;
      String objectId = bm.getObjectId();
      String path = bm.getString(IMAGE_PATH);
      String videoUrl = bm.getString(IMAGE_URL);

      if (!bm.isVideo) {
        print("null p $p not a video");
        videoManager.add(VideoManager(null, objectId));
        if (p == 0) startTimer();
        continue;
      }

      /*if (editMode)
        control = CachedVideoPlayerController.file(File(path));
      else*/
      control = CachedVideoPlayerController.network(videoUrl);
      /*control.initialize().then((value) {
        int index = videoManager.indexWhere((e) => e.id == objectId);
        if (index == -1) {
          videoManager.add(VideoManager(control, objectId));
        } else {
          videoManager[index] = VideoManager(control, objectId);
        }
        // if (playThis) {
        //   //control.play();
        //   startTimer();
        // }
        if (mounted) setState(() {});
        print("Has been reopened video but needs to be re-initialized!!!");
      });*/

      await control.initialize();
      int index = videoManager.indexWhere((e) => e.id == objectId);
      if (index == -1) {
        videoManager.add(VideoManager(control, objectId));
      } else {
        videoManager[index] = VideoManager(control, objectId);
      }
      if (mounted) setState(() {});
    }
    final allVideos =
        videoManager.where((element) => element.controller != null).toList();
    // if (allVideos.isEmpty)
    Future.delayed(Duration(seconds: 1), () => startTimer());
  }

  loadAnimation({bool forward = false}) {
    animController =
        AnimationController(duration: Duration(seconds: seconds), vsync: this);
    animation = Tween(begin: 0.0, end: 1.0).animate(animController);
    /*animController.addListener(() {
      bool completed = animation.status == AnimationStatus.completed;
      progressValue = animation.value;
      if (completed) {
        seconds = 6;
        progressValue = 0;
        if (mounted) setState(() {});
        changePage(false);
      }
      if (mounted) setState(() {});
    });*/
    animation.addListener(animationListener);
    if (forward) animController?.forward(from: 0);
  }

  animationListener() {
    bool completed = animation.status == AnimationStatus.completed;
    progressValue = animation.value;
    if (completed) {
      seconds = 6;
      progressValue = 0;
      if (mounted) setState(() {});
      changePage(false);
    }
    if (mounted) setState(() {});
  }

  displayCurrentProfile(int p) {
    pageStory = stories[p];
    fullName = pageStory.getName();
    imageUrl = pageStory.userImage;
    taggedPersons = pageStory.getList(TAGGED_PERSONS);
    message = pageStory.getString(MESSAGE);
    myPost = pageStory.myItem();
    time = getTimeAgo(pageStory.getInt(TIME_UPDATED));
    setState(() {});
  }

  pauseAnyVideo() async {
    //pause mine.....
    for (int p = 0; p < videoManager.length; p++) {
      final manager = videoManager[p];
      if (null == manager.controller) {
        continue;
      }
      try {
        videoManager[p].controller.pause();
        if (mounted) setState(() {});
        await Future.delayed(Duration(milliseconds: 500));
      } catch (e) {
        print(e);
      }
    }
  }

  playVideoInPage(int p, {bool pause = false}) {
    // final controller = controllers[p];
    // if (controller == null) {
    //   print("show error failed to load video!");
    //   return;
    // }
    // if (pause)
    //   controller?.pause();
    // else
    //   controller?.play();
    // if (mounted) setState(() {});
    if (videoManager.isEmpty) return;
    final manager = videoManager[p];
    if (null == manager.controller) {
      print("show error failed to load video!");
      return;
    }
    if (pause)
      manager.controller?.pause();
    else
      manager.controller?.play();
    if (mounted) setState(() {});
  }

  disposeVideo() async {
    for (int p = 0; p < videoManager.length; p++) {
      final manager = videoManager[p];
      if (null == manager.controller) {
        continue;
      }
      try {
        manager.controller.pause();
        videoManager[p] = VideoManager(null, manager.id);
        if (mounted) setState(() {});
        await Future.delayed(Duration(seconds: 1));
        await manager.controller.dispose();
      } catch (e) {
        print(e);
      }
    }
  }

  pageListener() {
    // animController?.stop();
    // seconds = 6;
    // int lastIndex = stories.length - 1;
    //bool isLastStory = currentPage == (toLeft ? 0 : lastIndex);
  }

  changePage(bool toLeft) {
    animController?.stop();
    seconds = 6;
    int lastIndex = stories.length - 1;
    bool isLastStory = currentPage == (toLeft ? 0 : lastIndex);
    pauseAnyVideo();

    if (isLastStory && !toLeft) {
      lastRightStory = true;
      setState(() {});
      print("this is the last story on the right @page $storyPage");
      //pauseAnyVideo();
      //if(lastStoryPageUp&&)
      storyPageChangeController.add(currentPage);
      return;
    }

    if (isLastStory && toLeft) {
      print("this is the last story on the left @page $storyPage");
      //pauseAnyVideo();
      storyPageChangeController.add(currentPage);
      //closeStoryPageController.add(true);
      return;
    }

    if (toLeft) {
      lastRightStory = false;
      pageController.previousPage(
          duration: Duration(milliseconds: 800), curve: Curves.ease);
      Future.delayed(Duration(seconds: 1), () => startTimer());
      if (mounted) setState(() {});
      return;
    }

    if (!toLeft) {
      pageController.nextPage(
          duration: Duration(milliseconds: 800), curve: Curves.ease);
      if (mounted) setState(() {});
      Future.delayed(Duration(seconds: 1), () => startTimer());
      return;
    }
  }

  pausePage() {
    print("Pausing page at $progressValue");
    playVideoInPage(currentPage, pause: true);
    pagePaused = true;
    animController?.stop();
  }

  resumePage() {
    print("Resuming page at $progressValue");
    pagePaused = false;
    animController?.forward(from: progressValue);
    playVideoInPage(
      currentPage,
    );
  }

  startTimer() async {
    // return;

    if (disposed) {
      return;
    }

    print("timer has started");
    //currentPage = pageController.page.round();
    if (videoManager.isNotEmpty) {
      final manager = videoManager[currentPage];
      if (manager.controller == null) {
        seconds = 6;
        animController.duration = Duration(seconds: seconds);
      }

      if (null != manager.controller) {
        await Future.delayed(Duration(milliseconds: 500));
        print("A video is here... $currentPage");
        videoManager[currentPage].controller?.seekTo(Duration(seconds: 0));
        await videoManager[currentPage].controller?.play();
        seconds = videoManager[currentPage].controller.value.duration.inSeconds;
        animController.duration = Duration(seconds: seconds);
        // animController?.forward(from: 0);
        animController?.forward(from: 0);
      }

      return;
    }

    //await Future.delayed(Duration(seconds: changeDelay));
    //loadAnimation(forward: true);
    animController?.forward(from: 0);
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        storyGesture(storyPages()),
        indicatorItem(),
      ],
    );
  }

  storyGesture(Widget child) {
    return Stack(
      children: [
        GestureDetector(
          onTapDown: (td) {
            print("tapping down");
            if (tappedDown) return;
            tappedDown = true;
            int now = DateTime.now().millisecondsSinceEpoch;
            Future.delayed(Duration(milliseconds: 200), () {
              int diff = now - tappedUpTime;
              if (diff < 200) {
                bool toLeft = td.globalPosition.dx <= 100;
                print("Changing Page");
                //oldStory = stories[currentPage][pageItemPosition];
                progressValue = 0;
                //changePage(toLeft);
              } else {
                pausePage();
              }
              tappedDown = false;
            });
          },
          onTapUp: (td) {
            print("tapping up");
            tappedUpTime = DateTime.now().millisecondsSinceEpoch;
            if (pagePaused) resumePage();
          },
          child: child,
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: GestureDetector(
            onTap: () {
              progressValue = 0;
              changePage(true);
            },
            child: Container(
              color: transparent,
              width: 100,
            ),
          ),
        ),
        Align(
          alignment: Alignment.centerRight,
          child: GestureDetector(
            onTap: () {
              progressValue = 0;
              changePage(false);
            },
            child: Container(
              color: transparent,
              width: 100,
            ),
          ),
        ),
      ],
    );
  }

  storyPages() {
    return PageView.builder(
      controller: pageController,
      physics: NeverScrollableScrollPhysics(),
      scrollDirection: Axis.horizontal,
      onPageChanged: (p) {
        currentPage = p;
        setState(() {});

        return;

        animController?.stop();
        seconds = 6;
        progressValue = 0;
        setState(() {});
        //pauseAnyVideo();
        displayCurrentProfile(p);
        startTimer();
      },
      itemCount: stories.length,
      itemBuilder: (c, p) {
        BaseModel story = stories[p];
        bool isVideo = story.isVideo;

        if (isVideo) return videoItem(p, story);
        return imageItem(story);
      },
    );
  }

  indicatorItem() {
    return Container(
      padding: EdgeInsets.only(left: 10, right: 10, top: 45),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(bottom: 10, top: 5, left: 5, right: 5),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                gradient: LinearGradient(colors: [
                  black.withOpacity(.2),
                  black.withOpacity(.1),
                  black.withOpacity(.2),
                ], stops: [
                  0,
                  0.5,
                  1.0
                ], begin: Alignment.topLeft, end: Alignment.topRight)),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  children: List.generate(stories.length, (p) {
                    return Flexible(
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(5),
                        child: Container(
                          height: 8,
                          padding: EdgeInsets.all(2),
                          child: LinearProgressIndicator(
                            value: (p == currentPage && lastRightStory)
                                ? 1
                                : (p == currentPage && progressValue != 1)
                                    ? progressValue
                                    : (currentPage > p)
                                        ? 1
                                        : 0,
                            backgroundColor: white.withOpacity(0.4),
                            valueColor: AlwaysStoppedAnimation<Color>(white),
                          ),
                        ),
                      ),
                    );
                  }),
                ),
                addSpace(10),
                Row(
                  children: [
                    imageHolder(50, imageUrl, stroke: 2, strokeColor: white),
                    addSpaceWidth(10),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(myPost ? "Your Story" : fullName,
                            style: textStyle(true, 14, white)),
                        //5.spaceHeight(),
                        Text(time, style: textStyle(false, 12, white)),
                      ],
                    )
                  ],
                ),
              ],
            ),
          ),
          Spacer(),
          Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                if (message.isNotEmpty)
                  Container(
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.all(10),
                    alignment: Alignment.centerLeft,
                    decoration: BoxDecoration(
                        color: black.withOpacity(.2),
                        border:
                            Border.all(width: 2, color: white.withOpacity(.1)),
                        borderRadius: BorderRadius.circular(8)),
                    child: ReadMoreText(
                      message,
                      fontSize: 16,
                      textColor: white,
                    ),
                  ),
                if (myPost)
                  Row(
                    children: [
                      addSpaceWidth(10),
                      GestureDetector(
                        onTap: () {
                          tagShowing = true;
                          setState(() {});
                          clickTagged(context, pageStory);
                        },
                        child: Container(
                          width: 35.0 * taggedPersons.length.clamp(0, 8),
                          height: 50,
                          child: Stack(
                            children: List.generate(
                                taggedPersons.length.clamp(0, 8), (index) {
                              int max = taggedPersons.length.clamp(0, 8);
                              BaseModel model =
                                  BaseModel(items: taggedPersons[index]);
                              return Container(
                                height: 40,
                                width: 40,
                                margin: EdgeInsets.only(left: index * 30.0),
                                padding: EdgeInsets.all(1),
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        width: .5,
                                        color: black.withOpacity(.1)),
                                    color: white,
                                    shape: BoxShape.circle),
                                child: Stack(
                                  children: [
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(25),
                                      child: Stack(
                                        children: [
                                          Container(
                                            height: 40,
                                            width: 40,
                                            color: appColor.withOpacity(.6),
                                          ),
                                          if (model.userImage.isNotEmpty)
                                            ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(20),
                                              child: CachedNetworkImage(
                                                imageUrl: model.userImage,
                                                height: 40,
                                                width: 40,
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                        ],
                                      ),
                                    ),
                                    if (max - 1 > 3)
                                      Container(
                                        height: 40,
                                        width: 40,
                                        child: Center(
                                            child: Text(
                                          "${max - index}+",
                                          style: textStyle(true, 10, white),
                                        )),
                                        decoration: BoxDecoration(
                                            color: black.withOpacity(.4),
                                            shape: BoxShape.circle),
                                      )
                                  ],
                                ),
                              );
                            }),
                          ),
                        ),
                      ),
                      Spacer(),
                      RaisedButton(
                        onPressed: () {
                          deleteStoryAtController.add(currentPage);
                          pageStory.deleteItem();
                          allStories.removeWhere((e) =>
                              e.getObjectId() == pageStory.getObjectId());
                          stories.removeWhere((e) =>
                              e.getObjectId() == pageStory.getObjectId());
                          setState(() {});
                        },
                        shape: CircleBorder(),
                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        child: Icon(
                          Icons.delete,
                          size: 18,
                          color: Colors.red,
                        ),
                      )
                    ],
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  videoItem(int p, BaseModel story) {
    // final controller = controllers[p];
    // if (null == controller) return videoLoading;

    if (videoManager.isEmpty) return videoBuffering(p);
    final controller = videoManager[p].controller;
    if (null == controller) return videoBuffering(p);

    return Center(
      child: AspectRatio(
        aspectRatio: controller.value.aspectRatio,
        child: CachedVideoPlayer(controller),
      ),
    );
  }

  videoBuffering(int p) {
    BaseModel bm = stories[p];
    String thumbPath = bm.getString(THUMBNAIL_PATH);
    String thumbUrl = bm.getString(THUMBNAIL_URL);
    bool onlineImage = thumbUrl.contains("http");

    return Stack(
      alignment: Alignment.center,
      children: [
        if (onlineImage)
          Center(
            child: Image.network(
              thumbUrl,
              fit: BoxFit.cover,
              alignment: Alignment.center,
              width: getScreenWidth(context),
            ),
          )
        else
          Center(
            child: Image.file(
              File(thumbPath),
              fit: BoxFit.cover,
              alignment: Alignment.center,
              width: getScreenWidth(context),
            ),
          ),
        videoLoading,
        /* Container(
          //color: Colors.red,
          child: Center(
            child: Container(
              height: 60,
              width: 60,
              padding: 3.padAll(),
              margin: 6.padAll(),
              decoration: BoxDecoration(
                color: black.withOpacity(.5),
                border: Border.all(color: white, width: 2),
                shape: BoxShape.circle,
              ),
              alignment: Alignment.center,
              child: Icon(
                Icons.play_arrow,
                color: white,
                size: 20,
              ),
            ),
          ),
        ),*/
      ],
    );
  }

  imageItem(BaseModel story) {
    String url = story.getString(IMAGE_URL);
    return ExtendedImage.network(
      url,
      fit: BoxFit.cover,
      cache: true,
      alignment: Alignment.center,
      mode: ExtendedImageMode.gesture,
      loadStateChanged: (ExtendedImageState state) {
        switch (state.extendedImageLoadState) {
          case LoadState.loading:
            return Center(
              child: Container(
                width: 30,
                height: 30,
                child: CircularProgressIndicator(
                  //value: 20,
                  valueColor: AlwaysStoppedAnimation<Color>(white),
                  strokeWidth: 2,
                ),
              ),
            );
            break;

          ///if you don't want override completed widget
          ///please return null or state.completedWidget
          //return null;
          case LoadState.completed:
            //startTimer();
            return state.completedWidget;
            break;
          case LoadState.failed:
            return Container(
              color: transparent,
            );

          default:
            return Container();
        }
      },
    );
  }
}
