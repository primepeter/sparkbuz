import 'package:flutter/material.dart';

enum EditType { filter, text, image }

// class CamEditor extends StatefulWidget {
//   final EditType editType;
//   final File videoFile;
//   final File thumbnailFile;
//   const CamEditor(
//       {Key key,
//       @required this.editType,
//       @required this.videoFile,
//       this.thumbnailFile})
//       : super(key: key);
//   @override
//   _CamEditorState createState() => _CamEditorState();
// }
//
// class _CamEditorState extends State<CamEditor> {
//   CachedVideoPlayerController currentPlayer;
//   File videoFile;
//   bool controllerReady = false;
//   Color spectrumColor = Colors.transparent;
//   List<BaseModel> filters = [];
//
//   String get buttonTitle {
//     switch (widget.editType) {
//       case EditType.filter:
//         // TODO: Handle this case.
//         return "Apply Filter";
//         break;
//       case EditType.text:
//         // TODO: Handle this case.
//         return "Apply Text";
//         break;
//       case EditType.image:
//         // TODO: Handle this case.
//         return "Apply Image";
//         break;
//     }
//   }
//
//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//     videoFile = widget.videoFile;
//   }
//
//   @override
//   void dispose() {
//     // TODO: implement dispose
//     super.dispose();
//     currentPlayer?.dispose();
//   }
//
//   @override
//   void didChangeDependencies() {
//     // TODO: implement didChangeDependencies
//     super.didChangeDependencies();
//     currentPlayer = CachedVideoPlayerController.file(videoFile)
//       ..initialize().then((value) {
//         controllerReady = true;
//         currentPlayer.setLooping(false);
//         currentPlayer.play();
//         setState(() {});
//       });
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: black,
//       body: Column(
//         children: [
//           Expanded(
//             child: Stack(
//               children: [
//                 page(),
// //                Center(
// //                  child: Image.file(
// //                    widget.thumbnailFile,
// //                    fit: BoxFit.cover,
// //                  ),
// //                ),
//
//                 Align(
//                     alignment: Alignment.bottomCenter,
//                     child: ColorPicker(
//                       onColorSelected: (spectrumColor, selectedColor) {
//                         setState(() {
//                           this.spectrumColor = spectrumColor;
//                         });
//                       },
//                     )),
//                 closeButton,
//               ],
//             ),
//           ),
//           Container(
//             padding: const EdgeInsets.all(15.0),
//             child: Row(
//               children: [
//                 Flexible(
//                   child: FlatButton(
//                       onPressed: () async {
//                         // showProgress(true, context, msg: "Processing...");
//                         // final tapiocaBalls = [
//                         //   TapiocaBall.filterFromColor(spectrumColor),
//                         // ];
//                         // var tempDir = await getTemporaryDirectory();
//                         // final path = '${tempDir.path}/result.mp4';
//                         // final cup = Cup(Content(videoFile.path), tapiocaBalls);
//                         // cup.suckUp(path).then((_) {
//                         //   showProgress(false, context);
//                         //   Future.delayed(Duration(seconds: 1), () {
//                         //     Navigator.pop(context, path);
//                         //   });
//                         // });
//                       },
//                       color: AppConfig.appColor,
//                       padding: EdgeInsets.all(16),
//                       child: Center(
//                           child: Text(
//                         buttonTitle,
//                         style: textStyle(true, 16, white),
//                       ))),
//                 ),
//               ],
//             ),
//           )
//         ],
//       ),
//     );
//   }
//
//   Widget get closeButton => GestureDetector(
//         child: Container(
//           margin: EdgeInsets.only(top: 25),
//           padding: EdgeInsets.all(14),
//           decoration: BoxDecoration(
//               color: red,
//               borderRadius: BorderRadius.only(
//                   topRight: Radius.circular(30),
//                   bottomRight: Radius.circular(30))),
//           child: Icon(
//             Icons.clear,
//             color: white,
//             size: 18,
//           ),
//         ),
//         onTap: Navigator.of(context).pop,
//       );
//
//   page() {
//     if (!controllerReady) return videoLoading;
//
//     bool isPlaying = currentPlayer.value.isPlaying;
//
//     return Stack(
//       alignment: Alignment.center,
//       children: [
//         AspectRatio(
//             aspectRatio: currentPlayer.value.aspectRatio,
//             child: CachedVideoPlayer(currentPlayer)),
//         Container(
//           color: spectrumColor.withOpacity(.5),
//         ),
//         Tapped(
//           child: Container(
//             color: transparent,
//             alignment: Alignment.center,
//             child: AnimatedContainer(
//                 duration: Duration(milliseconds: 400),
//                 height: 50,
//                 width: 50,
//                 decoration: BoxDecoration(
//                     border: Border.all(color: white, width: 1),
//                     color: black.withOpacity(.5),
//                     shape: BoxShape.circle),
//                 child: Icon(
//                   isPlaying ? Icons.pause : Icons.play_arrow,
//                   color: white,
//                   size: 20,
//                 )),
//           ),
//           onTap: () {
//             if (isPlaying)
//               currentPlayer.pause();
//             else
//               currentPlayer.play();
//
//             setState(() {});
//           },
//         ),
//       ],
//     );
//   }
// }

List<Color> get pickerColors => [
      Colors.green.shade100,
      Colors.yellow.shade300,
      Colors.red.shade200,
      Colors.blue.shade300,
      Colors.grey.shade900,
      Color(0XFF808080),
      Color(0XFF778899),
      Color(0XFFF0FFFF),
      Color(0XFFD2691E),
      Color(0XFFDAA520),
      Color(0XFF000080),
      Color(0XFF4169E1),
      Color(0XFF87CEEB),
      Color(0XFF00FF7F),
      Color(0XFF8A2B2),
    ];

class ColorPicker extends StatelessWidget {
  final Function(Color spectrumColor, Color selectableColor) onColorSelected;

  const ColorPicker({Key key, this.onColorSelected}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: ClipPath(
        // clipper:
        //     SpectrumClipper(lineThickness: 10, colorCount: pickerColors.length),
        child: ColorSpectrum(
          colors: pickerColors,
          onColorSelected: this.onColorSelected,
        ),
      ),
    );
  }
}

class SpectrumClipper extends CustomClipper<Path> {
  final double lineThickness;
  final int colorCount;

  SpectrumClipper({@required this.lineThickness, @required this.colorCount});

  @override
  Path getClip(Size size) {
    Path path = Path();
    final double blobDiameter = size.height;
    final double separatorSpace = (size.width - (colorCount * blobDiameter)) /
        (colorCount - 1).toDouble();

    _addHorizontalLine(path, size, lineThickness);
    for (int i = 0; i < colorCount; i++) {
      addSpectrumBlob(path, size, blobDiameter, separatorSpace, i);
    }
    return path;
  }

  void _addHorizontalLine(
      Path path, Size availableSpace, double lineThickness) {
    path.addRect(Rect.fromLTWH(0, (availableSpace.height - lineThickness) / 2,
        availableSpace.width, lineThickness));
  }

  void addSpectrumBlob(Path path, Size availableSpace, double blobDiameter,
      double separatorSpace, int index) {
    final double blobRadius = blobDiameter / 2;
    path.addOval(Rect.fromCircle(
      center: Offset(blobRadius + (index * (blobDiameter + separatorSpace)),
          availableSpace.height / 2),
      radius: blobRadius,
    ));
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}

class ColorSpectrum extends StatefulWidget {
  final List<Color> colors;
  final Function(Color spectrumColor, Color selectableColor) onColorSelected;

  const ColorSpectrum({
    Key key,
    @required this.colors,
    this.onColorSelected,
  }) : super(key: key);

  @override
  _ColorSpectrumState createState() => _ColorSpectrumState();
}

class _ColorSpectrumState extends State<ColorSpectrum> {
  List<Color> colors = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    colors = widget.colors;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onPanDown: (d) {
        _selectColors(d.localPosition);
      },
      onHorizontalDragUpdate: (d) {
        _selectColors(d.localPosition);
      },
      child: Container(
        width: double.infinity,
        height: 50,
        decoration: BoxDecoration(
            gradient: LinearGradient(
          colors: pickerColors,
        )),
      ),
    );
  }

  void _selectColors(Offset touchPosition) {
    RenderBox renderBox = context.findRenderObject();
    final boxHeight = renderBox.size.height;
    final boxWidth = renderBox.size.width;
    final int colorCount = colors.length;
    final double blobDiameter = renderBox.size.height;
    final double blobRadius = blobDiameter / 2;
    final double separatorSpace =
        (boxWidth - (5 * blobDiameter)) / (colorCount - 1).toDouble();

    final double touchX = touchPosition.dx.clamp(0.0, boxWidth.toDouble());
    double fractionalTouchPosition =
        ((touchX - blobRadius) / (blobDiameter + separatorSpace));
    fractionalTouchPosition =
        fractionalTouchPosition.clamp(0.0, (colorCount - 1).toDouble());
    print("fractional touch position $fractionalTouchPosition");
    final int leftColorIndex = fractionalTouchPosition.floor();
    final int rightColorIndex = fractionalTouchPosition.ceil();
    final Color rightSelectableColor = colors[rightColorIndex];
    final Color leftSelectableColor = colors[leftColorIndex];
    final Color selectedColor =
        (fractionalTouchPosition - leftColorIndex) <= 0.5
            ? leftSelectableColor
            : rightSelectableColor;
    final Color spectrumColor = Color.lerp(leftSelectableColor,
        rightSelectableColor, fractionalTouchPosition - leftColorIndex);
    print("Color $spectrumColor");
    if (widget.onColorSelected != null)
      widget.onColorSelected(spectrumColor, selectedColor);
  }
}
