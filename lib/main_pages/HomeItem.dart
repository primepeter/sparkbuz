import 'dart:async';
import 'dart:io';

import 'package:cached_video_player/cached_video_player.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:line_icons/line_icons.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sparkbuz/AppConfig.dart';
import 'package:sparkbuz/AppEngine.dart';
import 'package:sparkbuz/MainAdmin.dart';
import 'package:sparkbuz/ShowPeople.dart';
import 'package:sparkbuz/ShowTopWinners.dart';
import 'package:sparkbuz/SparkIcon.dart';
import 'package:sparkbuz/admin/ShowUsers.dart';
import 'package:sparkbuz/assets.dart';
import 'package:sparkbuz/basemodel.dart';
import 'package:sparkbuz/basic/Tapped.dart';

import '../CreateReport.dart';
import '../Filterlayout.dart';
import '../ShowLikedVideos.dart';
import 'CameraSparkShare.dart';
import 'ShowVideoPost.dart';

int currentPageHomeItem = 0;

class HomeItem extends StatefulWidget {
  final BaseModel video;
  final int videoPage;
  const HomeItem({Key key, @required this.video, @required this.videoPage})
      : super(key: key);
  @override
  _HomeItemState createState() => _HomeItemState();
}

class _HomeItemState extends State<HomeItem> {
  final pageController = PageController();

  final profilePageController = PageController();
  int currentPageProfile = 0;

  CachedVideoPlayerController videoController;
  BaseModel theVideo;
  bool liked = false;
  List likedIds = [];

  BaseModel theUser;
  List<BaseModel> postList = [];
  List<BaseModel> likeList = [];
  bool postReady = false;
  bool likesReady = false;
  bool canPlay = true;

  final scrollController = ScrollController();
  final refreshController = RefreshController();

  List<StreamSubscription> subs = [];
  List followers, followings, likesCount = [];

  // @override
  // bool get wantKeepAlive => false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    theVideo = widget.video;
    theUser = widget.video;
    canPlay = currentPage == 0;
    likedIds = theVideo.getList(LIKES_COUNT);
    liked = likedIds.contains(userModel.getUserId());
    following = userModel.followings.contains(theUser.getUserId());

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      setState(() {});
      loadVideo();
      loadUser();
      loadLikes(false);
      loadPost(false);
    });

    scrollController.addListener(() {
      bool isScrolling = scrollController.position.isScrollingNotifier.value;
      bool showProfile = scrollController.position.pixels.isNegative ||
          (scrollController.position.pixels == 0);
      if (showProfile) {
        setState(() {
          hideProfile = false;
        });
      } else if (!hideProfile && isScrolling)
        setState(() {
          hideProfile = true;
        });
    });

    var a = pauseController.stream.listen((pause) {
      canPlay = pause;
      //if (globalPause) return;
      if (pause)
        videoController?.pause();
      else
        videoController?.play();
      setState(() {});
    });

    var b = loadController.stream.listen((b) {
      videoController?.pause();
      videoController = null;
      loadVideo();
    });
    subs.add(a);
    subs.add(b);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    videoController?.dispose();
    videoController = null;
    canPlay = true;
    for (var s in subs) s?.cancel();
    print('dispose');
    super.dispose();
  }

  // @override
  // didUpdateWidget(HomeItem oldWidget) {
  //   print('old ${oldWidget.video.getObjectId()} ${widget.videoPage}');
  //   print('new ${widget.video.getObjectId()} ${widget.videoPage}');
  //   if (oldWidget.video.getObjectId() != widget.video.getObjectId()) {
  //     print("reloading....");
  //     videoController?.pause();
  //     videoController = null;
  //     canPlay = true;
  //     if (mounted) setState(() {});
  //     //loadVideo();
  //     Future.delayed(Duration(milliseconds: 500), () {
  //       loadVideo();
  //     });
  //     super.didUpdateWidget(oldWidget);
  //   }
  // }

  bool loaded = false;
  DefaultCacheManager cacheManager = DefaultCacheManager();

  loadVideo() async {
    //canPlay = true;
    //await Future.delayed(Duration(seconds: 1));
    String objectId = theVideo.getObjectId();
    String videoUrl = theVideo.getString(VIDEO_URL);
    String videoPath = theVideo.getString(VIDEO_PATH);
    List seenby = theVideo.getList(SEEN_BY);
    bool watched = seenby.contains(userModel.getUserId());

    if (videoUrl.startsWith('http')) {
      videoController = CachedVideoPlayerController.network(videoUrl);
    } else if (videoUrl.isNotEmpty && theVideo.myItem()) {
      videoController = CachedVideoPlayerController.file(File(videoPath));
    }

    /*if (!theVideo.myItem())
      theVideo
        ..putInList(SEEN_BY, userModel.getUserId(), true)
        ..updateItems(updateTime: false);*/

    await Future.delayed(Duration(milliseconds: 500));

    if (/*null != theVideo &&*/ !theVideo.myItem() && !watched) {
      theVideo
        ..putInList(SEEN_BY, userModel.getUserId(), true)
        ..updateItems();
    }

    videoController
      ..initialize().then((value) {
        // if(widget.videoPage==0)  Future.delayed(Duration(milliseconds: 500));
        if (canPlay) videoController.play();
        videoController.setLooping(true);
        loaded = true;
        if (mounted) setState(() {});
      })
      ..addListener(() {
        //print("listened $loaded canPlay $canPlay");
        // if (videoController.value.initialized &&
        //     !videoController.value.isPlaying) {
        //   print("listened@@@ $loaded canPlay $canPlay");
        //   videoController.play();
        // }
        if (mounted) setState(() {});
      });
  }

  loadVideoCache() async {
    String objectId = theVideo.getObjectId();
    String videoUrl = theVideo.getString(VIDEO_URL);
    String videoPath = theVideo.getString(VIDEO_PATH);
    // FileInfo fileInfo = await cacheManager.getFileFromCache(objectId);
    //
    // bool exists = await fileInfo.file.exists();
    //
    // if (exists) {}

    if (videoUrl.startsWith('http')) {
      videoController = CachedVideoPlayerController.network(videoUrl);
    } else if (videoUrl.isNotEmpty && theVideo.myItem()) {
      videoController = CachedVideoPlayerController.file(File(videoPath));
    }
    videoController.initialize().then((value) {
      if (canPlay && currentPage == 0) videoController.play();
      videoController.setLooping(true);
      loaded = true;
      if (mounted) setState(() {});
    });
  }

  loadLikes(bool isNew) async {
    List local = [];
    FirebaseFirestore.instance
        .collection(POSTS_BASE)
        .where(LIKES_COUNT, arrayContains: theVideo.getUserId())
        .limit(20)
        .orderBy(TIME, descending: true)
        .startAt([
          !isNew
              ? (likeList.isEmpty
                  ? DateTime.now().millisecondsSinceEpoch
                  : likeList[likeList.length - 1].getTime())
              : (likeList.isEmpty ? 0 : likeList[0].getTime())
        ])
        .get()
        .then((shots) async {
          local = shots.docs;
          for (var doc in shots.docs) {
            final model = BaseModel(doc: doc);
            int p = likeList
                .indexWhere((e) => e.getObjectId() == model.getObjectId());
            if (p != -1) {
              likeList[p] = model;
            } else {
              likeList.add(model);
            }
          }

          if (isNew) {
            refreshController.refreshCompleted();
          } else {
            int oldLength = likeList.length;
            int newLength = local.length;
            if (newLength <= oldLength) {
              refreshController.loadNoData();
              refreshController.refreshCompleted();
              //canRefresh = false;
            } else {
              refreshController.loadComplete();
              refreshController.refreshCompleted();
            }
          }
          likesReady = true;
          likeList.sort((a, b) => b.getTime().compareTo(a.getTime()));
          if (mounted) setState(() {});
        });
  }

  loadPost(bool isNew) async {
    List local = [];
    FirebaseFirestore.instance
        .collection(POSTS_BASE)
        .where(USER_ID, isEqualTo: theVideo.getUserId())
        .limit(20)
        .orderBy(TIME, descending: true)
        .startAt([
          !isNew
              ? (postList.isEmpty
                  ? DateTime.now().millisecondsSinceEpoch
                  : postList[postList.length - 1].getTime())
              : (postList.isEmpty ? 0 : postList[0].getTime())
        ])
        .get()
        .then((shots) async {
          local = shots.docs;
          for (var doc in shots.docs) {
            final model = BaseModel(doc: doc);
            int p = postList
                .indexWhere((e) => e.getObjectId() == model.getObjectId());
            if (p != -1) {
              postList[p] = model;
            } else {
              postList.add(model);
            }
          }

          if (isNew) {
            refreshController.refreshCompleted();
          } else {
            int oldLength = postList.length;
            int newLength = local.length;
            if (newLength <= oldLength) {
              refreshController.loadNoData();
              refreshController.refreshCompleted();
              //canRefresh = false;
            } else {
              refreshController.loadComplete();
              refreshController.refreshCompleted();
            }
          }
          postReady = true;
          postList.sort((a, b) => b.getTime().compareTo(a.getTime()));
          if (mounted) setState(() {});
        });
  }

  loadUser() async {
    var sub = FirebaseFirestore.instance
        .collection(USER_BASE)
        .doc(theUser.getUserId())
        .snapshots()
        .listen((value) {
      print("loadUser ${null == value}");

      if (null == value) return;
      theUser = BaseModel(doc: value);
      followers = theUser.followers;
      followings = theUser.followings;
      likesCount = theUser.likesCount;
      followersIds = theUser.getList(FOLLOWERS);

      if (mounted) setState(() {});
    });
    subs.add(sub);
  }

  @override
  Widget build(BuildContext context) {
    //super.build(context);

    // print(videoController.value.aspectRatio);
    return PageView(
      controller: pageController,
      onPageChanged: (p) {
        if (p == 0) {
          videoController?.play();
          pageVideoController.add(false);
        } else {
          videoController?.pause();
          pageVideoController.add(true);
        }
        currentPageHomeItem = p;
        setState(() {});
      },
      children: [videoItem(), profileItem()],
    );
  }

  videoItem() {
    //print('CanPlay $canPlay ${widget.videoPage}');
    String thumbUrl = theVideo.getString(THUMBNAIL_URL);
    double ratio = theVideo.getDouble(ASPECT_RATIO);
    if (ratio == 0) ratio = 9 / 16;

    // 10000
    // print(videoController.value.aspectRatio);
    if (null == videoController || !videoController.value.initialized) {
      return AspectRatio(
          aspectRatio: ratio,
          child: Stack(
            alignment: Alignment.center,
            children: [
              Center(
                child: Image.network(
                  thumbUrl,
                  fit: BoxFit.cover,
                  alignment: Alignment.center,
                  width: getScreenWidth(context),
                ),
              ),
              // Center(
              //   child: CachedNetworkImage(
              //     imageUrl: thumbUrl,
              //     alignment: Alignment.center,
              //     fit: BoxFit.cover,
              //     //height: getScreenHeight(context) - 350,
              //     //width: getScreenWidth(context)
              //   ),
              // ),
              videoLoading,
              onScreenControls(),
            ],
          ));
    }
    return Stack(
      //alignment: Alignment.center,
      children: [
        /*Center(
          child: Image.network(
            thumbUrl,
            fit: BoxFit.cover,
            alignment: Alignment.center,
            width: getScreenWidth(context),
          ),
        ),*/
        Center(
          child: AspectRatio(
              aspectRatio: videoController.value.aspectRatio,
              child: CachedVideoPlayer(videoController)),
        ),
        FilterLayout(
          filters: theVideo.getListModel(FILTERS),
          enableFilter: false,
        ),
        Align(
          alignment: Alignment.topCenter,
          child: Container(
            padding: EdgeInsets.only(top: 30, right: 10, left: 10),
            child: Row(
              children: [
                if (theVideo.getBoolean(SHOW_LOGO))
                  Image.asset(
                    spark_logo,
                    height: 25,
                    fit: BoxFit.cover,
                  ),
                Spacer(),
                FlatButton(
                    onPressed: () {
                      pushAndResult(
                        context,
                        ShowTopWinners(),
                      );
                    },
                    color: appYellow,
                    //minWidth: 80,
                    height: 30,
                    padding: EdgeInsets.all(4),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                    child: Row(
                      children: [
                        Image.asset(
                          "assets/icons/crown.png",
                          height: 20,
                          width: 20,
                          fit: BoxFit.cover,
                        ),
                        addSpaceWidth(2),
                        Text(
                          'Top Winners',
                          style: textStyle(false, 12, black),
                        ),
                      ],
                    ))
              ],
            ),
          ),
        ),
        Tapped(
          showIcon: !videoController.value.isPlaying,
          // showIcon: videoListController.currentPlayer.value.isPlaying,
          child: Container(
            color: transparent,
            alignment: Alignment.center,
            child: AnimatedContainer(
                duration: Duration(milliseconds: 400),
                child: Icon(
                  videoController.value.isPlaying
                      ? Icons.pause
                      : Icons.play_arrow_sharp,
                  color: white.withOpacity(.3),
                  size: 100,
                )),
          ),
          onTapDown: () {
            loadVideo();
          },
          onTap: () {
            if (videoController.value.isPlaying)
              videoController.pause();
            else
              videoController.play();
            setState(() {});
          },
        ),
        onScreenControls()
      ],
    );
  }

  bool showOptions = true;
  bool showNotify = false;

  onScreenControls() {
    String username = theVideo.getUserName();
    String description = theVideo.getString(DESCRIPTION);
    String category = theVideo.getString(CATEGORY);

    String likes = formatToK(likedIds.length);
    // String likes = formatToK(theVideo.getList(LIKES_COUNT).length);
    String comments = formatToK(theVideo.getList(COMMENT_LIST).length);
    String shares = formatToK(theVideo.getList(SHARE_LIST).length);
    String photo = theVideo.getString(USER_IMAGE);

    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.only(bottom: 100, right: 15, left: 15),
        decoration: BoxDecoration(
            color: black.withOpacity(.4),
            border: Border.all(color: white.withOpacity(.1)),
            borderRadius: BorderRadius.circular(15)),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            InkWell(
              onTap: () {
                //print(theVideo.items);
                showOptions = !showOptions;
                if (showOptions) {
                  // loadVideo();
                  // print(theVideo.getString(VIDEO_URL));
                  // print(theVideo.getString(VIDEO_PATH));
                }
                if (mounted) setState(() {});
              },
              child: Row(
                children: [
                  Icon(
                    showOptions
                        ? Icons.keyboard_arrow_down
                        : Icons.keyboard_arrow_up,
                    color: white,
                  ),
                  Text(
                    "@$username",
                    style: textStyle(true, 16, white),
                  ),
                  Spacer(),
                  // Image.asset(
                  //   spark_logo,
                  //   height: 30,
                  //   fit: BoxFit.cover,
                  // ),
                ],
              ),
            ),
            addSpace(5),
            if (showOptions && description.isNotEmpty) ...[
              Text(
                description,
                style: textStyle(false, 14, white),
              ),
              addSpace(5),
            ],
            if (showOptions && category.isNotEmpty)
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Category -",
                    style: textStyle(false, 12, white),
                  ),
                  addSpaceWidth(5),
                  Text(
                    category,
                    style: textStyle(false, 14, white),
                  ),
                ],
              ),
            if (showOptions)
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    color: transparent,
                    child: Stack(
                      alignment: AlignmentDirectional.center,
                      children: <Widget>[
                        ClipRRect(
                          borderRadius: BorderRadius.circular(50),
                          child: Container(
                            height: 50,
                            width: 50,
                            child: userImageItem(context, theUser,
                                padLeft: false, strock: 1, size: 50),
                            decoration: BoxDecoration(
                                border: Border.all(
                                    color: Colors.white,
                                    width: 1.0,
                                    style: BorderStyle.solid),
                                color: Colors.black,
                                shape: BoxShape.circle),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 50),
                          height: 18,
                          width: 18,
                          child: Icon(Icons.add, size: 10, color: Colors.white),
                          decoration: BoxDecoration(
                              color: AppConfig.appColor,
                              shape: BoxShape.circle),
                        )
                      ],
                    ),
                  ),
                  if (showOptions)
                    Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          GestureDetector(
                            onTap: () {
                              onVideoLiked();
                              return;

                              liked = !liked;
                              theVideo
                                ..putInList(
                                    LIKES_COUNT, userModel.getUserId(), liked)
                                ..updateItems();
                              userModel
                                ..putInList(
                                    LIKES_COUNT, theVideo.getObjectId(), liked)
                                ..updateItems();
                              if (!liked) flareController.add(theVideo);
                              setState(() {});
                            },
                            child: Column(
                              children: [
                                Icon(
                                  Icons.favorite,
                                  color:
                                      liked ? appYellow : white.withOpacity(.5),
                                  size: 35,
                                ),
                                Text(
                                  likes,
                                  style: textStyle(true, 10, white),
                                )
                              ],
                            ),
                          ),
                          videoControlAction(
                              onTap: () {
                                pauseController.add(true);
                                pushAndResult(
                                    context,
                                    ShowVideoPost(
                                      videoModel: theVideo,
                                    ), result: (_) {
                                  if (null == _) return;
                                  theVideo = _;
                                  likedIds = theVideo.getList(LIKES_COUNT);
                                  liked =
                                      likedIds.contains(userModel.getUserId());
                                  videoController?.play();
                                  setState(() {});
                                });
                              },
                              icon: AppIcons.chat_bubble,
                              label: comments,
                              size: 25),
                          videoControlAction(
                              icon: AppIcons.reply,
                              label: shares,
                              size: 25,
                              onTap: () {
                                String appLink =
                                    appSettingsModel.getString(APP_LINK_IOS);
                                if (Platform.isAndroid)
                                  appLink = appSettingsModel
                                      .getString(APP_LINK_ANDROID);
                                shareApp(
                                    message:
                                        "Join SparkBuz and share memories with friends and family.\n $appLink");
                                theVideo
                                  ..putInList(
                                      SHARE_LIST, userModel.getUserId(), true)
                                  ..updateItems();
                                setState(() {});
                              }),
                        ],
                      ),
                    ),
                ],
              ),
          ],
        ),
      ),
    );
  }

  videoControlAction({IconData icon, String label, double size = 35, onTap}) {
    return Flexible(
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          //padding: EdgeInsets.only(top: 10, bottom: 10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                icon,
                color: Colors.white,
                size: size,
              ),
              addSpace(5),
              Text(
                label ?? "",
                style: textStyle(true, 10, white),
              )
            ],
          ),
        ),
      ),
    );
  }

  profileItem() {
    return Container(
      color: white,
      child: Column(
        children: [
          profileAppBar(),
          Expanded(
            child: Container(
              color: white,
              child: PageView(
                controller: profilePageController,
                onPageChanged: (p) {
                  currentPageProfile = p;
                  setState(() {});
                },
                children: List.generate(2, (index) => pageRefresh(index)),
              ),
            ),
          )
        ],
      ),
    );
  }

  List<BaseModel> get currentList {
    return currentPageProfile == 0 ? postList : likeList;
  }

  bool get currentReady {
    return currentPageProfile == 0 ? postReady : likesReady;
  }

  currentRefresh(bool v) {
    currentPageProfile == 0 ? loadPost(v) : loadLikes(v);
  }

  pageRefresh(int p) {
    if (!currentReady) return loadingLayout();

    if (currentList.isEmpty)
      return emptyLayout(
          Icons.search, "No Post", "You have not shared any videos yet.",
          clickText: "Refresh", click: () {
        if (p == 0)
          postReady = false;
        else
          likesReady = false;
        setState(() {});
        currentRefresh(false);
      });

    return SmartRefresher(
      controller: refreshController,
      enablePullDown: true,
      enablePullUp: false,
      header: WaterDropHeader(),
      footer: ClassicFooter(
        noDataText: currentList.length < 9
            ? ""
            : "Nothing more for now, check later...",
        textStyle: textStyle(false, 12, black.withOpacity(.7)),
      ),
      onLoading: () {
        if (currentPageProfile == 0)
          loadPost(false);
        else
          loadLikes(false);
      },
      onRefresh: () {
        if (currentPageProfile == 0)
          loadPost(true);
        else
          loadLikes(true);
      },
      child: ListView(
        controller: scrollController,
        shrinkWrap: true,
        padding: EdgeInsets.only(bottom: 100),
        children: [pageList()],
      ),
    );
  }

  pageList() {
    return Builder(
      builder: (ctx) {
        if (!postReady)
          return Container(
            height: (getScreenHeight(context) * .45) / 2,
            child: loadingLayout(),
          );

        if (currentList.isEmpty)
          return Container(
            height: (getScreenHeight(context) * .45) / 2,
            child: emptyLayout('assets/icons/youtube.png', "No Post",
                "${theUser.getString(USERNAME)} has not shared any videos yet.",
                nullColor: true),
          );

        return GridView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                childAspectRatio: .8,
                crossAxisCount: 3,
                mainAxisSpacing: 5,
                crossAxisSpacing: 5),
            padding: EdgeInsets.only(right: 5, left: 5, top: 10
                //top: getScreenHeight(context) * (hideProfile ? 0.23 : 0.38)
                ),
            itemCount: currentList.length,
            itemBuilder: (ctx, p) {
              String imageUrl = "https://bit.ly/3he5JOD";
              if (p.isEven) imageUrl = "https://bit.ly/3h8s5RE";

              BaseModel md = currentList[p];
              String image = md.getString(THUMBNAIL_URL);
              int seenBy = md.getList(SEEN_BY).length;

              return GestureDetector(
                onTap: () {
                  pushAndResult(
                      context,
                      ShowVideoPost(
                        videoModel: md,
                      ));
                },
                onLongPress: () {
                  if (md.myItem() || isAdmin) {
                    yesNoDialog(context, "Delete Video",
                        'Are you sure you want to delete this video?', () {
                      int p = playList.indexWhere(
                          (e) => e.model.getObjectId() == md.getObjectId());
                      if (p != -1) deleteVideoController.add(md);
                      currentList.remove(md);
                      postList.remove(md);
                      likeList.remove(md);
                      print("${md.getObjectId()} index $p ${playList.length}");
                      md.deleteItem();
                      setState(() {});
                    });
                  }
                },
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(15),
                  child: Container(
                    child: Stack(
                      children: [
                        CachedNetworkImage(
                          imageUrl: image,
                          height: double.infinity,
                          width: double.infinity,
                          fit: BoxFit.cover,
                          placeholder: (c, s) {
                            return placeHolder(double.infinity,
                                width: double.infinity);
                          },
                        ),
                        Container(
                          width: double.infinity,
                          height: double.infinity,
                          color: black.withOpacity(.09),
                          child: Center(
                            child: Container(
                              height: 40,
                              width: 40,
                              child: Icon(
                                Icons.play_arrow,
                                color: white.withOpacity(.7),
                                size: 25,
                              ),
                              /* decoration: BoxDecoration(
                                  color: black.withOpacity(0.8),
                                  border: Border.all(
                                      color: Colors.white, width: 1.5),
                                  shape: BoxShape.circle),*/
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: Container(
                            decoration: BoxDecoration(
                                color: black.withOpacity(.3),
                                borderRadius: BorderRadius.circular(5)),
                            padding: EdgeInsets.all(3),
                            margin: EdgeInsets.all(5),
                            child: Row(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Icon(
                                    LineIcons.eye,
                                    color: white,
                                    size: 15,
                                  ),
                                  addSpaceWidth(5),
                                  Text(
                                    seenBy.toString(),
                                    style: textStyle(false, 14, white),
                                  ),
                                ]),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            });
      },
    );
  }

  bool hideProfile = false;

  profileAppBar() {
    // bool following = theUser.followers.contains(userModel.getUserId());
    return Container(
        width: double.infinity,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(20),
                bottomLeft: Radius.circular(20)),
            color: white,
            boxShadow: [
              BoxShadow(color: black.withOpacity(.1), blurRadius: 9)
            ]),
        padding: EdgeInsets.only(
            top: 30,
            //right: 10, left: 10,
            bottom: 0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              padding: EdgeInsets.all(10),
              //height: 40,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    onTap: () {
                      // Navigator.pop(context);
                      pageController.jumpToPage(0);
                    },
                    child: Container(
                      width: 35,
                      height: 30,
                      color: transparent,
                      child: Icon(Icons.arrow_back_ios_rounded),
                    ),
                  ),
                  Container(
                    child: Text(
                      theUser.getName().isEmpty
                          ? '@${theUser.getUserName()}'
                          : theUser.getName(),
                      style: textStyle(true, 15, black),
                    ),
                  ),
                  if (!theUser.myItem())
                    InkWell(
                      onTap: () {
                        pushAndResult(
                            context,
                            CreateReport(
                              item: theUser,
                            ));
                      },
                      child: Container(
                        width: 35,
                        height: 30,
                        color: transparent,
                        child: Icon(Icons.report_outlined),
                      ),
                    )
                  else
                    Container(
                      height: 30,
                      width: 30,
                    )
                ],
              ),
            ),
            AnimatedContainer(
              duration: Duration(milliseconds: 60),
              curve: Curves.bounceInOut,
              padding: EdgeInsets.all(hideProfile ? 0 : 10),
              child: hideProfile
                  ? null
                  : Column(
                      children: [
                        //  addLine(.5, black.withOpacity(.09), 0, 0, 0, 10),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            userImageItem(context, theUser,
                                strock: 1,
                                size: 100,
                                backColor: black.withOpacity(.1),
                                upload: theUser.myItem()),
                            addSpaceWidth(8),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "@${theUser.getUserName().toLowerCase()}",
                                    style: textStyle(
                                        false, 18, black.withOpacity(.7)),
                                  ),
                                  // addSpace(9),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Container(
                                          padding: EdgeInsets.all(10),
                                          //width: 90,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(8),
                                              color: black.withOpacity(.06)),
                                          child: Center(
                                            child: Text(
                                              "${formatToK(postList.length)} videos",
                                              style: textStyle(true, 14,
                                                  black.withOpacity(.4)),
                                            ),
                                          ),
                                        ),
                                      ),
                                      addSpaceWidth(10),
                                      Expanded(
                                        child: FlatButton(
                                          onPressed: () {
                                            onFollow();
                                            return;

                                            theUser
                                              ..putInList(
                                                  FOLLOWERS,
                                                  userModel.getUserId(),
                                                  !following)
                                              ..updateItems();
                                            userModel
                                              ..putInList(
                                                  FOLLOWING,
                                                  theUser.getUserId(),
                                                  !following)
                                              ..updateItems();
                                            addToStatusParty(theUser,
                                                add: !following);

                                            pushNotificationToUsers(
                                              notifyType: following
                                                  ? NOTIFY_TYPE_FOLLOW
                                                  : NOTIFY_TYPE_UNFOLLOW,
                                              notifyId: theUser.getObjectId(),
                                              userIds: [theUser.getUserId()],
                                            );

                                            setState(() {});
                                          },
                                          color: appColor,
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(8)),
                                          child: Text(
                                            theUser.myItem()
                                                ? "Edit"
                                                : (following
                                                    ? "Following"
                                                    : "Follow"),
                                            style: textStyle(true, 14, white),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  if (theUser.myItem())
                                    FlatButton(
                                      onPressed: () {
                                        pauseController.add(true);
                                        pushAndResult(
                                            context, CameraSparkShare(),
                                            result: (_) {
                                          if (null == _) return;
                                          //if(currentPage==)  pauseController.add(false);
                                        });
                                      },
                                      color: appYellow,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(8)),
                                      child: Center(
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              'SparkShare',
                                              style: textStyle(true, 14, black),
                                            ),
                                            addSpaceWidth(5),
                                            Icon(
                                              LineIcons.video_camera,
                                              //size: 24,
                                              color: black,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        if (theUser.aboutMe.isNotEmpty)
                          Container(
                              margin: EdgeInsets.all(10),
                              padding: EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                  color: black.withOpacity(.02),
                                  borderRadius: BorderRadius.circular(15)),
                              child: ReadMoreText(
                                theUser.aboutMe,
                              )),
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: List.generate(3, (p) {
                              int count = theUser.followings.length;
                              String title = "Following";
                              List ids = theUser.followings;
                              if (p == 1) {
                                count = followersIds.length;
                                title = "Followers";
                                ids = followersIds;
                              }

                              if (p == 2) {
                                count = theUser.likesCount.length;
                                title = "Likes";
                                ids = theUser.likesCount;
                              }

                              return GestureDetector(
                                onTap: () {
                                  if (theUser.myItem() || isAdmin) if (p == 2)
                                    pushAndResult(context, ShowLikedVideos());
                                  else
                                    pushAndResult(
                                        context,
                                        ShowPeople(
                                            ids, title, 'No $title Found!'));
                                },
                                child: Container(
                                  color: transparent,
                                  padding: EdgeInsets.all(5),
                                  child: Row(
                                    children: [
                                      Column(
                                        children: [
                                          Text(
                                            formatToK(count),
                                            style: textStyle(true, 18, black),
                                          ),
                                          addSpace(1),
                                          Text(
                                            title,
                                            style: textStyle(false, 16,
                                                black.withOpacity(.5)),
                                          )
                                        ],
                                      ),
                                      if (p < 2) ...[
                                        addSpaceWidth(15),
                                        Container(
                                          height: 15,
                                          width: 1.5,
                                          color: black.withOpacity(.4),
                                        ),
                                      ]
                                    ],
                                  ),
                                ),
                              );
                            }),
                          ),
                        ),
                        addLine(.5, black.withOpacity(.09), 0, 10, 0, 10),
                      ],
                    ),
            ),
            GestureDetector(
              onTap: () {
                hideProfile = !hideProfile;
                setState(() {});
              },
              child: Container(
                alignment: Alignment.center,
                padding: EdgeInsets.all(10),
                color: transparent,
                child: Icon(
                  hideProfile
                      ? LineIcons.arrow_circle_down
                      : LineIcons.arrow_circle_up,
                  size: 30,
                  color: black.withOpacity(.5),
                ),
              ),
            ),
            if (hideProfile)
              Container(
                height: 45,
                width: double.infinity,
                margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                child: Card(
                  color: default_white,
                  elevation: 0,
                  clipBehavior: Clip.antiAlias,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(25)),
                      side:
                          BorderSide(color: black.withOpacity(.1), width: .5)),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(1, 1, 1, 1),
                    child: Row(
                      children: List.generate(2, (p) {
                        String title = p == 0 ? "Videos" : "Liked";
                        bool selected = p == currentPageProfile;
                        return Flexible(
                          child: GestureDetector(
                            onTap: () {
                              profilePageController.jumpToPage(p);
                            },
                            child: Container(
                                decoration: BoxDecoration(
                                    color: selected ? white : transparent,
                                    borderRadius: BorderRadius.circular(25),
                                    border: Border.all(
                                        color: !selected
                                            ? transparent
                                            : black.withOpacity(.1),
                                        width: .5)),
                                child: Center(
                                    child: Text(
                                  title,
                                  style: textStyle(
                                      selected,
                                      14,
                                      selected
                                          ? black
                                          : (black.withOpacity(.5))),
                                  textAlign: TextAlign.center,
                                ))),
                          ),
                          fit: FlexFit.tight,
                        );
                      }),
                    ),
                  ),
                ),
              ),
          ],
        ));
  }

  onVideoLiked() {
    String videoId = theVideo.getObjectId();
    String userId = userModel.getUserId();

    liked = likedIds.contains(userId);
    List videoLikes = userModel.getList(LIKES_COUNT);
    bool hasLiked = videoLikes.contains(videoId);

    if (liked) {
      likedIds.remove(userId);
    } else {
      likedIds.add(userId);
    }

    if (hasLiked) {
      videoLikes.remove(videoId);
    } else {
      videoLikes.add(videoId);
    }

    theVideo.put(LIKES_COUNT, likedIds);
    theVideo.updateItems();

    userModel.put(LIKES_COUNT, videoLikes);
    userModel.updateItems();

    if (!liked) flareController.add(theVideo);
    liked = !liked;
    setState(() {});

    //TODO Notify participants
    pushNotificationToUsers(
      notifyType: NOTIFY_TYPE_LIKE,
      notifyId: videoId,
      userIds: [videoId],
    );
  }

  List followersIds = [];
  bool following = false;

  onFollow() {
    String userId = userModel.getObjectId();
    String personId = theUser.getObjectId();

    following = followersIds.contains(userId);

    List followings = userModel.getList(FOLLOWING);
    bool hasFollowed = followings.contains(personId);

    if (following) {
      followersIds.remove(userId);
    } else {
      followersIds.add(userId);
    }

    if (hasFollowed) {
      followings.remove(personId);
    } else {
      followings.add(personId);
    }

    theUser.put(FOLLOWERS, followersIds);
    theUser.updateItems();

    userModel.put(FOLLOWING, followings);
    userModel.updateItems();

    following = !following;
    setState(() {});

    addToStatusParty(theUser, add: !following);
    pushNotificationToUsers(
      notifyType: following ? NOTIFY_TYPE_FOLLOW : NOTIFY_TYPE_UNFOLLOW,
      notifyId: personId,
      userIds: [personId],
    );
  }
}
