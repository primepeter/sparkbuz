import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:sparkbuz/AppEngine.dart';
import 'package:sparkbuz/assets.dart';
import 'package:sparkbuz/basemodel.dart';

import 'CreateAds.dart';

class ShowAds extends StatefulWidget {
  @override
  _ShowAdsState createState() => _ShowAdsState();
}

class _ShowAdsState extends State<ShowAds> {
  List adsList = [];
  bool setup = false;
  final List<StreamSubscription> subs = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadAds();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    for (var s in subs) s?.cancel();
  }

  loadAds() async {
    var ads = FirebaseFirestore.instance
        .collection(ADS_BASE)
        .where(USER_ID, isEqualTo: userModel.getUserId())
        .snapshots()
        .listen((value) {
      for (var doc in value.docs) {
        BaseModel model = BaseModel(doc: doc);
        int p =
            adsList.indexWhere((e) => e.getObjectId() == model.getObjectId());
        if (p != -1) {
          adsList[p] = model;
        } else {
          adsList.add(model);
        }
      }
      setup = true;
      if (mounted) setState(() {});
    });
    subs.add(ads);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.fromLTRB(0, 40, 0, 10),
            color: white,
            child: Row(
              children: <Widget>[
                InkWell(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Container(
                      width: 50,
                      height: 50,
                      child: Center(
                          child: Icon(
                        Icons.keyboard_backspace,
                        color: black,
                        size: 25,
                      )),
                    )),
                Text(
                  "My Ads",
                  style: textStyle(true, 25, black),
                ),
                Spacer(),
                Container(
                  height: 30,
                  width: 50,
                  child: new FlatButton(
                      padding: EdgeInsets.all(5),
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      onPressed: () {
                        pushAndResult(context, CreateAds());
                      },
                      shape: CircleBorder(),
                      child: Center(
                          child: Icon(
                        Icons.add,
                        size: 20,
                        color: black,
                      ))),
                ),
              ],
            ),
          ),
          page()
        ],
      ),
    );
  }

  page() {
    return Flexible(
      child: Builder(
        builder: (ctx) {
          if (!setup) return loadingLayout();
          if (adsList.isEmpty)
            return emptyLayout(
                Icons.trending_up, "No ads", "You have no ads with us",
                clickText: "Create One", click: () {
              pushAndResult(context, CreateAds());
            });
          return Container(
              child: ListView.builder(
            itemBuilder: (c, p) {
              return adsItem(p);
            },
            shrinkWrap: true,
            itemCount: adsList.length,
            padding: EdgeInsets.only(top: 10, right: 5, left: 5, bottom: 40),
          ));
        },
      ),
    );
  }

  adsItem(int p) {
    BaseModel model = adsList[p];
    //String imageUrl = getFirstPhoto(model);
    String imageUrl = getFirstPhoto(model.images);
    String title = model.getString(TITLE);
    String url = model.getString(ADS_URL);
    String placeName = model.getString(PLACE_NAME);
    final seenBy = model.getList(SEEN_BY);
    final likes = model.getList(LIKES_COUNT);
    final superLike = model.getList(SUPER_LIKE_LIST);
    final clicks = model.getList(CLICKS);
    int status = model.getInt(STATUS);
    bool declined = status == REJECTED;
    String reason = model.getString(REJECTED_MESSAGE);
    int endAt = model.getInt(ADS_EXPIRY);
    String invoiceLink = model.getString(INVOICE_LINK);
    bool hasInvoice = invoiceLink.isNotEmpty;
    bool hasPaid = model.getBoolean(HAS_PAID);
    bool ended = endAt > DateTime.now().millisecondsSinceEpoch;
    //(DateTime.now().millisecondsSinceEpoch - (Duration.millisecondsPerDay));

    print(
        "Expired $endAt E $ended  ${DateTime.fromMillisecondsSinceEpoch(endAt)}");

    String statusMsg = status == APPROVED
        ? "Approved"
        : status == PENDING
            ? "Pending Approval"
            : status == REJECTED
                ? "Rejected"
                : "Inactive";
    return GestureDetector(
      onTap: () {
        showListDialog(
          context,
          ['Edit', 'Delete'],
          (_) {
            if (_ == 0) {
              pushAndResult(
                  context,
                  CreateAds(
                    model: model,
                  ));
            }

            if (_ == 1) {
              model.deleteItem();
              adsList.removeAt(p);
              setState(() {});
            }
          },
        );
      },
      child: Container(
        decoration: BoxDecoration(
            color: white,
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: black.withOpacity(.09))),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Flexible(
              child: CachedNetworkImage(
                imageUrl: imageUrl,
                height: 200,
                width: double.infinity,
                fit: BoxFit.cover,
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: textStyle(true, 16, black),
                  ),
                  addSpace(10),
                  if (declined) ...[
                    Container(
                      decoration: BoxDecoration(
                          color: black.withOpacity(.09),
                          borderRadius: BorderRadius.circular(9)),
                      padding: EdgeInsets.all(10),
                      child: Text.rich(TextSpan(children: [
                        TextSpan(text: "Declined "),
                        TextSpan(
                            text: "$reason ", style: textStyle(true, 16, red))
                      ])),
                    ),
                    addSpace(10),
                  ],
                  Text.rich(TextSpan(children: [
                    TextSpan(text: "Active "),
                    TextSpan(text: "$ended ", style: textStyle(true, 14, black))
                  ])),
                  addSpace(10),
                  Text.rich(TextSpan(children: [
                    TextSpan(text: "Status "),
                    TextSpan(
                        text: "$statusMsg ", style: textStyle(true, 14, black))
                  ])),
                  Text.rich(TextSpan(children: [
                    TextSpan(text: "Location "),
                    TextSpan(
                        text: "$placeName ", style: textStyle(true, 14, black))
                  ])),
                  addSpace(10),
                  Row(
                    children: [
                      Text.rich(TextSpan(children: [
                        TextSpan(text: "Views "),
                        TextSpan(
                            text: "${seenBy.length} ",
                            style: textStyle(true, 14, black))
                      ])),
                      addSpaceWidth(10),
                      Text.rich(TextSpan(children: [
                        TextSpan(text: "Clicks "),
                        TextSpan(
                            text: "${clicks.length} ",
                            style: textStyle(true, 14, black))
                      ])),
                      addSpaceWidth(10),
                      Text.rich(TextSpan(children: [
                        TextSpan(text: "Likes "),
                        TextSpan(
                            text: "${likes.length} ",
                            style: textStyle(true, 14, black))
                      ])),
                      addSpaceWidth(10),
                      Text.rich(TextSpan(children: [
                        TextSpan(text: "SuperLikes "),
                        TextSpan(
                            text: "${superLike.length} ",
                            style: textStyle(true, 14, black))
                      ]))
                    ],
                  ),
                ],
              ),
            ),
            if (hasInvoice && !hasPaid)
              FlatButton(
                onPressed: () {
                  openLink(invoiceLink);
                },
                color: appYellow,
                child: Center(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('Make Payment'),
                    addSpaceWidth(10),
                    Icon(
                      Icons.payments_outlined,
                      size: 18,
                    )
                  ],
                )),
              )
          ],
        ),
      ),
    );
  }
}
