import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sparkbuz/AppConfig.dart';
import 'package:sparkbuz/AppEngine.dart';
import 'package:sparkbuz/MainAdmin.dart';
import 'package:sparkbuz/assets.dart';
import 'package:sparkbuz/basemodel.dart';
import 'package:sparkbuz/main_pages/SparkShare.dart';

import 'ShowProfile.dart';
import 'ShowVideoPost.dart';

class Notifications extends StatefulWidget {
  @override
  _NotificationsState createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications>
    with AutomaticKeepAliveClientMixin {
  final refreshController = RefreshController(initialRefresh: false);
  bool canRefresh = true;
  List listItems = [];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    showNewNotifyDot.clear();
    loadNotifications(false);
  }

  loadNotifications(bool isNew) async {
    final startFeedAt = [
      !isNew
          ? (listItems.isEmpty
              ? DateTime.now().millisecondsSinceEpoch
              : listItems[listItems.length - 1].createdAt)
          : (listItems.isEmpty ? 0 : listItems[0].createdAt)
    ];

    List local = [];
    FirebaseFirestore.instance
        .collection(NOTIFY_BASE)
        .where(PARTIES, arrayContains: userModel.getUserId())
        // .where(TIME,
        //     isGreaterThan: (DateTime.now().millisecondsSinceEpoch -
        //         (Duration.millisecondsPerDay * 7)))
        .limit(30)
        .orderBy(CREATED_AT, descending: !isNew)
        .startAt(startFeedAt)
        .get()
        .then((value) {
      local = value.docs;
      for (var doc in value.docs) {
        BaseModel model = BaseModel(doc: doc);
        if (model.getTime() >
            (DateTime.now().millisecondsSinceEpoch -
                (Duration.millisecondsPerDay * 7))) continue;
        int p =
            listItems.indexWhere((e) => e.getObjectId() == model.getObjectId());
        if (p != -1) {
          listItems[p] = model;
        } else {
          listItems.add(model);
        }
        model
          ..putInList(READ_BY, userModel.getUserId(), true)
          ..updateItems();
      }

      if (isNew) {
        refreshController.refreshCompleted();
      } else {
        int oldLength = listItems.length;
        int newLength = local.length;
        if (newLength <= oldLength) {
          refreshController.loadNoData();
          canRefresh = false;
        } else {
          refreshController.loadComplete();
        }
      }
      notifySetup = true;
      if (mounted)
        setState(() {
          //myNotifications.sort((a, b) => b.time.compareTo(a.time));
        });
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return WillPopScope(
      onWillPop: () async {
        Navigator.pop(context, '');
        return false;
      },
      child: Scaffold(
        backgroundColor: white,
        body: page(),
      ),
    );
  }

  page() {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.only(top: 30, right: 10, left: 10, bottom: 10),
          child: Row(
            children: [
              BackButton(
                color: black,
                onPressed: () {
                  Navigator.pop(context, "");
                },
              ),
              Text(
                "Notifications",
                style: textStyle(true, 25, black),
              ),
              Spacer(),
            ],
          ),
        ),
        refresher(),
      ],
    );
  }

  refresher() {
    return Flexible(
      child: SmartRefresher(
        controller: refreshController,
        enablePullDown: false,
        enablePullUp: false,
        header: WaterDropHeader(),
        footer: ClassicFooter(
          noDataText: "Nothing more for now, check later...",
          textStyle: textStyle(false, 12, white.withOpacity(.7)),
        ),
        onLoading: () {
          loadNotifications(false);
        },
        onRefresh: () {
          loadNotifications(true);
        },
        child: ListView(
          //controller: scrollControllers[1],
          shrinkWrap: true,
          //physics: NeverScrollableScrollPhysics(),
          padding: EdgeInsets.only(left: 10, right: 10, bottom: 120),

          children: <Widget>[
            body(),
          ],
        ),
      ),
    );
  }

  body() {
    return Builder(
      builder: (ctx) {
        if (!notifySetup)
          return Container(
            height: getScreenHeight(context) * .9,
            child: loadingLayout(trans: true),
          );
        if (nList.isEmpty)
          return Container(
            height: getScreenHeight(context) * .9,
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Icon(
                      Icons.notifications_active,
                      color: AppConfig.appColor,
                      size: 50,
                    ),
                    Text(
                      "No Notifications Yet",
                      style: textStyle(true, 20, black),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
            ),
          );

        return ListView.builder(
            itemCount: nList.length,
            padding: EdgeInsets.all(0),
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemBuilder: (c, p) {
              BaseModel model = nList[p];
              bool myItem = model.myItem();
              int type = model.getInt(NOTIFY_TYPE);
              IconData icon = iconType(type);
              String message = messageType(type);
              String name = getFirstName(model);
              if (type != NOTIFY_TYPE_SPARK_SHARE) message = '$name $message';
              bool atEnd = p == nList.length - 1;
              bool unRead =
                  !model.getList(READ_BY).contains(userModel.getObjectId());
              return InkWell(
                onTap: () {
                  showNewNotifyDot.remove(model.getObjectId());
                  print(model.getString(NOTIFY_ID));
                  // print(model.items);
                  // return;

                  if (type == NOTIFY_TYPE_SPARK_SHARE) return;
                  if (type == NOTIFY_TYPE_COMMENT ||
                      type == NOTIFY_TYPE_LIKE ||
                      type == NOTIFY_TYPE_SHARE ||
                      type == NOTIFY_TYPE_POST) {
                    pushAndResult(
                        context,
                        ShowVideoPost(
                          videoId: model.getString(NOTIFY_ID),
                        ));
                    return;
                  }
                  pushAndResult(
                      context,
                      ShowProfile(
                        model: model,
                        showCloseBtn: true,
                        profileId: model.getUserId(),
                      ));
                },
                onLongPress: () {
                  yesNoDialog(context, "Delete?",
                      "Are you sure you want to delete this notification?", () {
                    nList.remove(model);
                    model.deleteItem();
                    setState(() {});
                  });
                },
                child: Container(
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      color: black.withOpacity(unRead ? 0.03 : 0),
                      border: Border(
                          bottom: BorderSide(
                              color: black.withOpacity(atEnd ? 0 : 0.1)))),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Container(
                            height: 54,
                            width: 54,
                            child: Stack(
                              children: [
                                Container(
                                    height: 45,
                                    width: 45,
                                    decoration: BoxDecoration(
                                        color: AppConfig.appYellow,
                                        shape: BoxShape.circle),
                                    child: Icon(
                                      icon,
                                      size: 22,
                                    )),
                                Align(
                                  alignment: Alignment.bottomRight,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(30),
                                    child: Container(
                                      decoration: BoxDecoration(
                                          color: white,
                                          border: Border.all(
                                              color: white, width: 1.5)),
                                      child: CachedNetworkImage(
                                        imageUrl: userModel.userImage,
                                        fit: BoxFit.cover,
                                        height: 28,
                                        width: 28,
                                        placeholder: (c, s) {
                                          return Container(
                                            height: 28,
                                            width: 28,
                                            alignment: Alignment.center,
                                            child: Icon(
                                              LineIcons.user,
                                              size: 16,
                                            ),
                                            decoration: BoxDecoration(
                                                color: black.withOpacity(.08),
                                                shape: BoxShape.circle,
                                                border: Border.all(
                                                    color: white, width: 1.5)),
                                          );
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          addSpaceWidth(5),
                          Expanded(
                            child: Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        message,
                                        style: textStyle(false, 16, black),
                                      ),
                                      if (type ==
                                          NOTIFY_TYPE_SPARK_SHARE /*&&
                                          (!unRead)*/
                                      )
                                        Row(
                                          children: [
                                            Flexible(
                                                child: FlatButton(
                                              onPressed: () {
                                                showNewNotifyDot.remove(
                                                    model.getObjectId());
                                                nList.remove(model);
                                                setState(() {});
                                                model
                                                  ..putInList(
                                                      READ_BY,
                                                      userModel.getUserId(),
                                                      true)
                                                  ..updateItems();
                                                setState(() {});
                                                pushAndResult(
                                                    context,
                                                    SparkShare(
                                                      sparkShareId: model
                                                          .getString(NOTIFY_ID),
                                                    ));
                                              },
                                              color: green,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(8)),
                                              child:
                                                  Center(child: Text('ACCEPT')),
                                            )),
                                            addSpaceWidth(10),
                                            Flexible(
                                                child: FlatButton(
                                              onPressed: () {
                                                //if (!unRead) return;
                                                yesNoDialog(context, "Decline?",
                                                    "Are you sure you want to decline spark share request?",
                                                    () {
                                                  showNewNotifyDot.remove(
                                                      model.getObjectId());
                                                  nList.remove(model);
                                                  setState(() {});

                                                  FirebaseFirestore.instance
                                                      .collection(SPARK_BASE)
                                                      .doc(model
                                                          .getString(NOTIFY_ID))
                                                      .get()
                                                      .then((value) {
                                                    BaseModel model =
                                                        BaseModel(doc: value);
                                                    model.deleteItem();
                                                    // handleShareInvitation(
                                                    //     model, false, () {
                                                    //   model.deleteItem();
                                                    //   setState(() {});
                                                    // });
                                                  });
                                                });
                                              },
                                              color: red,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(8)),
                                              child: Center(
                                                  child: Text('DECLINE')),
                                            )),
                                          ],
                                        ),
                                    ],
                                  ),
                                ),
                                addSpaceWidth(10),
                                Text(
                                  getTimeAgo(model.getTime()),
                                  style: textStyle(
                                      false, 12, black.withOpacity(.5)),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              );
            });
      },
    );
  }

  IconData iconType(int type) {
    IconData icon = LineIcons.video_camera;
    if (type == NOTIFY_TYPE_FOLLOW) icon = LineIcons.user;
    if (type == NOTIFY_TYPE_UNFOLLOW) icon = LineIcons.user;
    if (type == NOTIFY_TYPE_LIKE) icon = LineIcons.thumbs_up;
    if (type == NOTIFY_TYPE_COMMENT) icon = LineIcons.comment;
    if (type == NOTIFY_TYPE_SHARE) icon = LineIcons.share;
    if (type == NOTIFY_TYPE_POST) icon = LineIcons.video_camera;
    if (type == NOTIFY_TYPE_STORY) icon = LineIcons.circle;

    return icon;
  }

  String messageType(int type) {
    String message = 'Incoming SparkShare Request';
    if (type == NOTIFY_TYPE_FOLLOW) message = 'Followed you';
    if (type == NOTIFY_TYPE_UNFOLLOW) message = 'UnFollowed you';
    if (type == NOTIFY_TYPE_LIKE) message = 'Liked your video';
    if (type == NOTIFY_TYPE_COMMENT) message = 'Commented on your video';
    if (type == NOTIFY_TYPE_SHARE) message = 'Shared your video';
    if (type == NOTIFY_TYPE_POST) message = 'Shared a video';
    if (type == NOTIFY_TYPE_STORY) message = 'Shared a Story';
    return message;
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
