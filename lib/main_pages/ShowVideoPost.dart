import 'dart:async';
import 'dart:io';

import 'package:cached_video_player/cached_video_player.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sparkbuz/CreateReport.dart';
import 'package:sparkbuz/Filterlayout.dart';
import 'package:sparkbuz/MainAdmin.dart';
import 'package:sparkbuz/ShowPeople.dart';
import 'package:sparkbuz/SparkIcon.dart';
import 'package:sparkbuz/basemodel.dart';
import 'package:sparkbuz/basic/Tapped.dart';
import 'package:sparkbuz/like_animation.dart';
import 'package:timeago/timeago.dart' as timeAgo;
import 'package:visibility_detector/visibility_detector.dart';

import '../AppConfig.dart';
import '../AppEngine.dart';
import '../assets.dart';
import 'ShowProfile.dart';

class ShowVideoPost extends StatefulWidget {
  final BaseModel videoModel;
  final String videoId;
  const ShowVideoPost({Key key, this.videoModel, this.videoId})
      : super(key: key);
  @override
  _ShowVideoPostState createState() => _ShowVideoPostState();
}

class _ShowVideoPostState extends State<ShowVideoPost> {
  BaseModel videoModel = BaseModel();
  //String thumbnail = "";
  //String thumbnail = "";
  final visibleComments = ValueNotifier<List<int>>([]);
  CachedVideoPlayerController controller;
  bool hasSetUp = false;
  bool showPlayIcon = true;
  List<BaseModel> commentList = [];
  bool commentsLoaded = false;
  List<StreamSubscription> subs = [];
  final commentController = TextEditingController();
  bool isTyping = false;
  //bool liked = false;
  BaseModel theUser;

/*  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    if (null != widget.videoModel && !widget.videoModel.myItem()) {
      liked = widget.videoModel
          .getList(LIKES_COUNT)
          .contains(userModel.getUserId());
      widget.videoModel
        ..putInList(SEEN_BY, userModel.getUserId(), true)
        ..updateItems();
    }
  }*/

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.videoModel != null) {
      videoModel = widget.videoModel;
      likedIds = videoModel.getList(LIKES_COUNT);
      liked = likedIds.contains(userModel.getUserId());
    }
    loadData();
    visibleComments.addListener(() {
      if (mounted) setState(() {});
    });
    commentController.addListener(() {
      isTyping = commentController.text.isNotEmpty;
      if (mounted) setState(() {});
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    controller?.dispose();
    for (var s in subs) s?.cancel();
  }

  loadData() {
    if (widget.videoModel == null) {
      if (widget.videoId.isEmpty) return;
      FirebaseFirestore.instance
          .collection(POSTS_BASE)
          .doc(widget.videoId)
          .get()
          .then((value) {
        if (null == value) return;
        videoModel = BaseModel(doc: value);
        likedIds = videoModel.getList(LIKES_COUNT);
        liked = likedIds.contains(userModel.getUserId());

        String videoUrl = videoModel.getString(VIDEO_URL);
        controller = CachedVideoPlayerController.network(videoUrl)
          ..initialize().then((value) {
            controller.setLooping(true);
            controller.play();
            setState(() {
              hasSetUp = true;
            });
          });
        controller.addListener(() {
          bool isPlaying = controller.value.isPlaying;
          if (mounted)
            setState(() {
              showPlayIcon = !isPlaying;
            });
        });
        setState(() {});
        loadUser();
        loadComments();
        print(value.data());
        if (null != videoModel && !videoModel.myItem()) {
          List seenby = videoModel.getList(SEEN_BY);
          if (!seenby.contains(userModel.getUserId()))
            videoModel
              ..putInList(SEEN_BY, userModel.getUserId(), true)
              ..updateItems();
        }
      });
      return;
    }
    loadUser();
    loadCached();
    loadComments();
  }

  loadUser() {
    if (videoModel.getUserId().isEmpty) return;
    FirebaseFirestore.instance
        .collection(USER_BASE)
        .doc(videoModel.getUserId())
        .get()
        .then((value) {
      theUser = BaseModel(doc: value);
      setState(() {});
    });
  }

  loadCached() async {
    String videoUrl = videoModel.getString(VIDEO_URL);
    controller = CachedVideoPlayerController.network(videoUrl)
      ..initialize().then((value) {
        controller.setLooping(true);
        controller.play();
        setState(() {
          hasSetUp = true;
        });
      });
    controller.addListener(() {
      bool isPlaying = controller.value.isPlaying;
      if (mounted)
        setState(() {
          showPlayIcon = !isPlaying;
        });
    });
  }

  loadComments() async {
    if (videoModel.getObjectId().isEmpty) return;

    var sub = FirebaseFirestore.instance
        .collection(COMMENT_BASE)
        .where(VIDEO_ID, isEqualTo: videoModel.getObjectId())
        .snapshots()
        .listen((event) {
      for (var doc in event.docs) {
        BaseModel md = BaseModel(doc: doc);
        int p =
            commentList.indexWhere((e) => e.getObjectId() == md.getObjectId());
        if (p == -1)
          commentList.add(md);
        else
          commentList[p] = md;
      }
      commentsLoaded = true;
      if (mounted) setState(() {});
    });
    subs.add(sub);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.pop(context, videoModel);
        return false;
      },
      child: Scaffold(
        backgroundColor: black,
        body: page(),
      ),
    );
  }

  bool showView = true;

  page() {
    return Stack(
      children: [
        videoStreamView(),
        commentStreamView(),
        Container(
          padding: EdgeInsets.only(top: 25),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              GestureDetector(
                onTap: () {
                  Navigator.pop(context, videoModel);
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: red,
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(18),
                          bottomRight: Radius.circular(18))),
                  padding: EdgeInsets.all(10),
                  //margin: EdgeInsets.only(top: 20),
                  child: Icon(
                    Icons.clear,
                    color: white,
                  ),
                ),
              ),
              addSpaceWidth(10),
              Container(
                //margin: EdgeInsets.only(top: 20),
                child: Image.asset(
                  spark_logo,
                  height: 30,
                  fit: BoxFit.cover,
                ),
              ),
              Spacer(),
              Container(
                decoration: BoxDecoration(
                    color: black.withOpacity(.2),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(25),
                        bottomLeft: Radius.circular(25))),
                padding: EdgeInsets.all(5),
                //margin: EdgeInsets.only(top: 20),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    GestureDetector(
                      onTap: () {
                        showView = !showView;
                        setState(() {});
                      },
                      child: Container(
                        height: 30,
                        width: 30,
                        child: Center(
                          child: Icon(
                            showView
                                ? Icons.navigate_next
                                : Icons.navigate_before,
                            color: white,
                            size: 25,
                          ),
                        ),
                      ),
                    ),
                    if (showView)
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          GestureDetector(
                            onTap: () {
                              controller?.pause();
                              pushAndResult(
                                  context,
                                  ShowProfile(
                                    showCloseBtn: true,
                                    model: videoModel,
                                    profileId: videoModel.getUserId(),
                                  ), result: (_) {
                                controller?.play();
                              });
                            },
                            child: userImageItem(context, videoModel,
                                iconSize: 10,
                                backColor: white,
                                strock: 0,
                                size: 30,
                                padLeft: false,
                                toProfile: true),
                          ),
                          addSpaceWidth(5),
                          InkWell(
                            onTap: () {
                              if (videoModel.myItem() || isAdmin) {
                                controller?.pause();
                                pushAndResult(
                                    context,
                                    ShowPeople(
                                        videoModel.likesCount,
                                        'Video Likes',
                                        'No Likes Yet!'), result: (_) {
                                  controller?.play();
                                });
                                return;
                              }

                              onVideoLiked();
                              return;

                              liked = !liked;
                              videoModel
                                ..putInList(
                                    LIKES_COUNT, userModel.getUserId(), liked)
                                ..updateItems();

                              userModel
                                ..putInList(LIKES_COUNT,
                                    videoModel.getObjectId(), liked)
                                ..updateItems();

                              if (null != theUser)
                                theUser
                                  ..putInList(
                                      LIKES_COUNT, userModel.getUserId(), liked)
                                  ..updateItems();

                              if (!liked) flareController.add(videoModel);
                              // liked = videoModel
                              //     .getList(LIKES_COUNT)
                              //     .contains(userModel.getUserId());

                              setState(() {});
                            },
                            child: Container(
                              height: 30,
                              width: 30,
                              color: transparent,
                              child: Center(
                                child: Icon(
                                  Icons.favorite,
                                  color:
                                      liked ? appYellow : white.withOpacity(.5),
                                  //size: 30,
                                ),
                              ),
                            ),
                          ),
                          if (videoModel.myItem())
                            GestureDetector(
                              onTap: () {
                                if (videoModel.myItem() || isAdmin) {
                                  controller?.pause();

                                  pushAndResult(
                                      context,
                                      ShowPeople(videoModel.getList(SEEN_BY),
                                          'Seen By', 'No Views Yet!'));
                                }
                              },
                              child: Container(
                                height: 30,
                                width: 30,
                                child: Stack(
                                  children: [
                                    Align(
                                      alignment: Alignment.center,
                                      child: Container(
                                        height: 30,
                                        width: 30,
                                        child: Icon(
                                          Icons.remove_red_eye,
                                          color: AppConfig.appYellow,
                                          //size: 18,
                                        ),
                                      ),
                                    ),
                                    if (videoModel.getList(SEEN_BY).isNotEmpty)
                                      Align(
                                        alignment: Alignment.bottomRight,
                                        child: Container(
                                          // height: 10,
                                          // width: 10,
                                          padding: EdgeInsets.all(3),
                                          child: Text(
                                            videoModel
                                                .getList(SEEN_BY)
                                                .length
                                                .toString(),
                                            style: textStyle(false, 11, white),
                                          ),
                                          decoration: BoxDecoration(
                                              color: red,
                                              borderRadius:
                                                  BorderRadius.circular(5)
                                              //shape: BoxShape.circle
                                              ),
                                        ),
                                      )
                                  ],
                                ),
                              ),
                            ),
                          addSpaceWidth(5),
                          GestureDetector(
                            onTap: () {
                              //TODO Notify participants
                              pushNotificationToUsers(
                                notifyType: NOTIFY_TYPE_SHARE,
                                notifyId: videoModel.getObjectId(),
                                userIds: [videoModel.getUserId()],
                              );

                              String appLink =
                                  appSettingsModel.getString(APP_LINK_IOS);
                              if (Platform.isAndroid)
                                appLink = appSettingsModel
                                    .getString(APP_LINK_ANDROID);
                              shareApp(
                                  message:
                                      "Join SparkBuz and share memories with friends and family.\n $appLink");
                              videoModel
                                ..putInList(
                                    SHARE_LIST, userModel.getUserId(), true)
                                ..updateItems();
                            },
                            child: Container(
                              height: 30,
                              width: 30,
                              child: Icon(
                                AppIcons.reply,
                                color: white,
                                size: 18,
                              ),
                            ),
                          ),
                          addSpaceWidth(5),
                          if (!videoModel.myItem())
                            GestureDetector(
                              onTap: () {
                                //TODO Notify participants
                                controller?.pause();
                                pushAndResult(
                                    context,
                                    CreateReport(
                                      item: videoModel,
                                    ));
                              },
                              child: Container(
                                height: 30,
                                width: 30,
                                child: Icon(
                                  Icons.report_outlined,
                                  color: white,
                                  size: 25,
                                ),
                              ),
                            ),
                        ],
                      ),
                  ],
                ),
              ),
            ],
          ),
        ),
        LikeAnimation()
      ],
    );
  }

  videoStreamView() {
    if (!hasSetUp) return videoLoading;

    bool isPlaying = (null == controller) ? false : controller.value.isPlaying;
    return Stack(
      //fit: StackFit.expand,
      children: [
        Align(
          alignment: Alignment.center,
          child: AspectRatio(
              aspectRatio: controller.value.aspectRatio,
              child: CachedVideoPlayer(controller)),
        ),
        FilterLayout(
          //currentPlayer: controller,
          filters: videoModel.getListModel(FILTERS),
          enableFilter: false,
        ),
        // PlayTapScreen(
        //     isPlaying: isPlaying,
        //     showPlayIcon: showPlayIcon,
        //     controller: controller,
        //     playCallback: (b) {
        //       isPlaying = b;
        //       setState(() {});
        //     }),
        Tapped(
          showIcon: !controller.value.isPlaying,
          child: Container(
            color: transparent,
            alignment: Alignment.center,
            child: AnimatedContainer(
                duration: Duration(milliseconds: 400),
                decoration: BoxDecoration(
                    color: black.withOpacity(.1), shape: BoxShape.circle),
                height: 100,
                width: 100,
                child: Center(
                  child: Icon(
                    controller.value.isPlaying
                        ? Icons.pause
                        : Icons.play_arrow_sharp,
                    color: white.withOpacity(.5),
                    size: 90,
                  ),
                )),
          ),
          onTap: () {
            if (isPlaying)
              controller.pause();
            else
              controller.play();
            setState(() {});
          },
        ),
      ],
    );
  }

  commentStreamView() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: ValueListenableBuilder(
        valueListenable: visibleComments,
        builder: (c, ids, w) {
          return Container(
            height: getScreenHeight(context) * .4,
            margin: EdgeInsets.only(bottom: 20, right: 15, left: 15),
            padding: EdgeInsets.all(5),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Flexible(
                  child: ListView.builder(
                      shrinkWrap: true,
                      reverse: true,
                      itemCount: commentList.length,
                      itemBuilder: (ctx, p) {
                        return commentTiles(p);
                      }),
                ),
                addSpace(10),
                Row(
                  children: [
                    Flexible(
                      child: Container(
                        decoration: BoxDecoration(
                            color: white.withOpacity(
                                commentController.text.isEmpty ? 0.2 : 1),
                            borderRadius: BorderRadius.circular(25)),
                        //padding: EdgeInsets.all(5),
                        child: Row(
                          children: [
                            addSpaceWidth(5),
                            userImageItem(context, userModel,
                                strock: 0,
                                backColor: black.withOpacity(.1),
                                size: 40,
                                padLeft: false),
                            addSpaceWidth(10),
                            Flexible(
                              child: TextField(
                                controller: commentController,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "Write Comment...",
                                    hintStyle: textStyle(
                                        false,
                                        13,
                                        commentController.text.isEmpty
                                            ? white.withOpacity(.6)
                                            : black.withOpacity(.5))),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    addSpaceWidth(10),
                    GestureDetector(
                      onTap: () {
                        String message = commentController.text;
                        if (message.isEmpty) return;
                        commentController.clear();
                        String id = getRandomId();
                        this.videoModel
                          ..putInList(COMMENT_LIST, id, true)
                          ..updateItems();
                        final model = BaseModel();
                        model.put(MESSAGE, message);
                        model.put(OBJECT_ID, id);
                        model.put(VIDEO_ID, this.videoModel.getObjectId());
                        model.saveItem(COMMENT_BASE, true, document: id);
                        commentList.insert(0, model);
                        pushNotificationToUsers(
                            notifyType: NOTIFY_TYPE_COMMENT,
                            notifyId: this.videoModel.getObjectId(),
                            userIds: [this.videoModel.getUserId()]);
                        setState(() {});
                      },
                      child: Container(
                          //padding: EdgeInsets.all(5),
                          height: 45,
                          width: 45,
                          decoration: BoxDecoration(
                              color: white.withOpacity(isTyping ? 1 : 0.6),
                              shape: BoxShape.circle),
                          child: Center(
                            child: Icon(
                              Icons.send,
                              size: 16,
                            ),
                          )),
                    ),
                  ],
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  commentTiles(int p) {
    BaseModel model = commentList[p];
    String message = model.getString(MESSAGE);
    String username = model.getUserName();
    int time = model.getTime();

    int visibleIdsSize = visibleComments.value.reversed.length;
    bool inRange = p < visibleIdsSize;
    int rangeP = 0;

    //if (inRange) rangeP = visibleComments.value[p - 1];
//    int inRange =
//        visibleIdsSize - 1 > p ? visibleIdsSize : visibleComments.value[p - 1];
//    print(inRange);
//    print(rangeP);
    return VisibilityDetector(
      key: Key('my-widget-key$p'),
      onVisibilityChanged: (visibilityInfo) {
        var visiblePercentage = visibilityInfo.visibleFraction * 100;
//        debugPrint(
//            'Widget ${visibilityInfo.key} is $visiblePercentage% visible');
        if (visiblePercentage == 100) {
          if (!visibleComments.value.contains(p)) {
            visibleComments.value.add(p);
          }
        } else {
          visibleComments.value.remove(p);
        }
        //visibleComments.notifyListeners();
      },
      child: Opacity(
        //opacity: rangeP - 1 < p ? 1 : 0.4,
        opacity: 1,
        child: Container(
          margin: EdgeInsets.only(bottom: 8),
          child: Stack(
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(30, 0, 40, 15),
                decoration: BoxDecoration(
                    color: black.withOpacity(.4),
                    border: Border.all(color: white.withOpacity(.2)),
                    borderRadius: BorderRadius.circular(25)),
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            username,
                            maxLines: 1,
                            style: textStyle(true, 12, white),
                          ),
                          addSpaceWidth(5),
                          Text(
                            timeAgo.format(
                                DateTime.fromMillisecondsSinceEpoch(time),
                                locale: "en_short"),
                            style: textStyle(false, 12, white.withOpacity(.3)),
                          ),
                        ],
                      ),
                      addSpace(5),
                      Text(
                        message,
                        style: textStyle(false, 17, white),
                      ),
                    ],
                  ),
                ),
              ),
              userImageItem(context, model,
                  strock: 0,
                  backColor: white.withOpacity(.3),
                  size: 30,
                  padLeft: true),
            ],
          ),
        ),
      ),
    );
  }

  bool liked = false;
  List likedIds = [];

  onVideoLiked() {
    String videoId = videoModel.getObjectId();
    String userId = userModel.getUserId();

    liked = likedIds.contains(userId);
    List videoLikes = userModel.getList(LIKES_COUNT);
    bool hasLiked = videoLikes.contains(videoId);

    if (liked) {
      likedIds.remove(userId);
    } else {
      likedIds.add(userId);
    }

    if (hasLiked) {
      videoLikes.remove(videoId);
    } else {
      videoLikes.add(videoId);
    }

    videoModel.put(LIKES_COUNT, likedIds);
    videoModel.updateItems();

    userModel.put(LIKES_COUNT, videoLikes);
    userModel.updateItems();

    if (!liked) flareController.add(videoModel);
    liked = !liked;
    setState(() {});

    //TODO Notify participants
    pushNotificationToUsers(
      notifyType: NOTIFY_TYPE_LIKE,
      notifyId: videoModel.getObjectId(),
      userIds: [videoModel.getUserId()],
    );
  }
}
