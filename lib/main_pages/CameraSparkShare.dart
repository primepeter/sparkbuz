import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'dart:ui';

import 'package:agora_rtc_engine/agora_rtc_engine.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screen_recording/flutter_screen_recording.dart';
// import 'package:foreground_service/foreground_service.dart';
import 'package:line_icons/line_icons.dart';
// import 'package:agora_rtc_engine/rtc_engine.dart' as agora;
// import 'package:agora_rtc_engine/rtc_local_view.dart' as RtcLocalView;
// import 'package:agora_rtc_engine/rtc_remote_view.dart' as RtcRemoteView;
import 'package:permission_handler/permission_handler.dart';
import 'package:sparkbuz/AppConfig.dart';
import 'package:sparkbuz/CamPreview.dart';
import 'package:sparkbuz/ShowFollowers.dart';
import 'package:sparkbuz/basemodel.dart';
import 'package:video_compress/video_compress.dart';
import 'package:video_player/video_player.dart';

// import 'package:video_trimmer/video_trimmer.dart';

import '../AppEngine.dart';
import '../assets.dart';

void logError(String code, String message) =>
    print('Error: $code\nError Message: $message');

class CameraSparkShare extends StatefulWidget {
  @override
  _CameraSparkShareState createState() => _CameraSparkShareState();
}

class _CameraSparkShareState extends State<CameraSparkShare>
    with WidgetsBindingObserver, TickerProviderStateMixin {
  List<BaseModel> videoPaths = [];
  VideoPlayerController videoController;
  VoidCallback videoPlayerListener;
  bool enableAudio = true;
  bool frontCamera = false;
  final camBtnKey = GlobalKey();
  Size camBtnSize;
  Offset camBtnPosition;

  Animation<Color> timerAnimation;
  AnimationController timerController;

  AnimationController recordingController;
  Animation<Color> recordingAnimation;

  bool isRecording = false;
  bool isRecordingComplete = false;
  double zoomTo = 0;
  double top = 0;
  double left = 0;
  double recordingOpacity = 1;
  String recordTimerText = "00:00";

  bool shareAsStory = false;

  static const APP_ID = "40c37c3e06f840449394b70b93b177d5";
  static final _users = <int>[];
  final _infoStrings = <String>[];
  bool isScreenRecording = false;
  bool _joined = false;
  int _remoteUid = null;
  bool _switch = false;
  List<BaseModel> videoShare = [];
  String sparkShareId = getRandomId();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    requestPermissions();
    initializeAgora();

    recordingController =
        AnimationController(vsync: this, duration: Duration(seconds: 1));
    recordingAnimation =
        ColorTween(begin: red, end: transparent).animate(recordingController)
          ..addListener(() {})
          ..addStatusListener((status) {});

    timerController =
        AnimationController(vsync: this, duration: Duration(minutes: 2));
    timerAnimation =
        ColorTween(begin: white, end: white).animate(timerController)
          ..addListener(() {
            recordingOpacity = recordingOpacity == 1 ? 0 : 1;
            recordTimerText = timerString;
            //print("Timer $recordTimerText");
            if (mounted) setState(() {});
          })
          ..addStatusListener((status) {
            print("Status $status");
            if (status == AnimationStatus.completed) {
              isRecordingComplete = true;
              timerController.reset();
              recordingController.stop();
              recordingOpacity = 0;
              isRecording = false;
              return;
            }
          });

    /*if (Platform.isIOS)
      ScreenRecorderFlutter.init(onRecordingStarted: (started, msg) {
        print("Recording $started $msg");
        if (!started) return;
        timerController.forward();
        recordingController.repeat(reverse: true);
        // final bm = BaseModel();
        // bm.put(VIDEO_PATH, '');
        //videoPaths.add(bm);
        isRecording = true;
        if (mounted) setState(() {});
      }, onRecodingCompleted: (path) async {
        print("Recording completed $path");
        //showProgress(false, context);
        isRecording = false;
        timerController.stop();
        recordingController.stop();
        timerController.reset();
        if (mounted) setState(() {});
        await Future.delayed(Duration(seconds: 2));
        final Trimmer _trimmer = Trimmer();
        await _trimmer.loadVideo(videoFile: File(path));
        pushAndResult(
            context,
            TrimmerView(
              _trimmer,
            ), result: (_) async {
          if (null == _) return;
          String path = _[0];
          int length = _[1];
          File thumbnail = await VideoCompress.getFileThumbnail(path);
          BaseModel bm = BaseModel();
          bm.put(THUMBNAIL_PATH, thumbnail.path);
          bm.put(VIDEO_PATH, path);
          bm.put(VIDEO_LENGTH, length);
          bm.put(IS_VIDEO, true);
          bm.put(OBJECT_ID, getRandomId());
          pushAndResult(
              context,
              CamPreview(
                isStory: shareAsStory,
                camContext: context,
                files: [bm],
              ));
        });
      });*/
  }

  String get timerString {
    Duration duration = timerController.duration * timerController.value;
    return '${duration.inMinutes}:${(duration.inSeconds % 60).toString().padLeft(2, '0')}';
  }

  @override
  void dispose() {
    //WidgetsBinding.instance.removeObserver(this);
    AgoraRtcEngine?.destroy();
    timerController?.dispose();
    recordingController?.dispose();
    for (var s in subs) s?.cancel();
    super.dispose();
  }

  requestPermissions() async {
    await [
      Permission.storage,
      Permission.photos,
      Permission.microphone,
    ].request();
  }

  initializeAgora() async {
    await AgoraRtcEngine.create(APP_ID);
    await AgoraRtcEngine.enableAudio();
    await AgoraRtcEngine.enableVideo();
    await AgoraRtcEngine.enableWebSdkInteroperability(true);

    // await AgoraRtcEngine.setEnableSpeakerphone(enabled)

    await AgoraRtcEngine.setParameters(
        '''{\"che.video.lowBitRateStreamParameter\":{\"width\":320,\"height\":180,\"frameRate\":15,\"bitRate\":140}}''');
    await AgoraRtcEngine.joinChannel(null, sparkShareId, null, 0);

    AgoraRtcEngine.onError = (dynamic code) {
      setState(() {
        final info = 'onError: $code';
        _infoStrings.add(info);
      });
    };

    AgoraRtcEngine.onJoinChannelSuccess = (
      String channel,
      int uid,
      int elapsed,
    ) {
      setState(() {
        final info = 'onJoinChannel: $channel, uid: $uid';
        _infoStrings.add(info);
      });
    };

    AgoraRtcEngine.onLeaveChannel = () {
      setState(() {
        _infoStrings.add('onLeaveChannel');
        _users.clear();
      });
    };

    AgoraRtcEngine.onUserJoined = (int uid, int elapsed) {
      setState(() {
        final info = 'userJoined: $uid';
        _infoStrings.add(info);
        _users.add(uid);
      });
    };

    AgoraRtcEngine.onUserOffline = (int uid, int reason) {
      setState(() {
        final info = 'userOffline: $uid';
        _infoStrings.add(info);
        _users.remove(uid);
      });
    };

    AgoraRtcEngine.onFirstRemoteVideoFrame = (
      int uid,
      int width,
      int height,
      int elapsed,
    ) {
      setState(() {
        final info = 'firstRemoteVideo: $uid ${width}x $height';
        _infoStrings.add(info);
      });
    };
    //AgoraRtcEngine?.switchCamera();
    setState(() {});
  }

  BaseModel sShareModel;
  List<StreamSubscription> subs = [];

  listenToSparkBase() async {
    var shots = FirebaseFirestore.instance
        .collection(SPARK_BASE)
        .doc(sparkShareId)
        .snapshots()
        .listen((doc) {
      print("doc $doc");

      if (doc == null) return;
      sShareModel = BaseModel(doc: doc);
      setState(() {});
    });
    subs.add(shots);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (null != sShareModel) {
          sShareModel
            ..put(SESSION_ACTIVE, false)
            ..updateItems();
        }
        Navigator.pop(context, '');
        return false;
      },
      child: Scaffold(
        // backgroundColor: Color(0xFFf5f5f4),
        backgroundColor: black,
        body: SafeArea(
          bottom: false,
          child: Container(
            child: Stack(
              children: <Widget>[
                cameraViews,
                if (!isRecording) closeButton,
                if (!isRecording) addParticipants()
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget get cameraViews {
    return Stack(
      children: [
        videoPanel(),
        Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            padding: EdgeInsets.only(left: 5, right: 5),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                if (isRecording)
                  GestureDetector(
                    onTap: () {
                      handleRecording();
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color: black.withOpacity(.8),
                          borderRadius: BorderRadius.circular(25)),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          AnimatedBuilder(
                            animation: recordingAnimation,
                            builder: (context, child) => Container(
                              margin: EdgeInsets.all(10),
                              width: 20,
                              height: 20,
                              decoration: BoxDecoration(
                                  color: recordingAnimation.value,
                                  shape: BoxShape.circle),
                            ),
                          ),
                          Flexible(
                            child: Text(
                              recordTimerText,
                              style: textStyle(true, 12, white),
                            ),
                          ),
                          addSpaceWidth(10)
                          /*GestureDetector(
                              onTap: () {
                                handleRecording();
                              },
                              child: Container(
                                padding: EdgeInsets.all(10),
                                height: 16,
                                width: 16,
                                color: white,
                                // child: Icon(
                                //   Icons.close,
                                //   color: white,
                                //   size: 20,
                                // ),
                              ))*/
                        ],
                      ),
                    ),
                  ),
                if (!isRecording)
                  GestureDetector(
                    onTap: () {
                      handleRecording();
                    },
                    child: AnimatedContainer(
                      duration: Duration(milliseconds: 100),
                      key: camBtnKey,
                      height: isRecording ? 80 : 60,
                      width: isRecording ? 80 : 60,
                      //margin: EdgeInsets.only(bottom: 20),
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          if (isRecording)
                            SizedBox(
                              height: isRecording ? 80 : 60,
                              width: isRecording ? 80 : 60,
                              child: CircularProgressIndicator(
                                value: timerController.value,
                                //backgroundColor: white,
                                valueColor: timerAnimation,
                              ),
                            ),
                          if (isRecording)
                            Align(
                                alignment: Alignment.center,
                                child: Container(
                                  width: 20,
                                  height: 20,
                                  color: red0,
                                )),
                        ],
                      ),
                      decoration: BoxDecoration(
                        color: white.withOpacity(0.2),
                        borderRadius:
                            BorderRadius.circular(isRecording ? 45 : 30),
                        border: Border.all(
                          style: BorderStyle.solid,
                          color: isRecording ? red : white.withOpacity(0.4),
                          width: 6,
                        ),
                      ),
                    ),
                  ),
                Container(
                  padding: EdgeInsets.all(15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      if (!isRecording)
                        Container(
                            margin: EdgeInsets.all(5),
                            padding: EdgeInsets.all(10),
                            //width: 80,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                color: AppConfig.appYellow,
                                borderRadius: BorderRadius.circular(25)),
                            child: Text(
                              'SparkShare',
                              style: textStyle(false, 14, black),
                            )),
                    ],
                  ),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget get closeButton => GestureDetector(
        child: Container(
          margin: EdgeInsets.only(top: 0),
          padding: EdgeInsets.all(14),
          decoration: BoxDecoration(
              color: red,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(30),
                  bottomRight: Radius.circular(30))),
          child: Icon(
            Icons.clear,
            color: white,
            size: 18,
          ),
        ),
        onTap: () {
          if (null != sShareModel) {
            sShareModel
              ..put(SESSION_ACTIVE, false)
              ..updateItems();
          }
          Navigator.pop(context, '');
        },
      );

  cameraButtonPositionUpdate(LongPressMoveUpdateDetails p) {
    final v = p.globalPosition;
//        if (v.dy < boundary && v.dy > topMargin) {
//          top = max(v.dy - (60) / 2, 0);
//        }
//    getScreenWidth(context) - 60 - ((20) * 2)) / 2

    top = max(v.dy - (80) / 2, 0);
    zoomTo = ((getScreenHeight(context) - v.dy) * .5) / 100;
    zoomTo = double.parse(zoomTo.toStringAsFixed(2));
    zoomTo = zoomTo.isNegative
        ? 0
        : zoomTo > 1
            ? 1
            : zoomTo;
    print("Top $zoomTo");
    left = max(v.dx - (80) / 2, 0);
    //if (null != controller) controller.zoom(zoomTo);
    setState(() {});
  }

  cameraButtonCurrentPosition(DragDownDetails p) {
    RenderBox renderBox = camBtnKey.currentContext.findRenderObject();
    final btnPosition = renderBox.localToGlobal(Offset.zero);
    double size = renderBox.size.width;
    print("Size $size");
//    getScreenWidth(context) - 60 - ((20) * 2)) / 2

    top = max(btnPosition.dy - (90 + 12) / 2, 0);
    left = (getScreenWidth(context) - 80 - ((20) * 2)) / 2;
    setState(() {});
  }

  String timestamp() => DateTime.now().millisecondsSinceEpoch.toString();

  /// Helper function to get list of native views
  List<Widget> _getRenderViews() {
    final List<AgoraRenderWidget> list = [
      AgoraRenderWidget(0, local: true, preview: true),
    ];
    _users.forEach((int uid) => list.add(AgoraRenderWidget(uid)));
    return list;
  }

  /// Video view wrapper
  Widget _videoView(view) {
    return Expanded(child: Container(child: view));
  }

  /// Video view row wrapper
  Widget _expandedVideoRow(List<Widget> views) {
    final wrappedViews = views.map<Widget>(_videoView).toList();
    return Expanded(
      child: Row(
        children: wrappedViews,
      ),
    );
  }

  /// Video layout wrapper
  Widget videoPanel() {
    final views = _getRenderViews();
    //print("maugost ${views.length}");

    switch (views.length) {
      case 1:
        return Container(
            child: Column(
          children: <Widget>[_videoView(views[0])],
        ));
      case 2:
        return Container(
            child: Column(
          children: <Widget>[
            _expandedVideoRow([views[0]]),
            _expandedVideoRow(
              [views[1]],
            ),
          ],
        ));
      case 3:
        return Container(
            child: Column(
          children: <Widget>[
            _expandedVideoRow(views.sublist(0, 2)),
            _expandedVideoRow(views.sublist(2, 3)),
          ],
        ));
      case 4:
        return Container(
            child: Column(
          children: <Widget>[
            _expandedVideoRow(views.sublist(0, 2)),
            _expandedVideoRow(views.sublist(2, 4))
          ],
        ));
      default:
    }
    return Container(
      color: transparent,
    );
  }

  addParticipants() {
    if (isScreenRecording)
      return Container(
        height: 0,
        width: 0,
      );

    return Align(
      alignment: Alignment.topRight,
      child: Container(
        padding: EdgeInsets.all(5),
        child: FlatButton(
            onPressed: () {
              if (null != sShareModel &&
                  sShareModel.getList(PARTIES).isNotEmpty) return;
              pushAndResult(context, ShowFollowers(), result: (_) {
                if (null == _) return;
                videoShare = _;
                setState(() {});

                //TODO Add participants to SHARE_BASE
                BaseModel model = BaseModel();
                List parties = videoShare.map((e) => e.getUserId()).toList();
                model.put(PARTIES, parties);
                model.put(IMAGES, videoShare.map((e) => e.userImage).toList());
                model.put(PARTICIPANTS,
                    List.generate(parties.length, (index) => 0).toList());
                model.put(OBJECT_ID, sparkShareId);
                model.put(SESSION_ACTIVE, true);
                model.put(
                    SPARK_TIMEOUT,
                    DateTime.now()
                        .add(Duration(minutes: 2))
                        .millisecondsSinceEpoch);
                model.saveItem(SPARK_BASE, true, document: sparkShareId);

                //TODO Notify participants
                pushNotificationToUsers(
                  notifyType: NOTIFY_TYPE_SPARK_SHARE,
                  notifyId: sparkShareId,
                  userIds: videoShare.map((e) => e.getUserId()).toList(),
                );
                listenToSparkBase();
              });
            },
            color: AppConfig.appYellow,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                    '${(null != sShareModel && sShareModel.getList(PARTIES).isNotEmpty) ? "Waiting For Parties" : "Add Followers"}'),
                addSpaceWidth(5),
                Icon(LineIcons.user)
              ],
            )),
      ),
    );
  }

  handleRecording() async {
    //Use my modified screen recorder...
    /*if (Platform.isIOS) {
      if (isRecording) {
        //showProgress(true, context, msg: "Processing...");
        ScreenRecorderFlutter.stopScreenRecord;
        return;
      }
      ScreenRecorderFlutter.startScreenRecord;
      return;
    }*/

    //Android Alone.....
    if (isRecording) {
      //showProgress(true, context, msg: "Processing...");
      //ScreenRecorderFlutter.stopScreenRecord;
      String path = await FlutterScreenRecording.stopRecordScreen;
      isRecording = false;
      timerController.stop();
      recordingController.stop();
      timerController.reset();
      setState(() {});

      await Future.delayed(Duration(seconds: 2));

      File thumbnail = await VideoCompress.getFileThumbnail(path);
      BaseModel bm = BaseModel();
      bm.put(THUMBNAIL_PATH, thumbnail.path);
      bm.put(VIDEO_PATH, path);
      // bm.put(VIDEO_LENGTH, length);
      bm.put(IS_VIDEO, true);
      bm.put(OBJECT_ID, getRandomId());

      pushAndResult(
          context,
          CamPreview(
            isStory: false,
            camContext: context,
            files: [bm],
          ));

      // final Trimmer _trimmer = Trimmer();
      // await _trimmer.loadVideo(videoFile: File(path));
      // pushAndResult(
      //     context,
      //     TrimmerView(
      //       _trimmer,
      //     ), result: (_) async {
      //   if (null == _) return;
      //   String path = _[0];
      //   int length = _[1];
      //   File thumbnail = await VideoCompress.getFileThumbnail(path);
      //   BaseModel bm = BaseModel();
      //   bm.put(THUMBNAIL_PATH, thumbnail.path);
      //   bm.put(VIDEO_PATH, path);
      //   bm.put(VIDEO_LENGTH, length);
      //   bm.put(IS_VIDEO, true);
      //   bm.put(OBJECT_ID, getRandomId());
      //
      //   pushAndResult(
      //       context,
      //       CamPreview(
      //         isStory: false,
      //         camContext: context,
      //         files: [bm],
      //       ));
      //   if (mounted) setState(() {});
      // });
      return;
    }

    Size win = window.physicalSize / 4; // Reduce size
    int width = win.width ~/ 10 * 10; // Round to multiple of 10
    int height = win.height ~/ 10 * 10;

    bool started = false;
    started = await FlutterScreenRecording.startRecordScreenAndAudio(
        DateTime.now().millisecondsSinceEpoch.toString(),
        //width: width,
        //height: height,
        titleNotification: "SparkShare",
        messageNotification: "SparkShare On-Going");

    print("started $started");
    if (!started) return;
    timerController.forward();
    recordingController.repeat(reverse: true);
    //final bm = BaseModel();
    //bm.put(VIDEO_PATH, '');
    //videoPaths.add(bm);
    //if (started)
    isRecording = true;
    if (mounted) setState(() {});
  }
}
