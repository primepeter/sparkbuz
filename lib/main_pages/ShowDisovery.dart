import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dashed_circle/dashed_circle.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sparkbuz/MainAdmin.dart';
import 'package:sparkbuz/AppConfig.dart';
import 'package:sparkbuz/basemodel.dart';
import 'package:sparkbuz/main_pages/ShowVideoPost.dart';
import 'package:timeago/timeago.dart' as timeago;

import '../AppEngine.dart';
import '../ShowStatus.dart';
import '../assets.dart';

class ShowDiscovery extends StatefulWidget {
  final String category;
  final List<BaseModel> sorted;
  const ShowDiscovery({Key key, this.category, this.sorted}) : super(key: key);
  @override
  _ShowDiscoveryState createState() => _ShowDiscoveryState();
}

class _ShowDiscoveryState extends State<ShowDiscovery>
    with AutomaticKeepAliveClientMixin {
  final refreshController = RefreshController(initialRefresh: false);
  bool canRefresh = true;

  List get appCategories {
    final list = appSettingsModel.getList(POST_CATEGORIES);
    list.sort();
    return list;
  }

  List<List<BaseModel>> storyList = [];
  bool setup = false;
  var subs = [];
  String category;
  List<BaseModel> sorted = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    category = widget.category;
    sorted = widget.sorted;
    setup = false;
    refreshStories();
    var refreshSub = homeRefreshController.stream.listen((b) {
      refreshStories();
    });
    subs.add(refreshSub);
  }

  bool hasUnreadStory() {
    bool hasUnread = false;
    for (List<BaseModel> list in storyList) {
      for (BaseModel bm in list) {
        if (bm.myItem()) continue;
        if (!bm.getList(SHOWN).contains(userModel.getObjectId())) {
          hasUnread = true;
          break;
        }
      }
      if (hasUnread) break;
    }
    return hasUnread;
  }

  refreshStories() {
    for (BaseModel model in allStories) {
      int listIndex = storyList.indexWhere((List<BaseModel> models) =>
          models[0].getString(USER_ID) == model.getString(USER_ID));

      //Fetch the story list...
      List<BaseModel> stories = listIndex == -1 ? [] : storyList[listIndex];
      int storyIndex =
          stories.indexWhere((bm) => bm.getObjectId() == model.getObjectId());
      if (storyIndex == -1) {
        stories.add(model);
      }
      stories.sort((bm1, bm2) => bm1.getTime().compareTo(bm2.getTime()));

      if (listIndex == -1) {
        storyList.add(stories);
      } else {
        storyList[listIndex] = stories;
      }
    }

    //Split the list
    List<BaseModel> myList = [];
    List<List<BaseModel>> unreadList = [];
    List<List<BaseModel>> readList = [];
    for (List list in storyList) {
      bool myItem = list.isEmpty
          ? false
          : (list[0].getUserId() == userModel.getObjectId());
      if (myItem) {
        myList = list;
        continue;
      }
      int unread = list.indexWhere(
          (bm) => !bm.getList(SHOWN).contains(userModel.getObjectId()));
      if (unread != -1) {
        unreadList.add(list);
      } else {
        readList.add(list);
      }
    }
    unreadList.sort((list1, list2) => list2[list2.length - 1]
        .getTime()
        .compareTo(list1[list1.length - 1].getTime()));
    readList.sort((list1, list2) => list2[list2.length - 1]
        .getTime()
        .compareTo(list1[list1.length - 1].getTime()));
    //sort the stories
    //storyList.sort((listA, listB)=>listA);
    storyList.clear();
    if (myList.isNotEmpty) storyList.add(myList);
    storyList.addAll(unreadList);
    storyList.addAll(readList);
    setup = true;
    if (mounted) setState(() {});
  }

  loadStories(bool isNew) async {
    List local = [];
    FirebaseFirestore.instance
        .collection(STORY_BASE)
//        .where(TIME,
//            isGreaterThan: (DateTime.now().millisecondsSinceEpoch -
//                (Duration.millisecondsPerDay * 100)))
        .limit(10)
        .orderBy(TIME, descending: true)
        .startAt([
          !isNew
              ? (allStories.isEmpty
                  ? DateTime.now().millisecondsSinceEpoch
                  : allStories[allStories.length - 1].getTime())
              : (allStories.isEmpty ? 0 : allStories[0].getTime())
        ])
        .get()
        .then((shots) async {
          local = shots.docs;
          for (var doc in shots.docs) {
            final model = BaseModel(doc: doc);
            int p = allStories
                .indexWhere((e) => e.getObjectId() == model.getObjectId());
            if (p != -1) {
              allStories[p] = model;
            } else {
              allStories.add(model);
            }
          }

          if (isNew) {
            refreshController.refreshCompleted();
          } else {
            int oldLength = storyList.length;
            int newLength = local.length;
            if (newLength <= oldLength) {
              refreshController.loadNoData();
              refreshController.refreshCompleted();
              //canRefresh = false;
            } else {
              refreshController.loadComplete();
              refreshController.refreshCompleted();
            }
          }
          setup = true;
          allStories.sort((a, b) => b.getTime().compareTo(a.getTime()));
          homeRefreshController.add(true);
        });
  }

  @override
  dispose() {
    for (var sub in subs) sub?.cancel();
    super.dispose();
  }

  loadPlaylist(bool isNew) async {
    List local = [];
    FirebaseFirestore.instance
        .collection(POSTS_BASE)
        //.where(USER_ID,isEqualTo: userModel.getUserId())
        .limit(10)
        .orderBy(TIME, descending: true)
        .startAt([
          !isNew
              ? (allPostList.isEmpty
                  ? DateTime.now().millisecondsSinceEpoch
                  : allPostList[allPostList.length - 1].getTime())
              : (allPostList.isEmpty ? 0 : allPostList[0].getTime())
        ])
        .get()
        .then((shots) async {
          local = shots.docs;
          for (var doc in shots.docs) {
            final model = BaseModel(doc: doc);
            int p = allPostList
                .indexWhere((e) => e.getObjectId() == model.getObjectId());
            if (p != -1) {
              allPostList[p] = model;
            } else {
              allPostList.add(model);
            }
          }

          if (isNew) {
            refreshController.refreshCompleted();
          } else {
            int oldLength = allPostList.length;
            int newLength = local.length;
            if (newLength <= oldLength) {
              refreshController.loadNoData();
              refreshController.refreshCompleted();
              canRefresh = false;
            } else {
              refreshController.loadComplete();
              refreshController.refreshCompleted();
            }
          }
          allPostSetup = true;
          allPostList.sort((a, b) => b.getTime().compareTo(a.getTime()));
          if (mounted) setState(() {});
        });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: white,
      body: page(),
    );
  }

  page() {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.only(top: 40, right: 10, left: 0, bottom: 10),
          child: Row(
            children: [
              BackButton(),
              addSpaceWidth(5),
              Container(
                height: 40,
                width: 40,
                child: Text(
                  category.substring(0, 1).toUpperCase(),
                  style: textStyle(true, 14, white),
                ),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    color: AppConfig.appColor,
                    borderRadius: BorderRadius.circular(8),
//                        shape: BoxShape.circle,
                    border:
                        Border.all(color: black.withOpacity(.09), width: 0.8)),
              ),
              addSpaceWidth(5),
              Text(
                "$category",
                style: textStyle(true, 18, black),
              ),
              Spacer(),
            ],
          ),
        ),
        pageRefresh()
      ],
    );
  }

  pageRefresh() {
    return Flexible(
      child: SmartRefresher(
        controller: refreshController,
        enablePullDown: true,
        enablePullUp: allPostSetup,
        header: WaterDropHeader(),
        footer: ClassicFooter(
          noDataText: "Nothing more for now, check later...",
          textStyle: textStyle(false, 12, black.withOpacity(.7)),
        ),
        onLoading: () {
          loadPlaylist(false);
          loadStories(false);
        },
        onRefresh: () {
          loadPlaylist(true);
          loadStories(true);
        },
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(bottom: 100),
          children: [
            //sparkStories(),
            pageList()
          ],
        ),
      ),
    );
  }

  sparkStories() {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      padding: EdgeInsets.all(0),
      child: Row(
        children: List.generate(storyList.length, (p) {
          int likedCount = 0;
          List<BaseModel> list = storyList[p];
          BaseModel model = list[0];
          bool myItem = model.getUserId() == userModel.getObjectId();
          bool isVideo = model.getBoolean(IS_VIDEO);
          String image = model.getString(isVideo ? THUMBNAIL_URL : STORY_IMAGE);

          String text = model.getString(STORY_TEXT);
          String mColor = model.getString(COLOR_KEY);
          final dateT = DateTime.fromMillisecondsSinceEpoch(model.getTime());
          String timeAgo = timeago.format(dateT);
          String name = model.getString(NAME);
          bool readAll = true;
          for (BaseModel bm in list) {
            likedCount = likedCount + bm.getList(LIKED).length;
            if (bm.myItem()) continue;
            if (!bm.getList(SHOWN).contains(userModel.getObjectId())) {
              readAll = false;
              break;
            }
          }

          double screenWidth = MediaQuery.of(context).size.width;
          double size = 100.0;
          return Container(
            //width: screenWidth / 4,
            //height: screenWidth / 4,
            margin: EdgeInsets.all(10),
            child: Stack(
              children: [
                Opacity(
                  opacity: model.myItem() || !readAll ? 1 : (.4),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      GestureDetector(
                        onTap: () {
                          int startAt = list.lastIndexWhere((bm) => bm
                              .getList(SHOWN)
                              .contains(userModel.getObjectId()));
                          startAt = startAt + 1;
                          startAt = startAt > list.length - 1 ? 0 : startAt;
                          pushAndResult(context, ShowStatus(list, startAt),
                              result: (_) {
                            if (_ is String && _.isNotEmpty) {
                              if (list.length == 1) {
                                storyList.removeAt(p);
                              } else {
                                list.removeWhere((bm) => _ == bm.getObjectId());
                              }
                              allStories
                                  .removeWhere((bm) => _ == bm.getObjectId());
                            }
                            setState(() {});
                          }, depend: false);
                        },
                        child: Container(
                          width: screenWidth / 4,
                          height: screenWidth / 4,
                          color: transparent,
                          child: Stack(
                            fit: StackFit.expand,
                            children: <Widget>[
                              DashedCircle(
                                dashes: list.length == 1 ? 0 : list.length,
                                color: blue0,
                                strokeWidth: 1,
                                gapSize: 5,
                                child: Card(
                                  elevation: 0,
                                  clipBehavior: Clip.antiAlias,
                                  shape: CircleBorder(
                                      /*side: BorderSide(color: black.withOpacity(.2),width: 1)*/),
                                  child: image.isNotEmpty
                                      ? CachedNetworkImage(
                                          imageUrl: image,
                                          fit: BoxFit.cover,
                                          width: 70,
                                          height: 70,
                                        )
                                      : (Container(
                                          width: 70,
                                          height: 70,
                                          decoration: BoxDecoration(
                                              color: getColorForKey(mColor),
                                              shape: BoxShape.circle),
                                          child: Center(
                                            child: Padding(
                                              padding: const EdgeInsets.all(5),
                                              child: Text(
                                                text,
                                                style:
                                                    textStyle(true, 18, white),
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                          ),
                                        )),
                                ),
                              ),
                              if (likedCount > 0 && myItem)
                                Align(
                                  alignment: Alignment.topRight,
                                  child: new Container(
                                    //margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                                    padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
//                            width: 65,
                                    height: 25, width: 40,
                                    decoration: BoxDecoration(
                                        color: black.withOpacity(.8),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(25))),
                                    child: Center(
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          Text(
                                            likedCount > 9
                                                ? "9+"
                                                : "$likedCount",
                                            style: textStyle(true, 12, white),
                                          ),
                                          if (likedCount <= 9) addSpaceWidth(5),
//                              Image.asset(
//                                heart,
//                                color: red0,
//                                width: 10,
//                                height: 10,
//                              ),
                                          Icon(
                                            Icons.visibility,
                                            size: 14,
                                            color: white,
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                            ],
                          ),
                        ),
                      ),
                      addSpace(10),
                      Text(
                        model.myItem() ? "Your Story" : name,
                        style: textStyle(true, 14, black),
                      ),
                      Text(
                        timeAgo,
                        style: textStyle(false, 12, black),
                      ),
                    ],
                  ),
                ),
                if (!myItem && readAll)
                  Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                          height: 30,
                          width: 30,
                          decoration: BoxDecoration(
                              border: Border.all(color: white, width: 2),
                              shape: BoxShape.circle,
                              color: green),
                          child: Icon(
                            Icons.check,
                            size: 15,
                            color: white,
                          )))
              ],
            ),
          );
        }),
      ),
    );
  }

  pageList() {
    return Builder(
      builder: (ctx) {
        if (!allPostSetup)
          return Container(
            height:
                (getScreenHeight(context) - getScreenHeight(context) * .22) / 2,
            child: loadingLayout(),
          );

        return ListView.builder(
            padding: EdgeInsets.all(0),
            physics: NeverScrollableScrollPhysics(),
            itemCount: sorted.length,
            shrinkWrap: true,
            itemBuilder: (ctx, p) {
              return discoverItem(p);
            });
      },
    );
  }

  discoverItem(int p) {
    BaseModel md = sorted[p];
    String image = md.getString(THUMBNAIL_URL);
    String desc = md.getString(DESCRIPTION);
    int seenBy = md.getList(SEEN_BY).length;
    final dateT = DateTime.fromMillisecondsSinceEpoch(md.getTime());
    String timeAgo = timeago.format(dateT);

    return GestureDetector(
      onTap: () {
        pushAndResult(
            context,
            ShowVideoPost(
              videoModel: md,
            ));
      },
      onLongPress: () {
        if (md.myItem()) {
          yesNoDialog(context, "Delete Video",
              'Are you sure you want to delete this video?', () {
            allPostList.remove(md);
            sorted.remove(md);
            md.deleteItem();
            setState(() {});
          });
        }
      },
      child: ClipRRect(
        borderRadius: BorderRadius.circular(15),
        child: Container(
          margin: EdgeInsets.all(6),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(15),
            child: Container(
              height: 200,
              width: double.infinity,
              child: Stack(
                children: [
                  CachedNetworkImage(
                    imageUrl: image,
                    height: double.infinity,
                    width: double.infinity,
                    fit: BoxFit.cover,
                    placeholder: (c, s) {
                      return placeHolder(200, width: double.infinity);
                    },
                  ),
                  Container(
                    height: 200,
                    width: double.infinity,
                    color: black.withOpacity(.3),
                    child: Center(
                      child: Container(
                        height: 40,
                        width: 40,
                        child: Icon(
                          Icons.play_arrow,
                          color: Colors.white,
                          size: 18,
                        ),
                        decoration: BoxDecoration(
                            color: black.withOpacity(0.8),
                            border: Border.all(color: Colors.white, width: 1.5),
                            shape: BoxShape.circle),
                      ),
                    ),
                  ),
                  addSpaceWidth(10),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        padding: EdgeInsets.all(5),
                        child: Row(
                          children: [
                            userImageItem(context, md,
                                iconSize: 10,
                                backColor: AppConfig.appColor,
                                strock: 2,
                                size: 40,
                                padLeft: false),
                            addSpaceWidth(5),
                            Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  md.getUserName(),
                                  style: textStyle(true, 14, white),
                                ),
                                Text(
                                  "${formatToK(seenBy)} views",
                                  style: textStyle(false, 11, white),
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                      Spacer(),
                      Container(
                        padding: EdgeInsets.all(10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              desc,
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: textStyle(true, 18, white),
                            ),
                            addSpace(5),
                            Text(
                              timeAgo,
                              style: textStyle(false, 12, white),
                            ),
                          ],
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
