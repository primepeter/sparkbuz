// import 'dart:async';
// import 'dart:io';
//
// import 'package:cached_video_player/cached_video_player.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:flare_flutter/flare_actor.dart';
// import 'package:flutter/material.dart';
// import 'package:line_icons/line_icons.dart';
// import 'package:sparkbuz/AppConfig.dart';
// import 'package:sparkbuz/AppEngine.dart';
// import 'package:sparkbuz/Filterlayout.dart';
// import 'package:sparkbuz/MainAdmin.dart';
// import 'package:sparkbuz/SparkIcon.dart';
// import 'package:sparkbuz/basic/Tapped.dart';
// import 'package:sparkbuz/basic/VideoListController.dart';
// import 'package:sparkbuz/main_pages/ShowVideoPost.dart';
//
// import '../assets.dart';
// import '../basemodel.dart';
// import 'CameraSparkShare.dart';
// import 'Notifications.dart';
// import 'ShowProfile.dart';
//
// int mainCurrentPage = 0;
// final mainVp = PageController();
// List<VideoManager> videoManager = [];
//
// class VideoManager {
//   final CachedVideoPlayerController controller;
//   final String id;
//
//   VideoManager(this.controller, this.id);
// }
//
// class Home extends StatefulWidget {
//   @override
//   _HomeState createState() => _HomeState();
// }
//
// class _HomeState extends State<Home>
//     with AutomaticKeepAliveClientMixin, WidgetsBindingObserver {
//   int playerPage = 0;
//   final playerVp = PageController();
//   List<StreamSubscription> subs = [];
//   ScrollController playListScroll = ScrollController();
//
//   @override
//   initState() {
//     super.initState();
//     //WidgetsBinding.instance.addObserver(this);
//     var playSub = playerController.stream.listen((player) {
//       int p = playList.indexWhere(
//           (e) => e.model.getObjectId() == player.model.getObjectId());
//       if (p == -1) {
//         playList.add(player);
//       } else {
//         playList[p] = player;
//       }
//       setState(() {});
//     });
//
//     var pauseSub = pauseController.stream.listen((play) {
//       playCurrentVideo(play);
//     });
//
//     var insertSub = insertVideoController.stream.listen((player) {
//       playList.insert(0, player);
//       setState(() {});
//     });
//     var deleteSub = deleteVideoController.stream.listen((player) {
//       playList.removeWhere(
//           (e) => e.model.getObjectId() == player.model.getObjectId());
//       setState(() {});
//     });
//     subs.add(playSub);
//     subs.add(pauseSub);
//     subs.add(insertSub);
//     subs.add(deleteSub);
//   }
//
//   List items = [];
//   bool setup = false;
//
//   loadItems(bool isNew, {bool fromLocal = false}) async {
//     FirebaseFirestore.instance
//         .collection(POSTS_BASE)
//         //.where(PARTIES, arrayContains: userModel.getUserId())
//         .limit(10)
//         .orderBy(TIME, descending: !isNew)
//         .startAt([
//           !isNew
//               ? (items.isEmpty
//                   ? DateTime.now().millisecondsSinceEpoch
//                   : items[items.length - 1][TIME] ?? 0.toInt())
//               : (items.isEmpty ? 0 : items[0][TIME] ?? 0.toInt())
//         ])
//         .get()
//         .then((query) {
//           for (var doc in query.documents) {
//             BaseModel model = BaseModel(doc: doc);
//             //if (model.hidden.contains(userModel.getUserId())) continue;
//             int p = items.indexWhere((e) =>
//                 BaseModel(items: e).getObjectId() == model.getObjectId());
//             if (p == -1)
//               items.add(model.items);
//             else
//               items[p] = model.items;
//           }
//           loadVideos();
//           setup = true;
//           //items.sort((a, b) => b[TIME] ?? 0.compareTo(a[TIME] ?? 0));
//           if (mounted) setState(() {});
//         })
//         .catchError((e) {
//           if (e.toString().contains("CACHE")) {
//             loadItems(isNew, fromLocal: false);
//           }
//         });
//   }
//
//   loadVideos({bool initial = false}) {
//     for (int p = 0; p < allPostList.length; p++) {
//       BaseModel bm = allPostList[p];
//       CachedVideoPlayerController control;
//       bool playThis = 0 == p;
//       String objectId = bm.getObjectId();
//       String videoUrl = bm.getString(VIDEO_URL);
//       control = CachedVideoPlayerController.network(videoUrl);
//       control.initialize().then((value) {
//         int index = videoManager.indexWhere((e) => e.id == objectId);
//         if (index == -1) {
//           videoManager.add(VideoManager(control, objectId));
//         } else {
//           videoManager[index] = VideoManager(control, objectId);
//         }
//         //if (playThis)
//           control.setLooping(true);
//         if (mounted) setState(() {});
//         print("Has been reopened video but needs to be re-initialized!!!");
//       });
//       int index = videoManager.indexWhere((e) => e.id == objectId);
//       if (index == -1) {
//         videoManager.add(VideoManager(control, objectId));
//       } else {
//         videoManager[index] = VideoManager(control, objectId);
//       }
//       if (mounted) setState(() {});
//     }
//   }
//
//   pauseAnyVideo() async {
//     //pause mine.....
//     for (int p = 0; p < videoManager.length; p++) {
//       final manager = videoManager[p];
//       if (null == manager.controller) {
//         continue;
//       }
//       try {
//         videoManager[p].controller.pause();
//         if (mounted) setState(() {});
//         await Future.delayed(Duration(milliseconds: 500));
//       } catch (e) {
//         print(e);
//       }
//     }
//   }
//
//   playVideoInPage(int p, {bool pause = false}) {
//     if (videoManager.isEmpty) return;
//     final manager = videoManager[p];
//     if (null == manager.controller) {
//       print("show error failed to load video!");
//       return;
//     }
//     if (pause)
//       manager.controller?.pause();
//     else
//       manager.controller?.play();
//     if (mounted) setState(() {});
//   }
//
//   disposeVideo() async {
//     for (int p = 0; p < videoManager.length; p++) {
//       final manager = videoManager[p];
//       if (null == manager.controller) {
//         continue;
//       }
//       try {
//         manager.controller.pause();
//         videoManager[p] = VideoManager(null, manager.id);
//         if (mounted) setState(() {});
//         await Future.delayed(Duration(seconds: 1));
//         await manager.controller.dispose();
//       } catch (e) {
//         print(e);
//       }
//     }
//   }
//
//   playCurrentVideo(bool play) {
//     if (playList.isEmpty) return;
//     for (var c in playList) {
//       if (play)
//         c.controller?.play();
//       else
//         c.controller?.pause();
//     }
//     if (mounted) setState(() {});
//   }
//
//   @override
//   void dispose() {
//     WidgetsBinding.instance.removeObserver(this);
//     disposeVideo();
//
//     videoListController.currentPlayer?.pause();
//     videoListController?.dispose();
//     for (var s in subs) s?.cancel();
//     for (var p in playList) p.controller?.dispose();
//     super.dispose();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     super.build(context);
//     return Scaffold(
//       backgroundColor: black,
//       body: page(),
//     );
//   }
//
//   page() {
//     return Stack(
//       children: [
//         PageView(
//           onPageChanged: (p) {
//             if (playList.isNotEmpty) {
//               if (p == 0)
//                 playCurrentVideo(true);
//               else
//                 playCurrentVideo(false);
//             }
//             mainCurrentPage = p;
//             setState(() {});
//           },
//           controller: mainVp,
//           children: [
//             PageView.builder(
//                 controller: playerVp,
//                 //physics: NeverScrollableScrollPhysics(),
//                 itemCount: playList.length > 10 ? 10 : playList.length,
//                 scrollDirection: Axis.vertical,
//                 onPageChanged: (p) {
//                   handlePageChange(p, jumpTo: false);
//                 },
//                 itemBuilder: (c, p) {
//                   Player player = playList[p];
//                   BaseModel playModel = player.model;
//                   final controller = player.controller;
//
//                   if (!controller.value.initialized)
//                     return Center(child: videoLoading);
//
//                   return Stack(
//                     //fit: StackFit.expand,
//                     children: [
//                       Align(
//                         alignment: Alignment.center,
//                         child: AspectRatio(
//                           aspectRatio: controller.value.aspectRatio,
//                           child: CachedVideoPlayer(player.controller),
//                         ),
//                       ),
//                       FilterLayout(
//                         currentPlayer: controller,
//                         filters: playModel.getListModel(FILTERS),
//                         enableFilter: false,
//                       ),
//                       Tapped(
//                         showIcon: !controller.value.isPlaying,
//                         // showIcon: videoListController.currentPlayer.value.isPlaying,
//                         child: Container(
//                           color: transparent,
//                           alignment: Alignment.center,
//                           child: AnimatedContainer(
//                               duration: Duration(milliseconds: 400),
//                               height: 50,
//                               width: 50,
//                               decoration: BoxDecoration(
//                                   border: Border.all(color: white, width: 1),
//                                   color: black.withOpacity(.5),
//                                   shape: BoxShape.circle),
//                               child: Icon(
//                                 controller.value.isPlaying
//                                     ? Icons.pause
//                                     : Icons.play_arrow,
//                                 color: white,
//                                 size: 20,
//                               )),
//                         ),
//                         onTap: () {
//                           print(playModel.getObjectId());
//                           if (controller.value.isPlaying)
//                             controller.pause();
//                           else
//                             controller.play();
//                           setState(() {});
//                         },
//                       ),
//                     ],
//                   );
//                 }),
//             if (playList.isNotEmpty)
//               ShowProfile(
//                 model: getCurrentModel,
//                 profileId: getCurrentModel.getUserId(),
//               )
//           ],
//         ),
//         if (mainCurrentPage == 0 && playList.isNotEmpty) videoControl(),
//         if (mainCurrentPage == 0)
//           Align(
//             alignment: Alignment.topRight,
//             child: new Container(
//               height: 40,
//               //width: 60,
//               margin: EdgeInsets.only(top: 40, right: 10),
//               decoration: BoxDecoration(
//                   color: AppConfig.appYellow,
//                   borderRadius: BorderRadius.circular(10)),
//               child: Row(
//                 mainAxisSize: MainAxisSize.min,
//                 children: [
//                   Container(
//                     height: 40,
//                     width: 40,
//                     alignment: Alignment.center,
//                     child: Image.asset(
//                       "assets/sparkbuz/lightening.png",
//                       alignment: Alignment.centerRight,
//                     ),
//                   ),
//                   //addSpaceWidth(4),
//                   GestureDetector(
//                     onTap: () {
//                       showNotify = !showNotify;
//                       setState(() {});
//                     },
//                     child: Icon(showNotify
//                         ? Icons.navigate_next_sharp
//                         : Icons.navigate_before),
//                   ),
//                   if (showNotify)
//                     Stack(
//                       children: [
//                         GestureDetector(
//                           onTap: () {
//                             playCurrentVideo(false);
//                             pushAndResult(context, Notifications(),
//                                 result: (_) {
//                               if (null == _) return;
//                               playCurrentVideo(true);
//                             });
//                           },
//                           child: Container(
//                             padding: EdgeInsets.all(5),
//                             color: transparent,
//                             child: Icon(
//                               LineIcons.bell_o,
//                               size: 24,
//                               color: black,
//                             ),
//                           ),
//                         ),
//                         if (showNewNotifyDot.length > 0)
//                           Align(
//                             alignment: Alignment.topLeft,
//                             child: Container(
//                               // height: 10,
//                               // width: 10,
//                               padding: EdgeInsets.all(3),
//                               child: Text(
//                                 showNewNotifyDot.length.toString(), //  "New",
//                                 style: textStyle(false, 11, white),
//                               ),
//                               decoration: BoxDecoration(
//                                   color: red,
//                                   borderRadius: BorderRadius.circular(5)
//                                   //shape: BoxShape.circle
//                                   ),
//                             ),
//                           )
//                       ],
//                     ),
//                   if (showNotify)
//                     Container(
//                       width: 1,
//                       color: black.withOpacity(.2),
//                       height: 40,
//                       margin: EdgeInsets.all(2),
//                     ),
//                   if (showNotify)
//                     new FlatButton(
//                         padding: EdgeInsets.all(0),
//                         materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
//                         onPressed: () {
//                           playCurrentVideo(false);
//                           pushAndResult(context, CameraShare(), result: (_) {
//                             if (null == _) return;
//                             //playCurrentVideo(true);
//                           });
//                         },
//                         child: Container(
//                           child: Row(
//                             children: [
//                               Text('SShare'),
//                               addSpaceWidth(5),
//                               Icon(
//                                 LineIcons.video_camera,
//                                 //size: 24,
//                                 color: black,
//                               ),
//                             ],
//                           ),
//                         )),
//                 ],
//               ),
//             ),
//           ),
//       ],
//     );
//   }
//
//   bool showPlaylist = false;
//
//   videoControl() {
//     return Align(
//       alignment: Alignment.bottomCenter,
//       child: Container(
//         //color: red,
//         padding: EdgeInsets.only(bottom: 100),
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.end,
//           mainAxisSize: MainAxisSize.min,
//           children: [
//             Row(
//               children: [
//                 GestureDetector(
//                   onTap: () {
//                     setState(() {
//                       showPlaylist = !showPlaylist;
//                     });
//                   },
//                   child: Container(
//                     //width: 10,
//                     height: 160,
//                     color: black.withOpacity(1),
//                     child: Icon(
//                       showPlaylist
//                           ? Icons.navigate_before
//                           : Icons.navigate_next,
//                       color: white,
//                     ),
//                   ),
//                 ),
//                 Flexible(
//                   child: AnimatedContainer(
//                     duration: Duration(milliseconds: 600),
//                     child: showPlaylist ? videoPlaylist() : onScreenControls(),
//                   ),
//                 ),
//               ],
//             ),
//           ],
//         ),
//       ),
//     );
//   }
//
//   BaseModel get getCurrentModel {
//     return playList[playerPage].model;
//   }
//
//   videoPlaylist() {
//     return SingleChildScrollView(
//       scrollDirection: Axis.horizontal,
//       controller: playListScroll,
//       child: Row(
//         children: List.generate(
//             showPlaylist
//                 ? playList.length > 10
//                     ? 10
//                     : playList.length
//                 : 0,
//             // showPlaylist ? playerList.length > 10 ? 10 : playerList.length : 0,
//             (p) {
//           Player player = playList[p];
//           bool activePlayer = playerPage == p;
//           final controller = player.controller;
//           BaseModel md = player.model;
//           String image = md.getString(THUMBNAIL_URL);
//           int seenBy = md.getList(SEEN_BY).length;
//
//           return GestureDetector(
//             onTap: () {
//               handlePageChange(p);
//             },
//             child: Stack(
//               children: [
//                 ClipRRect(
//                   borderRadius: BorderRadius.circular(80),
//                   child: AnimatedContainer(
//                     duration: Duration(milliseconds: 600),
//                     height: 120,
//                     width: 120,
//                     margin: EdgeInsets.all(10),
//                     decoration: BoxDecoration(
//                         border: Border.all(
//                             color: AppConfig.appYellow,
//                             width: activePlayer ? 4 : 0),
//                         color: white,
//                         shape: BoxShape.circle),
//                     child: Stack(
//                       children: [
//                         ClipRRect(
//                           borderRadius: BorderRadius.circular(80),
//                           child: CachedNetworkImage(
//                             imageUrl: image,
//                             height: 120,
//                             width: 120,
//                             fit: BoxFit.cover,
//                             placeholder: (c, s) {
//                               return placeHolder(120, width: 120);
//                             },
//                           ),
// //                          Image.asset(
// //                        imageUrl,
// //                        height: 120,
// //                        width: 120,
// //                        fit: BoxFit.cover,
// //                      ),
//                         ),
//                         ClipRRect(
//                           borderRadius: BorderRadius.circular(80),
//                           child: Container(
//                             height: 120,
//                             width: 120,
//                             color: black.withOpacity(.15),
//                             alignment: Alignment.center,
//                             child: Container(
//                                 decoration: BoxDecoration(
//                                     border: Border.all(color: white, width: 1),
//                                     color: black.withOpacity(.5),
//                                     shape: BoxShape.circle),
//                                 alignment: Alignment.center,
//                                 height: 30,
//                                 width: 30,
//                                 child: Icon(
//                                   activePlayer ? Icons.pause : Icons.play_arrow,
//                                   color: white,
//                                   size: 20,
//                                 )),
//                           ),
//                         ),
//                         Align(
//                           alignment: Alignment.centerLeft,
//                           child: Container(
//                               decoration: BoxDecoration(
//                                   color: AppConfig.appYellow,
//                                   borderRadius: BorderRadius.only(
//                                       topLeft: Radius.circular(15),
//                                       bottomLeft: Radius.circular(15),
//                                       topRight: Radius.circular(20),
//                                       bottomRight: Radius.circular(20))),
//                               padding: EdgeInsets.all(10),
//                               margin: EdgeInsets.all(0),
//                               child: Text(
//                                 (p + 1).toString(),
//                                 style: textStyle(true, 16, black),
//                               )),
//                         )
//                       ],
//                     ),
//                   ),
//                 ),
//               ],
//             ),
//           );
//         }),
//       ),
//     );
//   }
//
//   onScreenControls() {
//     return Container(
//       padding: EdgeInsets.all(10),
//       child: Column(
//         //mainAxisSize: MainAxisSize.min,
//         crossAxisAlignment: CrossAxisAlignment.start,
//
//         children: [
//           if (showOptions) videoDescription(),
//           videoControls(),
//         ],
//       ),
//     );
//   }
//
//   videoDescription() {
//     String username = getCurrentModel.getUserName();
//     String description = getCurrentModel.getString(DESCRIPTION);
//     String category = getCurrentModel.getString(CATEGORY);
//
//     return Container(
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         mainAxisAlignment: MainAxisAlignment.end,
//         children: [
//           GestureDetector(
//             onTap: () {
//               pauseController.add(false);
//               pushAndResult(
//                   context,
//                   ShowProfile(
//                     showCloseBtn: true,
//                     model: getCurrentModel,
//                     profileId: getCurrentModel.getUserId(),
//                   ), result: (_) {
//                 pauseController.add(true);
//               });
//             },
//             child: Text(
//               "@$username",
//               style: textStyle(true, 16, white),
//             ),
//           ),
//           addSpace(5),
//           if (description.isNotEmpty) ...[
//             Text(
//               description,
//               style: textStyle(false, 14, white),
//             ),
//             addSpace(5),
//           ],
//           if (category.isNotEmpty)
//             Row(
//               mainAxisAlignment: MainAxisAlignment.start,
//               children: [
//                 Text(
//                   "Category -",
//                   style: textStyle(false, 12, white),
//                 ),
//                 addSpaceWidth(5),
//                 Text(
//                   category,
//                   style: textStyle(false, 14, white),
//                 ),
//               ],
//             ),
//         ],
//       ),
//     );
//   }
//
//   videoControls() {
//     String likes = formatToK(getCurrentModel.getList(LIKES_COUNT).length);
//
//     String comments = formatToK(getCurrentModel.getList(COMMENT_LIST).length);
//
//     String shares = formatToK(getCurrentModel.getList(SHARE_LIST).length);
//     String photo = getCurrentModel.getString(USER_IMAGE);
//
//     bool liked =
//         getCurrentModel.getList(LIKES_COUNT).contains(userModel.getUserId());
//
//     return Row(
//       mainAxisAlignment: MainAxisAlignment.end,
//       //crossAxisAlignment: CrossAxisAlignment.center,
//       // mainAxisSize: MainAxisSize.min,
//       children: [
//         GestureDetector(
//           onTap: () {
//             showOptions = !showOptions;
//             setState(() {});
//           },
//           child: Container(
//             color: transparent,
//             child: Stack(
//               alignment: AlignmentDirectional.center,
//               children: <Widget>[
//                 ClipRRect(
//                   borderRadius: BorderRadius.circular(50),
//                   child: Container(
//                     height: 50,
//                     width: 50,
//                     child: userImageItem(context, getCurrentModel,
//                         padLeft: false, strock: 1, size: 50),
//                     decoration: BoxDecoration(
//                         border: Border.all(
//                             color: Colors.white,
//                             width: 1.0,
//                             style: BorderStyle.solid),
//                         color: Colors.black,
//                         shape: BoxShape.circle),
//                   ),
//                 ),
//                 Container(
//                   margin: EdgeInsets.only(top: 50),
//                   height: 18,
//                   width: 18,
//                   child: Icon(Icons.add, size: 10, color: Colors.white),
//                   decoration: BoxDecoration(
//                       color: AppConfig.appColor, shape: BoxShape.circle),
//                 )
//               ],
//             ),
//           ),
//         ),
//         if (showOptions)
//           Expanded(
//             child: Row(
//               mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//               children: [
//                 GestureDetector(
//                   onTap: () {
//                     getCurrentModel
//                       ..putInList(LIKES_COUNT, userModel.getUserId(), !liked)
//                       ..updateItems();
//                     userModel
//                       ..putInList(
//                           LIKES_COUNT, getCurrentModel.getObjectId(), !liked)
//                       ..updateItems();
//                     setState(() {});
//                   },
//                   child: Column(
//                     children: [
//                       Container(
//                           //margin: EdgeInsets.only(right: 10),
//                           width: 35,
//                           height: 35,
//                           // decoration: BoxDecoration(
//                           //     color: green_dark, shape: BoxShape.circle),
//                           padding: EdgeInsets.all(6),
//                           child: FlareActor("assets/icons/Favorite.flr",
//                               shouldClip: false,
//                               color: liked
//                                   ? AppConfig.appYellow
//                                   : white.withOpacity(.5),
//                               fit: BoxFit.cover,
//                               animation: liked
//                                   ? "Favorite"
//                                   : "Unfavorite" //_animationName
//                               )),
//                       Text(
//                         likes,
//                         style: textStyle(true, 10, white),
//                       )
//                     ],
//                   ),
//                 ),
//                 videoControlAction(
//                     onTap: () {
//                       playCurrentVideo(false);
//                       pushAndResult(
//                           context,
//                           ShowVideoPost(
//                             videoModel: getCurrentModel,
//                           ));
//                     },
//                     icon: AppIcons.chat_bubble,
//                     label: comments,
//                     size: 25),
//                 videoControlAction(
//                     icon: AppIcons.reply,
//                     label: shares,
//                     size: 25,
//                     onTap: () {
//                       String appLink = appSettingsModel.getString(APP_LINK_IOS);
//                       if (Platform.isAndroid)
//                         appLink = appSettingsModel.getString(APP_LINK_ANDROID);
//                       shareApp(
//                           message:
//                               "Join SparkBuz and share memories with friends and family.\n $appLink");
//
//                       getCurrentModel
//                         ..putInList(SHARE_LIST, userModel.getUserId(), true)
//                         ..updateItems();
//                       setState(() {});
//                     }),
//               ],
//             ),
//           ),
//       ],
//     );
//   }
//
//   videoControlAction({IconData icon, String label, double size = 35, onTap}) {
//     return Flexible(
//       child: GestureDetector(
//         onTap: onTap,
//         child: Container(
//           //padding: EdgeInsets.only(top: 10, bottom: 10),
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: <Widget>[
//               Icon(
//                 icon,
//                 color: Colors.white,
//                 size: size,
//               ),
//               addSpace(5),
//               Text(
//                 label ?? "",
//                 style: textStyle(true, 10, white),
//               )
//             ],
//           ),
//         ),
//       ),
//     );
//   }
//
//   bool showOptions = true;
//   bool showNotify = false;
//
//   LinearGradient get audioDiscGradient => LinearGradient(colors: [
//         Colors.grey[800],
//         Colors.grey[900],
//         Colors.grey[900],
//         Colors.grey[800]
//       ], begin: Alignment.bottomLeft, end: Alignment.topRight);
//
//   @override
//   // TODO: implement wantKeepAlive
//   bool get wantKeepAlive => true;
//
//   void handlePageChange(int p, {bool jumpTo = true}) {
//     if (8 % p == 0) loadItems(false);
//
//     showPlaylist = true;
//     setState(() {});
//
//     Future.delayed(Duration(seconds: 1), () {
//       try {
//         playListScroll.animateTo((p * 120).toDouble(),
//             duration: Duration(milliseconds: 500), curve: Curves.ease);
//       } catch (e) {}
//     });
//
//     Player player = playList[p];
//     final controller = player.controller;
//     if (playerPage == p) return;
//     controller.seekTo(Duration(seconds: 0));
//     controller.setLooping(true);
//     playList[playerPage].controller.pause();
//     controller.play();
//     playerPage = p;
//     if (jumpTo) playerVp.jumpToPage(p);
//     setState(() {});
//     Future.delayed(Duration(seconds: 15), () {
//       showPlaylist = false;
//       setState(() {});
//     });
//   }
// }
