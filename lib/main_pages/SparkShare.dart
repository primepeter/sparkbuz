import 'dart:async';

// import 'package:agora_rtc_engine/rtc_engine.dart' as agora;
// import 'package:agora_rtc_engine/rtc_local_view.dart' as RtcLocalView;
// import 'package:agora_rtc_engine/rtc_remote_view.dart' as RtcRemoteView;
import 'package:agora_rtc_engine/agora_rtc_engine.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:sparkbuz/AppConfig.dart';
import 'package:sparkbuz/ShowFollowers.dart';
import 'package:sparkbuz/basemodel.dart';

import '../AppEngine.dart';
import '../assets.dart';

class SparkShare extends StatefulWidget {
  final String sparkShareId;

  const SparkShare({Key key, this.sparkShareId}) : super(key: key);
  @override
  _SparkShareState createState() => _SparkShareState();
}

class _SparkShareState extends State<SparkShare>
    with WidgetsBindingObserver, SingleTickerProviderStateMixin {
  static const APP_ID = "40c37c3e06f840449394b70b93b177d5";
  static final _users = <int>[];
  final _infoStrings = <String>[];
  bool _switch = false;
  List<BaseModel> videoShare = [];
  String get sparkShareId => widget.sparkShareId;

  @override
  void initState() {
    super.initState();
    initializeAgora();
    listenToSparkBase();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    for (var s in subs) s?.cancel();
    AgoraRtcEngine?.destroy();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.inactive ||
        state == AppLifecycleState.paused) {
      AgoraRtcEngine?.destroy();
    } else if (state == AppLifecycleState.resumed) {
      initializeAgora();
    }
  }

  initializeAgora() async {
    await AgoraRtcEngine.create(APP_ID);
    await AgoraRtcEngine.enableAudio();
    await AgoraRtcEngine.enableVideo();
    await AgoraRtcEngine.enableWebSdkInteroperability(true);
    await AgoraRtcEngine.setParameters(
        '''{\"che.video.lowBitRateStreamParameter\":{\"width\":320,\"height\":180,\"frameRate\":15,\"bitRate\":140}}''');
    await AgoraRtcEngine.joinChannel(null, sparkShareId, null, 0);

    AgoraRtcEngine.onError = (dynamic code) {
      setState(() {
        final info = 'onError: $code';
        _infoStrings.add(info);
      });
    };

    AgoraRtcEngine.onJoinChannelSuccess = (
      String channel,
      int uid,
      int elapsed,
    ) {
      setState(() {
        final info = 'onJoinChannel: $channel, uid: $uid';
        _infoStrings.add(info);
      });
    };

    AgoraRtcEngine.onLeaveChannel = () {
      setState(() {
        _infoStrings.add('onLeaveChannel');
        _users.clear();
      });
    };

    AgoraRtcEngine.onUserJoined = (int uid, int elapsed) {
      setState(() {
        final info = 'userJoined: $uid';
        _infoStrings.add(info);
        _users.add(uid);
      });
    };

    AgoraRtcEngine.onUserOffline = (int uid, int reason) {
      setState(() {
        final info = 'userOffline: $uid';
        _infoStrings.add(info);
        _users.remove(uid);
      });
    };

    AgoraRtcEngine.onFirstRemoteVideoFrame = (
      int uid,
      int width,
      int height,
      int elapsed,
    ) {
      setState(() {
        final info = 'firstRemoteVideo: $uid ${width}x $height';
        _infoStrings.add(info);
      });
    };
  }

  BaseModel sShareModel = BaseModel();
  List<StreamSubscription> subs = [];
  int participantIndex;
  List participants = [];
  List parties = [];

  listenToSparkBase() async {
    var shots = FirebaseFirestore.instance
        .collection(SPARK_BASE)
        .doc(sparkShareId)
        .snapshots()
        .listen((event) {
      sShareModel = BaseModel(doc: event);
      //participantIndex=sShareModel.getList(key)
      bool sessionActive = sShareModel.getBoolean(SESSION_ACTIVE);
      parties = sShareModel.getList(PARTIES);
      participants = sShareModel.getList(PARTICIPANTS);
      participantIndex = parties.indexWhere((e) => e == userModel.getUserId());
      if (!sessionActive) {
        //return;
        showMessage(context, Icons.error, red, "Session Ended!",
            "Creator has closed SparkShare Session", cancellable: false,
            onClicked: (_) {
          Navigator.pop(context);
        });
        return;
      }

      if (participants[participantIndex] == 0) {
        participants[participantIndex] = 1;
        sShareModel
          ..put(PARTICIPANTS, participants)
          ..updateItems();
      }
      setState(() {});
    });
    subs.add(shots);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        yesNoDialog(context, "Leave Session?",
            "Are you sure you want to leave this SparkShare Session?", () {
          if (sShareModel != null && participants.isNotEmpty) {
            participants[participantIndex] = 2;
            sShareModel
              ..put(PARTICIPANTS, participants)
              ..updateItems();
          }

          Navigator.pop(context);
        });
        return false;
      },
      child: Scaffold(
        // backgroundColor: Color(0xFFf5f5f4),
        backgroundColor: black,
        body: SafeArea(
          bottom: false,
          child: Container(
            child: Stack(
              children: <Widget>[
                videoPanel(),
                closeButton,
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget get closeButton => GestureDetector(
        child: Container(
          margin: EdgeInsets.only(top: 0),
          padding: EdgeInsets.all(14),
          decoration: BoxDecoration(
              color: red,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(30),
                  bottomRight: Radius.circular(30))),
          child: Icon(
            Icons.clear,
            color: white,
            size: 18,
          ),
        ),
        onTap: () {
          yesNoDialog(context, "Leave Session?",
              "Are you sure you want to leave this SparkShare Session?", () {
            if (sShareModel != null && participants.isNotEmpty) {
              participants[participantIndex] = 2;
              sShareModel
                ..put(PARTICIPANTS, participants)
                ..updateItems();
            }
            Navigator.pop(context);
          });
        },
      );

  /// Helper function to get list of native views
  List<Widget> _getRenderViews() {
    final List<AgoraRenderWidget> list = [
      AgoraRenderWidget(0, local: true, preview: true),
    ];
    _users.forEach((int uid) => list.add(AgoraRenderWidget(uid)));
    return list;
  }

  /// Video view wrapper
  Widget _videoView(view) {
    return Expanded(child: Container(child: view));
  }

  /// Video view row wrapper
  Widget _expandedVideoRow(List<Widget> views) {
    final wrappedViews = views.map<Widget>(_videoView).toList();
    return Expanded(
      child: Row(
        children: wrappedViews,
      ),
    );
  }

  /// Video layout wrapper
  Widget videoPanel() {
    final views = _getRenderViews();

    switch (views.length) {
      case 1:
        return Container(
            child: Column(
          children: <Widget>[_videoView(views[0])],
        ));
      case 2:
        return Container(
            child: Column(
          children: <Widget>[
            _expandedVideoRow([views[0]]),
            _expandedVideoRow([views[1]])
          ],
        ));
      case 3:
        return Container(
            child: Column(
          children: <Widget>[
            _expandedVideoRow(views.sublist(0, 2)),
            _expandedVideoRow(views.sublist(2, 3))
          ],
        ));
      case 4:
        return Container(
            child: Column(
          children: <Widget>[
            _expandedVideoRow(views.sublist(0, 2)),
            _expandedVideoRow(views.sublist(2, 4))
          ],
        ));
      default:
    }
    return Container(
      color: transparent,
    );
  }

  addParticipants() {
    return Container(
      padding: EdgeInsets.all(5),
      child: FlatButton(
          onPressed: () {
            pushAndResult(context, ShowFollowers(), result: (_) {
              if (null == _) return;
              videoShare = _;
              setState(() {});

              //TODO Add participants to SHARE_BASE
              BaseModel model = BaseModel();
              model.put(PARTIES, videoShare.map((e) => e.getUserId()).toList());
              model.put(PARTICIPANTS,
                  List.generate(videoShare.length, (index) => 0).toList());
              model.put(OBJECT_ID, sparkShareId);
              model.put(
                  SPARK_TIMEOUT,
                  DateTime.now()
                      .add(Duration(minutes: 2))
                      .millisecondsSinceEpoch);
              model.saveItem(SPARK_BASE, true);

              //TODO Notify participants
              pushNotificationToUsers(
                notifyType: NOTIFY_TYPE_SPARK_SHARE,
                notifyId: sparkShareId,
                userIds: videoShare.map((e) => e.getUserId()).toList(),
              );
            });
          },
          color: AppConfig.appYellow,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Add Followers'),
              addSpaceWidth(5),
              Icon(LineIcons.user)
            ],
          )),
    );
  }
}
