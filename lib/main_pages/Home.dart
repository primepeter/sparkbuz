import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:sparkbuz/AppEngine.dart';
import 'package:sparkbuz/MainAdmin.dart';
import 'package:sparkbuz/ShowPeople.dart';
import 'package:sparkbuz/ShowTopWinners.dart';
import 'package:sparkbuz/admin/ShowUsers.dart';
import 'package:sparkbuz/like_animation.dart';

import '../assets.dart';
import '../basemodel.dart';
import 'HomeItem.dart';

int mainCurrentPage = 0;
var mainVp = PageController();

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home>
    with AutomaticKeepAliveClientMixin, WidgetsBindingObserver {
  int playerPage = 0;
  final playerVp = PageController();
  List<StreamSubscription> subs = [];
  ScrollController playListScroll = ScrollController();
  bool showTop = false;
  bool isOnPageTurning = false;

  void scrollListener() {
    if (isOnPageTurning && playerVp.page == playerVp.page.roundToDouble()) {
      setState(() {
        playerPage = playerVp.page.toInt();
        isOnPageTurning = false;
      });
    } else if (!isOnPageTurning && playerPage.toDouble() != playerVp.page) {
      if ((playerPage.toDouble() - playerVp.page).abs() > 0.1) {
        setState(() {
          isOnPageTurning = true;
        });
      }
    }
  }

  @override
  initState() {
    super.initState();

    var insertSub = insertVideoController.stream.listen((model) {
      items.insert(0, model);

      // int p =
      // allPostList.indexWhere((e) => e.getObjectId() == md.getObjectId());
      // if (p == -1)
      //   allPostList.add(md);
      // else
      //   allPostList[p] = md;

      setState(() {});
    });

    var pgVideo = pageVideoController.stream.listen((show) {
      showTop = show;
      setState(() {});
    });

    var deleteSub = deleteVideoController.stream.listen((model) {
      //allPostList.remove(model.items);
      if (null == model) return;
      items.removeWhere((e) => e['objectId'] ?? '' == model.getObjectId());
      setState(() {});
    });

    var loadSub = loadMoreController.stream.listen((bool) {
      if (!bool) return;
      loadItems(false);
      setState(() {});
    });

    subs.add(pgVideo);
    subs.add(insertSub);
    subs.add(deleteSub);
    subs.add(loadSub);
  }

  List items = [];
  bool setup = false;

  loadItems(bool isNew, {bool fromLocal = false}) async {
    FirebaseFirestore.instance
        .collection(POSTS_BASE)
        //.where(PARTIES, arrayContains: userModel.getUserId())
        .limit(10)
        .orderBy(TIME, descending: !isNew)
        .startAt([
          !isNew
              ? (allPostList.isEmpty
                  ? DateTime.now().millisecondsSinceEpoch
                  : allPostList[allPostList.length - 1].getTime())
              : (allPostList.isEmpty ? 0 : allPostList[0].getTime())
        ])
        .get()
        .then((query) {
          for (var doc in query.docs) {
            BaseModel model = BaseModel(doc: doc);
            if (model.getList(HIDDEN).contains(userModel.getUserId())) continue;
            int p = allPostList
                .indexWhere((e) => e.getObjectId() == model.getObjectId());
            if (p == -1)
              allPostList.add(model);
            else
              allPostList[p] = model;
          }
          setup = true;
          //items.sort((a, b) => b[TIME] ?? 0.compareTo(a[TIME] ?? 0));
          if (mounted) setState(() {});
        });
    // .catchError((e) {
    //   if (e.toString().contains("CACHE")) {
    //     loadItems(isNew, fromLocal: false);
    //   }
    // });
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: black,
      body: page(),
    );
  }

  page() {
    return Stack(
      children: [
        PageView.builder(
          onPageChanged: (p) {
            /*if (p == mainCurrentPage)
              pauseController.add(true);
            else*/
            if (p % 3 == 0) loadItems(false);
            pauseController.add(true);
            // Future.delayed(Duration(milliseconds: 600), () {
            //   loadController.add(true);
            // });
            mainCurrentPage = p;
            setState(() {});
          },
          controller: mainVp,
          itemCount: allPostList.length,
          scrollDirection: Axis.vertical,
          itemBuilder: (c, p) {
            BaseModel video = allPostList[p];
            return HomeItem(
                key: Key(video.getObjectId()), video: video, videoPage: p);
          },
        ),
        LikeAnimation(),
        // if (currentPageHomeItem == 0)
        //   Align(
        //     alignment: Alignment.topCenter,
        //     child: Container(
        //       padding: EdgeInsets.only(top: 30, right: 10, left: 10),
        //       child: Row(
        //         children: [
        //           if (allPostList[mainCurrentPage].getBoolean(SHOW_LOGO))
        //             Image.asset(
        //               spark_logo,
        //               height: 25,
        //               fit: BoxFit.cover,
        //             ),
        //           Spacer(),
        //           FlatButton(
        //               onPressed: () {
        //                 pushAndResult(
        //                   context,
        //                   ShowTopWinners(),
        //                 );
        //               },
        //               color: appYellow,
        //               //minWidth: 80,
        //               height: 30,
        //               padding: EdgeInsets.all(4),
        //               shape: RoundedRectangleBorder(
        //                   borderRadius: BorderRadius.circular(5)),
        //               child: Row(
        //                 children: [
        //                   Image.asset(
        //                     "assets/icons/crown.png",
        //                     height: 20,
        //                     width: 20,
        //                     fit: BoxFit.cover,
        //                   ),
        //                   addSpaceWidth(2),
        //                   Text(
        //                     'Top Winners',
        //                     style: textStyle(false, 12, black),
        //                   ),
        //                 ],
        //               ))
        //         ],
        //       ),
        //     ),
        //   )
      ],
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
