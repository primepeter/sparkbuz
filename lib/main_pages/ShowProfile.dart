import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sparkbuz/AppEngine.dart';
import 'package:sparkbuz/MainAdmin.dart';
import 'package:sparkbuz/ShowLikedVideos.dart';
import 'package:sparkbuz/ShowPeople.dart';
import 'package:sparkbuz/assets.dart';
import 'package:sparkbuz/basemodel.dart';

import '../CreateReport.dart';
import 'CameraSparkShare.dart';
import 'ShowVideoPost.dart';

class ShowProfile extends StatefulWidget {
  final String profileId;
  final BaseModel model;
  final bool showCloseBtn;
  const ShowProfile(
      {Key key, this.showCloseBtn = false, this.profileId, this.model})
      : super(key: key);

  @override
  _ShowProfileState createState() => _ShowProfileState();
}

class _ShowProfileState extends State<ShowProfile>
    with AutomaticKeepAliveClientMixin {
  int currentPage = 0;
  final vp = PageController();
  final scrollController = ScrollController();
  final refreshController = RefreshController(initialRefresh: false);
  bool canRefresh = true;
  BaseModel theUser = BaseModel();
  //List followers, followings, likesCount = [];
  String profileId;
  List<BaseModel> myVideos = [];
  List<BaseModel> myLikes = [];
  bool videoReady = false;
  bool likesReady = false;

  StreamSubscription subs;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    profileId = widget.profileId;
    theUser = widget.model; //otherPeronInfo[widget.profileId];
    // followers = theUser.followers;
    // followings = theUser.followings;
    // likesCount = theUser.likesCount;
    //loadPost(profileId);
    loadLikeVideos(false);
    loadVideos(false);
    loadUser(profileId);
    scrollController.addListener(() {
      bool isScrolling = scrollController.position.isScrollingNotifier.value;
      bool showProfile = scrollController.position.pixels.isNegative ||
          (scrollController.position.pixels == 0);
      if (showProfile) {
        setState(() {
          hideProfile = false;
        });
      } else if (!hideProfile && isScrolling)
        setState(() {
          hideProfile = true;
        });
    });
    loadVideos(false);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    subs?.cancel();
    super.dispose();
  }

  @override
  void didUpdateWidget(ShowProfile oldWidget) {
    if (oldWidget.profileId != widget.profileId) {
      profileId = widget.profileId;
      theUser = widget.model;
      videoReady = false;
      myVideos.clear();
      if (mounted) setState(() {});
      Future.delayed(Duration(milliseconds: 15), () {
        loadPost(profileId);
        loadUser(profileId);
      });
      if (mounted) setState(() {});
    }
    super.didUpdateWidget(oldWidget);
  }

  loadVideos(bool isNew) async {
    List local = [];
    FirebaseFirestore.instance
        .collection(POSTS_BASE)
        .where(USER_ID, isEqualTo: widget.profileId)
        .limit(10)
        .orderBy(TIME, descending: true)
        .startAt([
          !isNew
              ? (myVideos.isEmpty
                  ? DateTime.now().millisecondsSinceEpoch
                  : myVideos[myVideos.length - 1].getTime())
              : (myVideos.isEmpty ? 0 : myVideos[0].getTime())
        ])
        .get()
        .then((shots) async {
          local = shots.docs;
          for (var doc in shots.docs) {
            final model = BaseModel(doc: doc);
            int p = myVideos
                .indexWhere((e) => e.getObjectId() == model.getObjectId());
            if (p != -1) {
              myVideos[p] = model;
            } else {
              myVideos.add(model);
            }
          }

          if (isNew) {
            refreshController.refreshCompleted();
          } else {
            int oldLength = myVideos.length;
            int newLength = local.length;
            if (newLength <= oldLength) {
              refreshController.loadNoData();
              refreshController.refreshCompleted();
              canRefresh = false;
            } else {
              refreshController.loadComplete();
              refreshController.refreshCompleted();
            }
          }
          videoReady = true;
          myVideos.sort((a, b) => b.getTime().compareTo(a.getTime()));
          if (mounted) setState(() {});
        });
  }

  loadLikeVideos(bool isNew) async {
    List local = [];
    FirebaseFirestore.instance
        .collection(POSTS_BASE)
        .where(LIKES_COUNT, arrayContains: widget.profileId)
        .limit(10)
        .orderBy(TIME, descending: true)
        .startAt([
          !isNew
              ? (myLikes.isEmpty
                  ? DateTime.now().millisecondsSinceEpoch
                  : myLikes[myVideos.length - 1].getTime())
              : (myLikes.isEmpty ? 0 : myLikes[0].getTime())
        ])
        .get()
        .then((shots) async {
          local = shots.docs;
          for (var doc in shots.docs) {
            final model = BaseModel(doc: doc);
            int p = myLikes
                .indexWhere((e) => e.getObjectId() == model.getObjectId());
            if (p != -1) {
              myLikes[p] = model;
            } else {
              myLikes.add(model);
            }
          }

          if (isNew) {
            refreshController.refreshCompleted();
          } else {
            int oldLength = myLikes.length;
            int newLength = local.length;
            if (newLength <= oldLength) {
              refreshController.loadNoData();
              refreshController.refreshCompleted();
              canRefresh = false;
            } else {
              refreshController.loadComplete();
              refreshController.refreshCompleted();
            }
          }
          likesReady = true;
          myLikes.sort((a, b) => b.getTime().compareTo(a.getTime()));
          if (mounted) setState(() {});
        });
  }

  loadUser(String id) {
    print("loadUser $id");

    if (null == id || id.isEmpty) return;
    // FirebaseFirestore.instance
    //     .collection(USER_BASE)
    //     .doc(id)
    //     .get()
    //     .then((value) {
    //   theUser = BaseModel(doc: value);
    //   followersIds = theUser.getList(FOLLOWERS);
    //
    //   setState(() {});
    // });
    // return;

    subs = FirebaseFirestore.instance
        .collection(USER_BASE)
        .doc(id)
        .snapshots()
        .listen((value) {
      print("loadUser ${null == value}");

      if (null == value) return;
      theUser = BaseModel(doc: value);
      followersIds = theUser.getList(FOLLOWERS);

      // followers = theUser.followers;
      // followings = theUser.followings;
      // likesCount = theUser.likesCount;
      if (mounted) setState(() {});
    });
  }

  loadPost(String id) {
    if (null == id || id.isEmpty) return;
    FirebaseFirestore.instance
        .collection(POSTS_BASE)
        .where(USER_ID, isEqualTo: id)
        .get()
        .then((value) {
      for (var doc in value.docs) {
        BaseModel md = BaseModel(doc: doc);
        int p = myVideos.indexWhere((e) => e.getObjectId() == md.getObjectId());
        if (p == -1)
          myVideos.add(md);
        else
          myVideos[p] = md;
      }
      videoReady = true;
      if (mounted) setState(() {});
    });
  }

  List<BaseModel> get currentList {
    return currentPage == 0 ? myVideos : myLikes;
  }

  bool get currentReady {
    return currentPage == 0 ? videoReady : likesReady;
  }

  currentRefresh(bool v) {
    currentPage == 0 ? loadVideos(v) : loadLikeVideos(v);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: white,
      body: page(),
    );
  }

  page() {
    return Column(
      children: [
        profileAppBar(),
        Expanded(
          child: PageView(
            controller: vp,
            onPageChanged: (p) {
              currentPage = p;
              setState(() {});
            },
            children: List.generate(2, (index) => pageRefresh(index)),
          ),
        )
      ],
    );
  }

  pageRefresh(int p) {
    return SmartRefresher(
      controller: refreshController,
      enablePullDown: true,
      enablePullUp: currentReady,
      header: WaterDropHeader(),
      footer: ClassicFooter(
        noDataText: currentList.length > 9
            ? "Nothing more for now, check later..."
            : "",
        textStyle: textStyle(false, 12, black.withOpacity(.7)),
      ),
      onLoading: () {
        currentRefresh(false);
      },
      onRefresh: () {
        currentRefresh(true);
      },
      child: ListView(
        shrinkWrap: true,
        controller: scrollController,
        padding: EdgeInsets.only(bottom: 100),
        children: [pageList()],
      ),
    );
  }

  pageList() {
    return Builder(
      builder: (ctx) {
        if (!currentReady)
          return Container(
            height: (getScreenHeight(context) * .45) / 2,
            child: loadingLayout(),
          );

        if (currentList.isEmpty)
          return Container(
            height: (getScreenHeight(context) * .45) / 2,
            child: emptyLayout('assets/icons/youtube.png', "No Post",
                "${theUser.getString(USERNAME)} has not shared any videos yet.",
                nullColor: true),
          );

        return GridView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                childAspectRatio: .8,
                crossAxisCount: 3,
                mainAxisSpacing: 5,
                crossAxisSpacing: 5),
            padding: EdgeInsets.only(right: 5, left: 5, top: 10
                //top: getScreenHeight(context) * (hideProfile ? 0.23 : 0.38)
                ),
            itemCount: currentList.length,
            itemBuilder: (ctx, p) {
              String imageUrl = "https://bit.ly/3he5JOD";
              if (p.isEven) imageUrl = "https://bit.ly/3h8s5RE";

              BaseModel md = currentList[p];
              String image = md.getString(THUMBNAIL_URL);
              int seenBy = md.getList(SEEN_BY).length;

              return GestureDetector(
                onTap: () {
                  pushAndResult(
                      context,
                      ShowVideoPost(
                        videoModel: md,
                      ));
                },
                onLongPress: () {
                  if (md.myItem() || isAdmin) {
                    yesNoDialog(context, "Delete Video",
                        'Are you sure you want to delete this video?', () {
                      int p = playList.indexWhere(
                          (e) => e.model.getObjectId() == md.getObjectId());
                      if (p != -1) deleteVideoController.add(md);
                      currentList.remove(md);
                      print("${md.getObjectId()} index $p ${playList.length}");
                      md.deleteItem();
                      setState(() {});
                    });
                  }
                },
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(15),
                  child: Container(
                    child: Stack(
                      children: [
                        CachedNetworkImage(
                          imageUrl: image,
                          height: double.infinity,
                          width: double.infinity,
                          fit: BoxFit.cover,
                          placeholder: (c, s) {
                            return placeHolder(double.infinity,
                                width: double.infinity);
                          },
                        ),
                        Container(
                          width: double.infinity,
                          height: double.infinity,
                          color: black.withOpacity(.09),
                          child: Center(
                            child: Container(
                              height: 40,
                              width: 40,
                              child: Icon(
                                Icons.play_arrow,
                                color: white.withOpacity(.7),
                                size: 25,
                              ),
                              /* decoration: BoxDecoration(
                                  color: black.withOpacity(0.8),
                                  border: Border.all(
                                      color: Colors.white, width: 1.5),
                                  shape: BoxShape.circle),*/
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: Container(
                            decoration: BoxDecoration(
                                color: black.withOpacity(.3),
                                borderRadius: BorderRadius.circular(5)),
                            padding: EdgeInsets.all(3),
                            margin: EdgeInsets.all(5),
                            child: Row(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Icon(
                                    LineIcons.eye,
                                    color: white,
                                    size: 15,
                                  ),
                                  addSpaceWidth(5),
                                  Text(
                                    seenBy.toString(),
                                    style: textStyle(false, 14, white),
                                  ),
                                ]),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            });
      },
    );
  }

  bool hideProfile = false;

  profileAppBar() {
    bool following = userModel.followings.contains(theUser.getUserId());
    bool canAdd = theUser.followers.contains(userModel.getObjectId());

    return Container(
        width: double.infinity,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(20),
                bottomLeft: Radius.circular(20)),
            color: white,
            boxShadow: [
              BoxShadow(color: black.withOpacity(.1), blurRadius: 9)
            ]),
        padding: EdgeInsets.only(
            top: 30,
            //right: 10, left: 10,
            bottom: 0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              padding: EdgeInsets.all(10),
              //height: 40,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      width: 35,
                      height: 30,
                      color: transparent,
                      child: Icon(Icons.arrow_back_ios_rounded),
                    ),
                  ),
                  Container(
                    child: Text(
                      theUser.getName().isEmpty
                          ? '@${theUser.getUserName()}'
                          : theUser.getName(),
                      style: textStyle(true, 15, black),
                    ),
                  ),
                  if (!theUser.myItem())
                    InkWell(
                      onTap: () {
                        pushAndResult(
                            context,
                            CreateReport(
                              item: theUser,
                            ));
                      },
                      child: Container(
                        width: 35,
                        height: 30,
                        color: transparent,
                        child: Icon(Icons.report_outlined),
                      ),
                    )
                  else
                    Container(
                      height: 30,
                      width: 30,
                    )
                ],
              ),
            ),
            AnimatedContainer(
              duration: Duration(milliseconds: 60),
              curve: Curves.bounceInOut,
              padding: EdgeInsets.all(hideProfile ? 0 : 10),
              child: hideProfile
                  ? null
                  : Column(
                      children: [
                        //  addLine(.5, black.withOpacity(.09), 0, 0, 0, 10),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            userImageItem(context, theUser,
                                strock: 1,
                                size: 100,
                                backColor: black.withOpacity(.1),
                                upload: theUser.myItem()),
                            addSpaceWidth(8),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "@${theUser.getUserName().toLowerCase()}",
                                    style: textStyle(
                                        false, 18, black.withOpacity(.7)),
                                  ),
                                  // addSpace(9),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Container(
                                          padding: EdgeInsets.all(10),
                                          //width: 90,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(8),
                                              color: black.withOpacity(.06)),
                                          child: Center(
                                            child: Text(
                                              "${formatToK(myVideos.length)} videos",
                                              style: textStyle(true, 14,
                                                  black.withOpacity(.4)),
                                            ),
                                          ),
                                        ),
                                      ),
                                      addSpaceWidth(10),
                                      Expanded(
                                        child: FlatButton(
                                          onPressed: () {
                                            onFollow();
                                          },
                                          color: appColor,
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(8)),
                                          child: Text(
                                            theUser.myItem()
                                                ? "Edit"
                                                : (following
                                                    ? "Following"
                                                    : "Follow"),
                                            style: textStyle(true, 14, white),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  if (theUser.myItem())
                                    FlatButton(
                                      onPressed: () {
                                        pauseController.add(true);
                                        pushAndResult(
                                            context, CameraSparkShare(),
                                            result: (_) {
                                          if (null == _) return;
                                          //pauseController.add(false);
                                        });
                                      },
                                      color: appYellow,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(8)),
                                      child: Center(
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              'SparkShare',
                                              style: textStyle(true, 14, black),
                                            ),
                                            addSpaceWidth(5),
                                            Icon(
                                              LineIcons.video_camera,
                                              //size: 24,
                                              color: black,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        if (theUser.aboutMe.isNotEmpty)
                          Container(
                              margin: EdgeInsets.all(10),
                              padding: EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                  color: black.withOpacity(.02),
                                  borderRadius: BorderRadius.circular(15)),
                              child: ReadMoreText(
                                theUser.aboutMe,
                              )),
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: List.generate(3, (p) {
                              int count = theUser.followings.length;
                              String title = "Following";
                              List ids = theUser.followings;
                              if (p == 1) {
                                count = followersIds.length;
                                title = "Followers";
                                ids = followersIds;
                              }

                              if (p == 2) {
                                count = theUser.likesCount.length;
                                title = "Likes";
                                ids = theUser.likesCount;
                              }

                              return GestureDetector(
                                onTap: () {
                                  if (theUser.myItem() || isAdmin) if (p == 2)
                                    pushAndResult(context, ShowLikedVideos());
                                  else
                                    pushAndResult(
                                        context,
                                        ShowPeople(
                                            ids, title, 'No $title Found!'));
                                },
                                child: Container(
                                  color: transparent,
                                  padding: EdgeInsets.all(5),
                                  child: Row(
                                    children: [
                                      Column(
                                        children: [
                                          Text(
                                            formatToK(count),
                                            style: textStyle(true, 18, black),
                                          ),
                                          addSpace(1),
                                          Text(
                                            title,
                                            style: textStyle(false, 16,
                                                black.withOpacity(.5)),
                                          )
                                        ],
                                      ),
                                      if (p < 2) ...[
                                        addSpaceWidth(15),
                                        Container(
                                          height: 15,
                                          width: 1.5,
                                          color: black.withOpacity(.4),
                                        ),
                                      ]
                                    ],
                                  ),
                                ),
                              );
                            }),
                          ),
                        ),
                        addLine(.5, black.withOpacity(.09), 0, 10, 0, 10),
                      ],
                    ),
            ),
            GestureDetector(
              onTap: () {
                hideProfile = !hideProfile;
                setState(() {});
              },
              child: Container(
                alignment: Alignment.center,
                padding: EdgeInsets.all(10),
                color: transparent,
                child: Icon(
                  hideProfile
                      ? LineIcons.arrow_circle_down
                      : LineIcons.arrow_circle_up,
                  size: 30,
                  color: black.withOpacity(.5),
                ),
              ),
            ),
            if (hideProfile)
              Container(
                height: 45,
                width: double.infinity,
                margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                child: Card(
                  color: default_white,
                  elevation: 0,
                  clipBehavior: Clip.antiAlias,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(25)),
                      side:
                          BorderSide(color: black.withOpacity(.1), width: .5)),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(1, 1, 1, 1),
                    child: Row(
                      children: List.generate(2, (p) {
                        String title = p == 0 ? "Videos" : "Liked";
                        bool selected = p == currentPage;
                        return Flexible(
                          child: GestureDetector(
                            onTap: () {
                              vp.jumpToPage(p);
                            },
                            child: Container(
                                decoration: BoxDecoration(
                                    color: selected ? white : transparent,
                                    borderRadius: BorderRadius.circular(25),
                                    border: Border.all(
                                        color: !selected
                                            ? transparent
                                            : black.withOpacity(.1),
                                        width: .5)),
                                child: Center(
                                    child: Text(
                                  title,
                                  style: textStyle(
                                      selected,
                                      14,
                                      selected
                                          ? black
                                          : (black.withOpacity(.5))),
                                  textAlign: TextAlign.center,
                                ))),
                          ),
                          fit: FlexFit.tight,
                        );
                      }),
                    ),
                  ),
                ),
              ),
          ],
        ));
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

  // handleFollow() {
  //   bool canAdd = theUser.followers.contains(userModel.getObjectId());
  //   theUser
  //     ..putInList(FOLLOWERS, userModel.getObjectId(), !canAdd)
  //     ..updateItems();
  //
  //   userModel
  //     ..putInList(FOLLOWING, userModel.getObjectId(), !canAdd)
  //     ..updateItems();
  //
  //   setState(() {});
  //
  //   addToStatusParty(theUser, add: !canAdd);
  //
  //   pushNotificationToUsers(
  //     notifyType: canAdd ? NOTIFY_TYPE_FOLLOW : NOTIFY_TYPE_UNFOLLOW,
  //     notifyId: theUser.getObjectId(),
  //     userIds: [theUser.getUserId()],
  //   );
  // }

  List followersIds = [];
  bool following = false;

  onFollow() {
    String userId = userModel.getObjectId();
    String personId = theUser.getObjectId();

    following = followersIds.contains(userId);

    List followings = userModel.getList(FOLLOWING);
    bool hasFollowed = followings.contains(personId);

    if (following) {
      followersIds.remove(userId);
    } else {
      followersIds.add(userId);
    }

    if (hasFollowed) {
      followings.remove(personId);
    } else {
      followings.add(personId);
    }

    theUser.put(FOLLOWERS, followersIds);
    theUser.updateItems();

    userModel.put(FOLLOWING, followings);
    userModel.updateItems();

    following = !following;
    setState(() {});

    addToStatusParty(theUser, add: !following);
    pushNotificationToUsers(
      notifyType: following ? NOTIFY_TYPE_FOLLOW : NOTIFY_TYPE_UNFOLLOW,
      notifyId: personId,
      userIds: [personId],
    );
  }
}
