import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dashed_circle/dashed_circle.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sparkbuz/AppConfig.dart';
import 'package:sparkbuz/MainAdmin.dart';
import 'package:sparkbuz/StoriesPage.dart';
import 'package:sparkbuz/app/app.dart';
import 'package:sparkbuz/app/dotsIndicator.dart';
import 'package:sparkbuz/basemodel.dart';
import 'package:sparkbuz/main_pages/ShowDisovery.dart';
import 'package:sparkbuz/main_pages/ShowVideoPost.dart';

import '../AppEngine.dart';
import '../SearchPeople.dart';
import '../assets.dart';

class Discovery extends StatefulWidget {
  @override
  _DiscoveryState createState() => _DiscoveryState();
}

class _DiscoveryState extends State<Discovery>
    with AutomaticKeepAliveClientMixin {
  int currentPageAds = 0;
  final vpAds = PageController();
  Timer timer;
  bool reversing = false;
  final _codeWheeler = CodeWheeler(milliseconds: 8000);

  final refreshController = RefreshController(initialRefresh: false);
  bool canRefresh = true;

  List get appCategories {
    final list = appSettingsModel.getList(POST_CATEGORIES);
    list.sort();
    return list;
  }

  List<BaseModel> items = [];
  List allCategories = appSettingsModel.getList(POST_CATEGORIES);
  String activeCategory = "";
  ScrollController categoryScroll = ScrollController();

  List<List<BaseModel>> storyList = [];
  bool setup = false;
  var subs = [];

  ScrollController scrollController = ScrollController();
  double appBarOpacity = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    pauseController.add(true);
    allCategories.sort();
    allCategories.insert(0, "All");
    activeCategory = allCategories[0];
    items = allPostList;
    setup = items.isNotEmpty;
    if (mounted) _codeWheeler.run(pageWheeler);
    //refreshStories();
    scrollController.addListener(() {
      if (scrollController.position.pixels == 0 &&
          !scrollController.position.outOfRange) {
        appBarOpacity = 0;
        setState(() {});
      } else {
        double value = (scrollController.position.pixels - 150) / 100;
        appBarOpacity = value.clamp(0.0, 1.0);
        setState(() {});
      }
    });

    var refreshSub = homeRefreshController.stream.listen((b) {
      //refreshStories();
    });

    /*var scrollSub = scrollTopController.stream.listen((p) {
      if (p != 2) return;
      scrollTotallyUp(scrollController);
      if (activeCategory != allCategories.first[OBJECT_ID]) {
        activeCategory = allCategories.first[OBJECT_ID];
        vp.jumpToPage(0);
        categoryScroll.animateTo((0 * 60).toDouble(),
            duration: Duration(milliseconds: 500), curve: Curves.ease);
        setState(() {});
        return;
      }

      loadItems(true);
    });*/

    subs.add(refreshSub);
    //subs.add(scrollSub);
    loadItems(true, fromLocal: true);
  }

  @override
  dispose() {
    for (var sub in subs) sub?.cancel();
    timer?.cancel();
    super.dispose();
  }

  pageWheeler() {
    int size = adsList.length;
    if (adsList.isEmpty) return;
    if (size == 1) return;
    if (null == vpAds || !mounted) return;
    if (!vpAds.hasClients) return;
    if (currentPageAds < size - 1 && !reversing) {
      reversing = false;
      if (mounted) setState(() {});
      vpAds.nextPage(duration: Duration(milliseconds: 12), curve: Curves.ease);
      return;
    }
    if (currentPageAds == size - 1 && !reversing) {
      Future.delayed(Duration(seconds: 2), () {
        reversing = true;
        if (mounted) setState(() {});
        vpAds.previousPage(
            duration: Duration(milliseconds: 12), curve: Curves.ease);
      });
      return;
    }
    if (currentPageAds == 0 && reversing) {
      Future.delayed(Duration(seconds: 2), () {
        reversing = false;
        if (mounted) setState(() {});
        vpAds.nextPage(
            duration: Duration(milliseconds: 12), curve: Curves.ease);
      });
      return;
    }

    if (currentPageAds == 0 && !reversing) {
      Future.delayed(Duration(seconds: 2), () {
        reversing = false;
        if (mounted) setState(() {});
        vpAds.nextPage(
            duration: Duration(milliseconds: 12), curve: Curves.ease);
      });
      return;
    }

    if (currentPageAds > 0 && reversing) {
      Future.delayed(Duration(seconds: 2), () {
        vpAds.previousPage(
            duration: Duration(milliseconds: 12), curve: Curves.ease);
      });
      return;
    }
  }

  loadItems(bool isNew, {bool fromLocal = false}) async {
    print(userModel.getUserId());
    FirebaseFirestore.instance
        .collection(POSTS_BASE)
        //.where(USER_ID,isEqualTo: userModel.getUserId())
        .limit(25)
        .orderBy(TIME, descending: !isNew)
        .startAt([
          !isNew
              ? (items.isEmpty
                  ? DateTime.now().millisecondsSinceEpoch
                  : items[items.length - 1].getTime())
              : (items.isEmpty ? 0 : items[0].getTime())
        ])
        .get()
        .then((query) {
          for (var doc in query.docs) {
            BaseModel model = BaseModel(doc: doc);
            //if (model.hidden.contains(userModel.getUserId())) continue;

            int p =
                items.indexWhere((e) => e.getObjectId() == model.getObjectId());
            if (p == -1)
              items.add(model);
            else
              items[p] = model;
          }

          if (isNew) {
            refreshController.refreshCompleted();
          } else {
            refreshController.loadComplete();
            // int oldLength = items.length;
            // int newLength = query.documents.length;
            // if (newLength <= oldLength) {
            //   refreshController.loadNoData();
            //   canRefresh = false;
            // } else {
            //   refreshController.loadComplete();
            // }
          }
          setup = true;
          if (mounted) setState(() {});
        })
        .catchError((e) {
          if (e.toString().contains("CACHE")) {
            loadItems(isNew, fromLocal: false);
          }
        });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: white,
      body: page(),
    );
  }

  page() {
    return Container(
      padding: EdgeInsets.only(bottom: 100),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(top: 40, right: 10, left: 10, bottom: 10),
            child: GestureDetector(
              onTap: () {
                pushAndResult(context, SearchPeople());
              },
              child: Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: default_white),
                child: Row(
                  children: [
                    Image.asset(
                      'assets/icons/search.png',
                      color: black.withOpacity(.5),
                      height: 20,
                      width: 20,
                      fit: BoxFit.cover,
                    ),
                    addSpaceWidth(4),
                    Text(
                      "Search",
                      style: textStyle(
                        false,
                        16,
                        black.withOpacity(.6),
                      ),
                    ),
                    // Spacer(),
                    // Icon(
                    //   LineIcons.sort,
                    //   color: black.withOpacity(.5),
                    // ),
                  ],
                ),
              ),
            ),
          ),
          SingleChildScrollView(
            controller: categoryScroll,
            scrollDirection: Axis.horizontal,
            child: Row(
                children: List.generate(allCategories.length, (p) {
              String category = allCategories[p];
              bool selected = activeCategory == category;

              return GestureDetector(
                onTap: () {
                  activeCategory = category;
                  try {
                    if ((categoryScroll.position.pixels == 0 &&
                            !categoryScroll.position
                                .outOfRange) /*||
                                  (categoryScroll.position.pixels <=
                                      categoryScroll.position.maxScrollExtent)*/
                        )
                      categoryScroll.animateTo((p * 60).toDouble(),
                          duration: Duration(milliseconds: 500),
                          curve: Curves.ease);
                    vp.jumpToPage(p);
                    loadItems(true);
                  } catch (e) {}
                  setState(() {});
                },
                child: Container(
                  height: 40,
                  padding:
                      EdgeInsets.only(left: 5, right: 5, top: 3, bottom: 3),
                  margin: EdgeInsets.all(4),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      color: selected ? appColor : bg_alpha,
                      border: Border.all(color: black.withOpacity(.09)),
                      borderRadius: BorderRadius.circular(25)),
                  child: Row(
                    children: [
                      addSpaceWidth(8),
                      Text(
                        category,
                        style:
                            textStyle(selected, 14, selected ? white : black),
                      ),
                      addSpaceWidth(8),
                    ],
                  ),
                ),
              );
            })),
          ),
          listItems()
        ],
      ),
    );
  }

  final vp = PageController();
  int currentPage = 0;

  listItems() {
    return Expanded(
      child: PageView.builder(
        controller: vp,
        onPageChanged: (p) {
          currentPage = p;
          activeCategory = allCategories[p];
          categoryScroll.animateTo((p * 60).toDouble(),
              duration: Duration(milliseconds: 500), curve: Curves.ease);
          setState(() {});
        },
        itemCount: allCategories.length,
        itemBuilder: (c, p) {
          String cat = allCategories[p];

          List<BaseModel> list = [];

          if (cat == "All") {
            list.addAll(items);
          } else {
            for (var item in items) {
              bool hasCategory = item.getString(CATEGORY) == cat;
              int p =
                  list.indexWhere((e) => e.getObjectId() == item.getObjectId());
              if (!hasCategory) continue;
              if (p == -1)
                list.add(item);
              else
                list[p] = item;
            }
          }

          if (!setup) return loadingLayout();
          if (list.isEmpty)
            return Container(
              height: 400,
              child: emptyLayout(Icons.search, "No Content",
                  "No content has been shared on this category...",
                  clickText: "Refresh", click: () {
                setup = false;
                setState(() {});
                loadItems(true);
              }),
            );

          CollageType type = CollageType.FourLeftBig;

          return SmartRefresher(
            controller: refreshController,
            enablePullDown: true,
            enablePullUp: true,
            header: WaterDropHeader(),
            footer: ClassicFooter(
              noDataText: "Nothing more for now, check later...",
              textStyle: textStyle(false, 12, black.withOpacity(.7)),
            ),
            onLoading: () {
              loadItems(false);
            },
            onRefresh: () {
              loadItems(true);
            },
            child: ListView(
              controller: scrollController,
              padding: EdgeInsets.zero,
              // padding: EdgeInsets.only(bottom: 100),

              children: [
                adsSlider(),
                storiesLayout(context),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: List.generate(list.length, (p) {
                    BaseModel model = list[p];
                    bool isVideo = model.isVideo;
                    String heroTag = "image$p";
                    String image =
                        model.getString(isVideo ? THUMBNAIL_URL : VIDEO_URL);

                    String username = model.getUserName();
                    String description = model.getString(DESCRIPTION);
                    String category = model.getString(CATEGORY);

                    String likes = formatToK(model.getList(LIKES_COUNT).length);
                    String comments =
                        formatToK(model.getList(COMMENT_LIST).length);
                    String shares = formatToK(model.getList(SHARE_LIST).length);
                    String photo = model.getString(USER_IMAGE);
                    bool liked = model
                        .getList(LIKES_COUNT)
                        .contains(userModel.getUserId());
                    String seenBy = model.getList(SEEN_BY).length.toString();
                    String time = getTimeAgo(model.getTime());

                    return GestureDetector(
                      onTap: () {
                        pushAndResult(
                            context,
                            ShowVideoPost(
                              videoModel: model,
                            ));
                      },
                      onLongPress: () {
                        if (!isAdmin) return;
                        showListDialog(context, [
                          "Reset This Timestamp",
                          "Reset All Timestamp",
                          "Delete"
                        ], (_) {
                          if (_ == 0) {
                            showMessage(
                                context,
                                Icons.warning,
                                red,
                                "Update Time",
                                "Update/Reset video upload time to current time now?",
                                onClicked: (_) {
                              if (!_) return;
                              model.put(
                                  UPDATED_AT, FieldValue.serverTimestamp());
                              model.put(
                                  CREATED_AT, FieldValue.serverTimestamp());
                              model.put(
                                  TIME, DateTime.now().millisecondsSinceEpoch);
                              final search =
                                  getSearchString('$description $category');
                              model.put(SEARCH, search);

                              model.put(TIME_UPDATED,
                                  DateTime.now().millisecondsSinceEpoch);
                              model.updateItems(updateTime: true);

                              int p = items.indexWhere((e) =>
                                  e.getObjectId() == model.getObjectId());
                              if (p != -1) items[p] = model;
                              items.sort(
                                  (a, b) => b.getTime().compareTo(a.getTime()));
                              setState(() {});
                            });
                          }

                          if (_ == 1) {
                            showMessage(
                                context,
                                Icons.warning,
                                red,
                                "Reset all Time",
                                "Update/Reset video upload time to current time now?",
                                onClicked: (_) {
                              if (!_) return;

                              for (var model in items) {
                                model.put(
                                    UPDATED_AT, FieldValue.serverTimestamp());
                                model.put(
                                    CREATED_AT, FieldValue.serverTimestamp());
                                model.put(TIME,
                                    DateTime.now().millisecondsSinceEpoch);
                                model.put(TIME_UPDATED,
                                    DateTime.now().millisecondsSinceEpoch);
                                final search =
                                    getSearchString('$description $category');
                                model.put(SEARCH, search);

                                model.updateItems(
                                    updateTime: true, delaySeconds: 2);

                                int p = items.indexWhere((e) =>
                                    e.getObjectId() == model.getObjectId());
                                if (p != -1) items[p] = model;
                              }

                              items.sort(
                                  (a, b) => b.getTime().compareTo(a.getTime()));
                              setState(() {});
                            });
                          }

                          if (_ == 2) {
                            showMessage(
                                context,
                                Icons.warning,
                                red,
                                "Delete Video",
                                "Are you sure you want to delete this video?",
                                onClicked: (_) {
                              if (!_) return;
                              model.deleteItem();
                              items.removeWhere((e) =>
                                  e.getObjectId() == model.getObjectId());
                              setState(() {});
                            });
                          }
                        });
                      },
                      child: Container(
                        margin:
                            EdgeInsets.only(bottom: 10, right: 10, left: 10),
                        decoration: BoxDecoration(
                            border: Border.all(color: black.withOpacity(.09))),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              height: 200,
                              child: Stack(
                                //fit: StackFit.passthrough,
                                children: <Widget>[
                                  Container(
                                    child: CachedNetworkImage(
                                      imageUrl: image,
                                      fit: BoxFit.cover,
                                      width: double.infinity,
                                      placeholder: (_, s) {
                                        return placeHolder(200,
                                            width: double.infinity);
                                      },
                                    ),
                                  ),
                                  if (isVideo)
                                    Container(
                                      child: Center(
                                        child: Container(
                                          height: 40,
                                          width: 40,
                                          padding: EdgeInsets.all(3),
                                          margin: EdgeInsets.all(6),
                                          decoration: BoxDecoration(
                                            color: black.withOpacity(.2),
                                            // border:
                                            //     Border.all(color: white, width: 2),
                                            shape: BoxShape.circle,
                                          ),
                                          alignment: Alignment.center,
                                          child: Icon(
                                            Icons.play_arrow,
                                            color: white.withOpacity(.5),
                                            size: 30,
                                          ),
                                        ),
                                      ),
                                    ),
                                  Container(
                                    alignment: Alignment.bottomRight,
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        Container(
                                          decoration: BoxDecoration(
                                              color: black.withOpacity(.3),
                                              borderRadius:
                                                  BorderRadius.circular(5)),
                                          padding: EdgeInsets.all(2),
                                          margin: EdgeInsets.all(3),
                                          child: Row(
                                            children: [
                                              Text(
                                                "${model.getList(LIKES_COUNT).length}",
                                                style:
                                                    textStyle(true, 10, white),
                                              ),
                                              addSpaceWidth(2),
                                              Icon(
                                                Icons.favorite,
                                                color: white,
                                                size: 14,
                                              )
                                            ],
                                          ),
                                        ),
                                        Container(
                                          decoration: BoxDecoration(
                                              color: black.withOpacity(.3),
                                              borderRadius:
                                                  BorderRadius.circular(5)),
                                          padding: EdgeInsets.all(2),
                                          margin: EdgeInsets.all(3),
                                          child: Row(
                                            children: [
                                              Text(
                                                "${model.getList(SEEN_BY).length}",
                                                style:
                                                    textStyle(true, 10, white),
                                              ),
                                              addSpaceWidth(2),
                                              Icon(
                                                Icons.remove_red_eye,
                                                color: white,
                                                size: 14,
                                              )
                                            ],
                                          ),
                                        ),
                                        if (cat == "All")
                                          Flexible(
                                            child: GestureDetector(
                                              onTap: () {
                                                pushAndResult(
                                                    context,
                                                    ShowDiscovery(
                                                      category: model
                                                          .getString(CATEGORY),
                                                      sorted: items
                                                          .where((e) =>
                                                              e.getString(
                                                                  CATEGORY) ==
                                                              model.getString(
                                                                  CATEGORY))
                                                          .toList(),
                                                    ));
                                              },
                                              child: Container(
                                                decoration: BoxDecoration(
                                                    color: AppConfig.appYellow,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            5)),
                                                padding: EdgeInsets.all(3),
                                                margin: EdgeInsets.all(3),
                                                child: Text(
                                                  "${model.getString(CATEGORY)}",
                                                  maxLines: 1,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style: textStyle(
                                                      true, 10, black),
                                                ),
                                              ),
                                            ),
                                          ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            addSpace(10),
                            Container(
                              padding: EdgeInsets.all(5),
                              child: Row(
                                children: [
                                  ClipRRect(
                                    borderRadius: BorderRadius.circular(50),
                                    child: Container(
                                      height: 40,
                                      width: 40,
                                      child: userImageItem(context, model,
                                          padLeft: false, strock: 1, size: 40),
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              color: black.withOpacity(.1),
                                              width: 1.0,
                                              style: BorderStyle.solid),
                                          color: white,
                                          shape: BoxShape.circle),
                                    ),
                                  ),
                                  addSpaceWidth(5),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "$username: $description",
                                          maxLines: 2,
                                          style: textStyle(true, 16, black),
                                        ),
                                        addSpace(4),
                                        Text(
                                          "$seenBy Views . $time",
                                          style: textStyle(
                                              false, 13, black.withOpacity(.6)),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    );
                  }),
                ),
                if (false)
                  StaggeredGridView.countBuilder(
                      shrinkWrap: true,
                      itemCount: list.length,
                      padding: EdgeInsets.only(top: 10, bottom: 100),
                      physics: NeverScrollableScrollPhysics(),
                      crossAxisCount: getCrossAxisCount(type),
                      itemBuilder: (BuildContext context, int p) {
                        BaseModel model = list[p];
                        bool isVideo = model.isVideo;
                        String heroTag = "image$p";
                        String image = model
                            .getString(isVideo ? THUMBNAIL_URL : VIDEO_URL);

                        return GestureDetector(
                          onTap: () {
                            pushAndResult(
                                context,
                                ShowVideoPost(
                                  videoModel: model,
                                ));
                          },
                          onLongPress: () {
                            if (!isAdmin) return;
                            showListDialog(
                                context, ["Reset Timestamp", "Delete"], (_) {
                              if (_ == 1) {
                                showMessage(
                                    context,
                                    Icons.warning,
                                    red,
                                    "Delete Video",
                                    "Are you sure you want to delete this video?",
                                    onClicked: (_) {
                                  if (!_) return;
                                  model.deleteItem();
                                  items.removeWhere((e) =>
                                      e.getObjectId() == model.getObjectId());
                                  setState(() {});
                                });
                              }

                              if (_ == 0) {
                                showMessage(
                                    context,
                                    Icons.warning,
                                    red,
                                    "Update Time",
                                    "Update/Reset video upload time to current time now?",
                                    onClicked: (_) {
                                  if (!_) return;
                                  model.put(
                                      UPDATED_AT, FieldValue.serverTimestamp());
                                  model.put(
                                      CREATED_AT, FieldValue.serverTimestamp());
                                  model.put(TIME,
                                      DateTime.now().millisecondsSinceEpoch);
                                  model.put(TIME_UPDATED,
                                      DateTime.now().millisecondsSinceEpoch);
                                  model.updateItems(updateTime: true);

                                  int p = items.indexWhere((e) =>
                                      e.getObjectId() == model.getObjectId());
                                  if (p != -1) items[p] = model;
                                  items.sort((a, b) =>
                                      b.getTime().compareTo(a.getTime()));
                                  setState(() {});
                                });
                              }
                            });
                          },
                          child: Stack(
                            fit: StackFit.passthrough,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.all(1.2),
                                child: CachedNetworkImage(
                                  imageUrl: image,
                                  fit: BoxFit.cover,
                                  placeholder: (_, s) {
                                    return placeHolder(250,
                                        width: double.infinity);
                                  },
                                ),
                              ),
                              if (isVideo)
                                Container(
                                  height: 250,
                                  child: Center(
                                    child: Container(
                                      height: 40,
                                      width: 40,
                                      padding: EdgeInsets.all(3),
                                      margin: EdgeInsets.all(6),
                                      decoration: BoxDecoration(
                                        color: black.withOpacity(.2),
                                        // border:
                                        //     Border.all(color: white, width: 2),
                                        shape: BoxShape.circle,
                                      ),
                                      alignment: Alignment.center,
                                      child: Icon(
                                        Icons.play_arrow,
                                        color: white.withOpacity(.5),
                                        size: 30,
                                      ),
                                    ),
                                  ),
                                ),
                              Container(
                                alignment: Alignment.bottomCenter,
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Container(
                                      decoration: BoxDecoration(
                                          color: black.withOpacity(.3),
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                      padding: EdgeInsets.all(2),
                                      margin: EdgeInsets.all(3),
                                      child: Row(
                                        children: [
                                          Text(
                                            "${model.getList(LIKES_COUNT).length}",
                                            style: textStyle(true, 10, white),
                                          ),
                                          addSpaceWidth(2),
                                          Icon(
                                            Icons.favorite,
                                            color: white,
                                            size: 14,
                                          )
                                        ],
                                      ),
                                    ),
                                    Container(
                                      decoration: BoxDecoration(
                                          color: black.withOpacity(.3),
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                      padding: EdgeInsets.all(2),
                                      margin: EdgeInsets.all(3),
                                      child: Row(
                                        children: [
                                          Text(
                                            "${model.getList(SEEN_BY).length}",
                                            style: textStyle(true, 10, white),
                                          ),
                                          addSpaceWidth(2),
                                          Icon(
                                            Icons.remove_red_eye,
                                            color: white,
                                            size: 14,
                                          )
                                        ],
                                      ),
                                    ),
                                    if (cat == "All")
                                      Flexible(
                                        child: GestureDetector(
                                          onTap: () {
                                            pushAndResult(
                                                context,
                                                ShowDiscovery(
                                                  category:
                                                      model.getString(CATEGORY),
                                                  sorted: items
                                                      .where((e) =>
                                                          e.getString(
                                                              CATEGORY) ==
                                                          model.getString(
                                                              CATEGORY))
                                                      .toList(),
                                                ));
                                          },
                                          child: Container(
                                            decoration: BoxDecoration(
                                                color: AppConfig.appYellow,
                                                borderRadius:
                                                    BorderRadius.circular(5)),
                                            padding: EdgeInsets.all(3),
                                            margin: EdgeInsets.all(3),
                                            child: Text(
                                              "${model.getString(CATEGORY)}",
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style: textStyle(true, 10, black),
                                            ),
                                          ),
                                        ),
                                      ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                      staggeredTileBuilder: (int index) {
                        return StaggeredTile.count(
                            getCellCountM(
                                index: index,
                                repeatIndex: 8,
                                isForCrossAxis: true,
                                type: type),
                            getCellCountM(
                                index: index,
                                repeatIndex: 8,
                                isForCrossAxis: false,
                                type: type));
                      }),
              ],
            ),
          );
        },
      ),
    );
  }

  adsSlider() {
    if (!adsSetup) return Container();

    if (adsList.isEmpty) return Container();

    return Container(
      height: getScreenHeight(context) * .22,
      margin: EdgeInsets.only(top: 10, bottom: 10),
      decoration: BoxDecoration(
          border: Border(
              top: BorderSide(width: 0.5, color: black.withOpacity(.09)),
              bottom: BorderSide(width: 0.5, color: black.withOpacity(.09)))),
      child: Stack(
        children: [
          PageView.builder(
              controller: vpAds,
              scrollDirection: Axis.horizontal,
              onPageChanged: (p) {
                currentPageAds = p;
                setState(() {});
              },
              itemCount: adsList.length,
              itemBuilder: (ctx, p) {
                BaseModel model = adsList[p];
                String imageUrl = getFirstPhoto(model.images);
                String url = model.getString(ADS_URL);
                //"https://tinyurl.com/y3pqgajd";
                // if (p.isEven) imageUrl = "https://tinyurl.com/yxmafqng";
                //if (p.isOdd) imageUrl = "https://tinyurl.com/y6neowmn";

                return GestureDetector(
                  onTap: () {
                    openLink(url);
                  },
                  child: CachedNetworkImage(
                    imageUrl: imageUrl,
                    height: getScreenHeight(context) * .20,
                    width: double.infinity,
                    fit: BoxFit.cover,
                    placeholder: (c, s) {
                      return placeHolder(getScreenHeight(context) * .20,
                          width: double.infinity);
                    },
                  ),
                );
              }),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              decoration: BoxDecoration(
                  color: black.withOpacity(.1),
                  borderRadius: BorderRadius.circular(25)),
              padding: EdgeInsets.all(2),
              margin: EdgeInsets.all(5),
              child: DotsIndicator(
                dotsCount: adsList.length,
                position: currentPageAds,
                decorator: DotsDecorator(
                    activeColor: AppConfig.appColor,
                    spacing: EdgeInsets.all(3),
                    activeSize: Size(10, 10),
                    size: Size(5, 5)),
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => false;
}

storiesLayout(BuildContext context) {
  final foldedOthers =
      allStories.where((e) => !e.myItem()).groupBy((s) => s.getUserId());
  final foldedMine =
      allStories.where((e) => e.myItem()).groupBy((s) => s.getUserId());

  var keys;
  List<BaseModel> mStory = [];
  BaseModel story;
  bool noStory = foldedMine.isEmpty;
  String imageUrl;
  int playIndex = 0;

  if (storiesReady && foldedMine.isNotEmpty) {
    keys = foldedMine.keys.toList()[0];
    mStory = foldedMine[keys];
    playIndex = (mStory.length - 1).clamp(0, mStory.length - 1);
    story = mStory[0];
    imageUrl = story.getString(story.isVideo ? THUMBNAIL_URL : IMAGE_URL);
    if (noStory) imageUrl = userModel.userImage;

    print(story.getString(THUMBNAIL_PATH));
  }

  double storyBoxSize = 80;

  return Container(
    width: double.infinity,
    padding: EdgeInsets.all(10),
    child: SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      padding: EdgeInsets.zero,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            width: storyBoxSize,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                DashedCircle(
                  strokeWidth: 3,
                  color: appColor,
                  dashes: mStory.length,
                  gapSize: 5,
                  child: Container(
                    height: storyBoxSize - 10,
                    width: storyBoxSize - 10,
                    child: Stack(
                      children: [
                        Container(
                            padding: EdgeInsets.all(3),
                            alignment: Alignment.center,
                            child: imageHolder(storyBoxSize - 10,
                                imageUrl ?? userModel.userImage,
                                iconHolder: Icons.camera,
                                iconHolderSize: 25,
                                stroke: 1,
                                strokeColor: noStory
                                    ? black.withOpacity(.5)
                                    : appYellow, onImageTap: () {
                              if (noStory) {
                                //pauseController.add(true);
                                storyController.add([]);
                                return;
                              }
                              openStoryAt(
                                  context: context,
                                  position: 0,
                                  playIndex: 0,
                                  key: userModel.getObjectId());
                            })),
                        Align(
                          alignment: Alignment.bottomRight,
                          child: GestureDetector(
                            onTap: () {
                              pauseController.add(true);
                              storyController.add([]);
                              return;
                            },
                            child: Container(
                              height: 30,
                              width: 30,
                              decoration: BoxDecoration(
                                  color: white,
                                  border:
                                      Border.all(color: black.withOpacity(.09)),
                                  // gradient: LinearGradient(
                                  //     colors: [pink0, blue3, Colors.purple]),
                                  shape: BoxShape.circle),
                              child: Center(
                                child: Image.asset(
                                  'assets/icons/cloud.png',
                                  //color: black.withOpacity(.5),
                                  height: 16, width: 16,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                addSpace(5),
                Flexible(
                  child: Text(
                    "Your Story",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: textStyle(false, 14, black),
                  ),
                )
              ],
            ),
          ),
          Row(
            children: List.generate(foldedOthers.keys.length, (p) {
              final keys = foldedOthers.keys.toList()[p];
              final stories = foldedOthers[keys];
              final seenAll = stories
                      .where((e) =>
                          e.getList(SEEN_BY).contains(userModel.getUserId()))
                      .toList()
                      .length ==
                  stories.length;

              final defPosition = stories.indexWhere((e) =>
                  !e.getList(TAGGED_PERSONS).contains(userModel.getUserId()));
              int playIndex = (stories.length - 1).clamp(0, stories.length - 1);
              BaseModel story = stories[defPosition.clamp(0, defPosition)];
              bool isVideo = story.isVideo;
              String imageUrl =
                  story.getString(isVideo ? THUMBNAIL_URL : IMAGE_URL);
              String name = story.getString(NAME);
              String nickName = story.getUserName();
              bool myStory = story.myItem();
              if (nickName.startsWith("@")) {
                nickName = nickName.replaceAll("@", "");
                if (myStory) nickName = "YOU";
              }
              return Container(
                width: storyBoxSize,
                padding: EdgeInsets.all(3),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    DashedCircle(
                      strokeWidth: seenAll ? 1 : 3,
                      color: seenAll ? black.withOpacity(.2) : Colors.blueGrey,
                      dashes: stories.length > 15 ? 15 : stories.length,
                      gapSize: 5,
                      child: Container(
                          padding: EdgeInsets.all(3),
                          height: storyBoxSize - 10,
                          width: storyBoxSize - 10,
                          child: imageHolder(storyBoxSize - 10, imageUrl,
                              iconHolder: Icons.camera,
                              iconHolderSize: 20,
                              stroke: 1,
                              strokeColor: appColor, onImageTap: () {
                            print(p);
                            print(stories.length);
                            print(story.getUserId());
                            print(keys);
                            // return;

                            openStoryAt(
                                context: context,
                                position: p,
                                playIndex: defPosition.clamp(0, defPosition),
                                key: keys);
                            return;
                          })),
                    ),
                    addSpace(5),
                    Flexible(
                      child: Text(
                        name,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: textStyle(false, 14, black),
                      ),
                    )
                  ],
                ),
              );
            }),
          )
        ],
      ),
    ),
  );
}

openStoryAt(
    {@required BuildContext context,
    @required int position,
    @required int playIndex,
    @required String key}) {
  List<List<BaseModel>> stories = [];
  final foldedOthers = allStories
      .where((e) => !e.myItem())
      .toList()
      .groupBy((s) => s.getUserId());
  final foldedMine = allStories
      .where((e) => e.myItem())
      .toList()
      .groupBy((s) => s.getUserId());

  stories.addAll(foldedMine.values.toList());
  stories.addAll(foldedOthers.values.toList());

  int storyPage =
      stories.indexWhere((element) => element[0].getUserId() == key);
  BaseModel story = stories[storyPage][playIndex];
  print(story.isVideo);
  print(stories[storyPage].length);
  // return;

  pushAndResult(
      context,
      StoriesPage(
        stories,
        position: storyPage,
        storyIndex: 0,
      ));
}

extension MapsExtra on Map {}
