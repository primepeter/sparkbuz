import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:line_icons/line_icons.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sparkbuz/AppConfig.dart';
import 'package:sparkbuz/AppEngine.dart';
import 'package:sparkbuz/MainAdmin.dart';
import 'package:sparkbuz/ShowStatus.dart';
import 'package:sparkbuz/assets.dart';
import 'package:sparkbuz/basemodel.dart';
import 'package:timeago/timeago.dart' as timeago;

PageController chatPageController = new PageController();
List<BaseModel> notifyList = List();

class PageStories extends StatefulWidget {
  @override
  _PageStoriesState createState() => _PageStoriesState();
}

var subs = [];

class _PageStoriesState extends State<PageStories>
    with AutomaticKeepAliveClientMixin, TickerProviderStateMixin {
  List<List<BaseModel>> storyList = [];
  bool setup = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setup = false;
    refreshStories();
    var refreshSub = homeRefreshController.stream.listen((b) {
      refreshStories();
    });
    subs.add(refreshSub);
  }

  loadStories(bool isNew) async {
    List local = [];
    FirebaseFirestore.instance
        .collection(STORY_BASE)
        .where(TIME,
            isGreaterThan: (DateTime.now().millisecondsSinceEpoch -
                (Duration.millisecondsPerDay * 1)))
        .limit(10)
        .orderBy(TIME, descending: true)
        .startAt([
          !isNew
              ? (allStories.isEmpty
                  ? DateTime.now().millisecondsSinceEpoch
                  : allStories[allStories.length - 1].getTime())
              : (allStories.isEmpty ? 0 : allStories[0].getTime())
        ])
        .get()
        .then((shots) async {
          local = shots.docs;
          for (var doc in shots.docs) {
            final model = BaseModel(doc: doc);
            int p = allStories
                .indexWhere((e) => e.getObjectId() == model.getObjectId());
            if (p != -1) {
              allStories[p] = model;
            } else {
              allStories.add(model);
            }
          }

          if (isNew) {
            refreshController.refreshCompleted();
          } else {
            int oldLength = storyList.length;
            int newLength = local.length;
            if (newLength <= oldLength) {
              refreshController.loadNoData();
              refreshController.refreshCompleted();
              //canRefresh = false;
            } else {
              refreshController.loadComplete();
              refreshController.refreshCompleted();
            }
          }
          setup = true;
          allStories.sort((a, b) => b.getTime().compareTo(a.getTime()));
          homeRefreshController.add(true);
        });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    for (var sub in subs) sub.cancel();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: white,
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.only(top: 35, right: 10, left: 10, bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
//                BackButton(
//                  color: black,
//                  onPressed: () {
//                    Navigator.pop(context, "");
//                  },
//                ),
                Text(
                  "Meet SparkBuzzers",
                  style: textStyle(true, 25, black),
                ),
              ],
            ),
          ),
          pageRefresh()
        ],
      ),
    );
  }

  bool hasUnreadStory() {
    bool hasUnread = false;
    for (List<BaseModel> list in storyList) {
      for (BaseModel bm in list) {
        if (bm.myItem()) continue;
        if (!bm.getList(SHOWN).contains(userModel.getObjectId())) {
          hasUnread = true;
          break;
        }
      }
      if (hasUnread) break;
    }
    return hasUnread;
  }

  refreshStories() {
    for (BaseModel model in allStories) {
      int listIndex = storyList.indexWhere((List<BaseModel> models) =>
          models[0].getString(USER_ID) == model.getString(USER_ID));

      //Fetch the story list...
      List<BaseModel> stories = listIndex == -1 ? [] : storyList[listIndex];
      int storyIndex =
          stories.indexWhere((bm) => bm.getObjectId() == model.getObjectId());
      if (storyIndex == -1) {
        stories.add(model);
      }
      stories.sort((bm1, bm2) => bm1.getTime().compareTo(bm2.getTime()));

      if (listIndex == -1) {
        storyList.add(stories);
      } else {
        storyList[listIndex] = stories;
      }
    }

    //Split the list
    List<BaseModel> myList = [];
    List<List<BaseModel>> unreadList = [];
    List<List<BaseModel>> readList = [];
    for (List list in storyList) {
      bool myItem = list.isEmpty
          ? false
          : (list[0].getUserId() == userModel.getObjectId());
      if (myItem) {
        myList = list;
        continue;
      }
      int unread = list.indexWhere(
          (bm) => !bm.getList(SHOWN).contains(userModel.getObjectId()));
      if (unread != -1) {
        unreadList.add(list);
      } else {
        readList.add(list);
      }
    }
    unreadList.sort((list1, list2) => list2[list2.length - 1]
        .getTime()
        .compareTo(list1[list1.length - 1].getTime()));
    readList.sort((list1, list2) => list2[list2.length - 1]
        .getTime()
        .compareTo(list1[list1.length - 1].getTime()));
    //sort the stories
    //storyList.sort((listA, listB)=>listA);
    storyList.clear();
    if (myList.isNotEmpty) storyList.add(myList);
    storyList.addAll(unreadList);
    storyList.addAll(readList);
    setup = true;
    if (mounted) setState(() {});
  }

  RefreshController refreshController = RefreshController();

  pageRefresh() {
    return Flexible(
      child: SmartRefresher(
        controller: refreshController,
        enablePullDown: true,
        enablePullUp: setup,
        header: WaterDropHeader(),
        // footer: ClassicFooter(
        //   noDataText: myPostList.length > 9
        //       ? "Nothing more for now, check later..."
        //       : "",
        //   textStyle: textStyle(false, 12, black.withOpacity(.7)),
        // ),
        onLoading: () {
          loadStories(false);
        },
        onRefresh: () {
          loadStories(true);
        },
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(top: 5, bottom: 100),
          children: [pageList()],
        ),
      ),
    );
  }

  pageList() {
    return Builder(
      builder: (ctx) {
        if (!setup)
          return Container(
            height: getScreenHeight(context) * .7,
            child: loadingLayout(),
          );

        if (storyList.isEmpty)
          return Container(
            height: getScreenHeight(context) * .7,
            child: emptyLayout(LineIcons.dot_circle_o, "No Stories",
                "You have no stories activity yet."),
          );

        return GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: 0.7,
          ),
          itemBuilder: (c, p) {
            int likedCount = 0;
            List<BaseModel> list = storyList[p];
            BaseModel model = list[0];
            bool myItem = model.getUserId() == userModel.getObjectId();
            bool isVideo = model.getBoolean(IS_VIDEO);
            String image =
                model.getString(isVideo ? THUMBNAIL_URL : STORY_IMAGE);

            String text = model.getString(STORY_TEXT);
            String mColor = model.getString(COLOR_KEY);
            final dateT = DateTime.fromMillisecondsSinceEpoch(model.getTime());
            String timeAgo = timeago.format(dateT);
            String name = model.getString(NAME);
            bool readAll = true;
            for (BaseModel bm in list) {
              likedCount = likedCount + bm.getList(LIKED).length;
              if (bm.myItem()) continue;
              if (!bm.getList(SHOWN).contains(userModel.getObjectId())) {
                readAll = false;
                break;
              }
            }

            double screenWidth = MediaQuery.of(context).size.width;
            double size = 250.0;

            return GestureDetector(
              onTap: () {
                int startAt = list.lastIndexWhere((bm) =>
                    bm.getList(SHOWN).contains(userModel.getObjectId()));
                startAt = startAt + 1;
                startAt = startAt > list.length - 1 ? 0 : startAt;
                pushAndResult(context, ShowStatus(list, startAt), result: (_) {
                  if (_ is String && _.isNotEmpty) {
                    if (list.length == 1) {
                      storyList.removeAt(p);
                    } else {
                      list.removeWhere((bm) => _ == bm.getObjectId());
                    }
                    allStories.removeWhere((bm) => _ == bm.getObjectId());
                  }
                  setState(() {});
                }, depend: false);
              },
              child: ClipRRect(
                borderRadius: BorderRadius.circular((25)),
                child: Opacity(
                  opacity: 1,
                  //opacity: model.myItem() || !readAll ? 1 : (.4),

                  child: Container(
                    width: size,
                    height: size,
                    margin: EdgeInsets.all(10),
                    child: Stack(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular((25)),
                          child: CachedNetworkImage(
                            imageUrl: image,
                            height: double.infinity,
                            width: double.infinity,
                            fit: BoxFit.cover,
                            placeholder: (c, s) {
                              return placeHolder(180, width: 160);
                            },
                          ),
                        ),
                        // if (likedCount > 0 && myItem)
                        Align(
                          alignment: Alignment.topRight,
                          child: new Container(
                            padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                            height: 25,
                            width: 40,
                            decoration: BoxDecoration(
                                color: AppConfig.appColor,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(25))),
                            child: Center(
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Text(
                                    likedCount > 9 ? "9+" : "$likedCount",
                                    style: textStyle(true, 12, white),
                                  ),
                                  if (likedCount <= 9) addSpaceWidth(5),
                                  Icon(
                                    Icons.visibility,
                                    size: 14,
                                    color: white,
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        if (!myItem && readAll)
                          Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                  height: 30,
                                  width: 30,
                                  decoration: BoxDecoration(
                                      border:
                                          Border.all(color: white, width: 2),
                                      shape: BoxShape.circle,
                                      color: green),
                                  child: Icon(
                                    Icons.check,
                                    size: 15,
                                    color: white,
                                  ))),
                        if (isVideo)
                          Center(
                            child: Container(
                              height: 40,
                              width: 40,
                              child: Icon(
                                Icons.play_arrow,
                                color: Colors.white,
                                size: 18,
                              ),
                              decoration: BoxDecoration(
                                  color: black.withOpacity(0.8),
                                  border: Border.all(
                                      color: Colors.white, width: 1.5),
                                  shape: BoxShape.circle),
                            ),
                          ),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: Container(
                            padding: EdgeInsets.all(2),
                            decoration: BoxDecoration(
                                color: AppConfig.appColor,
                                borderRadius: BorderRadius.circular(20)),
                            child: Row(
                              children: [
                                userImageItem(context, model,
                                    iconSize: 10,
                                    backColor: black.withOpacity(.2),
                                    strock: 2,
                                    size: 40,
                                    padLeft: false),
                                addSpaceWidth(5),
                                Column(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      model.myItem()
                                          ? "Your Story"
                                          : model.getUserName(),
                                      style: textStyle(true, 14, white),
                                    ),
                                    Text(
                                      timeAgo,
                                      style: textStyle(
                                          false, 11, white.withOpacity(1)),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            );
          },
          shrinkWrap: true,
          itemCount: storyList.length,
        );
      },
    );
  }

  page() {
    if (!setup) return loadingLayout();

    if (allStories.isEmpty)
      return emptyLayout(LineIcons.dot_circle_o, "No Stories",
          "You have no stories activity yet.");

    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        childAspectRatio: 0.7,
      ),
      itemBuilder: (c, p) {
        int likedCount = 0;
        List<BaseModel> list = storyList[p];
        BaseModel model = list[0];
        bool myItem = model.getUserId() == userModel.getObjectId();
        bool isVideo = model.getBoolean(IS_VIDEO);
        String image = model.getString(isVideo ? THUMBNAIL_URL : STORY_IMAGE);

        String text = model.getString(STORY_TEXT);
        String mColor = model.getString(COLOR_KEY);
        final dateT = DateTime.fromMillisecondsSinceEpoch(model.getTime());
        String timeAgo = timeago.format(dateT);
        String name = model.getString(NAME);
        bool readAll = true;
        for (BaseModel bm in list) {
          likedCount = likedCount + bm.getList(LIKED).length;
          if (bm.myItem()) continue;
          if (!bm.getList(SHOWN).contains(userModel.getObjectId())) {
            readAll = false;
            break;
          }
        }

        double screenWidth = MediaQuery.of(context).size.width;
        double size = 250.0;

        return GestureDetector(
          onTap: () {
            int startAt = list.lastIndexWhere(
                (bm) => bm.getList(SHOWN).contains(userModel.getObjectId()));
            startAt = startAt + 1;
            startAt = startAt > list.length - 1 ? 0 : startAt;
            pushAndResult(context, ShowStatus(list, startAt), result: (_) {
              if (_ is String && _.isNotEmpty) {
                if (list.length == 1) {
                  storyList.removeAt(p);
                } else {
                  list.removeWhere((bm) => _ == bm.getObjectId());
                }
                allStories.removeWhere((bm) => _ == bm.getObjectId());
              }
              setState(() {});
            }, depend: false);
          },
          child: ClipRRect(
            borderRadius: BorderRadius.circular((25)),
            child: Opacity(
              opacity: 1,
              //opacity: model.myItem() || !readAll ? 1 : (.4),

              child: Container(
                width: size,
                height: size,
                margin: EdgeInsets.all(10),
                child: Stack(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular((25)),
                      child: CachedNetworkImage(
                        imageUrl: image,
                        height: double.infinity,
                        width: double.infinity,
                        fit: BoxFit.cover,
                        placeholder: (c, s) {
                          return placeHolder(180, width: 160);
                        },
                      ),
                    ),
                    // if (likedCount > 0 && myItem)
                    Align(
                      alignment: Alignment.topRight,
                      child: new Container(
                        padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                        height: 25,
                        width: 40,
                        decoration: BoxDecoration(
                            color: AppConfig.appColor,
                            borderRadius:
                                BorderRadius.all(Radius.circular(25))),
                        child: Center(
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Text(
                                likedCount > 9 ? "9+" : "$likedCount",
                                style: textStyle(true, 12, white),
                              ),
                              if (likedCount <= 9) addSpaceWidth(5),
                              Icon(
                                Icons.visibility,
                                size: 14,
                                color: white,
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    if (!myItem && readAll)
                      Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                              height: 30,
                              width: 30,
                              decoration: BoxDecoration(
                                  border: Border.all(color: white, width: 2),
                                  shape: BoxShape.circle,
                                  color: green),
                              child: Icon(
                                Icons.check,
                                size: 15,
                                color: white,
                              ))),
                    if (isVideo)
                      Center(
                        child: Container(
                          height: 40,
                          width: 40,
                          child: Icon(
                            Icons.play_arrow,
                            color: Colors.white,
                            size: 18,
                          ),
                          decoration: BoxDecoration(
                              color: black.withOpacity(0.8),
                              border:
                                  Border.all(color: Colors.white, width: 1.5),
                              shape: BoxShape.circle),
                        ),
                      ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                        padding: EdgeInsets.all(2),
                        decoration: BoxDecoration(
                            color: AppConfig.appColor,
                            borderRadius: BorderRadius.circular(20)),
                        child: Row(
                          children: [
                            userImageItem(context, model,
                                iconSize: 10,
                                backColor: black.withOpacity(.2),
                                strock: 2,
                                size: 40,
                                padLeft: false),
                            addSpaceWidth(5),
                            Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  model.myItem()
                                      ? "Your Story"
                                      : model.getUserName(),
                                  style: textStyle(true, 14, white),
                                ),
                                Text(
                                  timeAgo,
                                  style: textStyle(
                                      false, 11, white.withOpacity(1)),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      },
      shrinkWrap: true,
      padding: EdgeInsets.only(top: 5, bottom: 100),
      itemCount: storyList.length,
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
