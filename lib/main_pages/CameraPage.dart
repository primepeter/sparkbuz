import 'dart:async';
import 'dart:io';
import 'dart:math';

// import 'package:camera/camera.dart';
import 'package:cached_video_player/cached_video_player.dart';
import 'package:flutter/material.dart';
import 'package:flutter_better_camera/camera.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:sparkbuz/CamPreview.dart';
import 'package:sparkbuz/CameraArMain.dart';
import 'package:sparkbuz/MainAdmin.dart';
import 'package:sparkbuz/app/app.dart';
import 'package:sparkbuz/basemodel.dart';
import 'package:video_compress/video_compress.dart';
import 'package:video_player/video_player.dart';

// import 'package:wechat_assets_picker/wechat_assets_picker.dart';

import '../AppEngine.dart';
import '../assets.dart';

void logError(String code, String message) =>
    print('Error: $code\nError Message: $message');

class CameraPage extends StatefulWidget {
  final bool storyMode;

  const CameraPage({Key key, this.storyMode = false}) : super(key: key);
  @override
  _CameraPageState createState() => _CameraPageState();
}

class _CameraPageState extends State<CameraPage>
    with WidgetsBindingObserver, TickerProviderStateMixin {
  CameraController controller;
  String imagePath;
  String filePath;
  VideoPlayerController videoController;
  VoidCallback videoPlayerListener;
  bool enableAudio = true;
  //FlashMode flashMode = FlashMode.autoFlash;
  ResolutionPreset resolutionPreset = ResolutionPreset.high;
  bool frontCamera = false;
  final camBtnKey = GlobalKey();
  Size camBtnSize;
  Offset camBtnPosition;

  Animation<Color> timerAnimation;
  AnimationController timerController;

  AnimationController recordingController;
  Animation<Color> recordingAnimation;

  bool isRecording = false;
  bool isRecordingComplete = false;
  double zoomTo = 1;
  double top = 0;
  double left = 0;
  double recordingOpacity = 1;
  String recordTimerText = "00:00";

  bool shareAsStory = false;
  int currentPage = 1;
  PageController vp = PageController(initialPage: 1);

  bool isScreenRecording = false;
  bool _switch = false;
  List<BaseModel> videoShare = [];
  String sparkShareId = getRandomId();
  List<CameraDescription> cameras = [];

  initializeCamera() async {
    try {
      availableCameras().then((cam) {
        cameras = cam;
        if (cameras.isNotEmpty) onNewCameraSelected(cameras[0]);
        if (mounted) setState(() {});
      });
    } on CameraException catch (e) {
      print("Init Camera Err $e");
    }
  }

  @override
  void initState() {
    super.initState();
    pauseController.add(true);

    if (widget.storyMode) {
      currentPage = 0;
      shareAsStory = true;
      vp = PageController(initialPage: 0);
    }
    WidgetsBinding.instance.addObserver(this);
    WidgetsBinding.instance.addPostFrameCallback((_) {
      initializeCamera();
    });

    recordingController =
        AnimationController(vsync: this, duration: Duration(seconds: 1));
    recordingAnimation =
        ColorTween(begin: red, end: transparent).animate(recordingController)
          ..addListener(() {})
          ..addStatusListener((status) {
            // bool completed = status == AnimationStatus.completed;
            // if (!isRecording) {
            //   recordingController.stop(canceled: true);
            //   //recordingController.reset();
            //   if (mounted) setState(() {});
            //   return;
            // }
            // if (mounted) setState(() {});
          });

    timerController =
        AnimationController(vsync: this, duration: Duration(minutes: 2));
    timerAnimation =
        ColorTween(begin: white, end: white).animate(timerController)
          ..addListener(() {
            recordingOpacity = recordingOpacity == 1 ? 0 : 1;
            recordTimerText = timerString;
            print("Timer $recordTimerText");
            if (mounted) setState(() {});
          })
          ..addStatusListener((status) {
            print("Status $status");
            if (status == AnimationStatus.completed) {
              isRecordingComplete = true;
              timerController.reset();
              recordingController.stop();
              recordingOpacity = 0;
              isRecording = false;
              return;
            }
          });
  }

  String get timerString {
    Duration duration = timerController.duration * timerController.value;
    return '${duration.inMinutes}:${(duration.inSeconds % 60).toString().padLeft(2, '0')}';
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    //for (var s in subs) s?.cancel();
    controller?.dispose();
    timerController?.dispose();
    recordingController?.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    requestPermissions();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.inactive ||
        state == AppLifecycleState.paused) {
      controller?.dispose();
    } else if (state == AppLifecycleState.resumed) {
      if (cameras.isNotEmpty) onNewCameraSelected(cameras[frontCamera ? 1 : 0]);
    }
  }

  requestPermissions() async {
    Map<Permission, PermissionStatus> statuses = await [
      Permission.storage,
      Permission.photos,
      Permission.microphone,
    ].request();
  }

  void onNewCameraSelected(CameraDescription cameraDescription) async {
    if (controller != null) {
      await controller?.dispose();
    }
    controller = CameraController(
      cameraDescription,
      resolutionPreset,
      //autoFocusEnabled: true,
      //flashMode: flashMode,
      enableAudio: enableAudio,
    );

    // If the controller is updated then update the UI.
    controller.addListener(() {
      if (mounted) setState(() {});
      if (controller.value.hasError) {
        //showInSnackBar('Camera error ${controller.value.errorDescription}');
      }
    });

    try {
      await controller.initialize();
    } on CameraException catch (e) {
      //_showCameraException(e);
    }

    if (mounted) setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.pop(context, '');
        return false;
      },
      child: Scaffold(
        // backgroundColor: Color(0xFFf5f5f4),
        backgroundColor: black,
        body: SafeArea(
          bottom: false,
          child: Container(
            child: Stack(
              children: <Widget>[
                //cameraPreview,
                cameraViews,
                //cameraButton,
                closeButton,
                //if (isRecording)
                // Align(
                //   alignment: Alignment.centerRight,
                //   child: Container(
                //     color: transparent,
                //     height: 300,
                //     width: 60,
                //     child: RotatedBox(
                //       quarterTurns: 3,
                //       child: Slider(
                //         value: zoomTo,
                //         max: 4,
                //         min: 0,
                //         divisions: 10,
                //         onChanged: (newValue) {
                //           //...
                //           zoomTo = newValue;
                //           if (null != controller)
                //             controller.zoom(zoomTo.toInt());
                //           print(zoomTo);
                //           setState(() {});
                //         },
                //       ),
                //     ),
                //   ),
                // )
                //if (isRecording)
                Align(
                  alignment: Alignment.centerRight,
                  child: Container(
                    width: 50,
                    height: 300,
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      //color: AppConfig.appYellow,
                    ),
                    child: RotatedBox(
                      quarterTurns: 3,
                      child: Slider(
                        value: zoomTo,
                        onChanged: (z) {
                          zoomTo = z;
                          setState(() {});
                          controller?.zoom(z);
                        },
                        min: 1,
                        max: 10,
                        divisions: 25,
                      ),
                    ),
                  ),
                )
                //if (!isRecording) cameraText
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget get cameraButton {
    double size = isRecording ? 80 : 60;
    double widthCenter = (getScreenWidth(context) - size) / 2;
    double topBottom = (getScreenHeight(context)) * .8;
    if (left != 0) widthCenter = left;
    if (top != 0) topBottom = top;

    return Positioned(
      top: topBottom,
      left: widthCenter,
      child: GestureDetector(
        onLongPress: () {
          isRecording = true;
          startVideoRecording();
          setState(() {});
        },
        onLongPressMoveUpdate: (p) {
          if (!isRecording) return;
          //cameraButtonPositionUpdate(p.globalPosition);
        },
        onLongPressEnd: (p) {
          isRecording = false;
          zoomTo = top = left = 0;
          //check for video duration here
          bool tooSmall = timerController.value <= .2;
          stopVideoRecording(false, deleteFile: tooSmall);
          //if (!isRecordingComplete) pauseVideoRecording();
          //if (null != controller) controller.zoom(0);
          timerController.reset();
          setState(() {});
        },
        onPanDown: (p) {
          if (isRecording) return;
          cameraButtonPositionUpdate(p.globalPosition);
        },
        child: Container(
          //duration: Duration(milliseconds: 100),
          key: camBtnKey,
          height: isRecording ? 80 : 60,
          width: isRecording ? 80 : 60,
          //margin: EdgeInsets.only(bottom: 20),
          child: isRecording
              ? CircularProgressIndicator(
                  value: timerController.value,
                  //backgroundColor: white,
                  valueColor: timerAnimation,
                )
              : null,
          decoration: BoxDecoration(
            color: white.withOpacity(0.2),
            borderRadius: BorderRadius.circular(isRecording ? 45 : 30),
            border: Border.all(
              style: BorderStyle.solid,
              color: isRecording ? red : white.withOpacity(0.4),
              width: 6,
            ),
          ),
        ),
      ),
    );
  }

  Widget get cameraViews {
    return Stack(
      children: [
        Align(
          alignment: Alignment.center,
          child: Builder(
            builder: (c) {
              if (controller == null || !controller.value.isInitialized) {
                return Container(
                  //margin: EdgeInsets.only(bottom: 80),
                  alignment: Alignment.center,
                  child: CircularProgressIndicator(
                    strokeWidth: 2,
                    valueColor: AlwaysStoppedAnimation(white),
                  ),
                );
              }

              return Align(
                alignment: Alignment.center,
                child: Container(
                  width: getScreenWidth(context),
                  child: AspectRatio(
                    aspectRatio: controller.value.aspectRatio,
                    child: CameraPreview(controller),
                  ),
                ),
              );
            },
          ),
        ),
        PageView(
          controller: vp,
          physics: NeverScrollableScrollPhysics(),
          onPageChanged: (p) {
            if (widget.storyMode) return;
            currentPage = p;
            shareAsStory = p == 0;
            //if(p==2)controller.dispose();
            setState(() {});
          },
          children: List.generate(
            2,
            (index) => Container(
              color: transparent,
            ),
          ),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            padding: EdgeInsets.only(left: 5, right: 5),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                if (isRecording)
                  Container(
                    decoration: BoxDecoration(
                        color: black.withOpacity(.8),
                        borderRadius: BorderRadius.circular(25)),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        AnimatedBuilder(
                          animation: recordingAnimation,
                          builder: (context, child) => Container(
                            margin: EdgeInsets.all(10),
                            width: 20,
                            height: 20,
                            decoration: BoxDecoration(
                                color: recordingAnimation.value,
                                shape: BoxShape.circle),
                          ),
                        ),
                        Flexible(
                          child: Text(
                            recordTimerText,
                            style: textStyle(true, 12, white),
                          ),
                        ),
                        GestureDetector(
                            onTap: () {
                              onStopButtonPressed(true);
                            },
                            child: Container(
                              padding: EdgeInsets.all(10),
                              child: Icon(
                                Icons.close,
                                color: white,
                                size: 20,
                              ),
                            ))
                      ],
                    ),
                  ),
                GestureDetector(
                  onTap: () {
                    // controller.zoom(-1);
                    // return;

                    if (isRecording) {
                      onStopButtonPressed(false);
                      return;
                    }

                    startVideoRecording();
                  },
                  // onDoubleTap: () {
                  //   if (isRecording) return;
                  //   isRecording = true;
                  //   timerController.forward();
                  //   setState(() {});
                  //   startVideoRecording();
                  // },
                  child: AnimatedContainer(
                    duration: Duration(milliseconds: 100),
                    key: camBtnKey,
                    height: isRecording ? 80 : 60,
                    width: isRecording ? 80 : 60,
                    //margin: EdgeInsets.only(bottom: 20),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        if (isRecording)
                          SizedBox(
                            height: isRecording ? 80 : 60,
                            width: isRecording ? 80 : 60,
                            child: CircularProgressIndicator(
                              value: timerController.value,
                              //backgroundColor: white,
                              valueColor: timerAnimation,
                            ),
                          ),
                        if (isRecording)
                          Align(
                              alignment: Alignment.center,
                              child: Container(
                                width: 20,
                                height: 20,
                                color: red0,
                              )),
                      ],
                    ),
                    decoration: BoxDecoration(
                      color: white.withOpacity(0.2),
                      borderRadius:
                          BorderRadius.circular(isRecording ? 45 : 30),
                      border: Border.all(
                        style: BorderStyle.solid,
                        color: isRecording ? red : white.withOpacity(0.4),
                        width: 6,
                      ),
                    ),
                  ),
                ),
                // if (!isRecording) ...[
                //   addSpace(8),
                //   Text(
                //     'Double Tap for video,Tap for photo',
                //     style: textStyle(false, 14, white),
                //   )
                // ],

                Container(
                  padding: EdgeInsets.all(15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      //if (shareAsStory)
                      GestureDetector(
                        onTap: () {
                          pickImages(context,
                              requestType: RequestType.video,
                              singleMode: true, onPicked: (_) async {
                            if (null == _) return;
                            final bm = BaseModel();
                            File file = File(_[0][PATH]);
                            String path = file.path;

                            File thumbnail =
                                await VideoCompress.getFileThumbnail(path);

                            bm.put(THUMBNAIL_PATH, thumbnail.path);
                            bm.put(VIDEO_PATH, path);
                            bm.put(VIDEO_PATH, path);
                            // bm.put(VIDEO_LENGTH, length);
                            bm.put(IS_VIDEO, true);

                            bm.put(OBJECT_ID, getRandomId());
                            bm.put(IMAGE_PATH, path);
                            //setState(() {});

                            if (shareAsStory) {
                              Navigator.pop(context);
                              storyController.add([bm]);
                              return;
                            }

                            pushAndResult(
                                context,
                                CamPreview(
                                  camContext: context,
                                  files: [bm],
                                ));
                            // final Trimmer _trimmer = Trimmer();
                            // await _trimmer.loadVideo(videoFile: file);
                            // pushAndResult(
                            //     context,
                            //     TrimmerView(
                            //       _trimmer,
                            //     ), result: (_) async {
                            //   if (null == _) return;
                            //   String path = _[0];
                            //   int length = _[1];
                            //
                            //   File thumbnail =
                            //       await VideoCompress.getFileThumbnail(path);
                            //
                            //   bm.put(THUMBNAIL_PATH, thumbnail.path);
                            //   bm.put(VIDEO_PATH, path);
                            //   bm.put(VIDEO_PATH, path);
                            //   bm.put(VIDEO_LENGTH, length);
                            //   bm.put(IS_VIDEO, true);
                            //   bm.put(OBJECT_ID, getRandomId());
                            //   bm.put(IMAGE_PATH, path);
                            //   //setState(() {});
                            //
                            //   if (shareAsStory) {
                            //     Navigator.pop(context);
                            //     storyController.add([bm]);
                            //     return;
                            //   }
                            //
                            //   pushAndResult(
                            //       context,
                            //       CamPreview(
                            //         camContext: context,
                            //         files: [bm],
                            //       ));
                            // });
                          });
                        },
                        child: Container(
                          height: 40,
                          width: 40,
                          child: Stack(
                            children: [
                              Align(
                                alignment: Alignment.center,
                                child: Container(
                                  alignment: Alignment.center,
                                  // decoration: BoxDecoration(
                                  //     color: white,
                                  //     borderRadius: BorderRadius.circular(8)),
                                  child: Image.asset(
                                    'assets/icons/upload.png',
                                    color: white,
                                    height: 25,
                                    width: 25,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              /*   Align(
                                alignment: Alignment.bottomRight,
                                child: Container(
                                  height: 20,
                                  width: 20,
                                  child: Icon(
                                    Icons.add,
                                    color: white,
                                    size: 12,
                                  ),
                                  alignment: Alignment.center,
                                  padding: EdgeInsets.all(4),
                                  decoration: BoxDecoration(
                                      border: Border.all(color: white),
                                      color: blue3,
                                      shape: BoxShape.circle),
                                ),
                              )*/
                            ],
                          ),
                        ),
                      ),
                      Flexible(
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: List.generate(3, (p) {
                              String title = "Stories";
                              if (p == 1) title = 'Spark';
                              if (p == 2) title = 'Effects';
                              bool active = currentPage == p;
                              return GestureDetector(
                                onTap: () async {
                                  if (widget.storyMode) return;

                                  if (p == 2) {
                                    await controller?.dispose();
                                    pushReplacementAndResult(
                                        context, CameraArMain());
                                    return;
                                  }
                                  vp.jumpToPage(p);
                                  setState(() {});
                                },
                                child: Container(
                                    margin: EdgeInsets.all(5),
                                    padding: EdgeInsets.all(5),
                                    width: 80,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        color: active
                                            ? appYellow
                                            : white
                                                .withOpacity(active ? 1 : 0.1),
                                        borderRadius:
                                            BorderRadius.circular(25)),
                                    child: Text(
                                      title,
                                      style: textStyle(
                                          active,
                                          active ? 16 : 14,
                                          active
                                              ? black
                                              : white.withOpacity(.5)),
                                    )),
                              );
                            }),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          if (cameras.isNotEmpty)
                            onNewCameraSelected(cameras[frontCamera ? 0 : 1]);
                          frontCamera = !frontCamera;
                          setState(() {});
                        },
                        child: Container(
                          height: 35,
                          width: 35,
                          alignment: Alignment.center,
                          child: Image.asset(
                            'assets/icons/switch_cam.png',
                            color: white,
                            height: 25,
                            width: 25,
                            fit: BoxFit.cover,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget get closeButton => GestureDetector(
        child: Container(
          margin: EdgeInsets.only(top: 0),
          padding: EdgeInsets.all(14),
          decoration: BoxDecoration(
              color: red,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(30),
                  bottomRight: Radius.circular(30))),
          child: Icon(
            Icons.clear,
            color: white,
            size: 18,
          ),
        ),
        onTap: () => Navigator.pop(context, ''),
      );

  cameraButtonPositionUpdate(Offset v) {
    top = max(v.dy - (80) / 2, 0);
    zoomTo = ((getScreenHeight(context) - v.dy) * .5) / 100;
    zoomTo = double.parse(zoomTo.toStringAsFixed(2));
    zoomTo = zoomTo.isNegative
        ? 1
        : zoomTo > 11
            ? 10
            : zoomTo;
    print('zoom $zoomTo');
    //startZooming(zoomTo);
    left = max(v.dx - (80) / 2, 0);
    setState(() {});
  }

  // startZooming(double p) {
  //   if (p == 0) return;
  //   if (null != controller) controller.zoomIn();
  //   Future.delayed(Duration(milliseconds: 5), () {
  //     startZooming(p);
  //   });
  // }

  String timestamp() => DateTime.now().millisecondsSinceEpoch.toString();

  void onStopButtonPressed(bool cancel) {
    isRecording = false;
    timerController.stop();
    timerController.reset();
    zoomTo = 1;
    if (mounted) setState(() {});
    stopVideoRecording(true);
  }

  Future<String> startVideoRecording() async {
    if (null == controller) return null;
    if (!controller.value.isInitialized) {
      //showInSnackBar('Error: select a camera first.');
      return null;
    }

    final Directory extDir = await getApplicationDocumentsDirectory();
    final String dirPath = '${extDir.path}/SparkBuz/${userModel.getUserName()}';
    await Directory(dirPath).create(recursive: true);
    /*final String */ filePath = '$dirPath/${timestamp()}.mp4';

    if (controller.value.isRecordingVideo) {
      // A recording is already started, do nothing.
      return null;
    }

    try {
      /* final bm = BaseModel();
      bm.put(VIDEO_PATH, filePath);
      videoPaths.add(bm);*/
      await controller.startVideoRecording(filePath);
      //isRecording = controller.value.isRecordingVideo;
      //if (isRecording) timerController.forward();
      isRecording = true;
      timerController.forward();
      recordingController.repeat(reverse: true);
      setState(() {});
    } on CameraException catch (e) {
      //_showCameraException(e);
      return null;
    }
    return filePath;
  }

  Future<void> stopVideoRecording(bool force, {bool deleteFile = false}) async {
    if (null == controller) return null;
    if (!controller.value.isRecordingVideo) {
      return null;
    }
    try {
      await controller.stopVideoRecording();

      File thumbnail = await VideoCompress.getFileThumbnail(filePath);

      BaseModel bm = BaseModel();
      bm.put(THUMBNAIL_PATH, thumbnail.path);
      bm.put(VIDEO_PATH, filePath);
      // bm.put(VIDEO_LENGTH, length);
      bm.put(IS_VIDEO, true);
      bm.put(OBJECT_ID, getRandomId());

      List vidRa = await getVideoRatioAndDuration(filePath);
      var duration = vidRa[0];
      var aspectRatio = vidRa[1];
      bm.put(VIDEO_DURATION, duration);
      bm.put(ASPECT_RATIO, aspectRatio);
      bm.put(SHOW_LOGO, true);

      if (shareAsStory) {
        bm.put(IMAGE_PATH, filePath);
        Navigator.pop(context);
        storyController.add([bm]);
        return;
      }

      pushAndResult(
          context,
          CamPreview(
            camContext: context,
            files: [bm],
          ));

      // final Trimmer _trimmer = Trimmer();
      // await _trimmer.loadVideo(videoFile: File(filePath));
      // pushAndResult(
      //     context,
      //     TrimmerView(
      //       _trimmer,
      //     ), result: (_) async {
      //   if (null == _) return;
      //   String path = _[0];
      //   int length = _[1];
      //   File thumbnail = await VideoCompress.getFileThumbnail(path);
      //   BaseModel bm = BaseModel();
      //   bm.put(THUMBNAIL_PATH, thumbnail.path);
      //   bm.put(VIDEO_PATH, path);
      //   bm.put(VIDEO_LENGTH, length);
      //   bm.put(IS_VIDEO, true);
      //   bm.put(OBJECT_ID, getRandomId());
      //
      //   if (shareAsStory) {
      //     bm.put(IMAGE_PATH, path);
      //     Navigator.pop(context);
      //     storyController.add([bm]);
      //     return;
      //   }
      //
      //   pushAndResult(
      //       context,
      //       CamPreview(
      //         camContext: context,
      //         files: [bm],
      //       ));
      //   setState(() {});
      // });
    } on CameraException catch (e) {
      //_showCameraException(e);
      return null;
    }
  }

  Future<void> pauseVideoRecording() async {
    if (null == controller) return null;

    if (!controller.value.isRecordingVideo) {
      return null;
    }

    try {
      await controller.pauseVideoRecording();
    } on CameraException catch (e) {
      //_showCameraException(e);
      rethrow;
    }
  }

  Future<void> resumeVideoRecording() async {
    if (null == controller) return null;

    if (!controller.value.isRecordingVideo) {
      return null;
    }

    try {
      await controller.resumeVideoRecording();
    } on CameraException catch (e) {
      //_showCameraException(e);
      rethrow;
    }
  }
}
