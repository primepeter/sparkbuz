import 'package:cached_video_player/cached_video_player.dart';
import 'package:flutter/material.dart';
import 'package:sparkbuz/AppEngine.dart';

import '../assets.dart';

class SparkVideoPlayer extends StatefulWidget {
  final String videoPath;
  final String thumbnail;
  final int position;
  final CachedVideoPlayerController currentPlayer;

  const SparkVideoPlayer(
      {Key key,
      this.videoPath,
      this.position = 0,
      this.currentPlayer,
      this.thumbnail})
      : super(key: key);
  @override
  _SparkVideoPlayerState createState() => _SparkVideoPlayerState();
}

class _SparkVideoPlayerState extends State<SparkVideoPlayer>
    with AutomaticKeepAliveClientMixin {
  bool showPlay = false;
  CachedVideoPlayerController currentPlayer;
  String thumbnail;

  showPlayButton() {
    showPlay = !showPlay;
    if (showPlay)
      currentPlayer.pause();
    else
      currentPlayer.play();
    if (mounted) setState(() {});
    if (!currentPlayer.value.isPlaying) return;
    Future.delayed(Duration(seconds: 3), () {
      showPlay = false;
      if (mounted) setState(() {});
    });
  }

  @override
  initState() {
    super.initState();
    currentPlayer = widget.currentPlayer;
    thumbnail = widget.thumbnail;
    if (currentPlayer.value.initialized) {
      currentPlayer.seekTo(Duration(seconds: 0));
      currentPlayer.setLooping(true);
      currentPlayer.play();
    }
  }

  @override
  dispose() {
    super.dispose();
  }

  @override
  void didUpdateWidget(SparkVideoPlayer oldWidget) {
    if (oldWidget.currentPlayer != widget.currentPlayer) {
      currentPlayer = widget.currentPlayer;
      thumbnail = widget.thumbnail;
      currentPlayer.seekTo(Duration(seconds: 0));
      currentPlayer.setLooping(true);
      currentPlayer.play();
      setState(() {});
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: black,
      body: page(),
    );
  }

  page() {
    return Stack(
      fit: StackFit.expand,
      children: [
        // videoScreen(),
        AspectRatio(
            aspectRatio: currentPlayer.value.aspectRatio,
            child: CachedVideoPlayer(currentPlayer)),
        Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(colors: [
            black.withOpacity(.01),
            black.withOpacity(.5),
          ], begin: Alignment.center, end: Alignment.bottomRight)),
        ),
        videoControl()
      ],
    );
  }

  videoControl() {
    return GestureDetector(
      onTap: () {
        showPlayButton();
      },
      child: Container(
        color: transparent,
        alignment: Alignment.center,
        child: AnimatedContainer(
            duration: Duration(milliseconds: 400),
            height: showPlay ? 50 : 0,
            width: showPlay ? 50 : 0,
            decoration: BoxDecoration(
                border: Border.all(color: white, width: 1),
                color: black.withOpacity(.5),
                shape: BoxShape.circle),
            child: showPlay
                ? Icon(
                    Icons.play_arrow,
                    color: white,
                    size: 20,
                  )
                : null),
      ),
    );
  }

  videoScreen() {
    return Center(
      child: Container(
        height: getScreenHeight(context),
        width: getScreenWidth(context),
        alignment: Alignment.center,
        child: AspectRatio(
            aspectRatio: currentPlayer.value.aspectRatio,
            child: CachedVideoPlayer(currentPlayer)),
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => false;
}
