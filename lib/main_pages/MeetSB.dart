import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sparkbuz/AppEngine.dart';
import 'package:sparkbuz/MainAdmin.dart';
import 'package:sparkbuz/NewSparkbuzzers.dart';
import 'package:sparkbuz/assets.dart';

import '../basemodel.dart';
import 'ShowProfile.dart';

class MeetSB extends StatefulWidget {
  @override
  _MeetSBState createState() => _MeetSBState();
}

class _MeetSBState extends State<MeetSB>
    with AutomaticKeepAliveClientMixin, TickerProviderStateMixin {
  bool setup = false;
  var subs = [];
  final refreshController = RefreshController(initialRefresh: false);
  bool canRefresh = true;
  List items = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    pauseController.add(true);
    setup = false;
    loadItems(false);
  }

  loadItems(bool isNew) async {
    print(userModel.getUserId());
    QuerySnapshot query = await FirebaseFirestore.instance
        .collection(LINK_BASE)
        //.where(PROFILE_TO_LINK, isEqualTo: true)
        .limit(25)
        .orderBy(TIME, descending: !isNew)
        .startAt([
      !isNew
          ? (items.isEmpty
              ? DateTime.now().millisecondsSinceEpoch
              : items[items.length - 1][TIME] ?? 0.toInt())
          : (items.isEmpty ? 0 : items[0][TIME] ?? 0.toInt())
    ]).get();

    for (var doc in query.docs) {
      BaseModel model = BaseModel(doc: doc);
      int p = items.indexWhere(
          (e) => BaseModel(items: e).getObjectId() == model.getObjectId());
      if (p == -1)
        items.add(model.items);
      else
        items[p] = model.items;
    }

    if (isNew) {
      refreshController.refreshCompleted();
    } else {
      int oldLength = items.length;
      int newLength = query.docs.length;
      if (newLength <= oldLength) {
        refreshController.loadNoData();
        canRefresh = false;
      } else {
        refreshController.loadComplete();
      }
    }
    setup = true;
    //items.sort((a, b) => b[TIME] ?? 0.compareTo(a[TIME] ?? 0));
    if (mounted) setState(() {});
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    for (var sub in subs) sub.cancel();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: white,
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.only(top: 35, right: 10, left: 10, bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
//                BackButton(
//                  color: black,
//                  onPressed: () {
//                    Navigator.pop(context, "");
//                  },
//                ),
                Text(
                  "Meet SparkBuzzers",
                  style: textStyle(true, 22, black),
                ),
              ],
            ),
          ),
          pageRefresh()
        ],
      ),
    );
  }

  pageRefresh() {
    return Flexible(
      child: Builder(
        builder: (c) {
          if (!setup) return loadingLayout();

          if (items.isEmpty)
            return emptyLayout('assets/icons/team.png', "No SparkBuzzers",
                "SparkBuzzers Info currently not available check back later.",
                clickText: 'Refresh', click: () {
              setup = false;
              setState(() {});
              loadItems(false);
            });

          return SmartRefresher(
              controller: refreshController,
              enablePullDown: false,
              enablePullUp: false,
              header: WaterDropHeader(),
              footer: ClassicFooter(
                noDataText: items.length > 9
                    ? "Nothing more for now, check later..."
                    : "",
                textStyle: textStyle(false, 12, black.withOpacity(.7)),
              ),
              onLoading: () {
                loadItems(false);
              },
              onRefresh: () {
                loadItems(true);
              },
              child: GridView.builder(
                physics: NeverScrollableScrollPhysics(),
                //padding: EdgeInsets.all(0),
                padding: EdgeInsets.only(top: 5, bottom: 100),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  childAspectRatio: 0.7,
                ),
                itemBuilder: (c, p) {
                  Map item = items[p];
                  BaseModel model = BaseModel(items: item);

                  String imagePath = "assets/sparkbuz/s$p.jpeg";
                  return GestureDetector(
                    onTap: () {
                      pushAndResult(
                          context,
                          ShowProfile(
                            showCloseBtn: true,
                            model: model,
                            profileId: model.getUserId(),
                          ));
                    },
                    onLongPress: () {
                      showListDialog(
                        context,
                        ['Edit', 'Delete'],
                        (_) {
                          if (_ == 0) {
                            pushAndResult(
                                context,
                                NewSparkbuzzers(
                                  model: model,
                                ));
                          }

                          if (_ == 1) {
                            model.deleteItem();
                            items.removeAt(p);
                            setState(() {});
                          }
                        },
                      );
                    },
                    child: Container(
                      padding: EdgeInsets.all(15),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular((25)),
                        child: CachedNetworkImage(
                            imageUrl: model.getString(LINK_IMAGE),
                            fit: BoxFit.cover),
                      ),
                    ),
                  );
                },
                //shrinkWrap: true,
                itemCount: items.length,
              ));
        },
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
