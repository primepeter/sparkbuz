import 'dart:async';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:line_icons/line_icons.dart';
import 'package:sparkbuz/AppEngine.dart';
import 'package:sparkbuz/MainAdmin.dart';
import 'package:sparkbuz/SearchPlace.dart';
import 'package:sparkbuz/app/app.dart';
import 'package:sparkbuz/assets.dart';
import 'package:sparkbuz/basemodel.dart';
import 'package:sparkbuz/date_picker/flutter_datetime_picker.dart';

// import 'package:square_in_app_payments/google_pay_constants.dart'
//     as google_pay_constants;
// import 'package:square_in_app_payments/in_app_payments.dart';
// import 'package:square_in_app_payments/models.dart';

import '../AppConfig.dart';

//BuildContext _frameContext;

class CreateAds extends StatefulWidget {
  final BaseModel model;

  const CreateAds({Key key, this.model}) : super(key: key);
  @override
  _CreateAdsState createState() => _CreateAdsState();
}

class _CreateAdsState extends State<CreateAds> {
  BaseModel model = BaseModel();
  final titleController = TextEditingController();
  final urlLinkController = TextEditingController();
  final priceController = TextEditingController();
  File adsImage;
  int startAt = 0;
  int endAt = 0;
  String startAtStr;
  String endAtStr;

  String placeName = '';
  double lat, long = 0;
  double localAdsCost;

  String yourPaying = "";
  int adsRunFor;

  String baseCurrency = appSettingsModel.getString(APP_CURRENCY);
  String baseCurrencyName = appSettingsModel.getString(APP_CURRENCY_NAME);
  String myCountry = userModel.getString(COUNTRY);
  double adsCostPerDay = appSettingsModel.getDouble(APP_ADS_PER_DAY);

  List<BaseModel> imagesUrl = [
    // "https://tinyurl.com/yyd4zdhe",
    // "https://tinyurl.com/y5hfxrzu",
    // "https://tinyurl.com/yyglaujf",
    // "https://tinyurl.com/y2wamc9p",
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    int myPlan = userModel.getInt(ACCOUNT_TYPE);
    String key = myPlan == 0 ? FEATURES_REGULAR : FEATURES_PREMIUM;
    BaseModel package = appSettingsModel.getModel(key);
    //adsCostPerDay = package.getDouble(ADS_PRICE);
    //_initSquarePayment();
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

    if (widget.model != null) {
      model = widget.model;
      imagesUrl = model.getListModel(IMAGES);
      titleController.text = model.getString(TITLE);
      urlLinkController.text = model.getString(ADS_URL);
      placeName = model.getString(PLACE_NAME);
      lat = model.getDouble(LATITUDE);
      long = model.getDouble(LONGITUDE);
      localAdsCost = model.getDouble(ADS_PRICE);
      yourPaying = "(\$$localAdsCost)";
    }
  }

  bool isLoading = true;
  bool applePayEnabled = false;
  bool googlePayEnabled = false;

  String applePayMerchantId = '';
  String squareApplicationId = '';
  String squareLocationId = '';

  // Future<void> _initSquarePayment() async {
  //   await InAppPayments.setSquareApplicationId(squareApplicationId);
  //
  //   var canUseApplePay = false;
  //   var canUseGooglePay = false;
  //   if (Platform.isAndroid) {
  //     await InAppPayments.initializeGooglePay(
  //         squareLocationId, google_pay_constants.environmentTest);
  //     canUseGooglePay = await InAppPayments.canUseGooglePay;
  //   } else if (Platform.isIOS) {
  //     await _setIOSCardEntryTheme();
  //     await InAppPayments.initializeApplePay(applePayMerchantId);
  //     canUseApplePay = await InAppPayments.canUseApplePay;
  //   }
  //
  //   setState(() {
  //     isLoading = false;
  //     applePayEnabled = canUseApplePay;
  //     googlePayEnabled = canUseGooglePay;
  //   });
  // }
  //
  // Future _setIOSCardEntryTheme() async {
  //   var themeConfiguationBuilder = IOSThemeBuilder();
  //   themeConfiguationBuilder.saveButtonTitle = 'Pay';
  //   themeConfiguationBuilder.errorColor = RGBAColorBuilder()
  //     ..r = 255
  //     ..g = 0
  //     ..b = 0;
  //   themeConfiguationBuilder.tintColor = RGBAColorBuilder()
  //     ..r = 36
  //     ..g = 152
  //     ..b = 141;
  //   themeConfiguationBuilder.keyboardAppearance = KeyboardAppearance.light;
  //   themeConfiguationBuilder.messageColor = RGBAColorBuilder()
  //     ..r = 114
  //     ..g = 114
  //     ..b = 114;
  //
  //   await InAppPayments.setIOSCardEntryTheme(themeConfiguationBuilder.build());
  // }

  @override
  Widget build(BuildContext context) {
    print(appSettingsModel.getDouble('appAdsPerDay'));

    return Scaffold(
      backgroundColor: white,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.fromLTRB(0, 40, 0, 10),
            color: white,
            child: Row(
              children: <Widget>[
                InkWell(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Container(
                      width: 50,
                      height: 50,
                      child: Center(
                          child: Icon(
                        Icons.keyboard_backspace,
                        color: black,
                        size: 25,
                      )),
                    )),
                Text(
                  "Create Ads",
                  style: textStyle(true, 25, black),
                ),
                Spacer()
              ],
            ),
          ),
          AnimatedContainer(
            duration: Duration(milliseconds: 500),
            width: double.infinity,
            height: errorText.isEmpty ? 0 : 40,
            color: red0,
            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: Center(
                child: Text(
              errorText,
              style: textStyle(true, 16, white),
            )),
          ),
          page(),
          Container(
            padding: EdgeInsets.all(15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text.rich(TextSpan(children: [
                  TextSpan(
                      text: "AdsPerDayCost ",
                      style: textStyle(true, 14, black.withOpacity(0.6))),
                  TextSpan(
                      text: "\$$adsCostPerDay",
                      style: textStyle(true, 18, black)),
                ])),
                if (null != adsRunFor)
                  Text.rich(TextSpan(children: [
                    TextSpan(
                        text: "Your ads will run for ",
                        style: textStyle(true, 14, black.withOpacity(0.6))),
                    TextSpan(
                        text: " $adsRunFor day(s)",
                        style: textStyle(true, 18, black)),
                  ])),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(15),
            child: FlatButton(
              onPressed: () {
                handleSave();
              },
              padding: EdgeInsets.all(15),
              color: AppConfig.appColor,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)),
              child: Center(
                  child: Text(
                "PUBLISH $yourPaying",
                style: textStyle(true, 18, white),
              )),
            ),
          )
        ],
      ),
    );
  }

  page() {
    return Flexible(
      child: ListView(
        padding: EdgeInsets.all(15),
        children: [
          imagesView(),
          addSpace(5),
          Container(
            decoration: BoxDecoration(
                color: red, borderRadius: BorderRadius.circular(8)),
            padding: EdgeInsets.all(10),
            child: Row(
              children: [
                Icon(
                  Icons.info,
                  color: white,
                ),
                addSpaceWidth(10),
                Flexible(
                  child: Text(
                    "Note: You are advised to use a landscape image for better and clearer ads display",
                    style: textStyle(false, 14, white),
                  ),
                ),
              ],
            ),
          ),
          addSpace(20),
          inputTextView(
            "Enter Title",
            titleController,
            isNum: false,
          ),
          inputTextView(
            "Enter Web Address",
            urlLinkController,
            isNum: false,
          ),
          addSpace(10),
          FlatButton(
            onPressed: () {
              pushAndResult(context, SearchPlace(), result: (BaseModel res) {
                if (null == res) return;
                placeName = res.getString(PLACE_NAME);
                lat = res.getDouble(LATITUDE);
                long = res.getDouble(LONGITUDE);
                setState(() {});
              }, depend: false);
            },
            padding: EdgeInsets.all(15),
            color: blue09,
            shape: RoundedRectangleBorder(
                side: BorderSide(color: black.withOpacity(.1), width: .5),
                borderRadius: BorderRadius.circular(5)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Icon(
                  Icons.location_on,
                  color: black.withOpacity(.5),
                ),
                addSpaceWidth(5),
                Flexible(
                  child: Text(
                    placeName.isEmpty ? "Pick a Location" : (placeName),
                    style: textStyle(placeName.isNotEmpty, 18,
                        black.withOpacity(placeName.isEmpty ? 0.3 : 1)),
                  ),
                ),
              ],
            ),
          ),
          addSpace(10),
          Container(
            decoration: BoxDecoration(
                color: red, borderRadius: BorderRadius.circular(8)),
            padding: EdgeInsets.all(10),
            child: Row(
              children: [
                Icon(
                  Icons.info,
                  color: white,
                ),
                addSpaceWidth(10),
                Flexible(
                  child: Text(
                    "Select a Region/Country you would like your Ad to be displayed",
                    style: textStyle(false, 14, white),
                  ),
                ),
              ],
            ),
          ),
          addSpace(10),
          Text(
            "Start Date for Ads",
            style: textStyle(true, 12, black),
          ),
          addSpace(10),
          FlatButton(
            onPressed: () {
              FocusScope.of(context).requestFocus(FocusNode());
              DatePicker.showDatePicker(context,
                  showTitleActions: true,
                  //minTime: DateTime(1930, 12, 31),
                  minTime: DateTime.now(),
                  //maxTime: DateTime.now(),
                  onChanged: (date) {}, onConfirm: (date) {
                startAt = date.millisecondsSinceEpoch;
                setState(() {});
                doConversion();
              },
                  currentTime: startAt == 0
                      ? null
                      : DateTime.fromMillisecondsSinceEpoch(startAt));
            },
            padding: EdgeInsets.all(15),
            color: blue09,
            shape: RoundedRectangleBorder(
                side: BorderSide(color: black.withOpacity(.1), width: .5),
                borderRadius: BorderRadius.circular(5)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Icon(
                  Icons.access_time,
                  color: black.withOpacity(.5),
                ),
                addSpaceWidth(5),
                Text(
                  startAt == 0 ? "Choose Date" : formatTimeChosen(startAt),
                  style: textStyle(startAt != 0, 18,
                      black.withOpacity(startAt == 0 ? 0.3 : 1)),
                ),
              ],
            ),
          ),
          addSpace(10),
          Text(
            "End Date for Ads",
            style: textStyle(true, 12, black),
          ),
          addSpace(10),
          FlatButton(
            onPressed: () {
              FocusScope.of(context).requestFocus(FocusNode());
              DatePicker.showDatePicker(context,
                  showTitleActions: true,
                  //minTime: DateTime(1930, 12, 31),
                  minTime: DateTime.now().add(Duration(days: 1)),
                  //maxTime: DateTime.now(),
                  onChanged: (date) {}, onConfirm: (date) {
                setState(() {
                  endAt = date.millisecondsSinceEpoch;
                });
                doConversion();
              },
                  currentTime: endAt == 0
                      ? null
                      : DateTime.fromMillisecondsSinceEpoch(endAt));
            },
            padding: EdgeInsets.all(15),
            color: blue09,
            shape: RoundedRectangleBorder(
                side: BorderSide(color: black.withOpacity(.1), width: .5),
                borderRadius: BorderRadius.circular(5)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Icon(
                  Icons.access_time,
                  color: black.withOpacity(.5),
                ),
                addSpaceWidth(5),
                Text(
                  endAt == 0 ? "Choose Date" : formatTimeChosen(endAt),
                  style: textStyle(
                      endAt != 0, 18, black.withOpacity(endAt == 0 ? 0.3 : 1)),
                ),
              ],
            ),
          ),
          addSpace(30),
        ],
      ),
    );
  }

  bool converted = false;

  doConversion() async {
    final startDT = DateTime.fromMillisecondsSinceEpoch(startAt);
    final endDT = DateTime.fromMillisecondsSinceEpoch(endAt);
    adsRunFor = endDT.difference(startDT).inDays;
    localAdsCost = (adsRunFor * adsCostPerDay).roundToDouble();
    yourPaying = "(\$$localAdsCost)";
    print("Ads Costs $localAdsCost");
    print("Run ---  ${endDT.difference(startDT).inDays}");
    setState(() {
      converted = true;
    });
  }

  String formatTimeChosen(int time) {
    final date = DateTime.fromMillisecondsSinceEpoch(time);
    return new DateFormat("MMMM d y").format(date);
  }

  handleSave() async {
    FocusScope.of(context).requestFocus(FocusNode());
    String title = titleController.text;
    String urlLink = urlLinkController.text.toLowerCase();

    var result = await (Connectivity().checkConnectivity());
    if (result == ConnectivityResult.none) {
      showError("No internet connectivity");
      return;
    }

    if (imagesUrl.isEmpty) {
      showError("Add Advert Image");
      return;
    }

    if (title.isEmpty) {
      showError("Add Title to Ads");
      return;
    }

    if (urlLink.isEmpty) {
      showError("Add WebAddress to Ads");
      return;
    }

    if (!urlLink.startsWith("http")) {
      showError("Web Address must start with http");
      return;
    }

    if (placeName.isEmpty) {
      showError("Pick a location!");
      return;
    }

    if (startAt == 0) {
      showError("Add Starting Date to Ads");
      return;
    }

    if (endAt == 0) {
      showError("Add Ending Date to Ads");
      return;
    }

    model.put(ADS_PRICE, localAdsCost);
    model.put(PLACE_NAME, placeName);
    model.put(LATITUDE, lat);
    model.put(LONGITUDE, long);
    model.put(HAS_PAID, false);
    model.put(TITLE, title);
    model.put(ADS_URL, urlLink);
    model.put(ADS_START_DATE, startAt);
    model.put(ADS_END_DATE, endAt);
    model.put(STATUS, /*isAdmin ? APPROVED :*/ PENDING);
    model.put(ADS_EXPIRY,
        DateTime.now().add(Duration(days: adsRunFor)).millisecondsSinceEpoch);

    showProgress(true, context,
        msg: "${widget.model != null ? "Saving" : "Adding"} Ads...");
    List<BaseModel> modelsUploaded = [];
    List<BaseModel> images = this.imagesUrl;

    saveCategoryImages(images, modelsUploaded, (_) {
      String id = getRandomId();
      if (widget.model != null) id = widget.model.getObjectId();
      model.put(OBJECT_ID, id);
      model.put(IMAGES, _.map((e) => e.items).toList());
      int p = adsList.indexWhere((e) => e.getObjectId() == id);
      if (p != -1) {
        adsList[p] = model;
      } else {
        adsList.add(model);
      }

      if (widget.model != null)
        model.updateItems();
      else
        model.saveItem(ADS_BASE, true, document: id);

      showProgress(false, context);
      titleController.clear();
      urlLinkController.clear();
      imagesUrl.clear();
      setState(() {});
      showMessage(context, Icons.check, green_dark, "Successful",
          'Ads Successfully ${widget.model != null ? "Updated" : "Added"}!',
          cancellable: true, onClicked: (_) {
        Navigator.pop(context);
      }, delayInMilli: 1200);
    });
  }

  saveCategoryImages(List<BaseModel> models, List<BaseModel> modelsUploaded,
      onCompleted(List<BaseModel> _)) {
    if (models.isEmpty) {
      onCompleted(modelsUploaded);
      return;
    }

    BaseModel model = models[0];
    String imagePath = model.getString(IMAGE_PATH);
    if (imagePath.isEmpty) {
      modelsUploaded.add(model);
      models.removeAt(0);
      saveCategoryImages(models, modelsUploaded, onCompleted);
      return;
    }
    File file = File(model.getString(IMAGE_PATH));
    uploadFile(file, (res, error) {
      if (error != null) {
        saveCategoryImages(models, modelsUploaded, onCompleted);
        return;
      }
      model.put(IMAGE_PATH, "");
      model.put(IMAGE_URL, res);
      modelsUploaded.add(model);
      models.removeAt(0);
      saveCategoryImages(models, modelsUploaded, onCompleted);
    });
  }

  String errorText = "";
  showError(String text, {bool wasLoading = false}) {
    if (wasLoading) showProgress(false, context);
    errorText = text;
    FocusScope.of(context).requestFocus(FocusNode());
    setState(() {});
    Future.delayed(Duration(seconds: 3), () {
      errorText = "";
      setState(() {});
    });
  }

  imagesView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Ads Images",
          style: textStyle(true, 14, black),
        ),
        addSpace(10),
        SingleChildScrollView(
          padding: EdgeInsets.fromLTRB(0, 0, 0, 15),
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              Row(
                mainAxisSize: MainAxisSize.min,
                children: List.generate(imagesUrl.length, (index) {
                  BaseModel model = imagesUrl[index];
                  String imageUrl = model.imageUrl;
                  String imagePath = model.getString(IMAGE_PATH);
                  bool online = imageUrl.startsWith("http");
                  bool active = 1 == index;
                  return Column(
                    children: [
                      GestureDetector(
                        onTap: () {},
                        child: Container(
                          padding: EdgeInsets.all(4),
                          margin: EdgeInsets.all(5),
                          height: 120,
                          width: 100,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: black.withOpacity(active ? 1 : 0.09)),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: online
                                ? CachedNetworkImage(
                                    imageUrl: imageUrl,
                                    height: 120,
                                    width: 100,
                                    fit: BoxFit.cover,
                                  )
                                : Image.file(
                                    File(imagePath),
                                    height: 120,
                                    width: 100,
                                    fit: BoxFit.cover,
                                  ),
                          ),
                        ),
                      ),
                      FlatButton(
                        onPressed: () {
                          imagesUrl.remove(model);
                          setState(() {});
                        },
                        color: red,
                        child: Text("REMOVE"),
                      )
                    ],
                  );
                }),
              ),
              if (imagesUrl.length < 1)
                GestureDetector(
                  onTap: () {
                    pickImages(context,
                        requestType: RequestType.image,
                        singleMode: true, onPicked: (_) {
                      if (null == _) return;
                      imagesUrl.addAll(_
                          .map((e) => BaseModel()
                            ..put(IMAGE_PATH, e[PATH])
                            ..put(IS_VIDEO, e[IS_VIDEO] ?? false)
                            ..put(THUMBNAIL_PATH, e[THUMBNAIL_PATH] ?? '')
                            ..items)
                          .toList());
                      setState(() {});
                    });

                    /* openGallery(context, onPicked: (_) {
                      if (null == _) return;
                      imagesUrl.addAll(_
                          .map((e) => BaseModel()
                            ..put(IMAGE_PATH, e.file.path)
                            ..put(IS_VIDEO, e.isVideo)
                            ..items)
                          .toList());
                      setState(() {});
                    });*/
                  },
                  child: Container(
                    padding: EdgeInsets.all(4),
                    margin: EdgeInsets.all(5),
                    height: 120,
                    width: 100,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: black.withOpacity(0.09)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [Icon(LineIcons.plus_circle), Text("Add")],
                    ),
                  ),
                )
            ],
          ),
        )
      ],
    );
  }
}
