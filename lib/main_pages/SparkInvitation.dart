
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:sparkbuz/AppConfig.dart';
import 'package:sparkbuz/AppEngine.dart';
import 'package:sparkbuz/MainAdmin.dart';

import '../assets.dart';
import '../basemodel.dart';

class SparkInvitation extends StatefulWidget {
  final BaseModel model;

  const SparkInvitation({Key key, this.model}) : super(key: key);

  @override
  _SparkInvitationState createState() => _SparkInvitationState();
}

class _SparkInvitationState extends State<SparkInvitation> {
  @override
  initState() {
    super.initState();
    sparkInviteVisible = true;
  }

  @override
  Widget build(BuildContext context) {
    return Material(
        color: black.withOpacity(.2),
        child: Stack(
          alignment: Alignment.center,
          children: [
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Container(
                color: black.withOpacity(.4),
              ),
            ),
            Container(
              margin: EdgeInsets.all(20),
              padding: EdgeInsets.all(20),
              decoration: BoxDecoration(
                  color: AppConfig.appYellow,
                  borderRadius: BorderRadius.circular(25)),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Image.asset(
                    "assets/icons/spark_plain.png",
                    scale: 3,
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    margin: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        color: white.withOpacity(0),
                        borderRadius: BorderRadius.circular(25)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: List.generate(
                        widget.model.getList(IMAGES).length,
                        (index) => Container(
                          margin: EdgeInsets.all(2),
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(100),
                                border: Border.all(
                                    color: black.withOpacity(.5), width: 2)),
                            padding: EdgeInsets.all(4),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(100),
                              child: CachedNetworkImage(
                                imageUrl: widget.model.getList(IMAGES)[index],
                                height: 50,
                                width: 50,
                                fit: BoxFit.cover,
                                placeholder: (c, s) {
                                  return Container(
                                    height: 50,
                                    width: 50,
                                    child: Icon(LineIcons.user),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(100),
                                      color: black.withOpacity(.1),
                                    ),
                                  );
                                },
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  addSpace(10),
                  Row(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25),
                            border: Border.all(
                                color: black.withOpacity(.5), width: 2)),
                        padding: EdgeInsets.all(4),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(25),
                          child: CachedNetworkImage(
                            imageUrl: widget.model.userImage,
                            height: 30,
                            width: 30,
                            fit: BoxFit.cover,
                            placeholder: (c, s) {
                              return Container(
                                height: 25,
                                width: 25,
                                child: Icon(
                                  LineIcons.user,
                                  size: 20,
                                ),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(100),
                                  color: black.withOpacity(.1),
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                      addSpaceWidth(10),
                      Flexible(
                        child: Text(
                          "@${getFirstName(widget.model)} has invited you to join a SparkShare Video Sharing session with 3 other participants.",
                          style: textStyle(false, 14, black.withOpacity(.7)),
                        ),
                      ),
                    ],
                  ),
                  addSpace(20),
                  Row(
                    children: [
                      Flexible(
                          child: FlatButton(
                        onPressed: () {
                          showNewNotifyDot.remove(widget.model.getObjectId());
                          pauseController.add(false);
                          Navigator.pop(this.context, true);
                        },
                        color: green,
                        padding: EdgeInsets.all(16),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8)),
                        child: Center(child: Text('ACCEPT')),
                      )),
                      addSpaceWidth(10),
                      Flexible(
                          child: FlatButton(
                        onPressed: () {
                          handleShareInvitation(widget.model, false, () {
                            sparkInviteVisible = false;
                            Navigator.pop(this.context);
                          });
                        },
                        color: red,
                        padding: EdgeInsets.all(16),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8)),
                        child: Center(child: Text('DECLINE')),
                      )),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ));
  }
}
