import 'package:flutter/material.dart';

import '../AppEngine.dart';
import '../assets.dart';

class fieldDialog extends StatefulWidget {
  String title;
  List items;
  String hint;
  String okText;
  TextInputType inputType;
  int maxLength;
  bool allowEmpty = false;
  Color color;
  fieldDialog(title,
      {items,
      hint,
      okText,
      inputType,
      maxLength = 10000,
      allowEmpty,
      color = appColor}) {
    this.title = title;
    this.items = items;
    this.hint = hint;
    this.okText = okText;
    this.inputType = inputType;
    this.maxLength = maxLength;
    this.allowEmpty = allowEmpty;
    this.color = color;
  }

  @override
  _fieldDialogState createState() => _fieldDialogState();
}

class _fieldDialogState extends State<fieldDialog> {
  final editingController = new TextEditingController();
  int clickBack = 0;
  List items = [];
  int activeItem = -1;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    items = widget.items;
    // editingController.addListener(() {
    //   String text = editingController.text;
    //   if (activeItem == -1) return;
    //   items[activeItem] = text;
    //   setState(() {});
    // });
  }

  @override
  Widget build(BuildContext c) {
    return WillPopScope(
        onWillPop: () async {
          int now = DateTime.now().millisecondsSinceEpoch;
          if ((now - clickBack) > 5000) {
            clickBack = now;
            showError(
              "Click back again to exit",
            );
            return false;
          } else {
            Navigator.pop(context);
          }

          return false;
        },
        child: Scaffold(backgroundColor: white, body: page()));
  }

  page() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        addSpace(30),
        new Container(
          width: double.infinity,
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Container(
                    width: 50,
                    height: 50,
                    child: Center(
                        child: Icon(
                      Icons.keyboard_backspace,
                      color: black,
                      size: 25,
                    )),
                  )),
              Flexible(
                fit: FlexFit.tight,
                flex: 1,
                child: new Text(
                  widget.title,
                  style: textStyle(true, 17, black),
                ),
              ),
              addSpaceWidth(10),
              FlatButton(
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25)),
                  color: widget.color,
                  onPressed: () {
                    //String text = editingController.text.trim();
                    if (items.isEmpty && !widget.allowEmpty) {
                      showError("Nothing to update");
                      return;
                    }
                    if (items.length > widget.maxLength) {
                      showError(
                          "Text should not be more than ${widget.maxLength} characters");
                      return;
                    }
                    Navigator.pop(context, items);
                  },
                  child: Text(
                    widget.okText == null ? "OK" : widget.okText,
                    style: textStyle(true, 14, white),
                  )),
              addSpaceWidth(15)
            ],
          ),
        ),
        addLine(1, black.withOpacity(.1), 0, 5, 0, 0),
        AnimatedContainer(
          duration: Duration(milliseconds: 500),
          width: double.infinity,
          height: errorText.isEmpty ? 0 : 40,
          color: red0,
          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: Center(
              child: Text(
            errorText,
            style: textStyle(true, 16, white),
          )),
        ),
        new Expanded(
          flex: 1,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: SingleChildScrollView(
              padding: EdgeInsets.all(0),
              child: new Column(
                children: [
                  TextField(
                    //textInputAction: TextInputAction.done,
                    textCapitalization: TextCapitalization.sentences,
                    autofocus: true,
                    //maxLength: widget.maxLength,
                    decoration: InputDecoration(
                        hintText: widget.hint,
                        hintStyle: textStyle(
                          false,
                          24,
                          black.withOpacity(.5),
                        ),
                        border: InputBorder.none),
                    style: textStyle(false, 24, black),
                    controller: editingController,
                    cursorColor: black,
                    cursorWidth: 1,
                    //maxLines: null,
                    keyboardType: widget.inputType == null
                        ? TextInputType.multiline
                        : widget.inputType,
                  ),
                  //if (activeItem == -1) ...[
                  addSpace(10),
                  FlatButton(
                    onPressed: () {
                      String text = editingController.text;
                      // items.remove(index);
                      // if(active)editingController.clear();

                      if (text.isEmpty) {
                        showError("Nothing to add");
                        return;
                      }

                      if (activeItem == -1)
                        items.add(text);
                      else
                        items[activeItem] = text;
                      editingController.clear();
                      setState(() {});
                    },
                    color: appColor,
                    child: Center(
                        child: Text(
                            activeItem == -1 ? "Add Item" : "Update Item",
                            style: textStyle(false, 14, white))),
                  ),
                  //],
                  addSpace(10),
                  ...List.generate(items.length, (index) {
                    bool active = activeItem == index;

                    return GestureDetector(
                        onTap: () {
                          editingController.text = items[index].toString();
                          activeItem = index;
                          setState(() {});
                        },
                        child: Container(
                            decoration: BoxDecoration(
                                color:
                                    active ? appColor : black.withOpacity(.05),
                                borderRadius: BorderRadius.circular(12),
                                border:
                                    Border.all(color: black.withOpacity(.04))),
                            padding: EdgeInsets.all(5),
                            margin: EdgeInsets.all(5),
                            child: Row(
                              children: [
                                Expanded(
                                    child: Text(
                                  items[index].toString(),
                                  style: textStyle(active, active ? 16 : 14,
                                      active ? white : black.withOpacity(0.7)),
                                )),
                                addSpaceWidth(10),
                                FlatButton(
                                  onPressed: () {
                                    items.removeAt(index);
                                    if (active) editingController.clear();
                                    setState(() {});
                                  },
                                  color: red0,
                                  shape: CircleBorder(),
                                  minWidth: 30,
                                  height: 30,
                                  child:
                                      Icon(Icons.clear, size: 16, color: white),
                                )
                              ],
                            )));
                  })
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  String errorText = "";
  showError(String text, {bool wasLoading = false}) {
    FocusScope.of(context).requestFocus(FocusNode());

    if (wasLoading) showProgress(false, context);
    errorText = text;
    setState(() {});
    Future.delayed(Duration(seconds: 3), () {
      errorText = "";
      setState(() {});
    });
  }
}
