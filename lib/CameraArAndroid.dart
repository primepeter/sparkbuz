import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:camera_deep_ar/camera_deep_ar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_better_camera/camera.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:sparkbuz/CamPreview.dart';
import 'package:sparkbuz/basemodel.dart';
import 'package:video_compress/video_compress.dart';

// import 'package:wechat_assets_picker/wechat_assets_picker.dart';

import 'AppEngine.dart';
import 'MainAdmin.dart';
import 'app/app.dart';
import 'assets.dart';

void logError(String code, String message) =>
    print('Error: $code\nError Message: $message');

class CameraArAndroid extends StatefulWidget {
  final bool storyMode;

  const CameraArAndroid({Key key, this.storyMode = false}) : super(key: key);

  @override
  _CameraArAndroidState createState() => _CameraArAndroidState();
}

class _CameraArAndroidState extends State<CameraArAndroid>
    with WidgetsBindingObserver, TickerProviderStateMixin {
  CameraDeepArController cameraDeepArController;
  int currentPagePost = 1;
  int currentPage = 0;
  int currentPageMode = 0;
  RecordingMode recordingMode = RecordingMode.video;
  CameraDirection cameraDirection = CameraDirection.back;
  CameraMode cameraMode = CameraMode.masks;

/*  final supportedMasks = [
    Masks.none,
    Masks.aviators,
    Masks.bigmouth,
    Masks.dalmatian,
    Masks.look2,
    Masks.flowers,
    Masks.grumpycat,
    Masks.lion,
  ];*/

  final supportedMasks = Masks.values;

  /*final supportedEffects = [
    Effects.none,
    Effects.heart,
    Effects.rain,
  ];*/

  final supportedEffects = Effects.values;
  final supportedFilters = Filters.values;

  int get modeSize {
    if (cameraMode == CameraMode.masks) return supportedMasks.length;
    if (cameraMode == CameraMode.effects) return supportedEffects.length;
    return supportedFilters.length;
  }

  List<BaseModel> videoPaths = [];
  bool enableAudio = true;
  final camBtnKey = GlobalKey();
  Size camBtnSize;
  Offset camBtnPosition;

  Animation<Color> timerAnimation;
  AnimationController timerController;

  AnimationController recordingController;
  Animation<Color> recordingAnimation;

  bool isRecording = false;
  bool isRecordingComplete = false;
  double zoomTo = 1;
  double top = 0;
  double left = 0;
  double recordingOpacity = 1;
  String recordTimerText = "00:00";

  bool shareAsStory = false;
  final vp = PageController(viewportFraction: .24);

  bool isScreenRecording = false;
  bool _switch = false;
  List<BaseModel> videoShare = [];
  String sparkShareId = getRandomId();
  List<CameraDescription> cameras = [];

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    WidgetsBinding.instance.addPostFrameCallback((_) {});

    if (widget.storyMode) {
      currentPagePost = 0;
      shareAsStory = true;
    }

    recordingController =
        AnimationController(vsync: this, duration: Duration(seconds: 1));
    recordingAnimation =
        ColorTween(begin: red, end: transparent).animate(recordingController)
          ..addListener(() {})
          ..addStatusListener((status) {
            // bool completed = status == AnimationStatus.completed;
            // if (!isRecording) {
            //   recordingController.stop(canceled: true);
            //   //recordingController.reset();
            //   if (mounted) setState(() {});
            //   return;
            // }
            // if (mounted) setState(() {});
          });

    timerController =
        AnimationController(vsync: this, duration: Duration(minutes: 2));
    timerAnimation =
        ColorTween(begin: white, end: white).animate(timerController)
          ..addListener(() {
            recordingOpacity = recordingOpacity == 1 ? 0 : 1;
            recordTimerText = timerString;
            print("Timer $recordTimerText");
            if (mounted) setState(() {});
          })
          ..addStatusListener((status) {
            print("Status $status");
            if (status == AnimationStatus.completed) {
              isRecordingComplete = true;
              timerController.reset();
              recordingController.stop();
              recordingOpacity = 0;
              isRecording = false;
              return;
            }
          });
  }

  String get timerString {
    Duration duration = timerController.duration * timerController.value;
    return '${duration.inMinutes}:${(duration.inSeconds % 60).toString().padLeft(2, '0')}';
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    //for (var s in subs) s?.cancel();
    timerController?.dispose();
    recordingController?.dispose();
    cameraDeepArController?.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    requestPermissions();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // if (state == AppLifecycleState.inactive ||
    //     state == AppLifecycleState.paused) {
    //   controller?.dispose();
    // } else if (state == AppLifecycleState.resumed) {
    //   onNewCameraSelected(cameras[frontCamera ? 1 : 0]);
    // }
  }

  requestPermissions() async {
    Map<Permission, PermissionStatus> statuses = await [
      Permission.storage,
      Permission.photos,
      Permission.microphone,
    ].request();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.pop(context, '');
        return false;
      },
      child: Scaffold(
        // backgroundColor: Color(0xFFf5f5f4),
        backgroundColor: black,
        body: SafeArea(
          bottom: false,
          child: Container(
            child: Stack(
              children: <Widget>[
                cameraViews,
                topItems,
                Align(
                  alignment: Alignment.centerRight,
                  child: Container(
                    width: 50,
                    height: 300,
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      //color: AppConfig.appYellow,
                    ),
                    child: RotatedBox(
                      quarterTurns: 3,
                      child: Slider(
                        value: zoomTo,
                        onChanged: (z) {
                          zoomTo = z;
                          setState(() {});
                          cameraDeepArController?.zoomTo(z.toInt());
                        },
                        min: 1,
                        max: 50,
                        divisions: 25,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

/*  Widget get cameraButton {
    double size = isRecording ? 80 : 60;
    double widthCenter = (getScreenWidth(context) - size) / 2;
    double topBottom = (getScreenHeight(context)) * .8;
    if (left != 0) widthCenter = left;
    if (top != 0) topBottom = top;

    return Positioned(
      top: topBottom,
      left: widthCenter,
      child: GestureDetector(
        onLongPress: () {
          isRecording = true;
          startVideoRecording();
          setState(() {});
        },
        onLongPressMoveUpdate: (p) {
          if (!isRecording) return;
          //cameraButtonPositionUpdate(p.globalPosition);
        },
        onLongPressEnd: (p) {
          isRecording = false;
          zoomTo = top = left = 0;
          //check for video duration here
          bool tooSmall = timerController.value <= .2;
          stopVideoRecording();
          timerController.reset();
          setState(() {});
        },
        onPanDown: (p) {
          if (isRecording) return;
          cameraButtonPositionUpdate(p.globalPosition);
        },
        child: Container(
          //duration: Duration(milliseconds: 100),
          key: camBtnKey,
          height: isRecording ? 80 : 60,
          width: isRecording ? 80 : 60,
          //margin: EdgeInsets.only(bottom: 20),
          child: isRecording
              ? CircularProgressIndicator(
                  value: timerController.value,
                  //backgroundColor: white,
                  valueColor: timerAnimation,
                )
              : null,
          decoration: BoxDecoration(
            color: white.withOpacity(0.2),
            borderRadius: BorderRadius.circular(isRecording ? 45 : 30),
            border: Border.all(
              style: BorderStyle.solid,
              color: isRecording ? red : white.withOpacity(0.4),
              width: 6,
            ),
          ),
        ),
      ),
    );
  }*/

  Widget get cameraViews {
    final recordings = videoPaths
        .where((e) => e.getString(THUMBNAIL_PATH).isNotEmpty)
        .toList();

    return Stack(
      children: [
        Align(
          alignment: Alignment.center,
          child: Container(
            padding: EdgeInsets.only(top: 60, bottom: 60),
            child: CameraDeepAr(
                onCameraReady: (isReady) {
                  print("Camera status $isReady");
                  //_platformVersion = "Camera status $isReady";
                  setState(() {});
                },
                recordingMode: recordingMode,
                cameraDirection: cameraDirection,
                cameraMode: cameraMode,
                supportedEffects: supportedEffects,
                supportedFilters: supportedFilters,
                supportedMasks: supportedMasks,
                onImageCaptured: (path) {},
                onVideoRecorded: (path) async {
                  isRecording = false;
                  setState(() {});

                  File thumbnail = await VideoCompress.getFileThumbnail(path);
                  BaseModel bm = BaseModel();
                  bm.put(THUMBNAIL_PATH, thumbnail.path);
                  bm.put(VIDEO_PATH, path);
                  // bm.put(VIDEO_LENGTH, length);
                  bm.put(SHOW_LOGO, true);
                  bm.put(IMAGE_PATH, path);
                  bm.put(OBJECT_ID, getRandomId());

                  List vidRa = await getVideoRatioAndDuration(path);
                  var duration = vidRa[0];
                  var aspectRatio = vidRa[1];
                  bm.put(VIDEO_DURATION, duration);
                  bm.put(ASPECT_RATIO, aspectRatio);
                  bm.put(SHOW_LOGO, true);

                  if (shareAsStory) {
                    Navigator.pop(context);
                    storyController.add([bm]);
                    return;
                  }

                  pushAndResult(
                      context,
                      CamPreview(
                        camContext: context,
                        files: [bm],
                      ));

                  // final Trimmer _trimmer = Trimmer();
                  // await _trimmer.loadVideo(videoFile: File(path));
                  // pushAndResult(
                  //     context,
                  //     TrimmerView(
                  //       _trimmer,
                  //     ), result: (_) async {
                  //   if (null == _) return;
                  //   String path = _[0];
                  //   int length = _[1];
                  //   File thumbnail = await VideoCompress.getFileThumbnail(path);
                  //   BaseModel bm = BaseModel();
                  //   bm.put(THUMBNAIL_PATH, thumbnail.path);
                  //   bm.put(VIDEO_PATH, path);
                  //   bm.put(VIDEO_LENGTH, length);
                  //   bm.put(IS_VIDEO, true);
                  //   bm.put(IMAGE_PATH, path);
                  //   bm.put(OBJECT_ID, getRandomId());
                  //
                  //   if (shareAsStory) {
                  //     Navigator.pop(context);
                  //     storyController.add([bm]);
                  //     return;
                  //   }
                  //
                  //   pushAndResult(
                  //       context,
                  //       CamPreview(
                  //         camContext: context,
                  //         files: [bm],
                  //       ));
                  //   setState(() {});
                  // });
                },
                androidLicenceKey: appSettingsModel.getString(DEEP_AR_ANDROID),
                iosLicenceKey: appSettingsModel.getString(DEEP_AR_IOS),
                cameraDeepArCallback: (c) async {
                  cameraDeepArController = c;
                  setState(() {});
                }),
          ),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            padding: EdgeInsets.only(left: 5, right: 5),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                if (isRecording)
                  Container(
                    decoration: BoxDecoration(
                        color: black.withOpacity(.8),
                        borderRadius: BorderRadius.circular(25)),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        AnimatedBuilder(
                          animation: recordingAnimation,
                          builder: (context, child) => Container(
                            margin: EdgeInsets.all(10),
                            width: 20,
                            height: 20,
                            decoration: BoxDecoration(
                                color: recordingAnimation.value,
                                shape: BoxShape.circle),
                          ),
                        ),
                        Flexible(
                          child: Text(
                            recordTimerText,
                            style: textStyle(true, 12, white),
                          ),
                        ),
                        GestureDetector(
                            onTap: () {
                              onStopButtonPressed(true);
                            },
                            child: Container(
                              padding: EdgeInsets.all(10),
                              child: Icon(
                                Icons.close,
                                color: white,
                                size: 20,
                              ),
                            ))
                      ],
                    ),
                  ),
                GestureDetector(
                  onTap: () {
                    if (cameraDeepArController == null) return;
                    if (isRecording) {
                      onStopButtonPressed(false);
                      return;
                    }

                    startVideoRecording();
                  },
                  // onDoubleTap: () {
                  //   if (isRecording) return;
                  //   isRecording = true;
                  //   timerController.forward();
                  //   setState(() {});
                  //   startVideoRecording();
                  // },
                  child: AnimatedContainer(
                    duration: Duration(milliseconds: 100),
                    key: camBtnKey,
                    height: isRecording ? 80 : 60,
                    width: isRecording ? 80 : 60,
                    //margin: EdgeInsets.only(bottom: 20),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        if (isRecording)
                          SizedBox(
                            height: isRecording ? 80 : 60,
                            width: isRecording ? 80 : 60,
                            child: CircularProgressIndicator(
                              value: timerController.value,
                              //backgroundColor: white,
                              valueColor: timerAnimation,
                            ),
                          ),
                        if (isRecording)
                          Align(
                              alignment: Alignment.center,
                              child: Container(
                                width: 20,
                                height: 20,
                                color: red0,
                              )),
                      ],
                    ),
                    decoration: BoxDecoration(
                      color: white.withOpacity(0.2),
                      borderRadius:
                          BorderRadius.circular(isRecording ? 45 : 30),
                      border: Border.all(
                        style: BorderStyle.solid,
                        color: isRecording ? red : white.withOpacity(0.4),
                        width: 6,
                      ),
                    ),
                  ),
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  padding: EdgeInsets.all(15),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: List.generate(modeSize, (p) {
                      bool active = currentPage == p;
                      return GestureDetector(
                        onTap: () {
                          changeCameraModes(p);
                        },
                        child: Container(
                            margin: EdgeInsets.all(5),
                            padding: EdgeInsets.all(12),
                            width: active ? 80 : 60,
                            height: active ? 80 : 60,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                color: active
                                    ? appYellow
                                    : white.withOpacity(active ? 1 : 0.1),
                                shape: BoxShape.circle,
                                border: Border.all(
                                    color: black, width: active ? 2 : 1)),
                            child: Text(
                              "$p",
                              textAlign: TextAlign.center,
                              style: textStyle(active, active ? 16 : 14,
                                  active ? black : white.withOpacity(.8)),
                            )),
                      );
                    }),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      //if (shareAsStory)
                      GestureDetector(
                        onTap: () {
                          pickImages(context,
                              requestType: RequestType.video,
                              singleMode: true, onPicked: (_) async {
                            print("maugost1 $_");

                            if (null == _) return;
                            final bm = BaseModel();
                            File file = File(_[0][PATH]);
                            String path = file.path;

                            File thumbnail =
                                await VideoCompress.getFileThumbnail(path);

                            bm.put(THUMBNAIL_PATH, thumbnail.path);
                            bm.put(VIDEO_PATH, path);
                            //bm.put(VIDEO_LENGTH, length);
                            bm.put(IS_VIDEO, true);
                            bm.put(OBJECT_ID, getRandomId());
                            bm.put(IMAGE_PATH, path);
                            //setState(() {});

                            if (shareAsStory) {
                              Navigator.pop(context);
                              storyController.add([bm]);
                              return;
                            }

                            pushAndResult(
                                context,
                                CamPreview(
                                  camContext: context,
                                  files: [bm],
                                ));

                            // final Trimmer _trimmer = Trimmer();
                            // await _trimmer.loadVideo(videoFile: file);
                            // pushAndResult(
                            //     context,
                            //     TrimmerView(
                            //       _trimmer,
                            //     ), result: (_) async {
                            //   if (null == _) return;
                            //   String path = _[0];
                            //   int length = _[1];
                            //
                            //   File thumbnail =
                            //       await VideoCompress.getFileThumbnail(path);
                            //
                            //   bm.put(THUMBNAIL_PATH, thumbnail.path);
                            //   bm.put(VIDEO_PATH, path);
                            //   bm.put(VIDEO_LENGTH, length);
                            //   bm.put(IS_VIDEO, true);
                            //   bm.put(OBJECT_ID, getRandomId());
                            //   bm.put(IMAGE_PATH, path);
                            //   //setState(() {});
                            //
                            //   if (shareAsStory) {
                            //     Navigator.pop(context);
                            //     storyController.add([bm]);
                            //     return;
                            //   }
                            //
                            //   pushAndResult(
                            //       context,
                            //       CamPreview(
                            //         camContext: context,
                            //         files: [bm],
                            //       ));
                            // });
                          });
                          // openGallery(context,
                          //     singleMode: true,
                          //     maxSelection: 1,
                          //     type: PickType.onlyVideo, onPicked: (_) async {
                          //   if (_.isEmpty) return;
                          //
                          //   List paths = [];
                          //
                          //   /*for(var b in _){
                          //
                          //   }*/
                          //
                          //   final bm = BaseModel();
                          //   File file = _[0].file;
                          //   final Trimmer _trimmer = Trimmer();
                          //   await _trimmer.loadVideo(videoFile: file);
                          //   pushAndResult(
                          //       context,
                          //       TrimmerView(
                          //         _trimmer,
                          //       ), result: (_) async {
                          //     if (null == _) return;
                          //     String path = _[0];
                          //     int length = _[1];
                          //
                          //     print("maugost $path");
                          //
                          //     File thumbnail =
                          //         await VideoCompress.getFileThumbnail(path);
                          //
                          //     bm.put(THUMBNAIL_PATH, thumbnail.path);
                          //     bm.put(VIDEO_PATH, path);
                          //     bm.put(VIDEO_PATH, path);
                          //     bm.put(VIDEO_LENGTH, length);
                          //     bm.put(IS_VIDEO, true);
                          //     bm.put(OBJECT_ID, getRandomId());
                          //     bm.put(IMAGE_PATH, path);
                          //     //setState(() {});
                          //
                          //     if (shareAsStory) {
                          //       Navigator.pop(context);
                          //       storyController.add([bm]);
                          //       return;
                          //     }
                          //
                          //     pushAndResult(
                          //         context,
                          //         CamPreview(
                          //           camContext: context,
                          //           files: [bm],
                          //         ));
                          //   });
                          // });
                        },
                        child: Container(
                          height: 40,
                          width: 40,
                          child: Stack(
                            children: [
                              Align(
                                alignment: Alignment.center,
                                child: Container(
                                  alignment: Alignment.center,
                                  // decoration: BoxDecoration(
                                  //     color: white,
                                  //     borderRadius: BorderRadius.circular(8)),
                                  child: Image.asset(
                                    'assets/icons/upload.png',
                                    color: white,
                                    height: 25,
                                    width: 25,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              /*   Align(
                                alignment: Alignment.bottomRight,
                                child: Container(
                                  height: 20,
                                  width: 20,
                                  child: Icon(
                                    Icons.add,
                                    color: white,
                                    size: 12,
                                  ),
                                  alignment: Alignment.center,
                                  padding: EdgeInsets.all(4),
                                  decoration: BoxDecoration(
                                      border: Border.all(color: white),
                                      color: blue3,
                                      shape: BoxShape.circle),
                                ),
                              )*/
                            ],
                          ),
                        ),
                      ),
                      Flexible(
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: List.generate(2, (p) {
                              String title = "Stories";
                              if (p == 1) title = 'Spark';
                              if (p == 2) title = 'Effects';
                              bool active = currentPagePost == p;
                              return GestureDetector(
                                onTap: () async {
                                  if (widget.storyMode) return;
                                  // vp.jumpToPage(p);
                                  currentPagePost = p;
                                  shareAsStory = p == 0;
                                  setState(() {});
                                },
                                child: Container(
                                    margin: EdgeInsets.all(5),
                                    padding: EdgeInsets.all(5),
                                    width: 80,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        color: active
                                            ? appYellow
                                            : white
                                                .withOpacity(active ? 1 : 0.1),
                                        borderRadius:
                                            BorderRadius.circular(25)),
                                    child: Text(
                                      title,
                                      style: textStyle(
                                          active,
                                          active ? 16 : 14,
                                          active
                                              ? black
                                              : white.withOpacity(.5)),
                                    )),
                              );
                            }),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          if (cameraDirection == CameraDirection.front) {
                            cameraDirection = CameraDirection.back;
                          } else {
                            cameraDirection = CameraDirection.front;
                          }

                          print(cameraDirection);
                          cameraDeepArController.switchCameraDirection(
                              direction: cameraDirection);
                          setState(() {});
                        },
                        child: Container(
                          height: 35,
                          width: 35,
                          alignment: Alignment.center,
                          child: Image.asset(
                            'assets/icons/switch_cam.png',
                            color: white,
                            height: 25,
                            width: 25,
                            fit: BoxFit.cover,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget get topItems {
    return Align(
      alignment: Alignment.topRight,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          GestureDetector(
            child: Container(
              margin: EdgeInsets.only(top: 0),
              padding: EdgeInsets.all(14),
              decoration: BoxDecoration(
                  color: red,
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(30),
                      bottomRight: Radius.circular(30))),
              child: Icon(
                Icons.clear,
                color: white,
                size: 18,
              ),
            ),
            onTap: () => Navigator.pop(context, ''),
          ),
          Container(
            height: 50,
            child: Center(
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: List.generate(3, (p) {
                    String title = "Masks";
                    if (p == 1) title = 'Effects';
                    if (p == 2) title = 'Filters';
                    bool active = currentPageMode == p;
                    return GestureDetector(
                      onTap: () async {
                        changeModes(p);
                      },
                      child: Container(
                          margin: EdgeInsets.all(5),
                          padding: EdgeInsets.all(5),
                          width: 70,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              color: active
                                  ? appYellow
                                  : white.withOpacity(active ? 1 : 0.1),
                              borderRadius: BorderRadius.circular(25)),
                          child: Text(
                            title,
                            style: textStyle(active, active ? 14 : 12,
                                active ? black : white.withOpacity(.5)),
                          )),
                    );
                  }),
                ),
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              if (cameraDeepArController == null) return;
              cameraDeepArController.switchCameraDirection(
                  direction: cameraDirection == CameraDirection.front
                      ? CameraDirection.back
                      : CameraDirection.front);
              setState(() {});
            },
            child: Container(
              height: 50,
              width: 50,
              margin: EdgeInsets.only(top: 0, right: 20),
              padding: EdgeInsets.all(14),
              alignment: Alignment.center,
              child: Image.asset(
                'assets/icons/switch_cam.png',
                color: white,
                height: 25,
                width: 25,
                fit: BoxFit.cover,
              ),
            ),
          )
        ],
      ),
    );
  }

  cameraButtonPositionUpdate(Offset v) {
    top = max(v.dy - (80) / 2, 0);
    zoomTo = ((getScreenHeight(context) - v.dy) * .5) / 100;
    zoomTo = double.parse(zoomTo.toStringAsFixed(2));
    zoomTo = zoomTo.isNegative
        ? 1
        : zoomTo > 11
            ? 10
            : zoomTo;
    print('zoom $zoomTo');
    //startZooming(zoomTo);
    left = max(v.dx - (80) / 2, 0);
    setState(() {});
  }

  // startZooming(double p) {
  //   if (p == 0) return;
  //   if (null != controller) controller.zoomIn();
  //   Future.delayed(Duration(milliseconds: 5), () {
  //     startZooming(p);
  //   });
  // }

  String timestamp() => DateTime.now().millisecondsSinceEpoch.toString();

  void onStopButtonPressed(bool cancel) async {
    isRecording = false;
    timerController.stop();
    timerController.reset();
    zoomTo = 1;
    if (null == cameraDeepArController) return;
    await cameraDeepArController.stopVideoRecording();
    if (mounted) setState(() {});
  }

  Future<void> startVideoRecording() async {
    if (null == cameraDeepArController) return;
    if (isRecording) return;
    //final bm = BaseModel();
    //bm.put(VIDEO_PATH, '');
    //videoPaths.add(bm);
    await cameraDeepArController.startVideoRecording();
    isRecording = true;
    timerController.forward();
    //isRecording = true;
    recordingController.repeat(reverse: true);
    setState(() {});
  }

  changeCameraModes(int p) {
    // if (cameraMode == CameraMode.masks) {
    //   cameraDeepArController.changeMask(p);
    // } else if (cameraMode == CameraMode.filters) {
    //   cameraDeepArController.changeFilter(p);
    // } else if (cameraMode == CameraMode.effects) {
    //   cameraDeepArController.changeEffect(p);
    // }

    if (currentPageMode == 0) {
      cameraDeepArController.changeMask(p);
    } else if (currentPageMode == 1) {
      cameraDeepArController.changeFilter(p);
    } else if (currentPageMode == 2) {
      cameraDeepArController.changeEffect(p);
    }

    currentPage = p;
    setState(() {});
  }

  changeModes(int p) {
    if (p == 0) cameraMode = CameraMode.masks;
    if (p == 1) cameraMode = CameraMode.effects;
    if (p == 2) cameraMode = CameraMode.filters;
    currentPageMode = p;
    setState(() {});
  }
}
