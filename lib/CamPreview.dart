import 'dart:async';
import 'dart:io';

import 'package:cached_video_player/cached_video_player.dart';
import 'package:flutter/material.dart';
import 'package:sparkbuz/AppConfig.dart';
import 'package:sparkbuz/basic/VideoListController.dart';
import 'package:video_compress/video_compress.dart';

import 'AppEngine.dart';
import 'CamPost.dart';
import 'Filterlayout.dart';
import 'MainAdmin.dart';
import 'assets.dart';
import 'basemodel.dart';

class CamPreview extends StatefulWidget {
  final bool image;
  final bool fromCamera;
  final bool isStory;
  final List<BaseModel> files;
  final BuildContext camContext;
  final int startAt;
  const CamPreview(
      {Key key,
      this.files,
      this.fromCamera = false,
      this.image = false,
      this.camContext,
      this.isStory = false,
      this.startAt = 0})
      : super(key: key);
  @override
  _CamPreviewState createState() => _CamPreviewState();
}

class _CamPreviewState extends State<CamPreview> {
  List<File> files = [];
  CachedVideoPlayerController videoPlayerController;
  VideoListController videoListController = VideoListController();
  PageController vp = PageController();
  int currentPage = 0;
  CachedVideoPlayerController currentPlayer;
  List<BaseModel> previewList = [];
  bool controllerReady = false;
  List<Player> playList = [];
  StreamSubscription subs;
  bool playerReady = false;

  @override
  void initState() {
    super.initState();
    vp = PageController(initialPage: widget.startAt);
    currentPage = widget.startAt;
    previewList = widget.files;
    files = widget.files.map((e) => File(e.getString(VIDEO_PATH))).toList();

    for (var bm in previewList) {
      CachedVideoPlayerController controller;
      File file = File(bm.getString(VIDEO_PATH));
      controller = CachedVideoPlayerController.file(file)
        ..initialize().then((value) {
          controller.play();
          controller.setLooping(true);
          final player = Player(bm, controller);
          playerController.add(player);
          if (mounted) setState(() {});
        });
    }

/*    videoListController = VideoListController()
      ..init(vp, previewList, () {
        playerReady = true;
      },type: VideoListType.file);*/

    subs = playerController.stream.listen((player) {
      playList.add(player);
      setState(() {});
    });
  }

  @override
  void dispose() {
    super.dispose();
    subs?.cancel();
    for (var p in playList) p.controller?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: black,
      body: SafeArea(
        child: Column(
          children: [
            Expanded(
                child: Stack(
              children: [
                PageView.builder(
                    controller: vp,
                    onPageChanged: (p) {
                      currentPage = p;
                      currentPlayer.pause();
                      currentPlayer = videoListController.playerOfIndex(p);
                      currentPlayer.play();
                      setState(() {});
                    },
                    itemCount: playList.length,
                    itemBuilder: (c, p) {
                      final player = playList[p];
                      final controller = player.controller;

                      if (!player.controller.value.initialized)
                        return videoLoading;

                      return Stack(
                        //fit: StackFit.expand,
                        alignment: Alignment.center,
                        children: [
                          AspectRatio(
                              aspectRatio: controller.value.aspectRatio,
                              child: CachedVideoPlayer(controller)),
                          // Tapped(
                          //   showIcon: videoListController
                          //       .currentPlayer.value.isPlaying,
                          //   child: Container(
                          //     color: transparent,
                          //     alignment: Alignment.center,
                          //     child: AnimatedContainer(
                          //         duration: Duration(milliseconds: 400),
                          //         height: 50,
                          //         width: 50,
                          //         decoration: BoxDecoration(
                          //             border:
                          //                 Border.all(color: white, width: 1),
                          //             color: black.withOpacity(.5),
                          //             shape: BoxShape.circle),
                          //         child: Icon(
                          //           isPlaying ? Icons.pause : Icons.play_arrow,
                          //           color: white,
                          //           size: 20,
                          //         )),
                          //   ),
                          //   onTap: () {
                          //     if (isPlaying)
                          //       videoListController.currentPlayer.pause();
                          //     else
                          //       videoListController.currentPlayer.play();
                          //     setState(() {});
                          //   },
                          // ),
                          FilterLayout(
                            //currentPlayer: controller,
                            filterCallback: (filter) {
                              previewList[currentPage].put(
                                  FILTERS, filter.map((e) => e.items).toList());
                              setState(() {});
                            },
                          ),
                        ],
                      );
                    }),
                closeButton,
                johnsTabs()
              ],
            )),
            Container(
              padding: const EdgeInsets.all(8.0),
              child: FlatButton(
                  onPressed: () async {
                    bool isStory = widget.isStory;
                    BaseModel bm = previewList[0]; //BaseModel();
                    //bm..put(VIDEO_PATH, previewList[0].path);//..put(key, value);
                    if (!isStory) {
                      String thumbnail =
                          (await VideoCompress.getFileThumbnail(files[0].path))
                              .path;
                      bm..put(THUMBNAIL_PATH, thumbnail);
                    }
                    bm..put(IS_STORY, widget.isStory);
                    bm..put(OBJECT_ID, getRandomId());
                    pushReplacementAndResult(
                        context,
                        CamPost(
                          camModel: bm,
                          contexts: [
                            widget.camContext,
                          ],
                        ));
                  },
                  color: AppConfig.appColor,
                  padding: EdgeInsets.all(16),
                  child: Center(
                      child: Text(
                    "Next",
                    style: textStyle(true, 16, white),
                  ))),
            )
          ],
        ),
      ),
    );
  }

  Widget get closeButton => GestureDetector(
        child: Container(
          margin: EdgeInsets.only(top: 25),
          padding: EdgeInsets.all(14),
          decoration: BoxDecoration(
              color: red,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(30),
                  bottomRight: Radius.circular(30))),
          child: Icon(
            Icons.clear,
            color: white,
            size: 18,
          ),
        ),
        onTap: Navigator.of(context).pop,
      );

  Widget cameraIconButton(
      {@required IconData icon,
      bool isActive = false,
      @required String title,
      @required VoidCallback onTap}) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
        color: isActive ? blue3 : transparent,
        child: DefaultTextStyle(
          style: TextStyle(shadows: [
            Shadow(
              color: Colors.black.withOpacity(0.15),
              offset: Offset(0, 1),
              blurRadius: 1,
            ),
          ]),
          child: Column(
            children: <Widget>[
              Icon(
                icon,
                color: white,
              ),
              Text(
                title,
                style: textStyle(false, 12, white),
              ),
            ],
          ),
        ),
      ),
    );
  }

  johnsTabs() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        height: 40,
        decoration: BoxDecoration(
          color: black.withOpacity(.4),
          //borderRadius: BorderRadius.circular(25)
        ),
        padding: EdgeInsets.all(4),
        alignment: Alignment.center,
        child: Row(
          children: List.generate(previewList.length, (p) {
            bool selected = currentPage == p;
            return Flexible(
              fit: FlexFit.tight,
              child: GestureDetector(
                onTap: () {
                  vp.animateToPage(p,
                      duration: Duration(milliseconds: 1000),
                      curve: Curves.ease);
                },
                child: Container(
                  height: 30,
                  child: Stack(
                    children: [
                      Center(
                        child: Container(
                          margin: EdgeInsets.fromLTRB(
                              p == 0
                                  ? (getScreenWidth(context) /
                                      (previewList.length * 2))
                                  : 0,
                              0,
                              p == previewList.length - 1
                                  ? (getScreenWidth(context) /
                                      (previewList.length * 2))
                                  : 0,
                              0),
                          height: 5,
                          color: white.withOpacity(.3),
                          width: double.infinity,
                        ),
                      ),
                      Center(
                        child: Container(
                          width: selected ? 30 : 25,
                          height: selected ? 30 : 25,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: white,
                              border: Border.all(
                                  color: black.withOpacity(.1), width: 1)),
                          alignment: Alignment.center,
                          child: Text(
                            '${p + 1}',
                            style: textStyle(selected, 13,
                                black.withOpacity(selected ? 1 : 0.5)),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          }),
        ),
      ),
    );
  }
}
