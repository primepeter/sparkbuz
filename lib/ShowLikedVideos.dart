import 'dart:io';
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'AppEngine.dart';
import 'MainAdmin.dart';
import 'assets.dart';
import 'basemodel.dart';
import 'main_pages/ShowVideoPost.dart';

class ShowLikedVideos extends StatefulWidget {
  @override
  _ShowLikedVideosState createState() => _ShowLikedVideosState();
}

class _ShowLikedVideosState extends State<ShowLikedVideos>
    with TickerProviderStateMixin {
  bool canRefresh = true;
  bool setup = false;
  RefreshController refreshController = RefreshController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(seconds: 1), () {
      loadItems(false);
    });
  }

  List<BaseModel> items = List();

  loadItems(bool isNew, {bool fromLocal = false}) async {
    print(userModel.getUserId());
    FirebaseFirestore.instance
        .collection(POSTS_BASE)
        .where(LIKES_COUNT, arrayContains: userModel.getUserId())
        .limit(25)
        .orderBy(TIME, descending: !isNew)
        .startAt([
          !isNew
              ? (items.isEmpty
                  ? DateTime.now().millisecondsSinceEpoch
                  : items[items.length - 1].getTime())
              : (items.isEmpty ? 0 : items[0].getTime())
        ])
        .get()
        .then((query) {
          for (var doc in query.docs) {
            BaseModel model = BaseModel(doc: doc);
            //if (model.hidden.contains(userModel.getUserId())) continue;

            int p =
                items.indexWhere((e) => e.getObjectId() == model.getObjectId());
            if (p == -1)
              items.add(model);
            else
              items[p] = model;
          }

          if (isNew) {
            refreshController.refreshCompleted();
          } else {
            int oldLength = items.length;
            int newLength = query.docs.length;
            if (newLength <= oldLength) {
              refreshController.loadNoData();
              canRefresh = false;
            } else {
              refreshController.loadComplete();
            }
          }
          setup = true;
          if (mounted) setState(() {});
        })
        .catchError((e) {
          if (e.toString().contains("CACHE")) {
            loadItems(isNew, fromLocal: false);
          }
        });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.pop(context, "");
        return false;
      },
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: transparent,
        body: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                child: Container(
                  color: black.withOpacity(.8),
                )),
            page()
          ],
        ),
      ),
    );
  }

  BuildContext con;

  Builder page() {
    return Builder(builder: (context) {
      this.con = context;
      return new Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          addSpace(30),
          new Container(
            width: double.infinity,
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                InkWell(
                    onTap: () {
                      Navigator.of(context).pop("");
                    },
                    child: Container(
                      width: 50,
                      height: 50,
                      child: Center(
                          child: Icon(
                        Icons.keyboard_backspace,
                        color: white,
                        size: 25,
                      )),
                    )),
                Flexible(
                  fit: FlexFit.tight,
                  flex: 1,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text(
                        'Liked Videos',
                        style: textStyle(true, 22, white),
                      ),
                    ],
                  ),
                ),
                addSpaceWidth(15),
              ],
            ),
          ),
//          addLine(1, black.withOpacity(.1), 0, 0, 0, 0),
          new Expanded(
            flex: 1,
            child: !setup
                ? loadingLayout(trans: true)
                : setup && items.isEmpty
                    ? emptyLayout(
                        Icons.video_collection_sharp, 'No Videos Liked', "",
                        trans: true)
                    : Container(
//                        color: white,
                        child: SmartRefresher(
                        controller: refreshController,
                        enablePullDown: false,
                        enablePullUp: true,
                        header: Platform.isIOS
                            ? WaterDropHeader()
                            : WaterDropMaterialHeader(),
                        footer: ClassicFooter(
                          idleText: "",
                          idleIcon:
                              Icon(Icons.arrow_drop_down, color: transparent),
                        ),
                        onLoading: () {
                          loadItems(false);
                        },
                        onOffsetChange: (_, d) {},
                        child: new GridView.builder(
                            //controller: scrollController,
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                                    childAspectRatio: .8,
                                    crossAxisCount: 3,
                                    mainAxisSpacing: 5,
                                    crossAxisSpacing: 5),
                            padding: EdgeInsets.only(right: 5, left: 5, top: 10
                                //top: getScreenHeight(context) * (hideProfile ? 0.23 : 0.38)
                                ),
                            itemCount: items.length,
                            itemBuilder: (ctx, p) {
                              String imageUrl = "https://bit.ly/3he5JOD";
                              if (p.isEven) imageUrl = "https://bit.ly/3h8s5RE";

                              BaseModel md = items[p];
                              String image = md.getString(THUMBNAIL_URL);
                              int seenBy = md.getList(SEEN_BY).length;

                              return GestureDetector(
                                onTap: () {
                                  pushAndResult(
                                    context,
                                    ShowVideoPost(
                                      videoModel: md,
                                    ),
                                  );
                                },
                                onLongPress: () {
                                  if (md.myItem()) {
                                    yesNoDialog(context, "Delete Video",
                                        'Are you sure you want to delete this video?',
                                        () {
                                      int p = playList.indexWhere((e) =>
                                          e.model.getObjectId() ==
                                          md.getObjectId());
                                      if (p != -1)
                                        deleteVideoController.add(md);
                                      items.remove(md);
                                      print(
                                          "${md.getObjectId()} index $p ${playList.length}");
                                      md.deleteItem();
                                      setState(() {});
                                    });
                                  }
                                },
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(15),
                                  child: Container(
                                    child: Stack(
                                      children: [
                                        CachedNetworkImage(
                                          imageUrl: image,
                                          height: double.infinity,
                                          width: double.infinity,
                                          fit: BoxFit.cover,
                                          placeholder: (c, s) {
                                            return placeHolder(double.infinity,
                                                width: double.infinity);
                                          },
                                        ),
                                        Container(
                                          width: double.infinity,
                                          height: double.infinity,
                                          color: black.withOpacity(.09),
                                          child: Center(
                                            child: Container(
                                              height: 40,
                                              width: 40,
                                              child: Icon(
                                                Icons.play_arrow,
                                                color: Colors.white,
                                                size: 18,
                                              ),
                                              decoration: BoxDecoration(
                                                  color: black.withOpacity(0.8),
                                                  border: Border.all(
                                                      color: Colors.white,
                                                      width: 1.5),
                                                  shape: BoxShape.circle),
                                            ),
                                          ),
                                        ),
                                        Align(
                                          alignment: Alignment.bottomCenter,
                                          child: Container(
                                            decoration: BoxDecoration(
                                                color: black.withOpacity(.3),
                                                borderRadius:
                                                    BorderRadius.circular(5)),
                                            padding: EdgeInsets.all(3),
                                            margin: EdgeInsets.all(5),
                                            child: Row(
                                                mainAxisSize: MainAxisSize.min,
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: [
                                                  Icon(
                                                    LineIcons.eye,
                                                    color: white,
                                                    size: 15,
                                                  ),
                                                  addSpaceWidth(5),
                                                  Text(
                                                    seenBy.toString(),
                                                    style: textStyle(
                                                        false, 14, white),
                                                  ),
                                                ]),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            }),
                      )),
          ),
        ],
      );
    });
  }
}
