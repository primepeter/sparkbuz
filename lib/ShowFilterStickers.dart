import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';

import 'AddSticker.dart';
import 'AppConfig.dart';
import 'AppEngine.dart';
import 'assets.dart';
import 'basemodel.dart';

class ShowFilterStickers extends StatefulWidget {
  final bool popResult;
  final bool showSub;

  const ShowFilterStickers(
      {Key key, this.popResult = true, this.showSub = false})
      : super(key: key);
  @override
  _ShowFilterStickersState createState() => _ShowFilterStickersState();
}

class _ShowFilterStickersState extends State<ShowFilterStickers> {
  final searchController = TextEditingController();
  bool showCancel = false;
  bool searching = false;
  List<BaseModel> result = appSettingsModel.getListModel(STICKERS);
  List<BaseModel> appCategories = appSettingsModel.getListModel(STICKERS);

  @override
  initState() {
    super.initState();
    searchController.addListener(listener);
    result.sort((a, b) => a.getString(TITLE).compareTo(b.getString(TITLE)));
  }

  listener() async {
    String text = searchController.text.trim().toLowerCase();
    if (text.isEmpty) {
      result = appCategories;
      showCancel = false;
      searching = false;
      result.sort((a, b) => a.getString(TITLE).compareTo(b.getString(TITLE)));
      if (mounted) setState(() {});
      return;
    }
    showCancel = true;
    searching = true;
    if (mounted) setState(() {});

    result = appCategories
        .where((b) => b.getString(TITLE).toLowerCase().startsWith(text))
        .toList();

    searching = false;
    if (mounted) setState(() {});
  }

  @override
  dispose() {
    super.dispose();
    searchController?.removeListener(listener);
    searchController?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: widget.popResult ? transparent : white,
      backgroundColor: black.withOpacity(.5),
      body: page(),
    );
  }

  page() {
    return Stack(
      children: [
        BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
            child: Container(
              color: black.withOpacity(.6),
            )),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              padding: EdgeInsets.only(top: 30, right: 10, left: 10, bottom: 0),
              child: Row(
                children: [
                  BackButton(
                    color: white,
                  ),
                  Text(
                    "Choose Sticker",
                    style: textStyle(true, 16, white),
                  ),
                  Spacer(),
                  //if (isAdmin)
                  RaisedButton(
                    color: AppConfig.appYellow,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25)),
                    child: Text(
                      "Add More",
                      style: textStyle(true, 14, black),
                    ),
                    onPressed: () {
                      pushAndResult(context, AddSticker(), result: (_) {
                        result = appSettingsModel.getListModel(STICKERS);
                        setState(() {});
                      });
                    },
                  ),
                  addSpaceWidth(5),
                ],
              ),
            ),
            addSpace(5),
            Container(
              margin: EdgeInsets.only(left: 15, right: 15, bottom: 10),
              child: Container(
                  padding: EdgeInsets.only(left: 15, right: 15),
                  decoration: BoxDecoration(
                      color: white.withOpacity(.04),
                      border: Border.all(
                        color: white.withOpacity(.09),
                      ),
                      borderRadius: BorderRadius.circular(10)),
                  child: Row(
                    children: [
                      Icon(
                        Icons.search,
                        color: white.withOpacity(.4),
                        size: 20,
                      ),
                      addSpaceWidth(5),
                      Expanded(
                        child: TextField(
                          controller: searchController,
                          cursorColor: white,
                          decoration: InputDecoration(
                              hintText: "Search in Stickers ",
                              hintStyle:
                                  textStyle(false, 14, white.withOpacity(.5)),
                              border: InputBorder.none),
                        ),
                      ),
                      if (showCancel)
                        GestureDetector(
                          onTap: () {
                            searchController.clear();
                            result = appCategories;
                            showCancel = false;
                            searching = false;
                            result.sort((a, b) => a
                                .getString(TITLE)
                                .compareTo(b.getString(TITLE)));

                            setState(() {});
                          },
                          child: Icon(
                            LineIcons.close,
                            color: black.withOpacity(.5),
                          ),
                        ),
                    ],
                  )),
            ),
            AnimatedContainer(
              duration: Duration(milliseconds: 400),
              child: LinearProgressIndicator(
                valueColor: AlwaysStoppedAnimation(orange03),
              ),
              height: searching ? 2 : 0,
              margin: EdgeInsets.only(bottom: searching ? 5 : 0),
            ),
            Expanded(
              child: GridView.builder(
                padding: EdgeInsets.all(0),
                itemCount: result.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 4,
                  //childAspectRatio: 0.9,
                ),
                itemBuilder: (c, p) {
                  return resultItem(p);
                },
              ),
            )
          ],
        ),
      ],
    );
  }

  resultItem(int index) {
    BaseModel model = result[index];
    String categoryName = model.getString(TITLE);
    String image = getFirstPhoto(model.images);
    return InkWell(
      onTap: () {
        if (widget.popResult) Navigator.pop(context, image);
      },
      onLongPress: () {
        showListDialog(context, ['Edit', 'Delete'], (_) {
          if (_ == 0) {
            pushAndResult(
                context,
                AddSticker(
                  model: model,
                ), result: (_) {
              appCategories = appSettingsModel.getListModel(STICKERS);
              setState(() {});
            });
          }

          if (_ == 1) {
            result.removeAt(index);
            appSettingsModel
              ..put(STICKERS, result.map((e) => e.items).toList())
              ..updateItems();
            setState(() {});
          }
          return;
        });
      },
      child: Container(
        padding: EdgeInsets.all(5),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: Container(
            height: 80,
            width: 80,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                border: Border.all(color: black.withOpacity(.1))),
            child: CachedNetworkImage(
              imageUrl: image,
              height: 80,
              width: 80,
              fit: BoxFit.cover,
              placeholder: (c, s) {
                return Container(
                  height: 80,
                  width: 80,
                  color: black.withOpacity(.09),
                  child: Icon(LineIcons.image),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
