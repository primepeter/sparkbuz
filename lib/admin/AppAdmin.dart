import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:sparkbuz/AppEngine.dart';
import 'package:sparkbuz/ShowFilterStickers.dart';
import 'package:sparkbuz/admin/ShowAdsAdmin.dart';
import 'package:sparkbuz/assets.dart';
import 'package:sparkbuz/basemodel.dart';
import 'package:sparkbuz/dialogs/fieldDialog.dart';
import 'package:sparkbuz/dialogs/inputDialog.dart';

import '../NewSparkbuzzers.dart';
import '../Reports.dart';
import 'ShowUsers.dart';
import 'ShowVideos.dart';

class AppAdmin extends StatefulWidget {
  @override
  _AppAdminState createState() => _AppAdminState();
}

class _AppAdminState extends State<AppAdmin> {
  List<BaseModel> postList = [];
  List<BaseModel> usersList = [];
  List<BaseModel> adsList = [];
  bool setup = false;
  List<StreamSubscription> subs = [];
  BaseModel mostLiked;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadUsers();
    loadAds();
    loadMostLiked();
  }

  @override
  void dispose() {
    for (var s in subs) s?.cancel();
    super.dispose();
  }

  loadUsers() async {
    var sub = FirebaseFirestore.instance
        .collection(USER_BASE)
        .snapshots()
        .listen((value) {
      for (var doc in value.docs) {
        BaseModel model = BaseModel(doc: doc);
        int p = usersList.indexWhere(
            (element) => element.getObjectId() == model.getObjectId());
        if (p != -1) {
          usersList[p] = model;
        } else {
          usersList.add(model);
        }
      }
      setup = true;
      if (mounted) setState(() {});
    });
    subs.add(sub);
  }

  loadAds() async {
    var sub = FirebaseFirestore.instance
        .collection(ADS_BASE)
        .snapshots()
        .listen((value) {
      for (var doc in value.docs) {
        BaseModel model = BaseModel(doc: doc);
        int p = adsList.indexWhere(
            (element) => element.getObjectId() == model.getObjectId());
        if (p != -1) {
          adsList[p] = model;
        } else {
          adsList.add(model);
        }
      }
      setup = true;
      if (mounted) setState(() {});
    });
    subs.add(sub);
  }

  loadMostLiked() async {
    FirebaseFirestore.instance.collection(POSTS_BASE).get().then((value) {
      for (var doc in value.docs) {
        BaseModel model = BaseModel(doc: doc);
        int p = postList.indexWhere(
            (element) => element.getObjectId() == model.getObjectId());
        if (p != -1) {
          postList[p] = model;
        } else {
          postList.add(model);
        }
        //addBirthDate(model.getUserId(), model);
      }
      postList.shuffle();
      postList
          .sort((a, b) => b.likesCount.length.compareTo(a.likesCount.length));
      mostLiked = postList.first;

      if (mounted) setState(() {});
    });
    // subs.add(sub);
  }

  void addBirthDate(String id, BaseModel model) async {
    await Future.delayed(Duration(seconds: 2));
    FirebaseFirestore.instance
        .collection(USER_BASE)
        .doc(id)
        .get()
        .then((value) {
      BaseModel user = BaseModel(doc: value);
      String birth = user.birthDate;
      model.put(BIRTH_DATE, birth);
      model.updateItems();
      print("Birthday added $birth age ${getAge(DateTime.parse(birth))}");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.fromLTRB(0, 40, 0, 10),
            color: white,
            child: Row(
              children: <Widget>[
                InkWell(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Container(
                      width: 50,
                      height: 50,
                      child: Center(
                          child: Icon(
                        Icons.keyboard_backspace,
                        color: black,
                        size: 25,
                      )),
                    )),
                Text(
                  "Admin Portal",
                  style: textStyle(true, 25, black),
                ),
                Spacer(),
              ],
            ),
          ),
          page()
        ],
      ),
    );
  }

  page() {
    return Flexible(
      child: ListView(
        padding: EdgeInsets.all(0),
        children: [
          usersStatistics(),
          videoStatistics(),
          //adsStatistics(),
          ListTile(
            onTap: () {
              pushAndResult(
                  context,
                  ShowFilterStickers(
                    popResult: false,
                    showSub: true,
                  ));
            },
            title: Text("App Stickers"),
            subtitle: Text("Add and Update App Stickers"),
            trailing: Icon(Icons.navigate_next),
          ),
          // ListTile(
          //   onTap: () {
          //     pushAndResult(
          //         context,
          //         inputDialog(
          //           "Rave Public Key",
          //           hint: "Enter Rave Public Key",
          //           message: appSettingsModel.getString(PUBLIC_KEY),
          //         ), result: (_) {
          //       if (null == _) return;
          //       appSettingsModel
          //         ..put(PUBLIC_KEY, _)
          //         ..updateItems();
          //       setState(() {});
          //     }, depend: false);
          //   },
          //   title: Text("Rave Public Key"),
          //   subtitle: Text("Set and Update Rave Public Here"),
          //   trailing: Icon(Icons.navigate_next),
          // ),
          // ListTile(
          //   onTap: () {
          //     pushAndResult(
          //         context,
          //         inputDialog(
          //           "Rave Encryption Key",
          //           hint: "Enter Rave Encryption Key",
          //           message: appSettingsModel.getString(ENCRYPTION_KEY),
          //         ), result: (_) {
          //       if (null == _) return;
          //       appSettingsModel
          //         ..put(ENCRYPTION_KEY, _)
          //         ..updateItems();
          //       setState(() {});
          //     }, depend: false);
          //   },
          //   title: Text("Rave Encryption Key"),
          //   subtitle: Text("Set and Update Rave Encryption Here"),
          //   trailing: Icon(Icons.navigate_next),
          // ),
          // ListTile(
          //   onTap: () {
          //     pushAndResult(context, listDialog(["True", "False"]),
          //         result: (_) {
          //       if (null == _) return;
          //       appSettingsModel
          //         ..put(RAVE_LIVE, _ == "True")
          //         ..updateItems();
          //       setState(() {});
          //     }, depend: false);
          //   },
          //   title: Text(
          //       "Rave in Production Mode ${appSettingsModel.getBoolean(RAVE_LIVE)}"),
          //   subtitle: Text("Set Rave Production Mode"),
          //   trailing: Icon(Icons.navigate_next),
          // ),

          ListTile(
            onTap: () {
              pushAndResult(context, Reports());
            },
            title: Text("Show Reports"),
            subtitle: Text("View reports on profiles and videos here."),
            trailing: Icon(Icons.navigate_next),
          ),

          ListTile(
            onTap: () {
              pushAndResult(context, NewSparkbuzzers());
            },
            title: Text("Create SparkBuzzers"),
            subtitle: Text("Create SparkBuzzers here"),
            trailing: Icon(Icons.navigate_next),
          ),
          ListTile(
            onTap: () {
              pushAndResult(
                  context,
                  fieldDialog(
                    "Report Categories",
                    hint: "Categories",
                    items: appSettingsModel.getList(APP_REPORT_CATEGORIES),
                  ), result: (_) {
                if (null == _) return;
                appSettingsModel
                  ..put(APP_REPORT_CATEGORIES, _)
                  ..updateItems();
                setState(() {});
              }, depend: false);
            },
            title: Text("Report Category"),
            subtitle: Text("Update Report Categories here"),
            trailing: Icon(Icons.navigate_next),
          ),
          ListTile(
            onTap: () {
              pushAndResult(
                  context,
                  inputDialog(
                    "About Us",
                    hint: "Enter About Us url link",
                    message: appSettingsModel.getString(ABOUT_LINK),
                  ), result: (_) {
                if (null == _) return;
                appSettingsModel
                  ..put(ABOUT_LINK, _)
                  ..updateItems();
                setState(() {});
              }, depend: false);
            },
            title: Text("About Us"),
            subtitle: Text("Update About the app here"),
            trailing: Icon(Icons.navigate_next),
          ),
          ListTile(
            onTap: () {
              pushAndResult(
                  context,
                  inputDialog(
                    "Privacy Policy",
                    hint: "Enter Privacy Policy url link",
                    message: appSettingsModel.getString(PRIVACY_LINK),
                  ), result: (_) {
                if (null == _) return;
                appSettingsModel
                  ..put(PRIVACY_LINK, _)
                  ..updateItems();
                setState(() {});
              }, depend: false);
            },
            title: Text("Privacy Policy"),
            subtitle: Text("Update Privacy Policy of the app here"),
            trailing: Icon(Icons.navigate_next),
          ),
          ListTile(
            onTap: () {
              pushAndResult(
                  context,
                  inputDialog(
                    "Terms of Service",
                    hint: "Enter Terms of Service url link",
                    message: appSettingsModel.getString(TERMS_LINK),
                  ), result: (_) {
                if (null == _) return;
                appSettingsModel
                  ..put(TERMS_LINK, _)
                  ..updateItems();
                setState(() {});
              }, depend: false);
            },
            title: Text("Terms of Service"),
            subtitle: Text("Update Terms of Service of the app here"),
            trailing: Icon(Icons.navigate_next),
          ),
          ListTile(
            onTap: () {
              pushAndResult(
                  context,
                  inputDialog(
                    "Support Email",
                    hint: "Enter Support Email",
                    message: appSettingsModel.getString(SUPPORT_EMAIL),
                  ), result: (_) {
                if (null == _) return;
                appSettingsModel
                  ..put(SUPPORT_EMAIL, _)
                  ..updateItems();
                setState(() {});
              }, depend: false);
            },
            title: Text("Support Email"),
            subtitle: Text("Set Support Email Here"),
            trailing: Icon(Icons.navigate_next),
          ),
          ListTile(
            onTap: () {
              // pushAndResult(context, NewUpdate());
            },
            title: Text("Release Update"),
//            subtitle: Text(""),
            trailing: Icon(Icons.navigate_next),
          ),
        ],
      ),
    );
  }

  usersStatistics() {
    int total = usersList.length;
    final regular =
        usersList.where((e) => e.getInt(ACCOUNT_TYPE) == 0).toList();
    final premium =
        usersList.where((e) => e.getInt(ACCOUNT_TYPE) == 1).toList();
    final reports =
        usersList.where((e) => e.getInt(ACCOUNT_TYPE) == 1).toList();
    return Container(
      decoration: BoxDecoration(
          color: black.withOpacity(0.09),
          borderRadius: BorderRadius.circular(10)),
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Users Statistics"),
          addSpace(10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: List.generate(4, (p) {
              String title;
              int count;
              if (p == 0) {
                title = "Total";
                count = total;
              }
              if (p == 1) {
                title = "Regular";
                count = regular.length;
              }
              if (p == 2) {
                title = "Premium";
                count = premium.length;
              }
              if (p == 3) {
                title = "Reports";
                count = 0;
              }

              return Flexible(
                child: GestureDetector(
                  onTap: () {
                    if (p == 3) return;
                    print("okkk");
                    pushAndResult(
                        context,
                        ShowUsers(
                          users: p == 0
                              ? usersList
                              : p == 1
                                  ? regular
                                  : premium,
                          title: title + " Users",
                        ),
                        depend: false);
                  },
                  child: Container(
                    width: getScreenWidth(context) / 4,
                    color: transparent,
                    child: Column(
                      children: [
                        Text(
                          "${count}",
                          textAlign: TextAlign.center,
                          style: textStyle(true, 22, black),
                        ),
                        Text(
                          "$title",
                          textAlign: TextAlign.center,
                          style: textStyle(true, 14, black.withOpacity(.5)),
                        )
                      ],
                    ),
                  ),
                ),
              );
            }),
          ),
        ],
      ),
    );
  }

  usersRevenue() {
    int total = adsList.length;
    final active = adsList.where((e) => e.getType() == 0).toList();
    final inActive = adsList.where((e) => e.getType() == 1).toList();
    final pending = adsList.where((e) => e.getType() == 2).toList();
    return Container(
      decoration: BoxDecoration(
          color: black.withOpacity(0.09),
          borderRadius: BorderRadius.circular(10)),
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Users Revenue Statistics \$"),
          addSpace(10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: List.generate(4, (p) {
              String title;
              int count;
              if (p == 0) {
                title = "Total";
                count = total;
              }
              if (p == 1) {
                title = "Regular";
                count = active.length;
              }
              if (p == 2) {
                title = "Premium";
                count = inActive.length;
              }
              if (p == 3) {
                title = "Pending";
                count = pending.length;
              }

              return Flexible(
                child: GestureDetector(
                  onTap: () {
                    print("okkk");
                    pushAndResult(
                        context,
                        ShowUsers(
                          users: usersList,
                        ),
                        depend: false);
                  },
                  child: Container(
                    width: getScreenWidth(context) / 3,
                    color: transparent,
                    child: Column(
                      children: [
                        Text(
                          "${count.toDouble()}",
                          textAlign: TextAlign.center,
                          style: textStyle(true, 22, black),
                        ),
                        Text(
                          "$title",
                          textAlign: TextAlign.center,
                          style: textStyle(true, 14, black.withOpacity(.5)),
                        )
                      ],
                    ),
                  ),
                ),
              );
            }),
          ),
        ],
      ),
    );
  }

  adsStatistics() {
    int total = adsList.length;
    final active = adsList.where((e) => e.getInt(STATUS) == APPROVED).toList();
    final inActive =
        adsList.where((e) => e.getInt(STATUS) == INACTIVE).toList();
    final pending = adsList.where((e) => e.getInt(STATUS) == PENDING).toList();
    return Container(
      decoration: BoxDecoration(
          color: black.withOpacity(0.09),
          borderRadius: BorderRadius.circular(10)),
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Ads Statistics"),
          addSpace(10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: List.generate(4, (p) {
              String title;
              int count;
              List<BaseModel> list = [];
              if (p == 0) {
                title = "Total";
                count = total;
                list = adsList;
              }
              if (p == 1) {
                title = "Active";
                count = active.length;
                list = active;
              }
              if (p == 2) {
                title = "InActive";
                count = inActive.length;
                list = inActive;
              }
              if (p == 3) {
                title = "Pending";
                count = pending.length;
                list = pending;
              }

              return Flexible(
                child: GestureDetector(
                  onTap: () {
                    print("okkk");

                    if (list.isEmpty) return;

                    pushAndResult(
                        context,
                        ShowAdsAdmin(
                          models: list,
                        ));
                  },
                  child: Container(
                    width: getScreenWidth(context) / 3,
                    color: transparent,
                    child: Column(
                      children: [
                        Text(
                          "$count",
                          textAlign: TextAlign.center,
                          style: textStyle(true, 22, black),
                        ),
                        Text(
                          "$title",
                          textAlign: TextAlign.center,
                          style: textStyle(true, 14, black.withOpacity(.5)),
                        )
                      ],
                    ),
                  ),
                ),
              );
            }),
          ),
        ],
      ),
    );
  }

  videoStatistics() {
    int total = postList.length;
    BaseModel model = mostLiked;
    if (mostLiked == null) return Container();
    String likesCount = model.likesCount.length.toString();
    int age = 0;

    if (model.birthDate.isNotEmpty) {
      age = getAge(DateTime.parse(model.birthDate));
    }

    print(model.items);
    //print(postList.map((e) => e.likesCount).toList());

    return GestureDetector(
      onTap: () {
        pushAndResult(
            context,
            ShowVideos(
              items: postList,
            ));
      },
      child: Container(
        decoration: BoxDecoration(
            color: black.withOpacity(0.09),
            borderRadius: BorderRadius.circular(10)),
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Video Statistics"),
            addSpace(10),
            Row(
              children: [
                Column(
                  children: [
                    Text(
                      likesCount,
                      textAlign: TextAlign.center,
                      style: textStyle(true, 18, black),
                    ),
                    Text(
                      "Video Count",
                      textAlign: TextAlign.center,
                      style: textStyle(true, 12, black.withOpacity(.5)),
                    ),
                  ],
                ),
                addSpaceWidth(10),
                Expanded(
                  child: GestureDetector(
                    child: Container(
                      decoration: BoxDecoration(
                          color: white,
                          boxShadow: [
                            //BoxShadow(color: black.withOpacity(.3), blurRadius: 5)
                          ],
                          borderRadius: BorderRadius.circular(15)),
                      padding: EdgeInsets.all(5),
                      //margin: EdgeInsets.fromLTRB(0, .5, 0, 0),
                      child: Row(
                        children: [
                          Container(
                            margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                            width: 40,
                            height: 40,
                            child: Card(
                              color: black.withOpacity(.1),
                              elevation: 0,
                              clipBehavior: Clip.antiAlias,
                              shape: CircleBorder(
                                  side: BorderSide(
                                      color: black.withOpacity(.2), width: .9)),
                              child: CachedNetworkImage(
                                imageUrl: model.userImage,
                                fit: BoxFit.cover,
                                placeholder: (c, s) {
                                  return Container(
                                    width: 40,
                                    height: 40,
                                    child: Center(
                                      child: Icon(Icons.account_circle),
                                    ),
                                  );
                                },
                              ),
                            ),
                          ),
                          addSpaceWidth(10),
                          Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child: Column(
                              mainAxisSize: MainAxisSize.max,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  model.getEmail(),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  style: textStyle(true, 14, black),
                                ),
                                Text(
                                  model.getUserName(),
                                  style: textStyle(false, 12, black),
                                ),
                                Text(
                                  "Age:: " + age.toString(),
                                  style: textStyle(false, 12, black),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  adsRevenue() {
    int total = adsList.length;
    final active = adsList.where((e) => e.getType() == 0).toList();
    final inActive = adsList.where((e) => e.getType() == 1).toList();
    final pending = adsList.where((e) => e.getType() == 2).toList();
    return Container(
      decoration: BoxDecoration(
          color: black.withOpacity(0.09),
          borderRadius: BorderRadius.circular(10)),
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Ads Revenue Statistics \$"),
          addSpace(10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: List.generate(4, (p) {
              String title;
              int count;
              if (p == 0) {
                title = "Total";
                count = total;
              }
              if (p == 1) {
                title = "Active";
                count = active.length;
              }
              if (p == 2) {
                title = "InActive";
                count = inActive.length;
              }
              if (p == 3) {
                title = "Pending";
                count = pending.length;
              }

              return Flexible(
                child: GestureDetector(
                  onTap: () {},
                  child: Container(
                    width: getScreenWidth(context) / 3,
                    color: transparent,
                    child: Column(
                      children: [
                        Text(
                          "${count.toDouble()}",
                          textAlign: TextAlign.center,
                          style: textStyle(true, 22, black),
                        ),
                        Text(
                          "$title",
                          textAlign: TextAlign.center,
                          style: textStyle(true, 14, black.withOpacity(.5)),
                        )
                      ],
                    ),
                  ),
                ),
              );
            }),
          ),
        ],
      ),
    );
  }
}
