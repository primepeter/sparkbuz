import 'dart:convert';

import 'package:http/http.dart' as http;

class EmailService {
  static Map<String, String> header = {
    'Authorization':
        'Bearer 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',
    'Content-Type': ' application/json'
  };

  static sendEmailTemp(String otpCode, String emailTo) async {
    final payload = {
      "from": {"email": "info@convasapp.com"},
      "personalizations": [
        {
          "to": [
            {"email": emailTo}
          ],
          "dynamic_template_data": {"otp_code": "Your OTP CODE is $otpCode"}
        }
      ],
      "template_id": "d-d20df263fa3943d69d48557eba898cca"
    };
    http
        .post(Uri.parse("https://api.sendgrid.com/v3/mail/send"),
            headers: header, body: jsonEncode(payload))
        .then((value) {
      print("response ${value.body} ${value.statusCode}");
    }).catchError((e) {
      print(e);
    });
  }
}
