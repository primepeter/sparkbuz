import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:sparkbuz/AppEngine.dart';
import 'package:sparkbuz/assets.dart';
import 'package:sparkbuz/basemodel.dart';
import 'package:sparkbuz/dialogs/inputDialog.dart';
import 'package:sparkbuz/main_pages/CreateAds.dart';

class ShowAdsAdmin extends StatefulWidget {
  final List<BaseModel> models;

  const ShowAdsAdmin({Key key, this.models = const []}) : super(key: key);
  @override
  _ShowAdsAdminState createState() => _ShowAdsAdminState();
}

class _ShowAdsAdminState extends State<ShowAdsAdmin> {
  List<BaseModel> adsList = [];
  bool setup = false;
  final List<StreamSubscription> subs = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    adsList = widget.models;
    setup = adsList.isNotEmpty;
    // loadAds();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    for (var s in subs) s?.cancel();
  }

  loadAds() async {
    //model.put(STATUS, /*isAdmin ? APPROVED :*/ PENDING);

    var ads = FirebaseFirestore.instance
        .collection(ADS_BASE)
        .where(STATUS, isEqualTo: PENDING)
        .snapshots()
        .listen((value) {
      for (var doc in value.docs) {
        BaseModel model = BaseModel(doc: doc);
        int p =
            adsList.indexWhere((e) => e.getObjectId() == model.getObjectId());
        if (p != -1) {
          adsList[p] = model;
        } else {
          adsList.add(model);
        }
      }
      setup = true;
      if (mounted) setState(() {});
    });
    subs.add(ads);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.fromLTRB(0, 40, 0, 10),
            color: white,
            child: Row(
              children: <Widget>[
                InkWell(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Container(
                      width: 50,
                      height: 50,
                      child: Center(
                          child: Icon(
                        Icons.keyboard_backspace,
                        color: black,
                        size: 25,
                      )),
                    )),
                Text(
                  "My Ads",
                  style: textStyle(true, 25, black),
                ),
                Spacer(),
                Container(
                  height: 30,
                  width: 50,
                  child: new FlatButton(
                      padding: EdgeInsets.all(5),
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      onPressed: () {
                        //openAdsCreatePage(context);
                        pushAndResult(context, CreateAds());
                      },
                      shape: CircleBorder(),
                      child: Center(
                          child: Icon(
                        Icons.add,
                        size: 20,
                        color: black,
                      ))),
                ),
              ],
            ),
          ),
          page()
        ],
      ),
    );
  }

  page() {
    return Flexible(
      child: Builder(
        builder: (ctx) {
          if (!setup) return loadingLayout();
          if (adsList.isEmpty)
            return emptyLayout(
                Icons.trending_up, "No ads", "You have no ads with us",
                clickText: "Create One", click: () {
              pushAndResult(context, CreateAds());
            });
          return Container(
              child: ListView.builder(
            itemBuilder: (c, p) {
              return adsItem(p);
            },
            shrinkWrap: true,
            itemCount: adsList.length,
            padding: EdgeInsets.only(top: 10, right: 5, left: 5, bottom: 40),
          ));
        },
      ),
    );
  }

  adsItem(int p) {
    BaseModel model = adsList[p];
    //String imageUrl = getFirstPhoto(model);
    String imageUrl = getFirstPhoto(model.images);
    String title = model.getString(TITLE);
    String url = model.getString(ADS_URL);
    final seenBy = model.getList(SEEN_BY);
    final likes = model.getList(LIKES_COUNT);
    final superLike = model.getList(SUPER_LIKE_LIST);
    final clicks = model.getList(CLICKS);
    int status = model.getInt(STATUS);
    bool declined = status == REJECTED;
    bool approved = status == APPROVED;
    String reason = model.getString(REJECTED_MESSAGE);
    int endAt = model.getInt(ADS_EXPIRY);
    String invoiceLink = model.getString(INVOICE_LINK);
    bool hasInvoice = invoiceLink.isNotEmpty;

    bool hasPaid = model.getBoolean(HAS_PAID);
    bool ended = endAt > DateTime.now().millisecondsSinceEpoch;
    //(DateTime.now().millisecondsSinceEpoch - (Duration.millisecondsPerDay));

    print(
        "Expired $endAt E $ended  ${DateTime.fromMillisecondsSinceEpoch(endAt)}");

    String statusMsg = status == APPROVED
        ? "Approved"
        : status == PENDING
            ? "Pending Approval"
            : status == REJECTED
                ? "Rejected"
                : "Inactive";
    return Container(
      decoration: BoxDecoration(
          color: white,
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: black.withOpacity(.09))),
      child: Column(
        //mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CachedNetworkImage(
            imageUrl: imageUrl,
            height: 200,
            width: double.infinity,
            fit: BoxFit.cover,
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: textStyle(true, 16, black),
                ),
                addSpace(10),
                if (declined) ...[
                  Container(
                    decoration: BoxDecoration(
                        color: black.withOpacity(.09),
                        borderRadius: BorderRadius.circular(9)),
                    padding: EdgeInsets.all(10),
                    child: Text.rich(TextSpan(children: [
                      TextSpan(text: "Declined "),
                      TextSpan(
                          text: "$reason ", style: textStyle(true, 16, red))
                    ])),
                  ),
                  addSpace(10),
                ],
                Text.rich(TextSpan(children: [
                  TextSpan(text: "Active "),
                  TextSpan(text: "$ended ", style: textStyle(true, 14, black))
                ])),
                addSpace(10),
                Text.rich(TextSpan(children: [
                  TextSpan(text: "Status "),
                  TextSpan(
                      text: "$statusMsg ", style: textStyle(true, 14, black))
                ])),
                addSpace(10),
                Row(
                  children: [
                    Text.rich(TextSpan(children: [
                      TextSpan(text: "Views "),
                      TextSpan(
                          text: "${seenBy.length} ",
                          style: textStyle(true, 14, black))
                    ])),
                    addSpaceWidth(10),
                    Text.rich(TextSpan(children: [
                      TextSpan(text: "Clicks "),
                      TextSpan(
                          text: "${clicks.length} ",
                          style: textStyle(true, 14, black))
                    ])),
                    addSpaceWidth(10),
                    Text.rich(TextSpan(children: [
                      TextSpan(text: "Likes "),
                      TextSpan(
                          text: "${likes.length} ",
                          style: textStyle(true, 14, black))
                    ])),
                    addSpaceWidth(10),
                    Text.rich(TextSpan(children: [
                      TextSpan(text: "SuperLikes "),
                      TextSpan(
                          text: "${superLike.length} ",
                          style: textStyle(true, 14, black))
                    ]))
                  ],
                ),
              ],
            ),
          ),
          FlatButton(
            onPressed: () {
              pushAndResult(
                  context,
                  inputDialog(
                    "Payment Link",
                    allowEmpty: false,
                    hint: "Paste Payment Link",
                    okText: "Send",
                    message: model.getString(INVOICE_LINK),
                  ), result: (_) {
                model
                  ..put(INVOICE_LINK, _)
                  //..put(REJECTED_MESSAGE, _)
                  ..updateItems();
                adsList[p] = model;
                setState(() {});
              });
            },
            color: appYellow,
            child: Center(
                child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('Send Invoice'),
                addSpaceWidth(10),
                Icon(
                  Icons.email_outlined,
                  size: 18,
                )
              ],
            )),
          ),
          Row(
            children: [
              Expanded(
                child: Opacity(
                  opacity: approved ? 0.5 : 1,
                  child: FlatButton(
                    onPressed: () {
                      yesNoDialog(context, "Approve?",
                          "Are you sure you want to approve this ads?", () {
                        Geoflutterfire geo = Geoflutterfire();
                        GeoFirePoint center = geo.point(
                            latitude: model.getDouble(LATITUDE),
                            longitude: model.getDouble(LONGITUDE));

                        model
                          ..put(POSITION, center.data)
                          ..put(STATUS, APPROVED)
                          ..updateItems();
                        adsList[p] = model;
                        setState(() {});
                      });
                    },
                    color: green,
                    child: Center(
                        child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('Approve'),
                        addSpaceWidth(10),
                        Icon(
                          Icons.check,
                          size: 18,
                        )
                      ],
                    )),
                  ),
                ),
              ),
              addSpaceWidth(10),
              Expanded(
                child: Opacity(
                  opacity: 1,
                  child: FlatButton(
                    onPressed: () {
                      yesNoDialog(context, "Reset?",
                          "Are you sure you want to reset this ads?", () {
                        model
                          ..put(STATUS, PENDING)
                          ..updateItems();
                        adsList[p] = model;
                        setState(() {});
                      });
                    },
                    color: blue3,
                    child: Center(
                        child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('Reset'),
                        addSpaceWidth(10),
                        Icon(
                          Icons.refresh,
                          size: 18,
                        )
                      ],
                    )),
                  ),
                ),
              ),
              addSpaceWidth(10),
              Expanded(
                child: Opacity(
                  opacity: declined ? 0.5 : 1,
                  child: FlatButton(
                    onPressed: () {
                      pushAndResult(
                          context,
                          inputDialog(
                            "Reason",
                            allowEmpty: false,
                            hint: "Reason for rejecting ads",
                          ), result: (_) {
                        model
                          ..put(STATUS, REJECTED)
                          ..put(REJECTED_MESSAGE, _)
                          ..updateItems();
                        adsList[p] = model;
                        setState(() {});
                      });
                    },
                    color: red,
                    child: Center(
                        child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('Decline'),
                        addSpaceWidth(10),
                        Icon(
                          Icons.clear,
                          size: 18,
                        )
                      ],
                    )),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
