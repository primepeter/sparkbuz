import 'package:sparkbuz/main_pages/ShowVideoPost.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:flutter/material.dart';
import 'package:sparkbuz/AppEngine.dart';
import 'package:sparkbuz/assets.dart';
import 'package:sparkbuz/basemodel.dart';
import 'package:sparkbuz/dialogs/listDialog.dart';

class ShowVideos extends StatefulWidget {
  final List<BaseModel> items;
  final String title;

  const ShowVideos({Key key, this.items, this.title}) : super(key: key);
  @override
  _ShowVideosState createState() => _ShowVideosState();
}

class _ShowVideosState extends State<ShowVideos> {
  TextEditingController searchController = TextEditingController();
  bool _showCancel = false;
  FocusNode focusSearch = FocusNode();
  List itemList = [];

  reload() async {
    itemList.clear();
    String search = searchController.text.toString().toLowerCase().trim();
    for (BaseModel model in widget.items) {
      String contactName = model.getString(NAME).toLowerCase().trim();
      String number = model.getString(PHONE_NUMBER).toLowerCase().trim();
      if (search.isNotEmpty) {
        if (!contactName.contains(search)) {
          if (!number.contains(search)) continue;
        }
      }
      itemList.add(model);
    }
    itemList.sort((a, b) => b.likesCount.length.compareTo(a.likesCount.length));

    setState(() {});
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    reload();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.fromLTRB(0, 40, 0, 10),
            color: white,
            child: Row(
              children: <Widget>[
                InkWell(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Container(
                      width: 50,
                      height: 30,
                      child: Center(
                          child: Icon(
                        Icons.arrow_back_ios,
                        color: black,
                        size: 20,
                      )),
                    )),
                Flexible(
                    child: GestureDetector(
                  onTap: () {},
                  child: Center(
                      child: Text(
                          "${widget.title ?? "Videos"} ${itemList.length}",
                          style: textStyle(true, 20, black))),
                )),
                addSpaceWidth(10),
                new Container(
                  height: 30,
                  width: 50,
                  child: new FlatButton(
                      padding: EdgeInsets.all(0),
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      onPressed: () {},
                      child: Center(
                          child: Icon(
                        Icons.more_vert,
                        color: black,
                      ))),
                ),
              ],
            ),
          ),
          Container(
            height: 45,
            margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
            decoration: BoxDecoration(
                color: white,
                borderRadius: BorderRadius.circular(25),
                border: Border.all(color: black.withOpacity(0.2), width: 1)),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              //mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                addSpaceWidth(10),
                Icon(
                  Icons.search,
                  color: black.withOpacity(.5),
                  size: 17,
                ),
                addSpaceWidth(10),
                new Flexible(
                  flex: 1,
                  child: new TextField(
                    textInputAction: TextInputAction.search,
                    textCapitalization: TextCapitalization.sentences,
                    autofocus: false,
                    onSubmitted: (_) {
                      //reload();
                    },
                    decoration: InputDecoration(
                        hintText: "Search by name",
                        hintStyle: textStyle(
                          false,
                          18,
                          black.withOpacity(.5),
                        ),
                        border: InputBorder.none,
                        isDense: true),
                    style: textStyle(false, 16, black),
                    controller: searchController,
                    cursorColor: black,
                    cursorWidth: 1,
                    focusNode: focusSearch,
                    keyboardType: TextInputType.text,
                    onChanged: (s) {
                      _showCancel = s.trim().isNotEmpty;
                      setState(() {});
                      reload();
                    },
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      focusSearch.unfocus();
                      _showCancel = false;
                      searchController.text = "";
                    });
                    reload();
                  },
                  child: _showCancel
                      ? Padding(
                          padding: const EdgeInsets.fromLTRB(0, 0, 15, 0),
                          child: Icon(
                            Icons.close,
                            color: black,
                            size: 20,
                          ),
                        )
                      : new Container(),
                )
              ],
            ),
          ),
//          addSpace(10),
          Expanded(
              flex: 1,
              child: Builder(builder: (ctx) {
                //if (!contactSetup) return loadingLayout();
                if (itemList.isEmpty)
                  return emptyLayout(Icons.person, "No Videos Yet", "");

                return Container(
                    child: ListView.builder(
                  itemBuilder: (c, p) {
                    return videoItem(p);
                  },
                  shrinkWrap: true,
                  itemCount: itemList.length,
                  padding: EdgeInsets.only(top: 10),
                ));
              }))
        ],
      ),
    );
  }

  videoItem(int p) {
    BaseModel model = itemList[p];
    bool isVideo = model.isVideo;
    String heroTag = "image$p";
    String image = model.getString(isVideo ? THUMBNAIL_URL : VIDEO_URL);

    String username = model.getUserName();
    String description = model.getString(DESCRIPTION);
    String category = model.getString(CATEGORY);

    String likes = formatToK(model.getList(LIKES_COUNT).length);
    String comments = formatToK(model.getList(COMMENT_LIST).length);
    String shares = formatToK(model.getList(SHARE_LIST).length);
    String photo = model.getString(USER_IMAGE);
    bool liked = model.getList(LIKES_COUNT).contains(userModel.getUserId());
    String seenBy = model.getList(SEEN_BY).length.toString();
    String time = getTimeAgo(model.getTime());
    int age = 0;

    int likesCount = model.likesCount.length;

    if (model.birthDate.isNotEmpty) {
      age = getAge(DateTime.parse(model.birthDate));
    }

    return GestureDetector(
      onTap: () {
        // pushAndResult(
        //     context,
        //     ShowVideoPost(
        //       videoModel: model,
        //     ));
      },
      onLongPress: () {
        // if (!isAdmin) return;
        // showListDialog(context, [
        //   "Reset This Timestamp",
        //   "Reset All Timestamp",
        //   "Delete"
        // ], (_) {
        //   if (_ == 0) {
        //     showMessage(
        //         context,
        //         Icons.warning,
        //         red,
        //         "Update Time",
        //         "Update/Reset video upload time to current time now?",
        //         onClicked: (_) {
        //           if (!_) return;
        //           model.put(
        //               UPDATED_AT, FieldValue.serverTimestamp());
        //           model.put(
        //               CREATED_AT, FieldValue.serverTimestamp());
        //           model.put(
        //               TIME, DateTime.now().millisecondsSinceEpoch);
        //           final search =
        //           getSearchString('$description $category');
        //           model.put(SEARCH, search);
        //
        //           model.put(TIME_UPDATED,
        //               DateTime.now().millisecondsSinceEpoch);
        //           model.updateItems(updateTime: true);
        //
        //           int p = items.indexWhere((e) =>
        //           e.getObjectId() == model.getObjectId());
        //           if (p != -1) items[p] = model;
        //           items.sort(
        //                   (a, b) => b.getTime().compareTo(a.getTime()));
        //           setState(() {});
        //         });
        //   }
        //
        //   if (_ == 1) {
        //     showMessage(
        //         context,
        //         Icons.warning,
        //         red,
        //         "Reset all Time",
        //         "Update/Reset video upload time to current time now?",
        //         onClicked: (_) {
        //           if (!_) return;
        //
        //           for (var model in items) {
        //             model.put(
        //                 UPDATED_AT, FieldValue.serverTimestamp());
        //             model.put(
        //                 CREATED_AT, FieldValue.serverTimestamp());
        //             model.put(TIME,
        //                 DateTime.now().millisecondsSinceEpoch);
        //             model.put(TIME_UPDATED,
        //                 DateTime.now().millisecondsSinceEpoch);
        //             final search =
        //             getSearchString('$description $category');
        //             model.put(SEARCH, search);
        //
        //             model.updateItems(
        //                 updateTime: true, delaySeconds: 2);
        //
        //             int p = items.indexWhere((e) =>
        //             e.getObjectId() == model.getObjectId());
        //             if (p != -1) items[p] = model;
        //           }
        //
        //           items.sort(
        //                   (a, b) => b.getTime().compareTo(a.getTime()));
        //           setState(() {});
        //         });
        //   }
        //
        //   if (_ == 2) {
        //     showMessage(
        //         context,
        //         Icons.warning,
        //         red,
        //         "Delete Video",
        //         "Are you sure you want to delete this video?",
        //         onClicked: (_) {
        //           if (!_) return;
        //           model.deleteItem();
        //           items.removeWhere((e) =>
        //           e.getObjectId() == model.getObjectId());
        //           setState(() {});
        //         });
        //   }
        // });
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 10, right: 10, left: 10),
        decoration:
            BoxDecoration(border: Border.all(color: black.withOpacity(.09))),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 200,
              child: Stack(
                //fit: StackFit.passthrough,
                children: <Widget>[
                  Container(
                    child: CachedNetworkImage(
                      imageUrl: image,
                      fit: BoxFit.cover,
                      width: double.infinity,
                      placeholder: (_, s) {
                        return placeHolder(200, width: double.infinity);
                      },
                    ),
                  ),
                  if (isVideo)
                    Container(
                      child: Center(
                        child: Container(
                          height: 40,
                          width: 40,
                          padding: EdgeInsets.all(3),
                          margin: EdgeInsets.all(6),
                          decoration: BoxDecoration(
                            color: black.withOpacity(.2),
                            // border:
                            //     Border.all(color: white, width: 2),
                            shape: BoxShape.circle,
                          ),
                          alignment: Alignment.center,
                          child: Icon(
                            Icons.play_arrow,
                            color: white.withOpacity(.5),
                            size: 30,
                          ),
                        ),
                      ),
                    ),
                  Container(
                    alignment: Alignment.bottomRight,
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              color: black.withOpacity(.3),
                              borderRadius: BorderRadius.circular(5)),
                          padding: EdgeInsets.all(2),
                          margin: EdgeInsets.all(3),
                          child: Row(
                            children: [
                              Text(
                                "${model.getList(LIKES_COUNT).length}",
                                style: textStyle(true, 10, white),
                              ),
                              addSpaceWidth(2),
                              Icon(
                                Icons.favorite,
                                color: white,
                                size: 14,
                              )
                            ],
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                              color: black.withOpacity(.3),
                              borderRadius: BorderRadius.circular(5)),
                          padding: EdgeInsets.all(2),
                          margin: EdgeInsets.all(3),
                          child: Row(
                            children: [
                              Text(
                                "${model.getList(SEEN_BY).length}",
                                style: textStyle(true, 10, white),
                              ),
                              addSpaceWidth(2),
                              Icon(
                                Icons.remove_red_eye,
                                color: white,
                                size: 14,
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            addSpace(10),
            Container(
              padding: EdgeInsets.all(5),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(50),
                    child: Container(
                      height: 40,
                      width: 40,
                      child: userImageItem(context, model,
                          padLeft: false, strock: 1, size: 40),
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: black.withOpacity(.1),
                              width: 1.0,
                              style: BorderStyle.solid),
                          color: white,
                          shape: BoxShape.circle),
                    ),
                  ),
                  addSpaceWidth(5),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "$username: $description",
                          maxLines: 2,
                          style: textStyle(true, 16, black),
                        ),
                        addSpace(4),
                        Text(
                          "$seenBy Views . $time",
                          style: textStyle(false, 13, black.withOpacity(.6)),
                        ),
                        addSpace(4),
                        Text(
                          "${formatToK(likesCount)} Video Likes",
                          style: textStyle(false, 13, black.withOpacity(.6)),
                        ),
                        addSpace(4),
                        Text(
                          "Age: ${age}",
                          style: textStyle(false, 13, black.withOpacity(.6)),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
