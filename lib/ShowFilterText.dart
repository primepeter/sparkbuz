import 'dart:ui';

import 'package:flutter/material.dart';

import 'AppConfig.dart';
import 'AppEngine.dart';
import 'ShowFilterFonts.dart';
import 'assets.dart';

class ShowFilterText extends StatefulWidget {
  @override
  _ShowFilterTextState createState() => _ShowFilterTextState();
}

class _ShowFilterTextState extends State<ShowFilterText> {
  final searchController = TextEditingController();
  String fontName = '';

  @override
  initState() {
    super.initState();
  }

  @override
  dispose() {
    super.dispose();
    searchController?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: widget.popResult ? transparent : white,
      backgroundColor: black.withOpacity(.5),
      body: page(),
    );
  }

  page() {
    return Stack(
      children: [
        BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
            child: Container(
              color: black.withOpacity(.6),
            )),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              padding: EdgeInsets.only(top: 30, right: 10, left: 10, bottom: 0),
              child: Row(
                children: [
                  BackButton(
                    color: white,
                  ),
                  Text(
                    "Add a Text",
                    style: textStyle(true, 16, white),
                  ),
                  Spacer(),
                  //if (isAdmin)
                  RaisedButton(
                    color: AppConfig.appYellow,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25)),
                    child: Text(
                      "Apply",
                      style: textStyle(true, 14, black),
                    ),
                    onPressed: () {
                      if (searchController.text.isEmpty) {
                        showError("Type a text to add");
                        return;
                      }
                      Navigator.pop(context, [searchController.text,fontName]);
                    },
                  ),

                  addSpaceWidth(5),
                ],
              ),
            ),
            AnimatedContainer(
              duration: Duration(milliseconds: 500),
              width: double.infinity,
              height: errorText.isEmpty ? 0 : 40,
              color: red0,
              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: Center(
                  child: Text(
                errorText,
                style: textStyle(true, 16, white),
              )),
            ),
            addSpace(5),
            Expanded(
              child: Center(
                child: Container(
                  margin: EdgeInsets.only(left: 15, right: 15, bottom: 10),
                  child: Container(
                      padding: EdgeInsets.only(left: 15, right: 15),
                      decoration: BoxDecoration(
                          color: white.withOpacity(.04),
                          border: Border.all(
                            color: white.withOpacity(.09),
                          ),
                          borderRadius: BorderRadius.circular(10)),
                      child: TextField(
                        controller: searchController,
                        cursorColor: white,
                        cursorWidth: 2,
                        style: filterTextStyle(fontName, true, 40, white),
                        textAlign: TextAlign.center,
                        decoration: InputDecoration(
                            hintText: "Type a Text ",
                            hintStyle:
                                //     size: 40, color: white.withOpacity(.5)),
                                filterTextStyle(
                                    fontName, true, 40, white.withOpacity(.7)),
                            border: InputBorder.none),
                      )),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.all(16),
              child: RaisedButton(
                color: AppConfig.appYellow,
                padding: EdgeInsets.all(16),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                child: Center(
                  child: Text(
                    "Pick Font ${fontName.isEmpty ? '' : '($fontName)'}",
                    style: filterTextStyle(fontName, false, 16, black),
                    //style: textStyle(true, 16, black),
                  ),
                ),
                onPressed: () {
                  pushAndResult(context, ShowFilterFonts(), result: (_) {
                    fontName = _;
                    setState(() {});
                  });
                },
              ),
            ),
          ],
        ),
      ],
    );
  }

  String errorText = "";
  showError(String text, {bool wasLoading = false}) {
    if (wasLoading) showProgress(false, context);
    errorText = text;
    setState(() {});
    Future.delayed(Duration(seconds: 3), () {
      errorText = "";
      setState(() {});
    });
  }
}
