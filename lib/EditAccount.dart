import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:sparkbuz/AppEngine.dart';
import 'package:sparkbuz/AppConfig.dart';
import 'package:sparkbuz/assets.dart';
import 'package:sparkbuz/basemodel.dart';

class EditAccount extends StatefulWidget {
  @override
  _EditAccountState createState() => _EditAccountState();
}

class _EditAccountState extends State<EditAccount> {
  BaseModel model = userModel.getModel(PAYOUT_INFO);

  final acctNameController = TextEditingController();
  final acctNumberController = TextEditingController();
  final bankNameController = TextEditingController();
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    acctNameController.text = model.getString(ACCOUNT_NAME);
    acctNumberController.text = model.getString(ACCOUNT_NUMBER);
    bankNameController.text = model.getString(BANK_NAME);
  }

  String formatPackageDuration(int p) {
    return "${p + 1} Month${p == 0 ? "" : "s"}";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: white,
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.fromLTRB(0, 40, 0, 10),
            color: white,
            child: Row(
              children: <Widget>[
                InkWell(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Container(
                      width: 50,
                      height: 50,
                      child: Center(
                          child: Icon(
                        Icons.keyboard_backspace,
                        color: black,
                        size: 25,
                      )),
                    )),
                Text(
                  "Payout Information",
                  style: textStyle(true, 25, black),
                ),
                Spacer()
              ],
            ),
          ),
          page()
        ],
      ),
    );
  }

  page() {
    return Flexible(
      child: ListView(
        padding: EdgeInsets.all(15),
        children: [
          addSpace(10),
          textFieldBox(acctNameController, "Accout Name", (v) => null),
          textFieldBox(acctNumberController, "Accout Number", (v) => null,
              number: true),
          textFieldBox(bankNameController, "Bank Name", (v) => null),
          addSpace(30),
          Container(
            //padding: EdgeInsets.only(left: 25, right: 25),
            child: FlatButton(
              onPressed: validateFields,
              padding: EdgeInsets.all(20),
              color: AppConfig.appColor,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)),
              child: Center(
                  child: Text(
                "SAVE",
                style: textStyle(true, 18, white),
              )),
            ),
          ),
          addSpace(50),
        ],
      ),
    );
  }

  String formatTimeChosen(int time) {
    final date = DateTime.fromMillisecondsSinceEpoch(time);
    return new DateFormat("MMMM d y").format(date);
  }

  textFieldBox(TextEditingController controller, String hint, setstate(v),
      {focusNode,
      int maxLength,
      int maxLines,
      bool number = false,
      Color fillColor}) {
    if (fillColor == null) fillColor = black.withOpacity(.05);
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
      child: TextFormField(
        focusNode: focusNode,
        maxLength: maxLength,
        maxLines: maxLines,
        //maxLengthEnforced: false,
        controller: controller,
        decoration: InputDecoration(
            fillColor: fillColor,
            filled: true,
            labelText: hint,
            counter: Container(),
            border: InputBorder.none),
        onChanged: setstate,
//        onEditingComplete: setstate,
        keyboardType: number ? TextInputType.number : null,
      ),
    );
  }

  validateFields() async {
    String name = acctNameController.text;
    String number = acctNumberController.text;
    String bank = bankNameController.text;

    var result = await (Connectivity().checkConnectivity());
    if (result == ConnectivityResult.none) {
      snack("No internet connectivity");
      return;
    }

    if (name.isEmpty) {
      snack("Add Account Name");
      return;
    }

    if (number.isEmpty) {
      snack("Add Account Number");
      return;
    }

    if (bank.isEmpty) {
      snack("Add Bank Name");
      return;
    }

    BaseModel model = BaseModel();
    model
      ..put(ACCOUNT_NAME, name)
      ..put(ACCOUNT_NUMBER, number)
      ..put(BANK_NAME, bank);
    userModel
      ..put(PAYOUT_INFO, model.items)
      ..updateItems();
    showMessage(context, Icons.check, green, "Successful",
        "You have successfully updated your Payout Information",
        onClicked: (_) {
      Navigator.pop(context, "");
    });
  }

  snack(String text) {
    Future.delayed(Duration(milliseconds: 500), () {
      showSnack(scaffoldKey, text, useWife: true);
    });
  }
}
