import 'dart:io';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:sparkbuz/MainAdmin.dart';

import 'AppEngine.dart';
import 'assets.dart';
import 'basemodel.dart';

class CamPost extends StatefulWidget {
  final BaseModel camModel;
  final List<BuildContext> contexts;
  const CamPost({Key key, this.contexts, this.camModel}) : super(key: key);
  @override
  _CamPostState createState() => _CamPostState();
}

class _CamPostState extends State<CamPost> {
  BaseModel camModel;
  final descController = TextEditingController();
  String postCategory;
  List categories = appSettingsModel.getList(POST_CATEGORIES);

  @override
  void initState() {
    super.initState();
    camModel = widget.camModel;
    fixCategories();
  }

  fixCategories() {
    List cat = [];
    for (int p = 0; p < categories.length; p++) {
      String v = categories[p];
      if (cat.contains(v)) continue;
      cat.add(v);
    }
    cat.sort();
    appSettingsModel
      ..put(POST_CATEGORIES, cat)
      ..updateItems();

    categories = cat;
    if (mounted) setState(() {});
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      body: Stack(
        children: [
          Align(
            alignment: Alignment.bottomCenter,
            child: Image.file(
              File(camModel.getString(THUMBNAIL_PATH)),
              fit: BoxFit.cover,
              width: double.infinity,
              //height: 20,
              //height: getScreenHeight(context),
              alignment: Alignment.center,
            ),
          ),
          BackdropFilter(
            filter: ImageFilter.blur(
              sigmaX: 10.0,
              sigmaY: 10.0,
            ),
            child: Container(
              color: black.withOpacity(.2),
              // decoration: BoxDecoration(
              //     gradient: LinearGradient(colors: [
              //   appColor.withOpacity(.9),
              //   appColor.withOpacity(.2),
              // ], begin: Alignment.bottomCenter, end: Alignment.topCenter)),
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: Container(
              margin: EdgeInsets.only(right: 10, left: 10),
              decoration: BoxDecoration(
                color: white,
                borderRadius: BorderRadius.circular(10),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        padding: EdgeInsets.all(10),
                        child: Row(
                          children: [
                            BackButton(
                              color: black,
                              onPressed: () {
                                // fixCategories();

                                //return;
                                Navigator.pop(context, "");
                              },
                            ),
                            Text(
                              "Publish",
                              style: textStyle(true, 18, black),
                            ),
                            Spacer(),
                            FlatButton(
                                onPressed: () async {
                                  String description = descController.text;

                                  if (description.isEmpty) {
                                    showError("Add video description");
                                    return;
                                  }

                                  if (null == postCategory) {
                                    showError("Select a Category");
                                    return;
                                  }

                                  print(camModel.items);
                                  File file =
                                      File(camModel.getString(VIDEO_PATH));
                                  print(await file.exists());
                                  //return;

                                  List<BuildContext> contexts = widget.contexts;
                                  contexts.add(context);
                                  contextController.add(contexts);
                                  camModel.put(DESCRIPTION, description);
                                  camModel.put(CATEGORY, postCategory);
                                  final search = getSearchString(
                                      '$description $postCategory');
                                  camModel.put(SEARCH, search);
                                  cameraController.add(camModel);
                                },
                                color: appColor,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                //padding: EdgeInsets.all(10),
                                child: Text(
                                  "Post",
                                  style: textStyle(true, 14, white),
                                ))
                          ],
                        ),
                      ),
                      AnimatedContainer(
                        duration: Duration(milliseconds: 500),
                        width: double.infinity,
                        height: errorText.isEmpty ? 0 : 40,
                        color: red0,
                        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                        child: Center(
                            child: Text(
                          errorText,
                          style: textStyle(true, 16, white),
                        )),
                      ),
                      SingleChildScrollView(
                        padding: EdgeInsets.zero,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            /*GestureDetector(
                              onTap: () {
                                // pushAndResult(
                                //     context,
                                //     PreviewPostImages(
                                //       [camModel],
                                //       [],
                                //       edittable: true,
                                //       storyMode: true,
                                //       indexOf: 0,
                                //       heroTag: getRandomId(),
                                //     ), result: (List<BaseModel> _) {
                                //   if (_ == null || _.isEmpty) return;
                                //   camModel = _[0];
                                //   //saveStories(_);
                                // });
                              },
                              child: Container(
                                height: 200,
                                width: double.infinity,
                                padding: EdgeInsets.all(15),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(20),
                                  child: Stack(
                                    children: [
                                      Image.file(
                                        File(camModel.getString(THUMBNAIL_PATH)),
                                        fit: BoxFit.cover,
                                        width: double.infinity,
                                        height: 200,
                                        alignment: Alignment.center,
                                      ),
                                      Container(
                                        height: 200,
                                        width: double.infinity,
                                        color: black.withOpacity(.09),
                                        child: Center(
                                          child: Container(
                                            height: 40,
                                            width: 40,
                                            child: Icon(
                                              Icons.play_arrow,
                                              color: Colors.white,
                                              size: 18,
                                            ),
                                            decoration: BoxDecoration(
                                                color: black.withOpacity(0.8),
                                                border: Border.all(
                                                    color: Colors.white, width: 1.5),
                                                shape: BoxShape.circle),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),*/
                            textInputField(
                                controller: descController,
                                title: "",
                                hint: "Add a Description",
                                asset: null,
                                disableIconAsset: true,
                                fill: true,
                                maxLines: 4),
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 15.0, top: 15),
                              child: Text(
                                "Choose Category",
                                style:
                                    textStyle(true, 14, black.withOpacity(.5)),
                              ),
                            ),
                            Container(
                              padding: const EdgeInsets.all(15.0),
                              //width: double.infinity,
                              height: 200,
                              child: Row(
                                children: [
                                  Expanded(
                                    child: SingleChildScrollView(
                                      //scrollDirection: Axis.horizontal,
                                      child: Wrap(
                                        //alignment: WrapAlignment.start,
                                        //direction: Axis.vertical,
                                        children: categories.map((e) {
                                          String value = e;
                                          bool selected = e == postCategory;

                                          return GestureDetector(
                                            onTap: () {
                                              //if (isDefault) return;
                                              if (mounted)
                                                setState(() {
                                                  postCategory = e;
                                                });
                                            },
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                  left: 10,
                                                  right: 10,
                                                  top: 4,
                                                  bottom: 4),
                                              margin: EdgeInsets.all(5),
                                              decoration: BoxDecoration(
                                                  color: selected
                                                      ? appYellow.withOpacity(1)
                                                      : black.withOpacity(.06),
                                                  borderRadius:
                                                      BorderRadius.circular(8)),
                                              child: Text(
                                                value,
                                                style: textStyle(
                                                    selected,
                                                    16,
                                                    /*selected ? white :*/ black),
                                              ),
                                            ),
                                          );
                                        }).toList(),
                                      ),
                                    ),
                                  ),
                                  /*Container(
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Container(
                                            height: 60,
                                            width: 20,
                                            decoration: BoxDecoration(
                                                color: appColor,
                                                borderRadius:
                                                    BorderRadius.circular(20)),
                                            alignment: Alignment.topCenter,
                                            padding: EdgeInsets.all(3),
                                            child: Container(
                                              decoration: BoxDecoration(
                                                  color: white,
                                                  borderRadius:
                                                      BorderRadius.circular(20)),
                                              child: Icon(
                                                Icons.arrow_upward,
                                                size: 15,
                                                color: appColor,
                                              ),
                                            )),
                                        addSpace(10),
                                        Container(
                                            height: 60,
                                            width: 20,
                                            decoration: BoxDecoration(
                                                color: appColor,
                                                borderRadius:
                                                    BorderRadius.circular(20)),
                                            alignment: Alignment.bottomCenter,
                                            padding: EdgeInsets.all(3),
                                            child: Container(
                                              decoration: BoxDecoration(
                                                  color: white,
                                                  borderRadius:
                                                      BorderRadius.circular(20)),
                                              child: Icon(
                                                Icons.arrow_downward,
                                                size: 15,
                                                color: appColor,
                                              ),
                                            )),
                                      ],
                                    ),
                                  )*/
                                ],
                              ),
                            ),

                            /* selectorField(
                            value: postCategory,
                            title: "",
                            hint: "Category to Publish",
                            fill: true,
                            disableIconAsset: true,
                            onTap: () {
                              showListDialog(
                                context,
                                appSettingsModel.getList(POST_CATEGORIES),
                                (_) {
                                  postCategory =
                                      appSettingsModel.getList(POST_CATEGORIES)[_];
                                  setState(() {});
                                },
                              );
                            })*/
                          ],
                        ),
                      ),
                      addSpace(10)
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  String errorText = "";
  showError(String text, {bool wasLoading = false}) {
    if (wasLoading) showProgress(false, context);
    errorText = text;
    setState(() {});
    Future.delayed(Duration(seconds: 3), () {
      errorText = "";
      setState(() {});
    });
  }
}
