import 'dart:math';

import 'package:cached_video_player/cached_video_player.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:sparkbuz/assets.dart';
import 'package:sparkbuz/basemodel.dart';

import 'AppConfig.dart';
import 'AppEngine.dart';
import 'CamEditor.dart';
import 'ShowFilterFonts.dart';
import 'ShowFilterStickers.dart';
import 'ShowFilterText.dart';
import 'app/sliderWidget.dart';

class FilterLayout extends StatefulWidget {
  // final Widget child;
  //final CachedVideoPlayerController currentPlayer;
  final Function(List<BaseModel> filters) filterCallback;
  final bool enableFilter;
  final List<BaseModel> filters;

  const FilterLayout({
    Key key,
    //this.child,
    this.filterCallback,
    //this.currentPlayer,
    this.enableFilter = true,
    this.filters = const [],
  }) : super(key: key);
  @override
  _FilterLayoutState createState() => _FilterLayoutState();
}

class _FilterLayoutState extends State<FilterLayout> {
  CachedVideoPlayerController currentPlayer;
  List<BaseModel> filters = [];
  int currentFilter = -1;
  bool showColorFilter = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    filters = List.from(widget.filters);
    //currentPlayer = widget.currentPlayer;
    setState(() {});
  }

  @override
  void didUpdateWidget(FilterLayout oldWidget) {
    super.didUpdateWidget(oldWidget);
    if ((oldWidget.filters != widget.filters)
        // || (widget.currentPlayer != oldWidget.currentPlayer)
        ) {
      filters = List.from(widget.filters);
      //currentPlayer = widget.currentPlayer;
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    int p =
        filters.indexWhere((e) => e.getInt(FILTER_TYPE) == FILTER_TYPE_COLOR);

    //return widget.child;

    return Container(
      height: getScreenHeight(context),
      width: getScreenWidth(context),
      child: Stack(
        fit: StackFit.expand,
        children: [
          //widget.child,
          if (p != -1)
            Align(
              alignment: Alignment.center,
              child: Container(
                alignment: Alignment.center,
                color: Color(filters[p].getInt(FILTER_COLOR)),
              ),
            ),
          if (widget.enableFilter) editingItems(),

          Stack(
              children: List.generate(filters.length, (p) {
            BaseModel model = filters[p];
            int filterType = model.getInt(FILTER_TYPE);
            double filterOffsetDy = model.getDouble(FILTER_OFFSET_DY);
            double filterOffsetDx = model.getDouble(FILTER_OFFSET_DX);
            double filterScale = model.getDouble(FILTER_SCALE);
            if (filterScale == 0) filterScale = 1;
            String filterText = model.getString(FILTER_TEXT);
            String fontName = model.getString(FILTER_TEXT_FONT);
            String filterImagePath = model.imagesPath;
            String filterImageUrl = model.imageUrl;
            int filterColor = model.getInt(FILTER_COLOR);
            bool text = filterType == FILTER_TYPE_TEXT;
            bool active = currentFilter == p;

            if (filterType == FILTER_TYPE_COLOR) return Container();

            return Positioned(
                top: filterOffsetDy,
                left: filterOffsetDx,
                child: GestureDetector(
                  onTap: () {
                    // if (!widget.enableFilter) return;
                    // currentFilter = -1;
                    // showColorFilter = false;
                    // setState(() {});

                    if (!widget.enableFilter) return;
                    currentFilter = p;
                    showColorFilter = false;
                    setState(() {});
                  },
                  // onLongPress: () {
                  //   if (!widget.enableFilter) return;
                  //   currentFilter = p;
                  //   showColorFilter = false;
                  //   setState(() {});
                  // },
                  onPanStart: (d) {
                    if (!widget.enableFilter) return;

                    print(d.localPosition);
                    updateItemPosition(d.globalPosition, p);
                  },
                  onPanDown: (d) {
                    // if (!widget.enableFilter) return;
                    // updateItemPosition(d.localPosition, p);
                  },
                  onPanUpdate: (d) {
                    if (!widget.enableFilter) return;

                    updateItemPosition(d.globalPosition, p);
                  },
                  child: Stack(
                    children: [
                      if (text)
                        Container(
                          //height: 100,
                          //width: 100,
                          margin: EdgeInsets.only(left: active ? 40 : 0),
                          padding: EdgeInsets.all(20),
                          decoration: BoxDecoration(
                              color: transparent,
                              border: Border.all(
                                  color: active
                                      ? AppConfig.appYellow
                                      : black.withOpacity(0))),
                          child: Center(
                            child: Text(
                              filterText,
                              style: filterTextStyle(
                                  fontName, true, 20 * filterScale, white),
                            ),
                          ),
                        )
                      else
                        ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: Container(
                            height: 120 * filterScale,
                            width: 120 * filterScale,
                            margin: EdgeInsets.only(left: active ? 40 : 0),
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                color: transparent,
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(
                                    color: active
                                        ? AppConfig.appYellow
                                        : black.withOpacity(0))),
                            child: CachedNetworkImage(
                              imageUrl: filterImageUrl,
                              height: 100 * filterScale,
                              width: 100 * filterScale,
                              fit: BoxFit.cover,
                              placeholder: (c, s) {
                                return Container(
                                  height: 100 * filterScale,
                                  width: 100 * filterScale,
                                  color: black.withOpacity(.09),
                                  child: Icon(LineIcons.image),
                                );
                              },
                            ),
                          ),
                        ),
                      if (active)
                        Align(
                          alignment: Alignment.topLeft,
                          child: InkWell(
                            onTap: () {
                              filters.removeAt(p);
                              currentFilter = -1;
                              setState(() {});
                            },
                            child: Container(
                                color: red,
                                height: 30,
                                width: 30,
                                margin: EdgeInsets.only(left: 10),
                                alignment: Alignment.center,
                                child: Center(
                                  child: Icon(
                                    Icons.clear,
                                    color: white,
                                    size: 20,
                                  ),
                                )),
                          ),
                        )
                    ],
                  ),
                ));
          })),
        ],
      ),
    );
  }

  editingItems() {
    return Align(
      alignment: Alignment.bottomLeft,
      child: Container(
        margin: EdgeInsets.only(left: 10, bottom: 50),
        decoration: BoxDecoration(
            color: transparent, //black.withOpacity(.5),
            borderRadius: BorderRadius.circular(15)),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              mainAxisSize: MainAxisSize.min,
              children: List.generate(3, (index) {
                String title = "Filter";
                IconData icon = Icons.texture;

                if (index == 1) {
                  title = "Text";
                  icon = Icons.text_fields;
                }

                if (index == 2) {
                  title = "Sticker";
                  icon = Icons.image;
                }
                return GestureDetector(
                  onTap: () {
                    //widget.currentPlayer.pause();

                    if (index == 0) {
                      showColorFilter = !showColorFilter;
                      currentFilter = -1;
                      int p = filters.indexWhere((element) =>
                          element.getInt(FILTER_TYPE) == FILTER_TYPE_COLOR);
                      if (p == -1) {
                        BaseModel model = BaseModel();
                        model.put(FILTER_TYPE, FILTER_TYPE_COLOR);
                        model.put(FILTER_COLOR, transparent.value);
                        filters.add(model);
                        currentFilter = filters.length - 1;
                      }

                      setState(() {});
                      return;
                    }

                    if (index == 1) {
                      pushAndResult(context, ShowFilterText(), result: (_) {
                        int filterType = FILTER_TYPE_TEXT;
                        double filterOffsetDy =
                            Size.fromHeight(getScreenHeight(context) * .75)
                                .center(Offset.zero)
                                .dy;
                        double filterOffsetDx =
                            Size.fromWidth(getScreenWidth(context))
                                .center(Offset.zero)
                                .dx;
                        BaseModel model = BaseModel();
                        model.put(FILTER_OFFSET_DY, filterOffsetDy);
                        model.put(FILTER_OFFSET_DX, 20 * filters.length);
                        model.put(FILTER_TEXT, _[0]);
                        model.put(FILTER_TEXT_FONT, _[1]);
                        model.put(FILTER_TYPE, filterType);
                        model.put(FILTER_SCALE, 1);
                        filters.add(model);
                        currentFilter = filters.length - 1;
                        setState(() {});
                      });
                      return;
                    }
                    if (index == 2) {
                      pushAndResult(
                          context,
                          ShowFilterStickers(
                            popResult: true,
                          ), result: (_) {
                        int filterType = FILTER_TYPE_STICKER;
                        double filterOffsetDy =
                            Size.fromHeight(getScreenHeight(context) * .75)
                                .center(Offset.zero)
                                .dy;
                        double filterOffsetDx =
                            Size.fromWidth(getScreenWidth(context))
                                .center(Offset.zero)
                                .dx;
                        String filterText = _;
                        BaseModel model = BaseModel();
                        model.put(FILTER_OFFSET_DY, filterOffsetDy);
                        model.put(FILTER_OFFSET_DX,
                            20 * (filters.length == 0 ? 1 : filters.length));
                        model.put(IMAGE_URL, _);
                        model.put(FILTER_TYPE, filterType);
                        model.put(FILTER_SCALE, 1);

                        filters.add(model);
                        currentFilter = filters.length - 1;
                        setState(() {});
                      });
                      return;
                    }
                  },
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                    color: transparent,
                    child: DefaultTextStyle(
                      style: TextStyle(shadows: [
                        Shadow(
                          color: Colors.black.withOpacity(0.15),
                          offset: Offset(0, 1),
                          blurRadius: 1,
                        ),
                      ]),
                      child: Column(
                        children: <Widget>[
                          Icon(
                            icon,
                            color: white,
                          ),
                          Text(
                            title,
                            style: textStyle(false, 12, white),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              }),
            ),
            if (currentFilter != -1 && !showColorFilter)
              Container(
                padding: EdgeInsets.only(left: 10, right: 10),
                child: SliderWidget(
                  max: 50,
                  min: 1,
                  fullWidth: true,
                  callBack: (v) {
                    filters[currentFilter]
                        .put(FILTER_SCALE, (v / 10).clamp(1, 10));
                    print(v / 10);
                    setState(() {
                      //adDays = v;
                    });
                  },
                ),
              ),
            if (showColorFilter)
              ColorPicker(
                onColorSelected: (spectrumColor, selectedColor) {
                  int p = filters.indexWhere((element) =>
                      element.getInt(FILTER_TYPE) == FILTER_TYPE_COLOR);
                  if (p != -1) {
                    filters[p]
                        .put(FILTER_COLOR, spectrumColor.withOpacity(.1).value);
                  }

                  setState(() {
                    //this.spectrumColor = spectrumColor;
                  });
                },
              )
          ],
        ),
      ),
    );
  }

  void updateItemPosition(Offset localPosition, int p) {
    if (!widget.enableFilter) return;
    BaseModel model = filters[p];
    double top = max(localPosition.dy - (80) / 2, 0);
    double left = max(localPosition.dx - (80) / 2, 0);

    model.put(FILTER_OFFSET_DY, localPosition.dy);
    model.put(FILTER_OFFSET_DX, localPosition.dx);
    filters[p] = model;
    setState(() {});
    if (null != widget.filterCallback) widget.filterCallback(filters);
  }
}
