import 'dart:io';

import 'package:flutter/material.dart';
import 'package:sparkbuz/AppEngine.dart';
import 'package:sparkbuz/AppConfig.dart';
import 'package:video_player/video_player.dart';

import 'AppEngine.dart';
import 'PreAuth.dart';
import 'assets.dart';

class PreInit extends StatefulWidget {
  @override
  _PreInitState createState() => _PreInitState();
}

class _PreInitState extends State<PreInit> {
  int currentPage = 0;
  VideoPlayerController videoController;

  @override
  void initState() {
    super.initState();
    loadVideo();
  }

  bool hasSetup = false;

  loadVideo() async {
    File file =
        await loadFile('assets/sparkbuz/spark_video.mp4', "spark_video.mp4");
    videoController =
        VideoPlayerController.asset("assets/sparkbuz/spark_video.mp4")
          ..initialize().then((value) {
            videoController.setVolume(0.4);
            videoController.setLooping(true);
            videoController.play();
            hasSetup = true;
            setState(() {});
          });
  }

  @override
  void dispose() {
    videoController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: black,
      body: Stack(fit: StackFit.expand, children: [
        if (hasSetup)
          AspectRatio(
              aspectRatio: videoController.value.aspectRatio,
              child: VideoPlayer(videoController)),
        Container(color: black.withOpacity(.5)),
        page(),
        Align(
            alignment: Alignment.center,
            child: Container(
              //  padding: EdgeInsets.only(top: 40, left: 10),
              child: Image.asset(
                "assets/icons/spark_logo.png",
                height: 100,
              ),
            ))
      ]),
    );
  }

  page() {
    return Container(
      padding: EdgeInsets.only(bottom: 50),
      alignment: Alignment.bottomCenter,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Column(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 20, right: 20),
                    //width: 160,
                    child: FlatButton(
                      child: Center(
                        child:
                            Text('LOG IN', style: textStyle(true, 14, black)),
                      ),
                      onPressed: () {
                        pushAndResult(
                            context,
                            PreAuth(
                              p: 1,
                            ),
                            depend: false);
                      },
                      padding: EdgeInsets.all(18),
                      shape: RoundedRectangleBorder(
                          //side: BorderSide(color: white.withOpacity(.4), width: 2),
                          borderRadius: BorderRadius.circular(8)),
                      color: white,
                    ),
                  ),
                  addSpace(10),
                  Container(
                    padding: EdgeInsets.only(left: 20, right: 20),
                    //width: 160,
                    child: FlatButton(
                      child: Center(
                        child:
                            Text('SIGN UP', style: textStyle(true, 14, white)),
                      ),
                      onPressed: () {
                        pushAndResult(
                            context,
                            PreAuth(
                              p: 0,
                            ),
                            depend: false);
                      },
                      padding: EdgeInsets.all(18),
                      shape: RoundedRectangleBorder(
                          //side: BorderSide(color: white.withOpacity(.4), width: 2),
                          borderRadius: BorderRadius.circular(8)),
                      color: AppConfig.appColor,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
