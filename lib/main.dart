import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:devicelocale/devicelocale.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_login_facebook/flutter_login_facebook.dart';
import 'package:flutter_multi_formatter/flutter_multi_formatter.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:loading_indicator/loading_indicator.dart';
import 'package:sparkbuz/app/app.dart';
import 'package:sparkbuz/app/navigation.dart';
import 'package:sparkbuz/assets.dart';
import 'package:sparkbuz/basemodel.dart';

import 'AppEngine.dart';
import 'MainAdmin.dart';
import 'PreInit.dart';

RouteObserver<PageRoute> routeObserver = RouteObserver<PageRoute>();

StreamController<List<String>> galleryController =
    StreamController<List<String>>.broadcast();

// 5fedea30-8101-11eb-bb4d-5f9559302354

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  // PhoneInputFormatter.replacePhoneMask(
  //     countryCode: country.countryCode, newMask: "000-000-00000");
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext c) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "SparkBuz",
        color: white,
        theme: ThemeData(
          fontFamily: 'Larsseit',
          primarySwatch: Colors.red,
          pageTransitionsTheme: PageTransitionsTheme(
            builders: <TargetPlatform, PageTransitionsBuilder>{
              TargetPlatform.iOS: createTransition(),
              TargetPlatform.android: createTransition(),
            },
          ),
        ),
        navigatorObservers: [
          routeObserver,
        ],
        //debugShowMaterialGrid: true,
        home: AppSetter());
  }

  PageTransitionsBuilder createTransition() {
    return ZoomPageTransitionsBuilder();
  }
}

class AppSetter extends StatefulWidget {
  @override
  _AppSetterState createState() => _AppSetterState();
}

class _AppSetterState extends State<AppSetter> {
  @override
  void initState() {
    // TODO: implement initState
    checkUser();
    loadSettings();
    loadDeviceLocale();
    super.initState();
  }

  loadDeviceLocale() async {
    final locale = await Devicelocale.currentAsLocale;
    country = getCountries()
        .singleWhere((e) => e.countryISO == locale.countryCode.toLowerCase());
    print(country.countryName);
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: black,
      child: Container(
        alignment: Alignment.center,
        child: Container(
          width: 50,
          height: 80,
          padding: EdgeInsets.all(10),
          child: LoadingIndicator(
            indicatorType: Indicator.ballGridPulse,
            color: white,
          ),
        ),
      ),
    );
  }

  checkUser() async {
    await Future.delayed(Duration(seconds: 2));
    User user = FirebaseAuth.instance.currentUser;
    if (user == null) {
      popUpUntil(context, PreInit());
    } else {
      loadLocalUser(user.uid, onInComplete: () {
        popUpUntil(context, PreInit());
      }, onLoaded: () {
        popUpUntil(context, MainAdmin());
      });
    }
  }

  loadSettings() {
    FirebaseFirestore.instance
        .collection(APP_SETTINGS_BASE)
        .doc(APP_SETTINGS)
        .get(/*source: Source.cache*/)
        .then((doc) {
      if (!doc.exists) {
        appSettingsModel = new BaseModel();
        appSettingsModel.saveItem(APP_SETTINGS_BASE, false,
            document: APP_SETTINGS);
        return;
      }
      appSettingsModel = BaseModel(doc: doc);
    });
  }

  loadLocalUser(String userId, {onLoaded, onInComplete}) {
    FirebaseFirestore.instance
        .collection(USER_BASE)
        .doc(userId)
        .get()
        .then((doc) async {
      userModel = BaseModel(doc: doc);
      isAdmin = userModel.getBoolean(IS_ADMIN) ||
          userModel.getString(EMAIL) == "johnebere58@gmail.com" ||
          userModel.getString(EMAIL) == "james.iroha1@gmail.com" ||
          userModel.getString(EMAIL) == "ammaugost@gmail.com";

      if (!userModel.signUpCompleted || !doc.exists) {
        await GoogleSignIn().signOut();
        await FacebookLogin().logOut();
        await FirebaseAuth.instance.signOut();
        userModel.deleteItem();
        userModel = BaseModel();
        onInComplete();
        return;
      }
      onLoaded();
    }).catchError((e) {
      popUpUntil(context, PreInit());
    });
  }
}
