// import 'package:flutter/material.dart';
// import 'package:video_trimmer/trim_editor.dart';
// import 'package:video_trimmer/video_trimmer.dart';
// import 'package:video_trimmer/video_viewer.dart';
//
// import 'AppEngine.dart';
// import 'assets.dart';
//
// class TrimmerView extends StatefulWidget {
//   final Trimmer _trimmer;
//   final bool isPost;
//   TrimmerView(this._trimmer, {this.isPost = false});
//   @override
//   _TrimmerViewState createState() => _TrimmerViewState();
// }
//
// class _TrimmerViewState extends State<TrimmerView> {
//   double _startValue = 0.0;
//   double _endValue = 0.0;
//
//   bool _isPlaying = false;
//   bool _progressVisibility = false;
//
//   Future<String> _saveVideo() async {
//     setState(() {
//       _progressVisibility = true;
//     });
//
//     String _value;
//
//     await widget._trimmer
//         .saveTrimmedVideo(startValue: _startValue, endValue: _endValue)
//         .then((value) {
//       setState(() {
//         _progressVisibility = false;
//         _value = value;
//       });
//     });
//
//     return _value;
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: black,
//       body: Column(
//         children: [
//           addSpace(30),
//           Padding(
//             padding: const EdgeInsets.only(left: 0),
//             child: Row(
//               children: <Widget>[
//                 InkWell(
//                     onTap: () {
//                       Navigator.of(context).pop();
//                     },
//                     child: Container(
//                       width: 50,
//                       height: 50,
//                       child: Center(
//                           child: Icon(
//                         Icons.keyboard_backspace,
//                         color: white,
//                         size: 25,
//                       )),
//                     )),
//                 Expanded(
//                     child: Text(
//                   "Preview",
//                   style: textStyle(true, 20, white),
//                 )),
//                 RaisedButton(
//                   onPressed: _progressVisibility
//                       ? null
//                       : () async {
//                           _saveVideo().then((outputPath) {
//                             print('OUTPUT PATH: $outputPath');
//                             // final snackBar = SnackBar(
//                             //     content: Text('Video Saved successfully'));
//                             // Scaffold.of(context).showSnackBar(snackBar);
//                             Navigator.pop(context, [outputPath, 0]);
//                           });
//                         },
//                   child: Text("Save"),
//                   shape: RoundedRectangleBorder(
//                       borderRadius: BorderRadius.circular(15)),
//                 ),
//                 addSpaceWidth(10)
//               ],
//             ),
//           ),
//           Expanded(
//             child: Builder(
//               builder: (context) => Center(
//                 child: Container(
//                   padding: EdgeInsets.only(bottom: 30.0),
//                   color: Colors.black,
//                   child: Column(
//                     mainAxisAlignment: MainAxisAlignment.center,
//                     mainAxisSize: MainAxisSize.max,
//                     children: <Widget>[
//                       Visibility(
//                         visible: _progressVisibility,
//                         child: LinearProgressIndicator(
//                           backgroundColor: Colors.red,
//                         ),
//                       ),
//                       Expanded(
//                         child: VideoViewer(),
//                       ),
//                       Center(
//                         child: TrimEditor(
//                           viewerHeight: 50.0,
//                           viewerWidth: MediaQuery.of(context).size.width,
//                           minDuration: Duration(seconds: 1),
//                           maxDuration: Duration(seconds: 120),
//                           onChangeStart: (value) {
//                             _startValue = value;
//                           },
//                           onChangeEnd: (value) {
//                             _endValue = value;
//                           },
//                           onChangePlaybackState: (value) {
//                             setState(() {
//                               _isPlaying = value;
//                             });
//                           },
//                         ),
//                       ),
//                       FlatButton(
//                         child: _isPlaying
//                             ? Icon(
//                                 Icons.pause,
//                                 size: 80.0,
//                                 color: Colors.white,
//                               )
//                             : Icon(
//                                 Icons.play_arrow,
//                                 size: 80.0,
//                                 color: Colors.white,
//                               ),
//                         onPressed: () async {
//                           bool playbackState =
//                               await widget._trimmer.videPlaybackControl(
//                             startValue: _startValue,
//                             endValue: _endValue,
//                           );
//                           setState(() {
//                             _isPlaying = playbackState;
//                           });
//                         },
//                       )
//                     ],
//                   ),
//                 ),
//               ),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
