#-keepattributes *Annotation*
#-keep class kotlin.** { *; }
#-keep class org.jetbrains.** { *; }
-keepclassmembers class ai.deepar.ar.DeepAR { *; }
-keep class io.agora.**{*;}
-keep class io.flutter.app.** { *; }
-keep class io.flutter.plugin.**  { *; }
-keep class io.flutter.util.**  { *; }
-keep class io.flutter.view.**  { *; }
-keep class io.flutter.**  { *; }
-keep class io.flutter.plugins.**  { *; }
-keepclasseswithmembers public class com.flutterwave.raveandroid.** { *; }
-dontwarn com.flutterwave.raveandroid.card.CardFragment
-dontwarn android.**